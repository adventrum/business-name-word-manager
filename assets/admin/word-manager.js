var $ = jQuery;
var column, wordId, updatedWordsList = [], currentKeyword;
var removedWords = [];
$(document).ready(function(){
    var defaultTab = document.querySelector(".Nameideas");
    if( defaultTab != undefined ){
        defaultTab.click();
    }
    $('#saveEmailOptions #header_logo').on('click', open_custom_media_window);
    $('#saveEmailOptions #footer_image').on('click', open_custom_media_window);
        
    $("#request_select").on("change", function(){

        let requestcontrl = $(this).val();
        $(".ctrl").addClass("init-hide");
        $("."+requestcontrl+"-control").removeClass("init-hide");

    });

    $('.edit-word-col').on('click', function(){
        $('body').addClass('enable-word-edit');
        column = $(this).data('column');
        wordId = $(this).data('id');
        var keyword = $(this).data('keyword');
        currentKeyword = keyword;
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "getWordColumnData", 
                column : column,
                wordId: wordId
            },
            success: function(response) {
                var res = JSON.parse(response);
                if( res ){
                updatedWordsList = res;
                setTimeout(() => {
                   $('.edit-words-list').addClass('data-fetched');
                   var appendData = '';

                       res.forEach((val, i) => {
                           appendData += '<div class="single-word"><a href="javascript:void" class="btn" data-combined="'+val.word+' '+keyword+'" data-word="'+val.word+'" data-key="'+(parseInt(i)+1)+'">'+val.word+'</a><span class="word-action remove-word"></span></div>';
                        });
                        $('.all-words').html(appendData);
                        removeWordsFromDictionary();
                    }, 1500);
                }
                // column = '';
            }
         }) 
    });

    $('.word-editor .add-new-word').on('submit', function(e){
        e.preventDefault();
        var word = $(this).find('input[name="add_new_word"]').val();
        refreshWordKeys();

        updatedWordsList.unshift({ "word": word });

        $('.word-editor .all-words').prepend('<div class="single-word"><a href="javascript:void" class="btn" data-combined="'+word+' '+currentKeyword+'" data-word="'+word+'" data-key="0">'+word+'</a><span class="word-action remove-word"></span></div>');
        refreshWordKeys();
        addWordsToDictionary();
        removeWordsFromDictionary();
        $(this).find('input[name="add_new_word"]').val('');
    });

    $('.word-filter-btns .cancel').on('click', function(){
        $('.edit-words-list').removeClass('data-fetched');
        $('.word-editor .removed-words').html('');
        setTimeout(() => {
            $('body').removeClass('enable-word-edit');
        }, 600);
    });

    $('.word-filter-btns .apply').on('click', function(){
        $('.edit-words-list').removeClass('data-fetched');
        $('.word-editor .removed-words').html('');
        $('.edit-words-list .frame-loader').addClass('saving');

        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "saveApprovedWords", 
                approvedWords : { "approvedWords": updatedWordsList},
                column : column,
                wordId: wordId
            },
            success: function(response) {
                setTimeout(() => {
                if( response.msg == 'success' ){
                    $('body').removeClass('enable-word-edit');
                    $('.edit-words-list .frame-loader').removeClass('saving');
                }
               }, 1500);
            }
         }) 
    });

    $('.api-to-csv-container .button-primary').on('click', function(e){
       e.preventDefault();
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "ajaxDownloadCsv",
                apiUrl: $('input[name="api_to_csv"]').val()
            },
            success: function(response) {
                window.location.href = response.url;
            }
         }) 
    });

    $('.word-manager-container .update-category').on('click', function(e){
        e.preventDefault();
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "ajaxUpdateCategoryName",
                categoryName: $('input[name="word_category_name"]').val(),
                status: $('select[name="status"]').val(),
                categoryId: $('input[name="edit_cat_id"]').val()
            },
            success: function(response) {
                window.location.href = response.home_url+'/wp-admin/admin.php?page=bnwm_word_categories';
            }
         }); 
    });

    $('.word-manager-settings-container #setting-list .button-primary').on('click', function(){
        var dis = $(this);
        dis.parent().addClass('loading');
        var action = dis.data('action');
        var dataLanguage = $('select[name="bnwm_data_language"]').val();
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action,
                dataLanguage
            },
            success: function(response) {
                if( response.msg == 'success' ){
                    dis.parent().removeClass('loading');
                    dis.parent().next().text(response.data_language);
                }
            }
         }); 
    });
    $('.word-manager-container .bulk-approve a').on('click', function(){
        $('#word-list .word_status').each(function(){
            if($(this).val() != 'Approved'){
                setTimeout(() => {
                    $(this).val('Approved').trigger('change');
                }, 500);
            }
        });
    });
    

   $('.update-algorithm').on('click', function(e){
       $.ajax({
           type : "post",
           url : wordAjax.ajaxurl,
           data : {
               action: "UpdateAlgorithm",
               algorithmtype: $('select[name="bnwm_algorithm_type"]').val(),
           },
           success: function(response) {
               if(response == 'success'){
                   jQuery('.algorithm-updated').show();
                   
                   setTimeout(function() {
                    jQuery('.algorithm-updated').hide();
                   }, 1000);
               }
           }
        }); 
   });

   $('.create-popular-keywords-table').on('click', function(e){
        $.ajax({
            type : "post",
            url : wordAjax.ajaxurl,
            data : {
                action: "createPopularKeywordsTable",
            },
            success: function(response) {
                if(response == 'success'){
                    jQuery('.create-popular-keywords-table').parents('form').fadeOut();
                }
            }
        }); 
    });

    if( $('.word-industries-select') && typeof $('.word-industries-select').select2 === 'function' ){
        $('.word-industries-select').select2();
    }
    if( $('#createWord .bnwm_word_language') && typeof $('.word-industries-select').select2 === 'function' ){
        $('#createWord .bnwm_word_language').select2();
    }

    $('.word-manager-container .word_status').on('change', function(){
        var dis = $(this);
        var wordId = dis.parents('tr').attr('id').replace('word-', '');
        var status = dis.val();
        dis.parents('tr').find('.has-loader').addClass('loading');
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "ajaxUpdateWordStatus",
                wordId,
                status
            },
            success: function(response) {
                if( response.msg == 'success' ){
                    dis.parents('tr').find('.has-loader').removeClass('loading').addClass('loaded');
                    setTimeout(() => {
                        dis.parents().find('.has-loader').removeClass('loaded');
                    }, 3000);
                }
            }
         }); 
    });

    $('.word-manager-container .fetch-word-data').on('click', function(){
        var dis = $(this);
        var word_id = dis.data('id');
        var word = dis.data('name');
        $(this).parent().addClass('loading');
        $.ajax({
            type : "post",
            dataType : "json",
            url : wordAjax.ajaxurl,
            data : {
                action: "ajaxUpdateWordData",
                word_id,
                word
            },
            success: function(response) {
                if( response.msg == 'success' ){
                    dis.parent().removeClass('loading').addClass('loaded');
                    setTimeout(() => {
                        dis.parent().removeClass('loaded');
                    }, 3000);
                }
            }
         }); 
    });

    $('#createWord .word_name').on('keyup', function(){
        var dis = $(this);
        var word = dis.val();
        if( $.trim(word) != '' ){
            $.ajax({
                type : "post",
                dataType : "json",
                url : wordAjax.ajaxurl,
                data : {
                    action: "checkIfWordExists",
                    word
                },
                success: function(response) {
                    if( response.msg == 'success' ){
                        dis.parent().find('.error').removeClass('failure').addClass('success');
                        $('#createWord p.submit').removeClass('failure');
                        $('#createWord p.submit input[type="submit"]').removeAttr('disabled');
                    } else {
                        dis.parent().find('.error').removeClass('success').addClass('failure');
                        $('#createWord p.submit').addClass('failure');
                        $('#createWord p.submit input[type="submit"]').attr('disabled', 'disabled');
                    }
                }
            }); 
        } else {
            dis.parent().find('.error').removeClass('failure').removeClass('success');
        }
    });

    $('.add-filter-shortcode .filter-has-sub-fields').each(function(){
        if( $(this).is(':checked') ){
            $('#'+$(this).data('fields')).show();
        } else {
            $('#'+$(this).data('fields')).hide();
        }
    });

    $('.add-filter-shortcode .filter-has-sub-fields').on('change', function(){
        if( $(this).is(':checked') ){
            $('#'+$(this).data('fields')).show();
        } else {
            $('#'+$(this).data('fields')).hide();
        }
    });

    $('.add-generator-shortcode .add-country-affiliate').on('click', function(e){
        e.preventDefault();
        $('.affiliate-items').append('<li class="affiliate-item"><a class="close-box generator-close-btn" href="javascript:void(0);">×</a><select name="generator_affiliate_countries[]"> <option value="AD: ANDORRA">ANDORRA</option> <option value="AE: UNITED ARAB EMIRATES">UNITED ARAB EMIRATES</option> <option value="AF: AFGHANISTAN">AFGHANISTAN</option> <option value="AG: ANTIGUA AND BARBUDA">ANTIGUA AND BARBUDA</option> <option value="AI: ANGUILLA">ANGUILLA</option> <option value="AL: ALBANIA">ALBANIA</option> <option value="AM: ARMENIA">ARMENIA</option> <option value="AN: NETHERLANDS ANTILLES">NETHERLANDS ANTILLES</option> <option value="AO: ANGOLA">ANGOLA</option> <option value="AQ: ANTARCTICA">ANTARCTICA</option> <option value="AR: ARGENTINA">ARGENTINA</option> <option value="AS: AMERICAN SAMOA">AMERICAN SAMOA</option> <option value="AT: AUSTRIA">AUSTRIA</option> <option value="AU: AUSTRALIA">AUSTRALIA</option> <option value="AW: ARUBA">ARUBA</option> <option value="AZ: AZERBAIJAN">AZERBAIJAN</option> <option value="BA: BOSNIA AND HERZEGOVINA">BOSNIA AND HERZEGOVINA</option> <option value="BB: BARBADOS">BARBADOS</option> <option value="BD: BANGLADESH">BANGLADESH</option> <option value="BE: BELGIUM">BELGIUM</option> <option value="BF: BURKINA FASO">BURKINA FASO</option> <option value="BG: BULGARIA">BULGARIA</option> <option value="BH: BAHRAIN">BAHRAIN</option> <option value="BI: BURUNDI">BURUNDI</option> <option value="BJ: BENIN">BENIN</option> <option value="BL: SAINT BARTHELEMY">SAINT BARTHELEMY</option> <option value="BM: BERMUDA">BERMUDA</option> <option value="BN: BRUNEI DARUSSALAM">BRUNEI DARUSSALAM</option> <option value="BO: BOLIVIA">BOLIVIA</option> <option value="BR: BRAZIL">BRAZIL</option> <option value="BS: BAHAMAS">BAHAMAS</option> <option value="BT: BHUTAN">BHUTAN</option> <option value="BW: BOTSWANA">BOTSWANA</option> <option value="BY: BELARUS">BELARUS</option> <option value="BZ: BELIZE">BELIZE</option> <option value="CA: CANADA">CANADA</option> <option value="CC: COCOS (KEELING) ISLANDS">COCOS (KEELING) ISLANDS</option> <option value="CD: CONGO, THE DEMOCRATIC REPUBLIC OF THE">CONGO, THE DEMOCRATIC REPUBLIC OF THE</option> <option value="CF: CENTRAL AFRICAN REPUBLIC">CENTRAL AFRICAN REPUBLIC</option> <option value="CG: CONGO">CONGO</option> <option value="CH: SWITZERLAND">SWITZERLAND</option> <option value="CI: COTE D IVOIRE">COTE D IVOIRE</option> <option value="CK: COOK ISLANDS">COOK ISLANDS</option> <option value="CL: CHILE">CHILE</option> <option value="CM: CAMEROON">CAMEROON</option> <option value="CN: CHINA">CHINA</option> <option value="CO: COLOMBIA">COLOMBIA</option> <option value="CR: COSTA RICA">COSTA RICA</option> <option value="CU: CUBA">CUBA</option> <option value="CV: CAPE VERDE">CAPE VERDE</option> <option value="CX: CHRISTMAS ISLAND">CHRISTMAS ISLAND</option> <option value="CY: CYPRUS">CYPRUS</option> <option value="CZ: CZECH REPUBLIC">CZECH REPUBLIC</option> <option value="DE: GERMANY">GERMANY</option> <option value="DJ: DJIBOUTI">DJIBOUTI</option> <option value="DK: DENMARK">DENMARK</option> <option value="DM: DOMINICA">DOMINICA</option> <option value="DO: DOMINICAN REPUBLIC">DOMINICAN REPUBLIC</option> <option value="DZ: ALGERIA">ALGERIA</option> <option value="EC: ECUADOR">ECUADOR</option> <option value="EE: ESTONIA">ESTONIA</option> <option value="EG: EGYPT">EGYPT</option> <option value="ER: ERITREA">ERITREA</option> <option value="ES: SPAIN">SPAIN</option> <option value="ET: ETHIOPIA">ETHIOPIA</option> <option value="FI: FINLAND">FINLAND</option> <option value="FJ: FIJI">FIJI</option> <option value="FK: FALKLAND ISLANDS (MALVINAS)">FALKLAND ISLANDS (MALVINAS)</option> <option value="FM: MICRONESIA, FEDERATED STATES OF">MICRONESIA, FEDERATED STATES OF</option> <option value="FO: FAROE ISLANDS">FAROE ISLANDS</option> <option value="FR: FRANCE">FRANCE</option> <option value="GA: GABON">GABON</option> <option value="GB: UNITED KINGDOM">UNITED KINGDOM</option> <option value="GD: GRENADA">GRENADA</option> <option value="GE: GEORGIA">GEORGIA</option> <option value="GH: GHANA">GHANA</option> <option value="GI: GIBRALTAR">GIBRALTAR</option> <option value="GL: GREENLAND">GREENLAND</option> <option value="GM: GAMBIA">GAMBIA</option> <option value="GN: GUINEA">GUINEA</option> <option value="GQ: EQUATORIAL GUINEA">EQUATORIAL GUINEA</option> <option value="GR: GREECE">GREECE</option> <option value="GT: GUATEMALA">GUATEMALA</option> <option value="GU: GUAM">GUAM</option> <option value="GW: GUINEA-BISSAU">GUINEA-BISSAU</option> <option value="GY: GUYANA">GUYANA</option> <option value="HK: HONG KONG">HONG KONG</option> <option value="HN: HONDURAS">HONDURAS</option> <option value="HR: CROATIA">CROATIA</option> <option value="HT: HAITI">HAITI</option> <option value="HU: HUNGARY">HUNGARY</option> <option value="ID: INDONESIA">INDONESIA</option> <option value="IE: IRELAND">IRELAND</option> <option value="IL: ISRAEL">ISRAEL</option> <option value="IM: ISLE OF MAN">ISLE OF MAN</option> <option value="IN: INDIA">INDIA</option> <option value="IQ: IRAQ">IRAQ</option> <option value="IR: IRAN, ISLAMIC REPUBLIC OF">IRAN, ISLAMIC REPUBLIC OF</option> <option value="IS: ICELAND">ICELAND</option> <option value="IT: ITALY">ITALY</option> <option value="JM: JAMAICA">JAMAICA</option> <option value="JO: JORDAN">JORDAN</option> <option value="JP: JAPAN">JAPAN</option> <option value="KE: KENYA">KENYA</option> <option value="KG: KYRGYZSTAN">KYRGYZSTAN</option> <option value="KH: CAMBODIA">CAMBODIA</option> <option value="KI: KIRIBATI">KIRIBATI</option> <option value="KM: COMOROS">COMOROS</option> <option value="KN: SAINT KITTS AND NEVIS">SAINT KITTS AND NEVIS</option> <option value="KP: KOREA DEMOCRATIC PEOPLES REPUBLIC OF">KOREA DEMOCRATIC PEOPLES REPUBLIC OF</option> <option value="KR: KOREA REPUBLIC OF">KOREA REPUBLIC OF</option> <option value="KW: KUWAIT">KUWAIT</option> <option value="KY: CAYMAN ISLANDS">CAYMAN ISLANDS</option> <option value="KZ: KAZAKSTAN">KAZAKSTAN</option> <option value="LA: LAO PEOPLES DEMOCRATIC REPUBLIC">LAO PEOPLES DEMOCRATIC REPUBLIC</option> <option value="LB: LEBANON">LEBANON</option> <option value="LC: SAINT LUCIA">SAINT LUCIA</option> <option value="LI: LIECHTENSTEIN">LIECHTENSTEIN</option> <option value="LK: SRI LANKA">SRI LANKA</option> <option value="LR: LIBERIA">LIBERIA</option> <option value="LS: LESOTHO">LESOTHO</option> <option value="LT: LITHUANIA">LITHUANIA</option> <option value="LU: LUXEMBOURG">LUXEMBOURG</option> <option value="LV: LATVIA">LATVIA</option> <option value="LY: LIBYAN ARAB JAMAHIRIYA">LIBYAN ARAB JAMAHIRIYA</option> <option value="MA: MOROCCO">MOROCCO</option> <option value="MC: MONACO">MONACO</option> <option value="MD: MOLDOVA, REPUBLIC OF">MOLDOVA, REPUBLIC OF</option> <option value="ME: MONTENEGRO">MONTENEGRO</option> <option value="MF: SAINT MARTIN">SAINT MARTIN</option> <option value="MG: MADAGASCAR">MADAGASCAR</option> <option value="MH: MARSHALL ISLANDS">MARSHALL ISLANDS</option> <option value="MK: MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF">MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF </option> <option value="ML: MALI">MALI</option> <option value="MM: MYANMAR">MYANMAR</option> <option value="MN: MONGOLIA">MONGOLIA</option> <option value="MO: MACAU">MACAU</option> <option value="MP: NORTHERN MARIANA ISLANDS">NORTHERN MARIANA ISLANDS</option> <option value="MR: MAURITANIA">MAURITANIA</option> <option value="MS: MONTSERRAT">MONTSERRAT</option> <option value="MT: MALTA">MALTA</option> <option value="MU: MAURITIUS">MAURITIUS</option> <option value="MV: MALDIVES">MALDIVES</option> <option value="MW: MALAWI">MALAWI</option> <option value="MX: MEXICO">MEXICO</option> <option value="MY: MALAYSIA">MALAYSIA</option> <option value="MZ: MOZAMBIQUE">MOZAMBIQUE</option> <option value="NA: NAMIBIA">NAMIBIA</option> <option value="NC: NEW CALEDONIA">NEW CALEDONIA</option> <option value="NE: NIGER">NIGER</option> <option value="NG: NIGERIA">NIGERIA</option> <option value="NI: NICARAGUA">NICARAGUA</option> <option value="NL: NETHERLANDS">NETHERLANDS</option> <option value="NO: NORWAY">NORWAY</option> <option value="NP: NEPAL">NEPAL</option> <option value="NR: NAURU">NAURU</option> <option value="NU: NIUE">NIUE</option> <option value="NZ: NEW ZEALAND">NEW ZEALAND</option> <option value="OM: OMAN">OMAN</option> <option value="PA: PANAMA">PANAMA</option> <option value="PE: PERU">PERU</option> <option value="PF: FRENCH POLYNESIA">FRENCH POLYNESIA</option> <option value="PG: PAPUA NEW GUINEA">PAPUA NEW GUINEA</option> <option value="PH: PHILIPPINES">PHILIPPINES</option> <option value="PK: PAKISTAN">PAKISTAN</option> <option value="PL: POLAND">POLAND</option> <option value="PM: SAINT PIERRE AND MIQUELON">SAINT PIERRE AND MIQUELON</option> <option value="PN: PITCAIRN">PITCAIRN</option> <option value="PR: PUERTO RICO">PUERTO RICO</option> <option value="PT: PORTUGAL">PORTUGAL</option> <option value="PW: PALAU">PALAU</option> <option value="PY: PARAGUAY">PARAGUAY</option> <option value="QA: QATAR">QATAR</option> <option value="RO: ROMANIA">ROMANIA</option> <option value="RS: SERBIA">SERBIA</option> <option value="RU: RUSSIAN FEDERATION">RUSSIAN FEDERATION</option> <option value="RW: RWANDA">RWANDA</option> <option value="SA: SAUDI ARABIA">SAUDI ARABIA</option> <option value="SB: SOLOMON ISLANDS">SOLOMON ISLANDS</option> <option value="SC: SEYCHELLES">SEYCHELLES</option> <option value="SD: SUDAN">SUDAN</option> <option value="SE: SWEDEN">SWEDEN</option> <option value="SG: SINGAPORE">SINGAPORE</option> <option value="SH: SAINT HELENA">SAINT HELENA</option> <option value="SI: SLOVENIA">SLOVENIA</option> <option value="SK: SLOVAKIA">SLOVAKIA</option> <option value="SL: SIERRA LEONE">SIERRA LEONE</option> <option value="SM: SAN MARINO">SAN MARINO</option> <option value="SN: SENEGAL">SENEGAL</option> <option value="SO: SOMALIA">SOMALIA</option> <option value="SR: SURINAME">SURINAME</option> <option value="ST: SAO TOME AND PRINCIPE">SAO TOME AND PRINCIPE</option> <option value="SV: EL SALVADOR">EL SALVADOR</option> <option value="SY: SYRIAN ARAB REPUBLIC">SYRIAN ARAB REPUBLIC</option> <option value="SZ: SWAZILAND">SWAZILAND</option> <option value="TC: TURKS AND CAICOS ISLANDS">TURKS AND CAICOS ISLANDS</option> <option value="TD: CHAD">CHAD</option> <option value="TG: TOGO">TOGO</option> <option value="TH: THAILAND">THAILAND</option> <option value="TJ: TAJIKISTAN">TAJIKISTAN</option> <option value="TK: TOKELAU">TOKELAU</option> <option value="TL: TIMOR-LESTE">TIMOR-LESTE</option> <option value="TM: TURKMENISTAN">TURKMENISTAN</option> <option value="TN: TUNISIA">TUNISIA</option> <option value="TO: TONGA">TONGA</option> <option value="TR: TURKEY">TURKEY</option> <option value="TT: TRINIDAD AND TOBAGO">TRINIDAD AND TOBAGO</option> <option value="TV: TUVALU">TUVALU</option> <option value="TW: TAIWAN, PROVINCE OF CHINA">TAIWAN, PROVINCE OF CHINA</option> <option value="TZ: TANZANIA, UNITED REPUBLIC OF">TANZANIA, UNITED REPUBLIC OF</option> <option value="UA: UKRAINE">UKRAINE</option> <option value="UG: UGANDA">UGANDA</option> <option value="US: UNITED STATES">UNITED STATES</option> <option value="UY: URUGUAY">URUGUAY</option> <option value="UZ: UZBEKISTAN">UZBEKISTAN</option> <option value="VA: HOLY SEE (VATICAN CITY STATE)">HOLY SEE (VATICAN CITY STATE)</option> <option value="VC: SAINT VINCENT AND THE GRENADINES">SAINT VINCENT AND THE GRENADINES</option> <option value="VE: VENEZUELA">VENEZUELA</option> <option value="VG: VIRGIN ISLANDS, BRITISH">VIRGIN ISLANDS, BRITISH</option> <option value="VI: VIRGIN ISLANDS, U.S.">VIRGIN ISLANDS, U.S.</option> <option value="VN: VIET NAM">VIET NAM</option> <option value="VU: VANUATU">VANUATU</option> <option value="WF: WALLIS AND FUTUNA">WALLIS AND FUTUNA</option> <option value="WS: SAMOA">SAMOA</option> <option value="XK: KOSOVO">KOSOVO</option> <option value="YE: YEMEN">YEMEN</option> <option value="YT: MAYOTTE">MAYOTTE</option> <option value="ZA: SOUTH AFRICA">SOUTH AFRICA</option> <option value="ZM: ZAMBIA">ZAMBIA</option> <option value="ZW: ZIMBABWE">ZIMBABWE</option> </select><br><label>Desktop Device</label> <input type="text" class="generator_affiliate_urls" name="generator_affiliate_desk_urls[]"> <select name="generator_affiliate_desk_sponsor[]"> <option value="0" selected="">Select the sponsor</option> <option value="godaddy">GoDaddy</option> <option value="wix">Wix</option> </select><br><label>Tablet/Mobile Device</label> <input type="text" class="generator_affiliate_urls" name="generator_affiliate_tab_mob_urls[]"> <select name="generator_affiliate_tab_mob_sponsor[]"> <option value="0" selected="">Select the sponsor</option> <option value="godaddy">GoDaddy</option> <option value="wix">Wix</option> </select> </li>');
    });
    
    $(document).on('click' , '.generator-close-btn' ,function(e){
      $(this).parents('li').remove();
    });
    
});

function removeWordsFromDictionary(){
    $('.word-editor .all-words .remove-word').off('click');
    $('.word-editor .all-words .remove-word').on('click', function(){
        refreshWordKeys();
        var key = $(this).parents('.single-word').find('a').data('key');
        var keyword = $(this).parents('.single-word').find('a').data('combined');
        var current_word = updatedWordsList[key];
        removedWords[current_word.word] = current_word;
        updatedWordsList.splice(key, 1);

        $('.word-editor .removed-words').prepend('<div class="single-word"><a href="javascript:void" class="btn" data-combined="'+keyword+'" data-word="'+current_word.word+'" data-key="0">'+current_word.word+'</a><span class="word-action approve-word"></span></div>');
        
        $(this).parents('.single-word').remove();
        refreshWordKeys();
        addWordsToDictionary();
        removeWordsFromDictionary();
    });
}

function addWordsToDictionary(){
    $('.word-editor .removed-words .approve-word').off('click');
    $('.word-editor .removed-words .approve-word').on('click', function(){
        refreshWordKeys();
        var key = $(this).parents('.single-word').find('a').data('word');
        var keyword = $(this).parents('.single-word').find('a').data('combined');

        var current_word = removedWords[key];
        updatedWordsList.unshift(removedWords[key]);
        delete removedWords[key];

        $('.word-editor .all-words').prepend('<div class="single-word"><a href="javascript:void" class="btn" data-combined="'+keyword+'" data-word="'+current_word.word+'" data-key="0">'+current_word.word+'</a><span class="word-action remove-word"></span></div>');
        $(this).parents('.single-word').remove();
        refreshWordKeys();
        addWordsToDictionary();
        removeWordsFromDictionary();
    });
}

function refreshWordKeys(){
    $('.word-editor .removed-words .single-word').each(function(i, val){
        $(this).find('a').attr('data-key', i);
    });
    $('.word-editor .all-words .single-word').each(function(i, val){
        $(this).find('a').attr('data-key', i);
    });
}

function open_custom_media_window() {
    var container = '.' + $(this).attr('id');

    if (this.window === undefined) {
        this.window = wp.media({
            title: 'Insert Image',
            library: {
                type: 'image'
            },
            multiple: false,
            button: {
                text: 'Insert Image'
            }
        });

        var self = this;
        this.window.on('select', function () {
            var response = self.window.state().get('selection').first().toJSON();
            console.log(response);
            $(container + ' .wp_attachment_id').val(response.id);
            $(container + ' .image').attr('src', response.sizes.full.url);
            $(container + ' .image').show();
        });
    }

    this.window.open();
    return false;
}


jQuery(document).ready(function(){

    var file_frame;
    var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
    var set_to_post_id = 0; // Set this

    jQuery('.upload-media').on('click', function( event ){
        let attachment;
        event.preventDefault();

        let mediaPreview = $(this).attr('data-preview');
        let mediaAttachment = $(this).attr('data-attachment');

        let fileFormats = [ 'video', 'image' ];
        let selectedType = $(this).attr("media-type");
        
        if(selectedType == "Video"){
            fileFormats = ['video'];
        }else if(selectedType == "Image"){
            fileFormats = ['image'];
        }

        // Create the media frame.
        file_frame = wp.media.frames.file_frame = wp.media({
            title: 'Select a media to upload',
            button: {
                text: 'Use this media',
            },
            library: {
                type: fileFormats
            },
            multiple: false // Set to true to allow multiple files to be selected
        });

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
            // We set multiple to false so only get one image from the uploader
            attachment = file_frame.state().get('selection').first().toJSON();
            console.log(attachment);
            let mediaSrc = "/wp-includes/images/media/default.png";
            switch(attachment.type){
                case 'image':
                    mediaSrc = attachment.url;
                break;
                case 'video':
                    mediaSrc = "/wp-includes/images/media/video.png";
                break;
            }
            // Do something with attachment.id and/or attachment.url here
            
            $( mediaPreview ).attr( 'src', mediaSrc ).css( 'width', 'auto' );
            $( mediaAttachment ).val( attachment.id );

            // Restore the main post ID
            wp.media.model.settings.post.id = wp_media_post_id;
        });

            // Finally, open the modal
            file_frame.open();
    });

    // Restore the main ID when the add media button is pressed
    jQuery( 'a.add_media' ).on( 'click', function() {
        wp.media.model.settings.post.id = wp_media_post_id;
    });

    jQuery("#set-adv").on("click", function() {

        let advCount = parseInt(jQuery("#adv-count").val());
        let advOptionList = jQuery("#adv-list").html();
        let advSelectList = '<select name="advtpl['+advCount+'][advertisement]" >'+advOptionList+'</select>';

        let templateOptionList = jQuery("#template-list").html();
        let templateSelectList = '<select name="advtpl['+advCount+'][template]" >'+templateOptionList+'</select>';

        let trHtml = '<tr> <td> '+advSelectList+' </td> <td> '+templateSelectList+' </td> <td> <a href="javascript:void(0);" class="cross-adv">x</a> </td> </tr>';
        jQuery("#adv-section").append(trHtml);

        advCount++;
        jQuery("#adv-count").val(advCount);

        return false;

    })


    $("#adv-section").on("click", ".cross-adv", function() {
        $(this).parents("tr").first().remove();
        return false;
    })

    // Sticky
    jQuery("#set-sticky").on("click", function() {

        let stickyCount = parseInt(jQuery("#sticky-count").val());
        let stickyOptionList = jQuery("#sticky-list").html();
        let stickySelectList = '<select name="stickytpl['+stickyCount+'][advertisement]" >'+stickyOptionList+'</select>';

        let templateOptionList = jQuery("#template-list").html();
        let templateSelectList = '<select name="stickytpl['+stickyCount+'][template]" >'+templateOptionList+'</select>';

        let trHtml = '<tr> <td> '+stickySelectList+' </td> <td> '+templateSelectList+' </td> <td> <a href="javascript:void(0);" class="cross-adv">x</a> </td> </tr>';
        jQuery("#sticky-section").append(trHtml);

        stickyCount++;
        jQuery("#sticky-count").val(stickyCount);

        return false;

    })

    $("#sticky-section").on("click", ".cross-adv", function() {
        $(this).parents("tr").first().remove();
        return false;
    });

    jQuery("select[name*='advertisement_type']").on("change", function() {
       
       if( jQuery(this).val() == 'normal' ){
         jQuery('.position-fields').addClass('init-hide');
       }else{
         jQuery('.position-fields').removeClass('init-hide');
       }
      
    });

});

function openResultspageTab(evt, tabName) {
    evt.preventDefault();
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}