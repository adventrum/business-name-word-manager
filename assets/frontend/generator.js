
var $ = jQuery;
$(document).on('ready', function () {

    // if( jQuery('.keyword_lists').attr('total_list') !== '' ){
    //     var total_keywords = parseInt(jQuery('.keyword_lists').attr('total_list'));
    //     jQuery('.keyword_'+getRandomInt(total_keywords)+'').addClass('show').removeClass('hide').show(); 
    //     jQuery('.keyword_'+getRandomInt(total_keywords)+'').addClass('show').removeClass('hide').show(); 
    //     jQuery('.keyword_'+getRandomInt(total_keywords)+'').addClass('show').removeClass('hide').show(); 
    //     jQuery('.popular_keyword.hide').remove();
    //     jQuery('.keyword_lists').removeClass('default-blur'); 
    // }
  

    $(".name-search.word-generator .generatorbtn").on('submit, click', function (e) {
        e.preventDefault();
        var inputField = $(this).parent().find(".inputMain");
        handleWordGeneratorSubmission($(this).parent().find(".inputMain"), $(this).parent().parent(), $(this).parent().parent().parent(), inputField.data('popup-url'));
    });
    jQuery(".name-search.word-generator .inputMain").on('keypress', function (e) {
        if( e.which == 13 ){
            handleWordGeneratorSubmission($(this), $(this).parent(), $(this).parent().parent(), $(this).data('popup-url'));
        }
    });

    jQuery(".name-search.word-generator .inputMain").on('click', function (e) {
        if( jQuery(window).width() < 768 ){
         jQuery('.navbar').addClass('navbar-onscroll');
        }
        
     });

    $('.chekedomainid').on('click', function () {
        if (!$(this).is(':checked')) {
            $(this).parent().parents().removeAttr('target');
        } else {
            $(this).parent().parents().attr('target', '_blank');
        }
    });

    jQuery(".tooltip-box .close-box").on("click", function() {
        jQuery(this).parent().fadeOut();
        return false;
    });

    jQuery('.popular_keyword').on('click', function(){
      
        var formTag = $(this).parents('form');
        formTag.find('.inputMain').val($(this).text());
        formTag.find('.inputMain').attr('data-popup-url');
        formTag.find('.generatorbtn').trigger('click');
    });

});


function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}


function handleWordGeneratorSubmission(inputField, formTag, formContainer, redirectUrl) {
  

    var input1 = "";
    var value = "";

    var device = getDeviceResolution();
    
    var home_id       =  jQuery('input[name*="home_id"]').val();
    var gclid         =  jQuery('.gclid_val').val();
    var country_code  =  jQuery('.country-code').val();
    var sid_parameter =  jQuery('.sid-parameter').val();
    var parameters    =  sid_parameter + '-' + home_id + '-' + device + '-' + country_code + '';
    

    input1 = inputField.val();
    value = $.trim(input1.replace(/[^a-zA-Z ]/g, ""));
    count = value.split(' ').length;
    var error_msg = '';
    
    if (value == '' || value.length < 3 || count > 6) {
        if (value == '') {
            error_msg = jQuery('.generatorerror1').val();
        } else if (value.length < 3) {
            error_msg = jQuery('.generatorerror2').val();
        } else if (count > 6) {
            error_msg = jQuery('.generatorerror3').val();;;
        }
        formContainer.find('.alert').remove();
       // formContainer.prepend('<div class="alert alert-danger fade in"><div class="alert-wrap">' +
            // '<span>' + error_msg + '</span><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div></div>');
         closeGeneratorAlert();
        if(jQuery(".tooltip-box").length > 0) {
            jQuery(".tooltip-box").show();
        }
    } else {
        if (navigator.userAgent.indexOf("UCBrowser") > -1) {
            formTag.removeAttr('target');
            formTag.submit();
        } else {
            
            formTag.append('<input type="hidden" name="device" value="' + device + '">');

            if (formTag.find(".chekedomainid").is(':checked')) {
                formTag.append('<input type="hidden" class="click-event" data-category="generator" data-action="popunder-open" data-label="' + window.location.href + '">');
                formTag.find('.click-event').trigger('click');
                setTimeout(() => {
                    window.location.href = redirectUrl+parameters+gclid;
                }, 100);
            } else {
                formTag.append('<input type="hidden" class="click-event" data-category="generator" data-action="popunder-toggle-off" data-label="' + window.location.href + '">');
                formTag.find('.click-event').trigger('click');
            }

            inputField.val(value);
            formTag.submit();
        }
    }
}

function getDeviceResolution() {

    if ($(window).width() >= 768 && $(window).width() < 1024) {
        return 'tablet';
    } else if ($(window).width() < 767) {
        return 'mobile';
    } else {
        return 'desktop';
    }
}

function closeGeneratorAlert() {
    // setTimeout(function () {
    //     $('.alert .close').click(function (e) {
    //         e.preventDefault()
    //         $('.alert').fadeOut();
    //     });
    // }, 500);

    setTimeout(function () {
        jQuery('.tooltip-box').fadeOut();
    }, 5000);
}

function generatorPopupDomain(obj) {
    setTimeout(function () {
        var fileurl;
        if ($(obj).parent().find('.inputMain').data("popup-url")) {
            fileurl = $(obj).parent().find('.inputMain').data("popup-url");
        }
        window.location.href = fileurl;
    }, 10);

}