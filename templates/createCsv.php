<div class="wrap api-to-csv-container">
    <h1 class="wp-heading-inline">API to CSV</h1>
    <form method="post">
        <h2 class="wp-heading-inline">Please enter the API endpoint of Datamuse you need to convert to a Words CSV:</h2>
        <input type="text" name="api_to_csv" placeholder="Example: https://api.datamuse.com/words?ml=golf&max=10" required>
        <input type="hidden" name="action" value="bnwm_csv_download">
        <input type="submit" name="download_csv" class="button button-primary button-large" value="Download CSV">
    </form>
</div>