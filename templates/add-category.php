<div class="wrap word-manager-container">
    <?php $add_edit = ( $_GET['add_word_category'] != '' && $_GET['add_word_category'] == "true" ) ? "Add" : 'Edit'; ?>
    <h1 class="wp-heading-inline"><?php echo $add_edit; ?> Category</h1>
    <form method="post">
        <?php $value = ( isset($_GET['edit_cat_id']) &&  $_GET['edit_cat_id'] != '' ) ? $cat_name : ''; ?>
        <input type="text" name="word_category_name" placeholder="Category Name" value="<?php echo $value; ?>">

        <select name="status">
            <option value="Active">Active</option>
            <option value="Inactive">Inactive</option>
        </select>

        <?php if( isset($_GET['edit_cat_id']) && $_GET['edit_cat_id'] != '' ): ?>
            <input type="hidden" name="edit_cat_id" value="<?php echo $_GET['edit_cat_id']; ?>">
        <?php endif; ?>
        <input type="submit" name="download_csv" class="button button-primary button-large update-category" value="Publish">
    </form>
</div>