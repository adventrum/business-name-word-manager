<form method="post" class="add-template">
    <?php if( isset($_GET['edit_template']) ): ?>
        <input type="hidden" name="edit_template" value="<?php echo $_GET['edit_template']; ?>">
    <?php endif; ?>
        <legend>
            <h2>Create a new Template</h2>
        </legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Name</label>
                    </th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($template->name) ? $template->name : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Content</label>
                    </th>
                    <td>
                    <textarea name="content" id="content" rows="15"><?php echo isset($template->content) ? $template->content : ''; ?></textarea>
                    </td>
                </tr>
               
            </tbody>
        </table>
    <?php if (isset($_GET['edit_template'])) : ?>
        <input type="hidden" name="edit_template" value="<?php echo $_GET['edit_template']; ?>">
    <?php endif; ?>
    <p class="submit" style="margin-top: 0;"><input type="submit" name="createrequest" id="createrequest-btn" class="button button-primary" value="<?php echo (isset($_GET['edit_template'])) ? 'Update Template' : 'Add New Template'; ?>"></p>

</form>