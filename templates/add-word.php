<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<form method="post" id="createWord" enctype="multipart/form-data">
    <fieldset>
        <legend><h2>Words Details</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="name">Word</label>
                    </th>
                    <td>
                        <input name="word_name" class="word_name" type="text" id="name" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60" value="<?php echo isset($request) ? $request->name : ''; ?>" required>
                        <p class="error"><span class="success"><i class="loaded fa fa-check"></i> This is a new word.</span><span class="failure"><i class="fa fa-times" aria-hidden="true"></i> This word already exists.</span></p>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="name">Languages</label>
                    </th>
                    <td>
                    <select class="bnwm_word_language" name="word_language[]" multiple="multiple">
                        <?php foreach( $languages as $code => $name ): ?>
                        <option value="<?php echo $code; ?>"><?php echo $name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="name">Categories</label>
                    </th>
                    <td>
                    <select class="word-industries-select" name="word_category[]" multiple="multiple">
                        <?php foreach( $categories as $cat ): ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo ucfirst($cat->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>
    

    <?php
    $btnText = (isset($id)) ? "Update Word" : "Add New Word";
    ?>
    <p class="submit"><input type="submit" name="createword" id="createword-btn" class="button button-primary" value="<?php echo $btnText; ?>"></p>

</form>