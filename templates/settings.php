<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="wrap word-manager-settings-container">
    <h1 class="wp-heading-inline">Word Manager Settings</h1>
    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_word_categories&add_word_category=true" class="page-title-action">Add New</a>
    <h2 class="hndle ui-sortable-handle">Select language before importing the data</h2>
    <form method="post">
            <select name="bnwm_data_language" id="">
                <?php foreach( $languages as $code => $name ): ?>
                <option value="<?php echo $code; ?>"><?php echo $name; ?></option>
                <?php endforeach; ?>
            </select>
    </form>

    <h2 class="hndle ui-sortable-handle">Select Algorithm Type</h2>
    <form method="post">
            <select name="bnwm_algorithm_type" id="">
                <option value="nameidea_algo_1" <?= $algorithm_type == 'nameidea_algo_1' ? 'selected' : '';?>>Old Algorithm</option>
                <option value="nameidea_algo_2" <?= $algorithm_type == 'nameidea_algo_2' ? 'selected' : '';?>>New Algorithm</option>
            </select>
            <a href="javascript:void(0);" class="button button-primary update-algorithm">Save</a>
            <div class="algorithm-updated" style="display:none" role="alert">Sucessfully Updated</div>
    </form>

    <form>
        <h2 class="wp-heading-inline">Essential Data to Import</h2>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Data Type</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Data Action</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Languages Loaded</span></th>
                </tr>
            </thead>

            <tbody id="setting-list">
                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong>Categories</strong>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><a href="javascript:void(0);" data-action="loadCategories" class="button button-primary button-large">Load Data</a><span class="fa fa-circle-o-notch fa-spin spin-loader"></span><span class="spin-loader">Please wait, fetching Data</span></td>
                    <td><?php echo $categories_language; ?></td>
                </tr>

                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong>Category Words</strong>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><a href="javascript:void(0);" data-action="loadCategoryWords" class="button button-primary button-large">Load Data</a><span class="fa fa-circle-o-notch fa-spin spin-loader"></span><span class="spin-loader">Please wait, fetching Data</span></td>
                    <td><?php echo $category_words_language; ?></td>
                </tr>

                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong>Keywords</strong>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><a href="javascript:void(0);" data-action="loadKeywords" class="button button-primary button-large">Load Data</a><span class="fa fa-circle-o-notch fa-spin spin-loader"></span><span class="spin-loader">Please wait, fetching Data</span></td>
                    <td><?php echo $keywords_language; ?></td>
                </tr>

                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong>Word Attributes</strong>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><a href="javascript:void(0);" data-action="loadWordAttributes" class="button button-primary button-large">Load Data</a><span class="fa fa-circle-o-notch fa-spin spin-loader"></span><span class="spin-loader">Please wait, fetching Data</span></td>
                    <td><?php echo $word_attributes_language; ?></td>
                </tr>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Data Type</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Data Action</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Languages Loaded</span></th>
                </tr>
            </tfoot>

        </table>
    </form>
    <?php 
        $table = $this->db->query( "SELECT * 
        FROM information_schema.tables
        WHERE table_schema = '".$this->db->dbname."' 
            AND table_name = '".$this->config->getPopularKeywordsTableName()."'
        LIMIT 1;");
        if( $table == false ){
            ?>
        <form method="post">
            <h2 class="hndle ui-sortable-handle">Popular Keywords</h2>
            <a href="javascript:void(0);" class="button button-primary create-popular-keywords-table">Create Table</a>
        </form>
            <?php
        }

    ?>
</div>