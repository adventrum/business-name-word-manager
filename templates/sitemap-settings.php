<style>
    .sitemap-form .fields {
        display: flex;
        flex-direction: column;
        padding: 20px;
    }

    .sitemap-form label {
        line-height: 1.3;
        font-weight: 600;
        margin-bottom: 12px;
        font-size: 18px;
    }

    .sitemap-form textarea, .sitemap-form input[type="text"] {
        padding: 8px;
        width: 50%;
    }
    .sitemap-form input[type="submit"]{
        margin-left: 20px;
    }
    .sitemap-form .note{
        margin: 0;
        margin-bottom: 10px;
    }
</style>
<form class="sitemap-form" method="post" name='sitemap_dashboard_form'>
    <div class="fields">
        <label for="exclude_posts">Exclude post IDs</label>
        <p class="note">Just add the IDs, separated by a comma, of the pages you want to exclude.</p>
        <?php $excluded_posts = get_option('mlsg_exclude_posts') ? get_option('mlsg_exclude_posts') : ''; ?>
        <textarea name="exclude_posts" id="exclude_posts" cols="30" rows="10"><?php echo $excluded_posts; ?></textarea>
    </div>
    <?php $included_posts = get_option('mlsg_include_post_types'); ?>
    <div class="fields">
        <label for="include_post_types">Post types to include in Sitemap</label>
        <p class="note">Just add the name, separated by a comma, of the post types you want to include.</p>
        <?php $included_posts = get_option('mlsg_include_post_types') ? get_option('mlsg_include_post_types') : ''; ?>
        <input type="text" name="include_post_types" id="include_post_types" value="<?php echo $included_posts; ?>">
    </div>
    <input type="hidden" name="action" value="mlsg_sitemap_posts" />
    <input type="hidden" name="mlsg_nonce" value="<?php echo wp_create_nonce('mlsg_sitemap'); ?>" />
    <input type="submit" value="Save" class="button button-primary">
</form>