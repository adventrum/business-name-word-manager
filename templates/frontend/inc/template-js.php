<script type="text/javascript">
	
	
	jQuery(document).ready(function() {
	        
	   
	   <?php 
           $checked = 'false';
	  
	       if ( is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php','template-usernames.php','template-godaddy.php']) ) : 
	   
	       $class_name = is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php']) ? 'result-page' : 'usernames'; 
           
           if(is_page_template('template-godaddy.php')){
	        $checked = 'true';
	        $class_name = 'redirect_page';
           }
	    ?> 
	   
	    var checked = "<?= $checked; ?>";
	    if(jQuery('.stardomain').length < 25 || checked == 'true' ){
	       var className = "<?= $class_name; ?>";
	       pageHgt(className);
        }  
	   
	   <?php endif; ?>
	}); 

	jQuery(window).on('resize', function() {
		
		 <?php 
            
            $checked = 'false';
	  
	        if ( is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php','template-usernames.php','template-godaddy.php']) ) : 
	   
	        $class_name = is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php']) ? 'result-page' : 'usernames'; 
           
            if(is_page_template('template-godaddy.php')){
	          $checked = 'true';
	          $class_name = 'redirect_page';
            }
	    ?> 
	   
	    var checked = "<?= $checked; ?>";
	    if(jQuery('.stardomain').length < 30 || checked == 'true' ){
	       var className = "<?= $class_name; ?>";
	       pageHgt(className);
        }  
	   
	   <?php endif; ?>

	    // For exit popup make responsive on resize
        if(jQuery.cookie('exit-popup') != 'true' && jQuery('.exit-popup').hasClass('slideup') &&  
           	jQuery('.snp-bld-step').css('display') != 'none'){
	      setTimeout(function(){ 
	          
	          var maxHeight = jQuery(window).height(); 
	          var maxWidth = jQuery(window).width();   
	          var cur_step = jQuery('.snp-bld-showme');
	          var scaleX = maxWidth / 900;
	          var scaleY = maxHeight / 499;
	          var scale = ((scaleX > scaleY) ? scaleY : scaleX) - 0.01;
	          
	          if (scale > 1) {
	              scale = 1;
	          }
	          var parent = jQuery('.snp-bld-center');
	          if (scale < 1) {
	              parent.css('transform', 'translateX(-50%) translateY(-50%) scale(' + scale + ')');
	              parent.css('-webkit-transform', 'translateX(-50%) translateY(-50%) scale(' + scale + ')');
	              parent.css('-moz-transform', 'translateX(-50%) translateY(-50%) scale(' + scale + ')');
	              parent.css('-ms-transform', 'translateX(-50%) translateY(-50%) scale(' + scale + ')');
	          } else {
	              parent.css('transform', "");
	              parent.css('-webkit-transform', "");
	              parent.css('-moz-transform', "");
	              parent.css('-ms-transform', "");
	          }

	      }, 100);

	    }
	});

	jQuery(document).on('keyup','.search_bar input[type="text"]',function() 
	{       
		    var searchTerm = jQuery(this).val().toLowerCase();
	        
	        if(searchTerm == ''){
	        	jQuery('.live-search-list li').show();
	        	jQuery('.tld-dropdown-list li').show();
	        	return false;
	        }
		    

		    if(jQuery(this).attr('class') == 'indus-srch-fltr' && !jQuery(this).hasClass('tld-search') ){
		        jQuery('#live-search-list li, .domain-filter .live-search-list li').not('.domain-filter .word-type li').each(function() {
			      if (jQuery(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
			        jQuery(this).show();
			      } else {
                    jQuery(this).hide();
			      }

			    });
		  
		    }

	        if( jQuery(this).hasClass('tld-search') ){
		        
		        searchTerm =  searchTerm.replace('.', '');
		        if(searchTerm.length > 0){
			        jQuery('.tld-dropdown-list li').each(function() { 
			        	if (jQuery(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
			        	  jQuery(this).show();
			        	} else {
			        	  jQuery(this).hide();
			        	}

			         })
		        }
            }
		    //else
	      //  {
	    
			    // jQuery('.live-search-list li').each(function() {

			    //    if (jQuery(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
			    //      jQuery(this).show();
			    //    } else {
			    //      jQuery(this).hide();
			    //    }

 			   //  });
 
     	   // }  
        
	});
    

    jQuery(document).click(function(e) {
        var checked = '';
        
        if (!jQuery(e.target).closest('#myModal,#main-filter-modal,#myModalSaved').length &&
        	!jQuery(e.target).closest('.apply-fltr,.add-event,.eng-other-filter').length &&
            !jQuery(e.target).closest('.eng-industry-filter,.keywordmatching,.snp-builder').length && 
            checked == '') {
            
            if(jQuery('#myModal').css('display') == 'block'){
              jQuery('#myModal').modal('toggle');
              checked = '';
            }
            if(jQuery('#main-filter-modal').css('display') == 'block'){
              jQuery('#main-filter-modal').modal('toggle');
              checked = '';
            }
            if(jQuery('#myModalSaved').css('display') == 'block'){
              jQuery('#myModalSaved').modal('toggle');
              checked = '';
            }

            var url = window.location.pathname;
            var url = url.split("/");
            
            if(url[1] == "usernames"){

	            if(e.target.className != 'btn clear'){
		            if (!e.target.className == "eng-industry-filter" || !jQuery(e.target).parents(".elm").length) {  
		                if(jQuery('.eng-industry-filter').parent("div").find(".drop-down-data").css('display') == 'block'){
		                 jQuery('.eng-industry-filter').parent("div").removeClass('menu-open');
		                 jQuery('.eng-industry-filter').parent("div").find(".drop-down-data").slideToggle('fast');
		                 checked = '';
		                }
			        }
		        }
	          
	          
	            if(e.target.className != 'btn cancel cancel_btn'){
		            if (!e.target.className == "eng-other-filter" || !jQuery(e.target).parents(".elm").length) {  
		                if(jQuery('.eng-other-filter').parent("div").find(".drop-down-data").css('display') == 'block'){
			              jQuery('.eng-other-filter').parent("div").removeClass('menu-open');
			              jQuery('.eng-other-filter').parent("div").find(".drop-down-data").slideToggle('fast');
			              checked = '';
			            }
			        }
		        }
	        }

	        // for exit content popup

	        if(jQuery.cookie('exit-popup') != 'true' && jQuery('.exit-popup').hasClass('slideup') &&  
	        	jQuery('.snp-bld-step').css('display') != 'none'){
	        	jQuery('.exit-popup').css('display','none').removeClass('slideup');
	            jQuery('.snp-bld-step').css('display','none') 
	            jQuery('.snp-overlay').removeClass('snp-overlay-show'); 
	            jQuery.cookie('exit-popup','true',7000);
            } 

            checked = 1;
        }
    });
  

    // For exit content popup
    

    jQuery(document).on('mouseleave', function() {

       if(jQuery.cookie('exit-popup') != 'true' && !jQuery('.exit-popup').hasClass('slideup') && jQuery('.snp-bld-showme').length > 0 ){
         jQuery('.exit-popup').css('display','block').addClass('slideup');
         jQuery('.snp-bld-step').css('display','block'); 
         jQuery('.snp-overlay').addClass('snp-overlay-show'); 
       }
    });

    
    // For Sticky Banner Js

    jQuery(document).on('click','.right-cad, .bottom-cad',function(e){
      
	    if(e.target.tagName.toLowerCase() !== 'a'){
	        window.open(jQuery(this).attr('link'));
	    }

	    

    });

    jQuery(document).on('click','.hide-btn',function(e){
	   jQuery(this).parent("div").remove();  

    });

    jQuery(window).scroll(function(){
       
        if(jQuery('.leader-mobile').length > 0 || jQuery('.leader-bottom') > 0)
        {
        
	        if(jQuery(window).width() < 768 )
	        {
		      var top = jQuery(window).scrollTop();
		      
		      if(top > 50 && !jQuery('.leader-mobile').hasClass('slideup')  ){
		     
		        jQuery('.leader-mobile').slideToggle('slow').addClass('slideup');
		     
		      }
		      // else{
		      	
		      // 	if(top < 50 && jQuery('.leader-mobile').hasClass('slideup')){
		      // 	  jQuery('.leader-mobile').slideToggle('slow').removeClass('slideup');
		      //   }
		      // }
	        }

	        
	        var url = window.location.pathname;
	        var url = url.split("/");

	        if( (jQuery(window).width() > 768 && url[1] == "nameideas") || (jQuery(window).width() > 768 && url[1] == "usernames") )
	        {
		      var top = jQuery(window).scrollTop();
		      
		      if(top > 50 && !jQuery('.leader-bottom').hasClass('slideup')  ){
		     
		        jQuery('.leader-bottom').slideToggle('slow').addClass('slideup');
		     
		      }
		    }
	    }    
    });
   

    jQuery(document).on('click','.open-copy',function(){
           
        jQuery('#copy-code').val(jQuery(this).attr('nameidea'));
        var copyText = document.getElementById("copy-code");
           copyText.select();
           copyText.setSelectionRange(0, 99999)
           document.execCommand("copy");
        
        jQuery("#passwords").modal('show');
    });

    
    function pageHgt(className) {
        if(jQuery(window).width() >= 768 ){
          var viewHgt = jQuery(window).height()+'px';
          jQuery('.'+className).css('height',viewHgt);
        }
       
    }


   // Event Fired Related Code

   	jQuery(document).ready(function(){
   		
   		jQuery(document).on('click','.outbound-link',function(e){
             if ( e.target === jQuery(this).find('.hide-btn p')[0]) return;
   		  var event_cat = jQuery(this).data('category'); 
   		  var event_act = jQuery(this).data('action'); 
   		  var event_lab = jQuery(this).data('label'); 
   		  
   		  jQuery('.add-event').attr( 'data-category',event_cat);
   		  jQuery('.add-event').attr( 'data-action'  ,event_act);
   		  jQuery('.add-event').attr( 'data-label'   ,event_lab);
   		  
   		  jQuery('.add-event').click(); 
           
           }); 
   		

   		jQuery(document).on('click','.indus-filter',function(e){
                
               var event_cat = jQuery(this).data('category'); 
               var event_act = jQuery(this).data('action'); 
               var event_lab = '';
            
               var check_length = jQuery("#live-search-list input[name='industry[]']:checked").length;
               if(check_length > 0 && check_length < 2){
                 event_lab = jQuery("#live-search-list input[name='industry[]']:checked").parent().parent().attr('data-search-term'); 
               } 

               jQuery('.add-event').attr( 'data-category',event_cat);
               jQuery('.add-event').attr( 'data-action'  ,event_act);
               jQuery('.add-event').attr( 'data-label'   ,event_lab);

               jQuery('.add-event').click(); 
           }); 
   	});
    
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 1) {
			jQuery('.navbar').addClass('navbar-onscroll');
		} else {
		  jQuery('.navbar').removeClass('navbar-onscroll');
		}
	});      

</script>