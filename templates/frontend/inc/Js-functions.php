<script type="text/javascript">

    function Country_Code_Plus_Device(){
       
       var current_country = "<?php echo getenv( 'HTTP_GEOIP_COUNTRY_CODE' ); ?>";
       
       if(jQuery(window).width() >= 768 && jQuery(window).width() < 1024 ){
        return '-tablet-'+current_country;
       }else if(jQuery(window).width() < 767 ){
         return '-mobile-'+current_country;
       }else{
         return '-desktop-'+current_country;
       }
    }
    

    function Device_Detect(){
       
        if(jQuery(window).width() >= 768 && jQuery(window).width() < 1024 ){
         return 'tablet';
        }else if(jQuery(window).width() < 767 ){
         return 'mobile';
        }else{
         return 'desktop';
        }
    }

     function IpBasedAffiliatesPHP(sid_name='',home_id='',domain=''){
     
      <?php $lang = ''; if (defined('ICL_LANGUAGE_CODE')) {  $lang = ICL_LANGUAGE_CODE; }?>
      var current_country = "<?php echo getenv( 'HTTP_GEOIP_COUNTRY_CODE' ); ?>";
      var current_language = "<?php echo strtoupper($lang); ?>";
      var Ipbased_device  = Country_Code_Plus_Device(); 

      var aff_link;
     
      if( current_country == 'DE'){
        
        aff_link = 'https://acn.ionos.de/aff_c?offer_id=2&aff_id=1039&url=http%3A%2F%2Fionos.de%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.PU.PUt02K418211T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3D&source='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else if( current_country == 'IT'){
      
        aff_link = 'https://acn.ionos.it/aff_c?offer_id=8&aff_id=1039&url=https%3A%2F%2Fionos.it%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.IA.IAt02K418412T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3D&source='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else if( current_country == 'ES'){
      
        aff_link = 'https://acn.ionos.es/aff_c?offer_id=4&aff_id=1039&url=http%3A%2F%2Fionos.es%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.WE.WEt02K418216T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3D&source='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else if( current_country !== 'ES' && current_country !== 'MX' && current_language == 'ES' ){
      
        aff_link = 'https://acn.ionos.com/aff_c?offer_id=1&aff_id=1039&url=http%3A%2F%2Fionos.com%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.US.USt02K418213T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3Dsource='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else if( current_country == 'FR'){
      
        aff_link = 'https://acn.ionos.fr/aff_c?offer_id=5&aff_id=1039&url=http%3A%2F%2Fionos.fr%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.FR.FRt02K418215T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3D&source='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else if( current_country == 'MX'){
      
        aff_link = 'https://acn.ionos.mx/aff_c?offer_id=7&aff_id=1039&url=https%3A%2F%2Fionos.mx%2Fdcjump%3Fdomain%3D'+domain+'%26transaction_id%3D{transaction_id}%26ac%3DOM.MB.MBt02K418413T7073a%26affiliate_id%3D{affiliate_id}%26utm_source%3Daffiliate%26utm_medium%3D{affiliate_name}%26utm_campaign%3D&source='+sid_name+'-post-'+home_id+Ipbased_device+'%26utm_content%3D{file_name}';
      
      } else {
      
        aff_link = '';
      }
      return aff_link;

    }
    
    function WixAffiliateLinksJS(nameidea=''){
      
      <?php 
          $lang = getCurrentSite();
          $home_id =  !empty($_GET['home_id']) ? $_GET['home_id'] : '';
          $aff_nameideas = json_decode(get_option('bnwm_affiliate_links_nameideas'));
          $aff_nameideas_wix = json_decode(get_option('bnwm_affiliate_links_nameideas_wix'));
          $aff_domains = json_decode(get_option('bnwm_affiliate_links_domains'));
          $page_slug = CURRENT_MULTISITE_CODE == '' ? explode('/',$_SERVER['REDIRECT_URL'])[1] : explode('/',$_SERVER['REDIRECT_URL'])[2];
     ?>
       
      var home_id = "<?= $home_id ?>"; 
      var current_country = "<?php echo getenv( 'HTTP_GEOIP_COUNTRY_CODE' ); ?>";
      var current_language = "<?php echo strtoupper($lang); ?>";
      var Ipbased_device  = Country_Code_Plus_Device(); 
      var gclid = "<?= getGclidParam() ?>";
      var parameters = home_id+Ipbased_device+gclid;
      
      var country_codes = ['FR','DE','IT','ES','MX','BR','PT','NL','SE'];

      var response = [];
       
       <?php if( ( !empty($aff_nameideas->registerlinksavedidea) &&  !empty( $aff_nameideas->registerlinksavedidea->geo_url ) ) &&  $page_slug !== 'nameideas-wix'  ) :
                                           
         $registerlinksavedidea =  $aff_nameideas->registerlinksavedidea; 
         $sid =  $registerlinksavedidea->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
         $affiliate_link = $registerlinksavedidea->geo_url.$sid.$registerlinksavedidea->geo_sid;  
      
      ?> 
        var saveidregisterlink = '<?= $affiliate_link ?>-'+parameters; 

      <?php endif; ?> 
       
        <?php if( ( !empty($aff_nameideas_wix->registerlinksavedidea) &&  !empty( $aff_nameideas_wix->registerlinksavedidea->geo_url ) ) &&  $page_slug == 'nameideas-wix'  ) :
                                           
         $registerlinksavedidea =  $aff_nameideas_wix->registerlinksavedidea; 
         $sid =  $registerlinksavedidea->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
         $affiliate_link = $registerlinksavedidea->geo_url.$sid.$registerlinksavedidea->geo_sid;  
      
      ?> 
        var saveidregisterlink = '<?= $affiliate_link ?>-'+parameters; 

      <?php endif; ?> 
      
      <?php if( ( !empty($aff_domains->registerlinksavedidea) &&  !empty( $aff_domains->registerlinksavedidea->geo_url ) ) &&  $page_slug == 'domains'  ) :
                                           
         $registerlinksavedidea =  $aff_domains->registerlinksavedidea; 
         $sid =  $registerlinksavedidea->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
         $affiliate_link = $registerlinksavedidea->geo_url.$sid.$registerlinksavedidea->geo_sid;  
      
      ?> 
        var saveidregisterlink = '<?= $affiliate_link ?>-'+parameters; 

      <?php endif; ?> 


       // CHECK FOR ALT LINK 

       <?php  
      
       if( ( !empty($aff_nameideas->checkforaltdropdown) &&  !empty( $aff_nameideas->checkforaltdropdown->geo_url ) ) &&  $page_slug !== 'nameideas-wix'  ) :
        
         $checkforaltdropdown =  $aff_nameideas->checkforaltdropdown; 
         $sid =  $checkforaltdropdown->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $checkforaltdropdown->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
         $checkforaltdropdown_link = $checkforaltdropdown->geo_url.$sid.$checkforaltdropdown->geo_sid; 
      ?>
      var altlink = '<?= $checkforaltdropdown_link ?>-'+home_id+Ipbased_device+gclid;  

      <?php endif; ?> 

      <?php   if( ( !empty($aff_nameideas_wix->checkforaltdropdown) &&  !empty( $aff_nameideas_wix->checkforaltdropdown->geo_url ) ) &&  $page_slug == 'nameideas-wix'  ) :
                                           
         $checkforaltdropdown =  $aff_nameideas_wix->checkforaltdropdown; 
         $sid =  $checkforaltdropdown->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $checkforaltdropdown->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
         $checkforaltdropdown_link = $checkforaltdropdown->geo_url.$sid.$checkforaltdropdown->geo_sid;
      
      ?> 
      var altlink = '<?= $checkforaltdropdown_link ?>-'+home_id+Ipbased_device+gclid;
     
      <?php endif; ?> 
       

      if( current_country == 'FR' &&  current_language == 'FR'){
        
        var url = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=';
    //    var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Passez en ligne avec le bon nom de domaine';   
      //  var altlink = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=&s2=nameidea-alt-post-'+parameters;
        var disclosuretext = 'En savoir plus';    
      
      } else if( current_country == 'DE' &&  current_language == 'DE'){
      
        var url = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Mit der richtigen Domain online gehen';   
    //    var altlink = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=&s2=nameidea-alt-post-'+parameters;
        var disclosuretext = 'Mehr erfahren';    
      
      } else if( current_country == 'IT' &&  current_language == 'IT'){
      
        var url = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=';
    //    var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Vai online con il dominio giusto per te';   
     //   var altlink = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=&s2=nameidea-alt-post-'+parameters;
        var disclosuretext = 'Per saperne di più';    
      
      } else if( current_country == 'ES' &&  current_language == 'ES' ){
      
        var url = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Ve online con el nombre de dominio adecuado';   
       // var altlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=nameidea-alt-post-'+parameters;
        var disclosuretext = 'Leer más';    
      
      } else if( current_country == 'MX' &&  current_language == 'ES'){
      
        var url = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Ve online con el nombre de dominio adecuado';   
      //  var altlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Leer más';   
      
      } else if( current_country == 'BR' &&  current_language == 'PT-BR'){
      
        var url = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Fique online com o domínio certo para o seu site';   
      //  var altlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=nameidea-alt-post-'+parameters;
        var disclosuretext = 'Saiba mais';    
      
      } else if( current_country == 'PT' &&  current_language == 'PT-PT'){
      
        var url = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Fique online com o domínio certo para o seu site';   
       // var altlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Saiba mais';   
      
      } else if( current_country == 'PT' &&  current_language == 'PT-PT'){
      
        var url = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Fique online com o domínio certo para o seu site';   
      //  var altlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Saiba mais';   
      
      } else if( jQuery.inArray(current_country, country_codes) !== -1 && current_language == 'EN'){
      
        var url = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Get online with the right domain';   
       // var altlink = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Learn more';  
         
        <?php if( empty($affiliate_link) ) : ?>
          var saveidregisterlink = 'https://www.kqzyfj.com/click-3797283-15162961?sid=saveidea-register-post-'+home_id+Ipbased_device+gclid+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D' + nameidea + ''+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate'; 
        <?php endif; ?>  

      } else if( current_language == 'NL' ){
      
        var url = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Ga online met het juiste domein';   
       // var altlink = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Leer meer';   
      
      } else if( current_language == 'SV' ){
      
        var url = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=';
      //  var saveidregisterlink = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=&s2=saveidea-register-'+parameters;   
        var alttext = 'Kom online med rätt domän';   
      //  var altlink = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=&s2=nameidea-alt-post-'+parameters; 
        var disclosuretext = 'Läs mer';   
      
      } else {

        var url = 'https://www.kqzyfj.com/click-3797283-15162961';
        <?php $alt_text =  __('Check for alternative domains', 'thegem-child');   ?>
        <?php $disclosuretext =  __('Learn more.','thegem-child');   ?>
        var alttext        = ""; 
        var disclosuretext = ""; 
       
        var tld =  nameidea.split(".").length < 2 ? '.com' : '';
          
         
       <?php  
       
        if( ( !empty($aff_nameideas->registerlinksavedidea) &&  !empty( $aff_nameideas->registerlinksavedidea->url ) ) &&  $page_slug !== 'nameideas-wix'  ) :
         
          $registerlinksavedidea =  $aff_nameideas->registerlinksavedidea; 
          $sid =  $registerlinksavedidea->sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->sponsor == 'godaddy'  ? '?sid=' : '' ); 
          $registerlinksavedidea_link = $registerlinksavedidea->url.$sid.$registerlinksavedidea->sid; 
       ?>
       
       var check_sponsor = "<?= $registerlinksavedidea->sponsor  ?>";
       var end_link = check_sponsor == 'godaddy' ? '&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D' + nameidea + ''+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate' : ''; 
       var saveidregisterlink = '<?= $registerlinksavedidea_link ?>-'+home_id+Ipbased_device+gclid+end_link;
     
       <?php endif; ?> 

       <?php   if( ( !empty($aff_nameideas_wix->registerlinksavedidea) &&  !empty( $aff_nameideas_wix->registerlinksavedidea->url ) ) &&  $page_slug == 'nameideas-wix'  ) :
                                            
          $registerlinksavedidea =  $aff_nameideas_wix->registerlinksavedidea; 
          $sid =  $registerlinksavedidea->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
          $registerlinksavedidea_link = $registerlinksavedidea->geo_url.$sid.$registerlinksavedidea->geo_sid;  
      
       ?> 
         var saveidregisterlink = '<?= $registerlinksavedidea_link ?>-'+home_id+Ipbased_device+gclid; 
     
       <?php endif; ?> 

        <?php   if( ( !empty($aff_domains->registerlinksavedidea) &&  !empty( $aff_domains->registerlinksavedidea->url ) ) &&  $page_slug == 'domains'  ) :
                                            
          $registerlinksavedidea =  $aff_domains->registerlinksavedidea; 
          $sid =  $registerlinksavedidea->sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinksavedidea->sponsor == 'godaddy'  ? '?sid=' : '' ); 
          $registerlinksavedidea_link = $registerlinksavedidea->url.$sid.$registerlinksavedidea->sid;  
      
       ?> 
         var check_sponsor = "<?= $registerlinksavedidea->sponsor  ?>";
         var end_link = check_sponsor == 'godaddy' ? '&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D' + nameidea + ''+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate' : '';  
         var saveidregisterlink = '<?= $registerlinksavedidea_link ?>-'+home_id+Ipbased_device+gclid+end_link;
     
       <?php endif; ?> 
   

       <?php if( empty($registerlinksavedidea_link) ) : ?>
     
        var saveidregisterlink = 'https://www.kqzyfj.com/click-3797283-15162961?sid=saveidea-register-post-'+home_id+Ipbased_device+gclid+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D' + nameidea + ''+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate'; 
     
        <?php endif; ?>
        
        
        // CHECK FOR ALT LINK 

        <?php  
       
        if( ( !empty($aff_nameideas->checkforaltdropdown) &&  !empty( $aff_nameideas->checkforaltdropdown->url ) ) &&  $page_slug !== 'nameideas-wix'  ) :
         
          $checkforaltdropdown =  $aff_nameideas->checkforaltdropdown; 
          $sid =  $checkforaltdropdown->sponsor == 'wix' ? '&s1=&s2=' :  ( $checkforaltdropdown->sponsor == 'godaddy'  ? '?sid=' : '' ); 
          $checkforaltdropdown_link = $checkforaltdropdown->url.$sid.$checkforaltdropdown->sid; 
       ?>
       var check_sponsor = "<?= $checkforaltdropdown->sponsor  ?>";
       var end_link = check_sponsor == 'godaddy' ? '&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate' : '';    
       var altlink = '<?= $checkforaltdropdown_link ?>-'+home_id+Ipbased_device+gclid+end_link; 

       <?php endif; ?> 

       <?php   if( ( !empty($aff_nameideas_wix->checkforaltdropdown) &&  !empty( $aff_nameideas_wix->checkforaltdropdown->url ) ) &&  $page_slug == 'nameideas-wix'  ) :
                                            
          $checkforaltdropdown =  $aff_nameideas_wix->checkforaltdropdown; 
          $sid =  $checkforaltdropdown->sponsor == 'wix' ? '&s1=&s2=' :  ( $checkforaltdropdown->sponsor == 'godaddy'  ? '?sid=' : '' ); 
          $checkforaltdropdown_link = $checkforaltdropdown->url.$sid.$checkforaltdropdown->sid;
      
       ?> 
         var check_sponsor = "<?= $checkforaltdropdown->sponsor  ?>";
         var end_link = check_sponsor == 'godaddy' ? '&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate' : '';          
         var altlink = '<?= $checkforaltdropdown_link ?>-'+home_id+Ipbased_device+gclid+end_link; 
        

       <?php endif; ?> 
   

       <?php if( empty($checkforaltdropdown_link) ) : ?>
     
        var altlink = 'https://www.kqzyfj.com/click-3797283-15162961?sid=nameidea-alt-post-'+home_id+Ipbased_device+gclid+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate'; 

        <?php endif; ?>
      }

      response['url'] = url;
      response['saveidregisterlink'] = saveidregisterlink;
      response['alttext'] = alttext;
      response['altlink'] = altlink;
      response['disclosuretext'] = disclosuretext;
      return response;

    } 

    // For Drop Down Section Function

    function dropdown_section_nameideas(element,nameidea){
         
        
         <?php
           $translations = json_decode(get_option('bnwm_string_translations')); 
         
           $check_for_alt =  !empty($translations->domaincheckercheckforalt) ? str_replace("\\",'',$translations->domaincheckercheckforalt) : 'Check for alternative domains';
         
           $disclosuretext =  !empty($translations->domaincheckerdisclosuretext) ? str_replace("\\",'',$translations->domaincheckerdisclosuretext) : 'Our name generator is funded in part by affiliate commissions, at no extra cost to users';
        
           $learn_more =  !empty($translations->domaincheckerlearnmore) ? str_replace("\\",'',$translations->domaincheckerlearnmore) : 'Learn More';
         ?>
         

         
         var check_for_alt  = WixAffiliateLinksJS(nameidea)['alttext'];
         var check_for_alt  = check_for_alt !== '' ? check_for_alt : "<?= $check_for_alt; ?>";
         
         var learn_more     = WixAffiliateLinksJS(nameidea)['disclosuretext'];
         var learn_more     = learn_more !== '' ? learn_more : "<?= $learn_more; ?>";
         
         var aff_link       = WixAffiliateLinksJS(nameidea)['altlink']; 

         jQuery(element).after('<li id="results"><div class="innerResult clearfix"><div class="imgloadingshow" style="display: none;"><div class="inner"><img src="<?= WORD_MANAGER_PLUGIN_URL.'assets/images/BNG-loader.gif'?>" /></div></div><ul class="colm1"></ul><div class="row"><div class="col-md-4"></div><div class="col-md-4"><div class="mob-alternative"><div class="domain-advertise"> <a href="'+aff_link+'" class="tracking_event click-event" data-search="'+nameidea+'.com" data-category="ResultsPage" data-action="DomainAvailabilityCheck" data-label="CheckAltDom-ResultsDropdown" target="_blank">'+check_for_alt+'<span><i class="fas fa-external-link-alt"></i></span> </a></div><div class="domain-disclosure"><?php echo addslashes($disclosuretext); ?><a href="/disclosure/" target="_blank">'+learn_more+'</a></div></div></div><div class="col-md-4"></div></div></div></li>');  
    }

    function dropdown_section_businessname(element,nameidea){
         
         <?php
           
           $aff_disc = __('Affiliate Disclosure.','thegem-child');

           $page_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
           $device  = !empty($_GET['device'])  ? $_GET['device'] : '';
         
           $response = AltIpBasedAffiliatesBusiness('bng-nameidea-alt-post',$page_id,$device);
           $aff_link = !empty($response['aff_link']) ? $response['aff_link'] : '';
           
           
         ?>
         
         var page_id  = "<?= $page_id; ?>";
         var Ipbased_device = Country_Code_Plus_Device(); 
         var aff_link = '';
       
         
         <?php if(!empty($aff_link)) :  ?>
            
            var aff_link = "<?= $aff_link;  ?>"; 

         <?php else : ?>
     
           var aff_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=nameidea-alt-post-'+page_id+Ipbased_device+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomains%2Fsearchresults.aspx%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1'; 

         <?php endif; ?>  
          
         jQuery(element).after('<li id="results"><div class="innerResult clearfix"><div class="imgloadingshow" style="display:none;"><div class="inner"><img src="<?= WORD_MANAGER_PLUGIN_URL.'assets/images/BNG-loader.gif'?>"/></div></div><ul class="colm1"></ul><div class="row"><div class="col-md-4"></div><div class="col-md-4"><div class="domain-advertise"><a href="'+aff_link+'" class="tracking_event click-event" data-category="ResultsPage" data-action="DomainAvailabilityCheck" data-label="CheckAltDom-ResultsDropdown" target="_blank"><?php echo __('Check for alternative domains', 'thegem-child'); ?></a></div><div class="domain-disclosure"><?php echo __('We may earn a commission (at no extra cost to you) if you make a purchase through links on our site. See our', 'thegem-child'); ?><a href="/disclosure/" target="_blank"><?php echo addslashes($aff_disc); ?></a></div></div><div class="col-md-4"></div></div></div></li>');  
    }
    

    function save_ideas_section_businessname(element,nameidea){
         
         <?php
           
           $aff_disc = __('Affiliate Disclosure.','thegem-child');

           $page_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
           $device  = !empty($_GET['device'])  ? $_GET['device'] : '';
          
           $response = AltIpBasedAffiliatesBusiness('bng-saveidea-alt-post',$page_id,$device);
           $aff_link = !empty($response['aff_link']) ? $response['aff_link'] : '';
           
         ?>
         
         var page_id = "<?= $page_id; ?>";
         var Ipbased_device = Country_Code_Plus_Device(); 
         var aff_link = '';
         

         <?php if(!empty($aff_link)) :  ?>
            
            aff_link = "<?= $aff_link;  ?>"; 

         <?php else : ?>
         
           aff_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=saveidea-alt-post-'+page_id+Ipbased_device+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomains%2Fsearchresults.aspx%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1'; 

         <?php endif; ?>  


         jQuery(element).append('<div id="results"><div class="innerResult clearfix"><ul class="colm1"></ul><div class="row"><div class="col-md-3"></div><div class="col-md-6"><div class="domain-advertise"><a href="'+aff_link+'" class="tracking_event outbound-link" data-category="ResultsPage" data-action="DomainAvailabilityCheck" data-label="CheckAltDom-SavedIdeas-RegisterOverlay" target="_blank"><?php echo __('Check for alternative domains', 'thegem-child'); ?></a></div><div class="domain-disclosure"><?php echo __('We may earn a commission (at no extra cost to you) if you make a purchase through links on our site. See our', 'thegem-child'); ?><a href="/disclosure/" target="_blank"><?php echo addslashes($aff_disc); ?></a></div></div><div class="col-md-3"></div></div></div></div>');  
    }

    
    function dropdown_section_bus(element,nameidea){
         
         <?php
           
           $aff_disc = __('Affiliate Disclosure.','thegem-child');

           $page_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
        
         ?>
         
         var page_id = "<?= $page_id; ?>";
         var Ipbased_device = Country_Code_Plus_Device(); 
        
         var aff_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=nameidea-alt-post-'+page_id+Ipbased_device+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomains%2Fsearchresults.aspx%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1'; 


         jQuery(element).after('<li id="results"><div class="innerResult clearfix"><div class="imgloadingshow" style="display:none;"><div class="inner"><img src="<?= WORD_MANAGER_PLUGIN_URL.'assets/images/BNG-loader.gif'?>"/></div></div><ul class="colm1"></ul><div class="row"><div class="col-md-4"></div><div class="col-md-4"><div class="domain-advertise"><a href="'+aff_link+'" class="tracking_event click-event" data-category="ResultsPage" data-action="DomainAvailabilityCheck" data-label="CheckAltDom-ResultsDropdown" target="_blank"><?php echo __('Check for alternative domains', 'thegem-child'); ?></a></div><div class="domain-disclosure"><?php echo __('We may earn a commission (at no extra cost to you) if you make a purchase through links on our site. See our', 'thegem-child'); ?><a href="/disclosure/" target="_blank"><?php echo addslashes($aff_disc); ?></a></div></div><div class="col-md-4"></div></div></div></li>');  
    }
    
    
    function dropdown_section_usernames(element,nameidea){
         
         jQuery(element).after('<li id="results"><div class="innerResult clearfix"><div class="imgloadingshow" style="display:none;"><div class="inner"><img src="<?= WORD_MANAGER_PLUGIN_URL.'assets/images/BNG-loader.gif'?>"/></div></div><div class="row"> <div class="col-md-6"><ul class="colm1"></ul></div><div class="col-md-6"> <ul class="colm2"></ul><div class="domain-advertise"><a href="javascript:void(0);" class="open-copy outbound-link" data-category="username-resultspage" data-action="copyusername-button" data-label="" nameidea='+nameidea+'><span><i class="fas fa-clone"></i></span> Copy Username </a></div></div></div></div></li>');  
    }


    
    function save_ideas_section_bus(element,nameidea){
         
         <?php
           
           $aff_disc = __('Affiliate Disclosure.','thegem-child');

           $page_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
           
         ?>
         
         var page_id = "<?= $page_id; ?>";
         var Ipbased_device = Country_Code_Plus_Device(); 
        
         var aff_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=saveidea-alt-post-'+page_id+Ipbased_device+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomains%2Fsearchresults.aspx%3FdomainToCheck%3D'+nameidea+'.com%26checkAvail%3D1'; 


         jQuery(element).append('<div id="results"><div class="innerResult clearfix"><ul class="colm1"></ul><div class="row"><div class="col-md-3"></div><div class="col-md-6"><div class="domain-advertise"><a href="'+aff_link+'" class="tracking_event outbound-link" data-category="ResultsPage" data-action="DomainAvailabilityCheck" data-label="CheckAltDom-SavedIdeas-RegisterOverlay" target="_blank"><?php echo __('Check for alternative domains', 'thegem-child'); ?></a></div><div class="domain-disclosure"><?php echo __('We may earn a commission (at no extra cost to you) if you make a purchase through links on our site. See our', 'thegem-child'); ?><a href="/disclosure/" target="_blank"><?php echo addslashes($aff_disc); ?></a></div></div><div class="col-md-3"></div></div></div></div>');  
    }



    
    // For Save ideas section
    function savedideas_list(key,value,dt,category='ResultsPage',action='DomainAvailabilityCheck',label='Register-SavedIdeas-RegisterOverlay'){
      
         <?php
          $translations = json_decode(get_option('bnwm_string_translations'));  
          $register =   $translations->domaincheckerregister !== '' ? str_replace("\\",'',$translations->domaincheckerregister) : "Register";
         ?>
       
         var nameidea = dt;
         nameidea = nameidea.replace(/\s/g, '');
         nameidea = nameidea.toLowerCase();
 
         var aff_link =  WixAffiliateLinksJS(nameidea)['saveidregisterlink'];

       
        jQuery('.savedideas-drop-data ul').append("<li id='" + value + "' data-kname='" + dt + "'><div class='list-data'><a href='"+aff_link+"' target='_blank' class='outbound-link tracking_event register-time-to-click' data-category='"+category+"' data-action='"+action+"' data-label='"+label+"' data-search="+nameidea+"><div class='text'><input type='checkbox' class='ids' name='list[]' hidden='' value='" + value + "' checked='checked'><span class='sword outbound-link' data-category='"+category+"' data-action='"+action+"' data-label='"+label+"'>" + value + "</span><div class='register-btn'><?php echo addslashes($register); ?><i class='fas fa-external-link-alt'></i></div></div></a><div class='close-icon'><span class='" + value + " savespan elegant-icon icon_close' onClick='removeSavedResult(this)'></span></div></div></li>");
     }

     function filtersFixScroll() {
          if (jQuery(window).innerWidth() > 1024 && jQuery('#stickThis').length > 0) {
              var windScTp = jQuery(window).scrollTop(),
                  sstt = jQuery('.business-eng .row:eq(0)').offset().top,
                  stkboxhgt = jQuery('#stickThis').outerHeight(true);
                  scrHgt = sstt + jQuery('.business-eng .row:eq(0)').outerHeight() - (stkboxhgt),
                  scrollWidth = jQuery('#stickThis').outerWidth(true);
                  // console.log('position till fixed position ' + scrHgt);
                  // console.log('Window scrolled position ' + windScTp);
              if (windScTp > sstt) {
                  jQuery('#stickThis').removeAttr('style');
                  jQuery('#stickThis').css({
                      'position': 'fixed',
                      'width': scrollWidth,
                      'top': 0
                  });
                  if (windScTp > scrHgt) {
                  // console.log('%cposition Changed to absolute', 'font-style: italic;color: green;');
                      jQuery('#stickThis').removeAttr('style');
                      jQuery('#stickThis').css({
                          'position': 'absolute',
                          'width': scrollWidth,
                          'bottom': 0
                      });
                  }
              } else {
                  jQuery('#stickThis').removeAttr('style');
              }
          }
      }
      
      jQuery(document).ready(function() {
           
          jQuery.xhrPool = []; // array of uncompleted requests
            jQuery.xhrPool.abortAll = function() { // our abort function
              jQuery(this).each(function(idx, jqXHR) {
                jqXHR.abort();
              });
              jQuery.xhrPool.length = 0
            };
              
          var loading;
          var results;
          var display;
          var action;

          <?php if (ICL_LANGUAGE_CODE == 'fr') : ?>
            jQuery("#domain-checker-form").addClass("fr-domain-check");
          <?php endif; ?>
          
          jQuery("div[id='domain-checker-form']").on("submit", function(e) {
          e.preventDefault();
          
          jQuery.xhrPool.abortAll();
          var form = this;
          var domain = jQuery("input[name='domain']", form).val();
          var item_id = jQuery("input[name='item_id']", form).val();
          var tld = jQuery("input[name='tld']", form).val();
          var z = jQuery(".g-recaptcha-response", form).attr('id');

          if (z !== undefined) {
            z = z.match(/\d+/)

            if (z === null) {
              z = 0;
            } else {
              z = z[0];
            }
          }


          if (jQuery("input[name='domain']", form).val() === "") {
            jQuery("div[id='results']", form).html(unescape('<div class="alert alert-warning"><a href="#" class="close" data-bs-dismiss="alert" aria-label="close">×</a><p>' + domain_checker_script.req_domain_text + '</p></div>'));
            domain_checker_alert();
            return false;
          }

          dc_ajax_check_domain(form, domain, item_id, tld);
          return false;
        });
        return false;



          
        function dc_ajax_check_domain(form, domain, item_id, tld) {
            

            jQuery.xhrPool.abortAll();
            jQuery("div[id='results']", form).css('display', 'none').html('');
            jQuery("div[id='loading']", form).css('display', 'inline');
            jQuery("#Submit", form).prop("disabled", true);
            var page_id = "<?= !empty($_GET['home_id']) ? $_GET['home_id'] : '' ?>";
            
            var action   = 'dc_display_domain'; 
            var provider = '';
            var location = '';
            var status   = '';
            
            
            if(jQuery('#domain-checker-form').hasClass('au') ){
               action = 'dc_display_domain_with_diff_tlds';
            }

            if(jQuery('#domain-checker-form').hasClass('businessname') ){
               provider = 'businessname';
            }
            
            if(jQuery('#domain-checker-form').hasClass('new-result-page') ){

              <?php  if( is_page_template('template-nameideas.php') ) :  ?>
               status = 'WHOIS';
              <?php  endif;  ?>

              action = 'dc_display_dropdown_domain';
              provider = 'nameideas';
              location = "domain_checker";
            }
            
            // var domain = domain.split(".")[0];
            <?php $page_slug = CURRENT_MULTISITE_CODE == '' ? explode('/',$_SERVER['REDIRECT_URL'])[1] : explode('/',$_SERVER['REDIRECT_URL'])[2]; ?> 
            var data = {
              'action': action,
              'domain': domain,
              'item_id': item_id,
              'provider': provider,
              'tld': tld,
              'page_id' : page_id,
              'status'   : status,
              'device' : Device_Detect(),
              'glclid_url' : "<?php echo getGclidParam(); ?>",
              'page_slug' : "<?php echo $page_slug  ?>",
              'location' : location,
              'lang': '<?= ICL_LANGUAGE_CODE ?>',
              'security': domain_checker_ajax.domain_checker_nonce
            };

            jQuery.ajaxSetup({
              beforeSend: function(jqXHR) { // before jQuery send the request we will push it to our array
                jQuery.xhrPool.push(jqXHR);
              },
              complete: function(jqXHR) { // when some of the requests completed it will splice from the array
                var index = jQuery.xhrPool.indexOf(jqXHR);
                if (index > -1) {
                  jQuery.xhrPool.splice(index, 1);
                }
              }
            });

            jQuery.ajax({
              type: 'post',
              url: domain_checker_ajax.ajaxurl,
              data: data,
              success: function(response) {
                jQuery("#Submit", form).removeAttr('disabled');
                response = jQuery.parseJSON(response);

                jQuery("div[id='loading']", form).css('display', 'inline');
                jQuery("div[id='results']", form).css('display', 'block');
                jQuery("div[id='results']", form).css('overflow-x','hidden');
                var d = 100;

                jQuery.each(response, function(i, obj) {

                  setTimeout(function() {

                    jQuery("div[id='results']", form).append(unescape(response[i]));

                  }, d);

                  d += 500;
                });
                jQuery("div[id='loading']", form).css('display', 'none');
              }
            });

        }
  
          
        function domain_checker_alert(){
            
            setTimeout(function() {
              jQuery('.glyphicon-remove').click(function(e) {
                e.preventDefault()
                jQuery('.alert-warning').fadeOut();
              });
            }, 500);

            setTimeout(function() {
              jQuery('.alert-warning').fadeOut();
            }, 5000);

        }
});
      
</script>


<script type="text/javascript">
  
  
    jQuery(".generatorbtn").not('.name-search.word-generator .generatorbtn').click(function(e) {
      e.preventDefault();
       
        submitGenerator( jQuery(this) , e );
    });

    jQuery(".inputMain").not('.name-search.word-generator .generatorbtn').on('keypress', function (e) {
       
        if( e.which == 13 ){
          submitGenerator( jQuery(this) , e );
        }
    }); 


    function submitGenerator( ele , e ){

      var input1 = jQuery(ele).parents('form').find(".inputMain").val();
      var value = jQuery.trim(input1.replace(/[^a-zA-Z ]/g, ""));
      count = value.split(' ').length;
       
     
      <?php  $translations = json_decode(get_option('bnwm_string_translations'));  ?>

      if (value == '') {
        e.preventDefault();
        jQuery('.business-eng .alert-box').find('.alert-danger').remove();
        jQuery('.business-eng .alert-box').prepend('<div class="alert alert-danger fade in">' +
          '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
          "<span><?= $translations->generatorerror1; ?></span></div>");
        closeAlert();
      } else if (input1.length < 3) {
        e.preventDefault();
        jQuery('.business-eng .alert-box').find('.alert-danger').remove();
        jQuery('.business-eng .alert-box').prepend('<div class="alert alert-danger fade in">' +
          '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
          "<span><?= $translations->generatorerror2; ?></span></div>");
        closeAlert();
      } else if (count > 6) {
        e.preventDefault();
        jQuery('.business-eng .alert-box').find('.alert-danger').remove();
        jQuery('.business-eng .alert-box').prepend('<div class="alert alert-danger fade in">' +
          '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
          "<span><?= $translations->generatorerror3; ?></span></div>");
        closeAlert();
      } else {
        if (jQuery(ele).parents('form').find(".chekedomainid").is(':checked')) {
          popupDomain(ele);
        }
        jQuery(ele).parents('form').find(".inputMain").val(value);
        jQuery(ele).parents('form').submit();
      }
    }

    function closeAlert() {
      setTimeout(function() {
        jQuery('.alert .close').click(function(e) {
          e.preventDefault()
          jQuery('.alert').fadeOut();
        });
      }, 500);

      setTimeout(function() {
        jQuery('.alert').fadeOut();
      }, 5000);
    }
    
    jQuery('.indus-wrap').each(function(){
      jQuery(this).find('.all-indus-check:eq(1)').empty();
      jQuery(this).find('.all-indus-check:eq(2)').empty();
    });

                      
    jQuery(document).on('click','.all-indus-check',function(){  
      if( jQuery(this).find('input').is(':checked') ){
          jQuery(this).find('input').prop('checked',false);
      }else{
          jQuery('.live-search-list li input').prop('checked',true);
      }
    }); 

    jQuery('.all-indus-check').on('click', function(){
      var dis = jQuery(this);
      setTimeout(function(){
        if( jQuery('.all-indus-check input:checked').length ){  
          dis.parents('.indus-wrap').find('li input').prop('checked', true);
        } 
        jQuery('.industry-text .idea-count').text(dis.parents('.indus-wrap').find('li:not(.all-indus-check) input:checked').length);
      }, 500);
    });

    jQuery('.indus-wrap').find('li:not(.all-indus-check)').on('click', function(){
      var dis = jQuery(this);
      setTimeout(function(){
        if( dis.parents('.indus-wrap').find('li:not(.all-indus-check) input:checked').length == dis.parents('.indus-wrap').find('li:not(.all-indus-check) input').length ){ 
          dis.parents('.indus-wrap').find('li input').prop('checked', true);
        } else {
          jQuery('.all-indus-check input').prop('checked', false);
          jQuery("input[type='hidden'][name='all-indus']").remove();
        }
        jQuery('.industry-text .idea-count').text(dis.parents('.indus-wrap').find('li:not(.all-indus-check) input:checked').length);
      }, 500);
    });

    jQuery(document).on('click','.dropdown-toggle.nav-link', function(){

      if( jQuery(this).hasClass('show') ){
        jQuery(this).removeClass('show');
        jQuery(this).next('.dropdown-menu').removeClass('show');
       }else{
        jQuery(this).addClass('show');
        jQuery(this).next('.dropdown-menu').addClass('show');
      }
    });
    jQuery('.eng-industry-filter, .industry').on('click', function(){
      <?php 
          $checked;
          if( !empty($_GET['all-indus'])  && $_GET['all-indus']  == 'all'  ){
              $checked = 'true';
          } 
      ?>
      var checked  = "<?= $checked ?>";
      if( checked  == 'true' ){ jQuery("input[name='all-indus']").prop('checked', true);  }
    });  
  
    jQuery(window).ready(function()
    {
      jQuery.ajax({
              type: "GET",
              url : '<?php echo admin_url('admin-ajax.php'); ?>',
              data: {action: 'ajax_saved_idea'},
              cache: true,
              success: function (response)
              {
                  var cookie_arr = [];
                  if(response != null){
                    jQuery.each( jQuery.parseJSON(response), function( cookie_name, cookie_id ) {
                        cookie_arr.push(cookie_name);          
                    });  
                    var json_str = JSON.stringify(cookie_arr);
                    jQuery.removeCookie('savedideas');
                    jQuery.cookie('savedideas', json_str, {
                      expires: 15
                    });
                  }
              }
          });
          

          
          <?php if( getNameideaAlgo() ) :  ?>

            let data = JSON.parse('<?= json_encode($_GET); ?>')
            data.action = 'get_total_record'
            
            jQuery.ajax({
                type: "POST",
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: data,
                cache: false,
                success: function (response)
                { 
                  var res = JSON.parse(response);
                  var total = res.total;
                  var total_format = res.total_format;
                  if( jQuery('.title-found').length  ){
                    var res_text = jQuery('.title-found').data('text');
                    jQuery('.title-found span').text(total_format+' '+res_text);
                    jQuery('.title-found').show();
                  }
                  if( jQuery('.check-dom-txt').length  ){
                    jQuery('.check-dom-txt span').text(total_format);
                    jQuery('.check-dom-txt').show();
                  }
                  jQuery.ajax({
                    type: "POST",
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: { 
                      action: 'get_pagination',
                      limit: '<?= $limit; ?>',
                      device : Device_Detect(), 
                      total_record: total,
                      url: '<?= get_permalink(); ?>',
                      query_string: '<?= $_SERVER['QUERY_STRING']; ?>',
                      paged: '<?= count(explode('/',$_SERVER['REDIRECT_URL'])); ?>',
                    },
                    cache: false,
                    success: function (response){ 
                      jQuery('.Pagination_nav').html(response);
                    }
                  });
                }
            });

          <?php endif; ?>

          
      });
 

</script> 