<!-- Filter html starts -->
<?php $translations = json_decode(get_option('bnwm_string_translations'));  ?>
<?php if( isset($_SESSION['invite_modal_value']) ):
    $savedideas = $_SESSION['invite_modal_value'];
endif;
?>             
<div class="col-md-12">
<div class="filter-wrap">

<div class="busi-filter search-filter domain-result <?php if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en') { echo "int-lg"; } ?>">

<?php $home_id =  !empty($_GET['home_id']) ? $_GET['home_id'] : ''; 
$bname   =  !empty($_GET['bname'])   ? $_GET['bname'] : '';  
?>

<div class="mar_set">
<div class="row">
<div class="col-md-8">
    <div class="row">
    <div class="col-md-12">
        <div class="gen-search">
        <div class="name-search">
        <?php $strURL = $_SERVER['REQUEST_URI'];
          $arrVals = explode("?", $strURL);
        ?>
            <form class="generator-form" method="GET" action="<?php echo $arrVals[0]; ?>">
            
            <input type="hidden" value="<?= $home_id; ?>" name="home_id">
            
            <input class="inputMain" name="bname" type="text" value="<?= $bname; ?>" placeholder="<?= isset($translations->nameideasgplaceholdertext) ? $translations->nameideasgplaceholdertext : 'Enter words and click generate...';   ?>">  
            
            <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
              <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
            <?php endif; ?>


            <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                <input type="hidden" name="stickyads" value="true">
            <?php endif;  ?>  
            
            <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
            <?php endif;  ?>  

            <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])  ) : ?> 
                <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
            <?php endif;  ?>   

            <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
            <?php endif; ?> 
            
            <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    $value = $columns_count == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
            <?php endif; ?>
            
            <input type="hidden" name="fltr" value="rsult">

            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

            <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && !empty($_GET['one_word']) ) ? trim($_GET['one_word']) : 'on'; ?>">

            <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && !empty($_GET['two_word']) ) ? trim($_GET['two_word']) : 'on'; ?>">

            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

            <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

            <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
                <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
            <?php endif; ?> 

                <?php 
                
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                if( is_array($grammar) || is_object($grammar)):
                foreach ($grammar as $value) : 
                ?>
                <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach; endif; ?>

                <?php

                $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                if (is_array($industry) || is_object($industry)) :
                foreach ($industry as $value) : ?>
                    <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach;
                endif; ?>   

                <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
                <?php endif; ?> 

        <a href="javascript:void(0)" class="generatorbtn"><?= isset($translations->nameideasgeneratetext) ? $translations->nameideasgeneratetext : 'Generate';   ?></a>
        </form>
        </div>               
    </div>
    </div>
    <?php if( !empty($filter_options->enable_industry_filters) && isset($filter_options->enable_industry_filters->value) && $filter_options->enable_industry_filters->value == 'true' ): ?>
        <div class="col-md-4">
<div class="elm indus-wrap">
<?php $columns = [];
if(!empty($selectedIndustries)){
    $columns = $selectedIndustries;
} else {
    $columns = $_GET['industry'];
}
$inscolumns = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : array();
?>
<div class="cstm-dropdown eng-industry-filter no-margin-filter">

<button type="button" class="btn btn-default hidden-btn">

    <div class="industry-text">
    
    <span><i class="fas fa-filter"></i></span>
    <p><?php echo __($filter_options->enable_industry_filters->industry_title, 'business_name_word_manager'); ?></p>
        
        <?php if( (is_array($columns) && !empty($columns) ) || (is_array($inscolumns) && !empty($inscolumns))  ) : ?>
        
        <?php $indus_count = !empty($columns) ? $columns : $inscolumns; ?>
        
        <span class="idea-count"><?= count($indus_count); ?></span>
    
        <?php endif; ?>
    </div>
    
    <span class="arrow_carrot-down elegant-icon"></span>
</button>

</div>
<div class="drop-down-data eng-ind-filter hide-me">
<?php $strURL = $_SERVER['REQUEST_URI'];
    $arrVals = explode("?", $strURL);
?>
<form action="<?php echo $arrVals[0]; ?>" method="GET">

    <?php add_more_filter_inputs();  ?> 
    <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">

    <input type="hidden" name="fltr" value="rsult">

    <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

    <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && !empty($_GET['one_word']) ) ? trim($_GET['one_word']) : 'on'; ?>">

    <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && !empty($_GET['two_word']) ) ? trim($_GET['two_word']) : 'on'; ?>">

    <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

    <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

    <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
    <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
    <?php endif; ?> 
    
    <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
    <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
    <?php endif; ?> 

    <?php 
        
        $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
        if( is_array($grammar) || is_object($grammar)):
        foreach ($grammar as $value) : 
    ?>
        <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
    <?php endforeach; endif; ?>
    

    <div class="row">
        <div class="col-md-12 search_bar">
        <input type="text" name="" class="indus-srch-fltr"  placeholder="<?php echo __($filter_options->enable_industry_filters->industry_search_placeholder, 'business_name_word_manager'); ?>">
        </div>
    </div>

    <div class="row cstm_label" style="overflow-x:hidden;">

        <?php
        $inds_count = count($columsname);

        $rows = ceil(count($columsname) / 3);
        $lists  = array_chunk($columsname, $rows);
        if( is_array($lists) || is_object($lists)):
        $index = 0;
        foreach ($lists as $column) :
            echo '<div class="col-md-12"><ul class="live-search-list" id="live-search-list"><li class="all-indus-check"><label>'.$translations->all.'<input type="checkbox" name="all-indus" value="all"><span class="checkmark"></span></label></li>';
            foreach ($column as $name) :
        ?>
            <li data-search-term="<?= $name->name; ?>"> 
            <?php 
                $checked  = '';
                $disabled = '';
                    
                if (is_array($columns) && !empty($columns) && (in_array($name->id, $columns) || in_array($name->name, $columns))  ) :
                    $checked = 'checked="checked"';
                endif;

                if (is_array($inscolumns) && !empty($inscolumns) && in_array($name->id, $inscolumns)) :
                    $checked = 'checked="checked"';
                endif;

                ?>

                <label><?php echo $name->name; ?>
                    <input name="industry[]" data-index="<?php echo $index; ?>" type="checkbox" value="<?php echo $name->name; ?>" <?= $disabled.''.$checked; ?>>
                    <span class="checkmark"></span>
                </label>
            </li>
            <?php $index++; endforeach; ?>
            <?php echo '</ul></div>'; ?>
        <?php endforeach; ?>
        <?php endif;
        ?>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="industry-sub">
            <div class="industry-sub-left">
            <button type="button" class="btn clear"><?php echo __($filter_options->enable_industry_filters->industry_clear_button_text, 'business_name_word_manager'); ?></button>
            </div>
            <div class="industry-sub-right">
            <button type="submit" data-category="ResultsPage" data-action="IndustryFilters" data-label="" class="apply-btn btn indus-filter"><?php echo __($filter_options->enable_industry_filters->industry_apply_button_text, 'business_name_word_manager'); ?></button>
            </div>
        </div>
        </div>
    </div>
</form>
</div>
</div>
</div> 
<?php endif; ?>
<?php if( !empty($filter_options->enable_names_filter) && isset($filter_options->enable_names_filter->value) && $filter_options->enable_names_filter->value == 'true' ): ?>
<div class="col-md-4">
<div class="visible-md visible-lg">
<div class="elm">
<!--<h3>Result Filters</h3> -->
<div class="cstm-dropdown eng-other-filter no-margin-filter">
    <button type="button" class="btn btn-default hidden-btn"> 

    <div class=" select-text">
    <span><i class="fas fa-sliders-h"></i></span>
    <p><?php echo __($filter_options->enable_names_filter->names_title, 'business_name_word_manager'); ?></p>
    </div>
    <span class="arrow_carrot-down elegant-icon"></span>
    </button>
</div>
<div class="drop-down-data domain-filters hide-me">
    <?php $strURL = $_SERVER['REQUEST_URI'];
        $arrVals = explode("?", $strURL);

        if (defined('ICL_LANGUAGE_CODE')) {
            $lang = ICL_LANGUAGE_CODE;
        } 
    ?>
    <form action="<?php echo $arrVals[0]; ?>" method="GET">
    <div class="row">
        <div class="col-md-12">
        <div class="slidecontainer">
            <?php add_more_filter_inputs();  ?>
            <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">
            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? trim($_GET['position']) : 'before'; ?>">
            <?php

            $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
            if (is_array($industry) || is_object($industry)) :
            foreach ($industry as $value) : ?>
                <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
            <?php endforeach;
            endif; ?>
            
            
            <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
            <?php endif; ?> 
        
            <?php if( !empty($filter_options->enable_names_filter->enable_character_count) && isset($filter_options->enable_names_filter->enable_character_count->value) && $filter_options->enable_names_filter->enable_character_count->value == 'true' ): ?>
            <!-- <input type="hidden" name="fltr" value="rsult"> -->
            <div class="flex-row">
                <h3><?php echo __($filter_options->enable_names_filter->enable_character_count->character_count_title, 'business_name_word_manager'); ?></h3>
            </div>
            <input class="sliderRange deskslide" name="character" type="text" min="2" max="15" value="<?php echo (isset($_GET['character']) && !empty($_GET['character'])) ? trim($_GET['character']) : '15'; ?>" name="points" step="1">
            <?php endif; ?> 
        
        </div>
        </div>

    
        <div class="col-md-12 cstm_label">
        <ul class="live-search-list word-type <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : echo 'rhyming-on'; endif; ?>">
            
        <?php if( !empty($filter_options->enable_names_filter->enable_words_filter) && isset($filter_options->enable_names_filter->enable_words_filter->value) && $filter_options->enable_names_filter->enable_words_filter->value == 'true' ): ?>  
            
            <h3><?php echo __($filter_options->enable_names_filter->enable_words_filter->words_title, 'business_name_word_manager'); ?></h3>
            <li>
                <input class="one-word" name="one_word" type="hidden" value="<?php echo (isset($_GET['one_word']) && $_GET['one_word'] != 'on') ? trim($_GET['one_word']) : 'on'; ?>">

                <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->one_word_title, 'business_name_word_manager'); ?> <input data-index="" class="oneword" value="on" <?php if (!isset($_GET['one_word']) || (isset($_GET['one_word']) && $_GET['one_word'] == 'on')) : ?> checked="checked" <?php endif; ?> <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?> type="checkbox">
                <span class="checkmark"></span>
                </label>
            </li>
            <li data-search-term="<?php echo $_GET['bname']; ?>">

                <input class="two-word" name="two_word" type="hidden" value="<?php echo (isset($_GET['two_word']) && $_GET['two_word'] != 'on') ? trim($_GET['two_word']) : 'on'; ?>">

                <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->two_word_title, 'business_name_word_manager'); ?> <input data-index="" class="twoword" <?php if (!isset($_GET['two_word']) || (isset($_GET['two_word']) && $_GET['two_word'] == 'on' || (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on'))) : ?> checked="checked" <?php endif; ?> <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?> value="on" type="checkbox">
                <span class="checkmark"></span>
                </label>
            </li>  

            <?php endif; ?>
            
            <?php if( !empty($filter_options->enable_grammar_filters) && isset($filter_options->enable_grammar_filters->value) && $filter_options->enable_grammar_filters->value == 'true' ):
            
            $checked = '';
            
            ?>
            <h3><?php echo __($filter_options->enable_names_filter->enable_words_filter->grammar_title, 'business_name_word_manager'); ?></h3>
                <?php if( $filter_options->enable_grammar_filters->masculine_singular_title !== '' ) :  ?>
                <li>
                <label><?php echo __($filter_options->enable_grammar_filters->masculine_singular_title, 'business_name_word_manager'); ?> 
                    <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                    {
                        if (in_array(('masculine'), $_GET['grammar']))
                        {
                        $checked = 'checked="checked"';
                        }
                    }
                    ?>
                    <input name="grammar[]" class="grammar1" type="checkbox" value="masculine" <?= $checked; ?>>
                    <span class="checkmark"></span>
                </label>
                </li>
                <?php endif; ?>
                
                <?php if( $filter_options->enable_grammar_filters->feminine_singular_title !== '' ) :  ?>
                <li>
                <label><?php echo __($filter_options->enable_grammar_filters->feminine_singular_title, 'business_name_word_manager'); ?>
                <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                    {
                    if (in_array(('feminine'), $_GET['grammar']))
                    {
                        $checked = 'checked="checked"';
                    }
                    }
                    ?>
                    <input name="grammar[]" class="grammar2" type="checkbox" value="feminine" <?= $checked; ?>>
                    <span class="checkmark"></span>
                </label>
                </li>
                <?php endif; ?>

                <?php if( $filter_options->enable_grammar_filters->masculine_plural_title !== '' ) :  ?>
                <li>
                <label><?php echo __($filter_options->enable_grammar_filters->masculine_plural_title, 'business_name_word_manager'); ?>
                    <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                    {
                    if (in_array(('masculine plural'), $_GET['grammar']))
                    {
                        $checked = 'checked="checked"';
                    }
                    }
                    ?>
                    <input name="grammar[]" class="grammar3" type="checkbox" value="masculine plural" <?= $checked; ?>>
                    <span class="checkmark"></span>
                </label>
                </li>
                <?php endif; ?>

                <?php if( $filter_options->enable_grammar_filters->feminine_plural_title !== '' ) :  ?>
                <li>
                <label><?php echo __($filter_options->enable_grammar_filters->feminine_plural_title, 'business_name_word_manager'); ?>
                    <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                    {
                    if (in_array(('feminine plural'), $_GET['grammar']))
                    {
                        $checked = 'checked="checked"';
                    }
                    }
                    ?>
                    <input name="grammar[]" class="grammar4" type="checkbox" value="feminine plural" <?= $checked; ?>>
                    <span class="checkmark"></span>
                </label>
                </li>
                <?php endif; ?>
                
                <?php if( $filter_options->enable_grammar_filters->plural_title !== '' ) :  ?>
                <li>
                <label><?php echo __($filter_options->enable_grammar_filters->plural_title, 'business_name_word_manager'); ?>
                    <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                    {
                    if (in_array(('plural'), $_GET['grammar']))
                    {
                        $checked = 'checked="checked"';
                    }
                    }
                    ?>
                    <input name="grammar[]" class="grammar0" type="checkbox" value="plural" <?= $checked; ?>>
                    <span class="checkmark"></span>
                </label>
                </li>
                <?php endif; ?>
            <?php  endif; ?>

        </ul>
        </div>
        <?php if( !empty($filter_options->enable_names_filter->enable_keyword_filter) && isset($filter_options->enable_names_filter->enable_keyword_filter->value) && $filter_options->enable_names_filter->enable_keyword_filter->value == 'true' ): ?>  
        <div class="col-md-12 cstm_label">
        <ul class="keywords">
            <h3><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->keyword_title, 'business_name_word_manager'); ?></h3>
            <li>
                <input class="radio-btn" type="radio" name="position" id="inlineRadio" value="before" <?php echo (isset($_GET['position']) && $_GET['position'] == 'before') ? 'checked' : ''; ?>>
                <label class="form-check-label" for="inlineRadio"><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->before_title, 'business_name_word_manager'); ?></label>
            </li>
            <li>
                <input class="radio-btn" type="radio" name="position" id="inlineRadio2" value="after" <?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? 'checked' : ''; ?>>
                <label class="form-check-label" for="inlineRadio2"><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->after_title, 'business_name_word_manager'); ?></label>
            </li>
        </ul>
        </div>

        <?php endif; ?>

        <?php if( !empty($filter_options->enable_names_filter->enable_rhyming_filter) && isset($filter_options->enable_names_filter->enable_rhyming_filter->value) && $filter_options->enable_names_filter->enable_rhyming_filter->value == 'true' ): ?> 
        
        <div class="col-md-12 col-sm-12">
        <div class="filter desk clearfix">
    
            <div class="switch-toggle rhyming">
            <div class="inner">
                <span><?php echo __($filter_options->enable_names_filter->enable_rhyming_filter->rhyming_title, 'business_name_word_manager'); ?></span>
                <label class="switch deskslide">
                <input class="rhyming-val" name="rhyming" type="hidden" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">
                    <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">
                <input type="checkbox" class="can_rhyming" <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> checked="checked" <?php endif; ?> value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] != 'on') ? trim($_GET['rhyming']) : 'on'; ?>">
                <span class="slider round"></span>
                </label>
                <div class="condition">
                <p><?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? __('On', 'business_name_word_manager') : __('Off', 'business_name_word_manager'); ?>
                </p>
                </div>
            </div>
            </div>
        </div>
        </div>

        <?php endif; ?> 

    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="industry-sub">
            <div class="industry-sub-left">
            <button type="button" class="btn cancel cancel_btn"><?php echo __($filter_options->enable_names_filter->names_cancel_button, 'business_name_word_manager'); ?></button>
            </div>
            <div class="industry-sub-right">
            <button type="submit" data-category="ResultsPage" data-action="NamesFilters" data-label="" class="apply-btn btn btn-flitr click-event"><?php echo __($filter_options->enable_names_filter->names_apply_button, 'business_name_word_manager'); ?></button>
            </div>
        </div>
        </div>
    </div>
    </form>
</div>
</div>

</div>
</div> 
<?php endif; ?>
<div class="col-md-4">
<div class="drop-down-area">
<?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL);?>
<form id="saved_id" method="POST" action="<?php echo $arrVals[0]; ?>">
<?php if( !empty($filter_options->enable_saved_ideas) && isset($filter_options->enable_saved_ideas->value) && $filter_options->enable_saved_ideas->value == 'true' ): ?>
<div class="dropdown-main update">
<div class="cstm-dropdown2">
    
    <button type="button" class="btn btn-default">
    <div class="text">
        <span class="icon_star_alt elegant-icon"></span> 
        <p><?php echo __($filter_options->enable_saved_ideas->saved_ideas_title, 'business_name_word_manager'); ?></p>
    </div>
    
    <div class="count-down">
        <span class="idea-count" style="display:none"></span>
        <span class="arrow_carrot-down elegant-icon"></span>
    </div>
    </button>
</div>
<div class="savedideas-drop-data">
    <div class="row">
        <div class="col-md-12">
        <ul></ul>
        </div>
        <div class="col-md-12">
        <div class="save-ideas">
                <p><?php echo __($filter_options->enable_saved_ideas->saved_ideas_subtitle, 'business_name_word_manager'); ?></p>
                
                <div class="responsebar">
                </div>
        
                <div class="input-main">
                <input class="email-input" id="email" name="email" type="Email" data-popup-url="" data-active="" placeholder="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_placeholder, 'business_name_word_manager'); ?>">
                <a href="javascript:void(0)" class="send sharemail"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?></a>
                <input type="submit" class="send sharemail" value="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?>" style="display: none;">
                </div>
                <div class="form-group">
                <input class="chekedomainid" name="marketing_email" type="checkbox" id="html">
                
                <label for="html"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_optin_notice, 'business_name_word_manager'); ?><a href="<?php echo $filter_options->enable_saved_ideas->saved_ideas_policy_url; ?>" rel="nofollow" target="_blank"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_policy_text, 'business_name_word_manager'); ?></a></label>
                </div>
        </div>
        </div>
    </div>
</div>
</div>
<?php endif; ?>
</form>
</div>
</div> 

    </div>
</div> 
<?php if( !empty($filter_options->enable_domains_availability_widget) && isset($filter_options->enable_domains_availability_widget->value) && $filter_options->enable_domains_availability_widget->value == 'true' ): ?>
    <div class="col-md-4">
        <div class="domain-checker">
            <div  class="domains-head">
            <span><i class="fas fa-search"></i></span> 
            <p><?php echo __($filter_options->enable_domains_availability_widget->domains_title, 'business_name_word_manager'); ?></p>
            </div>
            <div class="domain-widget">
            
            <?php
            $button_text = __($filter_options->enable_domains_availability_widget->domains_button_text, 'business_name_word_manager');
            $placeholder = __($filter_options->enable_domains_availability_widget->domains_name_placeholder, 'business_name_word_manager');
            echo do_shortcode('[wpdomainchecker button="Check" placeholdermessage="'.$placeholder.'" button_text="'.$button_text.'"]'); ?>
            </div> 
        </div>
    </div> 
    <?php endif; ?>
</div>
</div>
</div>
<div class="col-md-12">
<div class="row align-set">
<div class="col-md-12">
    <div class="foot-gen-search">
    <div class="gen-search">
        <div class="name-search testing-aa">
            <form  action="/results/" method="GET">
            
            <input type="hidden" value="<?= $home_id; ?>" name="home_id">
            
            <input class="inputMain" name="bname" type="text" value="<?= $bname; ?>" placeholder="<?= isset($translations->nameideasgplaceholdertext) ? $translations->nameideasgplaceholdertext : 'Enter words and click generate...';   ?>">

            <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
              <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
            <?php endif; ?>

            <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                <input type="hidden" name="stickyads" value="true">
            <?php endif;  ?>  
            
            <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
            <?php endif;  ?>  

            <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])  ) : ?> 
                <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
            <?php endif;  ?>   

            <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
            <?php endif; ?> 
            
            <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    $value = $columns_count == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
                <?php endif; ?>
            
            <input type="hidden" name="fltr" value="rsult">

            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

            <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && !empty($_GET['one_word']) ) ? trim($_GET['one_word']) : 'on'; ?>">

            <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && !empty($_GET['two_word']) ) ? trim($_GET['two_word']) : 'on'; ?>">

            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

            <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

            <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
                <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
            <?php endif; ?> 

                <?php 
                
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                if( is_array($grammar) || is_object($grammar)):
                foreach ($grammar as $value) : 
                ?>
                <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach; endif; ?>

                <?php

                $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                if (is_array($industry) || is_object($industry)) :
                foreach ($industry as $value) : ?>
                    <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach;
                endif; ?>  
                
                <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
                <?php endif; ?> 

        <a href="javascript:void(0)" class="generatorbtn"><?= isset($translations->nameideasgeneratetext) ? $translations->nameideasgeneratetext : 'Generate';   ?></a>
        </form>
        </div>               
    </div>
    </div>
</div>
    <div class="col-xs-4">
    <?php if( !empty($filter_options->enable_industry_filters) && isset($filter_options->enable_industry_filters->value) && $filter_options->enable_industry_filters->value == 'true' ): ?>
    <div class="fiter-drop">
        <h3><?php echo __($filter_options->enable_industry_filters->industry_title, 'business_name_word_manager'); ?>
        </h3>
        <button type="button industry-text" class="btn btn-default industry apply-fltr" data-bs-toggle="modal" data-bs-target="#industryModal">
        <span><i class="fas fa-filter"></i></span>
        
        <?php if( (is_array($columns) && !empty($columns) ) || (is_array($inscolumns) && !empty($inscolumns))  ) : ?>
            <?php $indus_count = !empty($columns) ? $columns : $inscolumns; ?>
        <span class="idea-count"><?= count($indus_count); ?></span>
        <?php endif; ?>

        </button>
    </div>
    <?php endif; ?>
    </div>
    <?php if( !empty($filter_options->enable_names_filter) && isset($filter_options->enable_names_filter->value) && $filter_options->enable_names_filter->value == 'true' ): ?>
    <div class="col-xs-4">
    <div class="fiter-drop">
        <h3><?php echo __($filter_options->enable_names_filter->names_title, 'business_name_word_manager'); ?></h3>
        <button type="button" class="btn btn-default apply-fltr" data-bs-toggle="modal" data-bs-target="#main-filter-modal">
        <span><i class="fas fa-sliders-h"></i></span>
        </button>
    </div>
    </div>
    <?php endif; ?>

    
<?php if( !empty($filter_options->enable_saved_ideas) && isset($filter_options->enable_saved_ideas->value) && $filter_options->enable_saved_ideas->value == 'true' ): ?>
<div class="col-xs-4 drop-down-area">
    <div class="fiter-drop">
    <h3><?php echo __($filter_options->enable_saved_ideas->saved_ideas_title_mobile, 'business_name_word_manager'); ?></h3>
    <div class="dropdown-main">
    <div class="cstm-dropdown2">
        <button type="button" class="btn btn-default apply-fltr">
        <span class="icon_star_alt elegant-icon"></span> 
        <span class="idea-count" style="display:none"></span>
        </button>
    </div>
    <div class="savedideas-drop-data">
        <div class="row">
        <div class="col-md-12">
            <ul>
            </ul>
        </div>
        <div class="col-md-12">
            <div class="industry-sub-right <?php if ($savedideas) { ?>added-saved-ideas<?php } ?>">
            <button data-category="ResultsPage" data-action="SavedIdeas" data-label="ShareEmailClick" type="button" class="send-or-email btn click-event" data-bs-toggle="modal" data-bs-target="#myModalShare"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_share_email_text, 'business_name_word_manager'); ?></button>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
</div> 
<?php endif; ?>

</div>
</div>
</div>
    
<?php 
$savedideas = $_SESSION['invite_modal_value'];
?>

    
<div class="col-md-12 mar_0 hide-section">

<?php if (isset($_SESSION['messages']) && !empty($_SESSION['messages'])) : ?>
    <?php $alert = $_SESSION['messages'];
    if( is_array($alert) || is_object($alert)):
    foreach ($alert as $key => $value) :
    if (!empty($value['message'])) :
    ?>
        <?php if ($_SESSION['show'] !==  $value['message']) : ?>
        <div class="alert alert-<?php echo $value['type']; ?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p><?php echo $value['message']; ?></p>
        </div>
        <?php $_SESSION['show'] = $value['message'];
        endif; ?>
    <?php endif; ?>
    <?php endforeach; endif; ?>
<?php endif; ?>

</div>
</div> <!-- Filter html end -->

<!-- Mobile Modals Start -->

<!-- Industry Modal Start -->
<?php if( !empty($filter_options->enable_industry_filters) && isset($filter_options->enable_industry_filters->value) && $filter_options->enable_industry_filters->value == 'true' ): ?>
<div class="modal fade domain-filter" id="industryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL); ?>
        <?php if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en') :
        ?>
        <form id="filter-mobile-form" method="GET" action="<?php echo $arrVals[0]; ?>">
        <?php else : ?>
            <form id="filter-mobile-form" method="GET" action="<?php echo $arrVals[0]; ?>">
            <?php endif; ?>
            <div class="modal-header">
            <div class="industry-text">
                <span><i class="fas fa-filter"></i></span>
                <h4 class="modal-title"><?php echo __($filter_options->enable_industry_filters->industry_modal_title, 'business_name_word_manager'); ?>
                </h4>
            </div>
            </div>
            <div class="modal-body">
                            
                <div class="search_bar">
                    <input type="text" name="" class="indus-srch-fltr" placeholder="<?php echo __($filter_options->enable_industry_filters->industry_search_placeholder, 'business_name_word_manager'); ?>">
                </div>

                <div class="row">
                
                <?php if(isset($_GET['home_id']) && !empty($_GET['home_id'])) : ?>
                    <input type="hidden" name="home_id" value="<?=  trim($_GET['home_id']);  ?>">
                <?php endif; ?> 
                
                <?php if(isset($_GET['redirect']) && !empty($_GET['redirect'])) : ?>
                    <input type="hidden" name="redirect" value="<?=  trim($_GET['redirect']);  ?>">
                <?php endif; ?> 

                <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">

                <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
                  <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
                <?php endif; ?>

                <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                    <input type="hidden" name="stickyads" value="true">
                <?php endif;  ?> 

                <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])  ) : ?> 
                    <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
                <?php endif;  ?> 


                <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                    <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
                <?php endif;  ?>  
                
                <?php if(isset($_GET['sidebarads-type']) && !empty($_GET['sidebarads-type'])  ) : ?> 
                    <input type="hidden" name="sidebarads-type" value="<?= $_GET['sidebarads-type']; ?>">
                <?php endif;  ?>    


                <?php if(isset($_GET['copypopup-cateory']) && !empty($_GET['copypopup-cateory'])  ) : ?> 
                    <input type="hidden" name="copypopup-cateory" value="<?= $_GET['copypopup-cateory']; ?>">
                <?php endif;  ?> 

                <?php if(isset($_GET['mobile-results-iframe']) && !empty($_GET['mobile-results-iframe'])  ) : ?> 
                    <input type="hidden" name="mobile-results-iframe" value="<?= $_GET['mobile-results-iframe']; ?>">
                <?php endif;  ?> 

                <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                    <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
                <?php endif; ?> 

                <?php if(isset($_GET['all-tlds']) && !empty($_GET['all-tlds'])) : ?>
                    <input type="hidden" name="all-tlds" value="all">
                <?php endif; ?> 

                <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
                    <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
                <?php endif; ?> 
                    
                <input type="hidden" name="fltr" value="rsult">
                
                <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && !empty($_GET['position'])) ? trim($_GET['position']) : 'both'; ?>">

                <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    $value = $columns_count == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
                <?php endif; ?>
                
                <?php
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                if (is_array($grammar) || is_object($grammar)):

                foreach ($grammar as $value) : ?>
                    <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach; endif; ?>

                <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                    <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
                <?php endif; ?> 
                                    
                <div class="col-xs-12 mar_0">
                    <div class="domains-list cstm_label indus-wrap">
                        <ul class="live-search-list">
                        <li class="all-indus-check"><label><?= $translations->all ?><input type="checkbox" name="all-indus" value="all"><span class="checkmark"></span></label></li>
                        <?php $columns = [];
                        if(!empty($selectedIndustries)){
                            $columns = $selectedIndustries;
                        } else {
                            $columns = $_GET['industry'];
                        }
                        $inscolumns = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : array();
                        
                        $index = 0;
                        $col = 1;
                        if (is_array($columsname) || is_object($columsname)):
                            foreach ($columsname as $name) :
                       
                             $name->name = strtolower($name->name) == 'realestate' ? 'Real Estate' : $name->name; 
                        ?>

                        <li data-search-term="<?= $name->name; ?>">
                            <?php $checked = ''; ?>
                                <label><?php echo $name->name; ?>
                                <input name="industry[]" data-index="<?php echo $index; ?>" type="checkbox" value="<?php echo $name->name; ?>" <?php if ($name == 'general_list') : ?>disabled <?php endif; ?> <?php if (is_array($columns) && !empty($columns) && (in_array($name->id, $columns) || in_array($name->name, $columns) )  || $checked) : ?>checked="checked" 
                                   <?php endif; ?> 
                                   <?php if (is_array($inscolumns) && !empty($inscolumns) && in_array($name->id, $inscolumns) || in_array($name->name, $inscolumns)  ) : ?>checked="checked" <?php endif; ?>>
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <?php $index++;
                            $col++;
                            endforeach; ?>
                        <?php endif;
                        ?>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
            <div class="inner">
                
                <?php $events_cat = is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php']) ? 'ResultsPage' : 'username-resultspage';  ?>
                <button type="submit" class="btn btn-primary apply-btn indus-filter" data-category="<?= $events_cat; ?>" data-action="IndustryFilters" data-label="" ><?php echo __($filter_options->enable_industry_filters->industry_apply_button_text, 'business_name_word_manager'); ?></button>
                <button type="button" class="btn btn-default" data-bs-dismiss="modal"><?php echo __($filter_options->enable_industry_filters->industry_close_button_text, 'business_name_word_manager'); ?></button>
            </div>
            </div>
    </div>
    </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<?php endif; ?>

<!-- Industry Modal Ends -->
<?php if( !empty($filter_options->enable_names_filter) && isset($filter_options->enable_names_filter->value) && $filter_options->enable_names_filter->value == 'true' ): ?>
<!-- Names Filters Start -->

<div class="modal fade domain-filter name-filter" id="main-filter-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL);?>
        <?php  if (defined('ICL_LANGUAGE_CODE')) {
                $lang = ICL_LANGUAGE_CODE;
            }      
        ?>
        <form id="filter-mobile-form" method="GET" action="<?php echo $arrVals[0]; ?>">
            <div class="modal-header">
                <span><i class="fas fa-sliders-h"></i></span>
                <h4 class="modal-title"><?php echo __($filter_options->enable_names_filter->names_mobile_modal_title, 'business_name_word_manager'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                
                <?php if(isset($_GET['home_id']) && !empty($_GET['home_id'])) : ?>
                    <input type="hidden" name="home_id" value="<?=  trim($_GET['home_id']);  ?>">
                <?php endif; ?>
                
                <?php if(isset($_GET['redirect']) && !empty($_GET['redirect'])) : ?>
                    <input type="hidden" name="redirect" value="<?=  trim($_GET['redirect']);  ?>">
                <?php endif; ?> 

                <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">
                
                <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
                  <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
                <?php endif; ?>

                
                <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                    <input type="hidden" name="stickyads" value="true">
                <?php endif;  ?>  

                <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])) : ?> 
                    <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
                <?php endif;  ?>  
                

                <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                    <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
                <?php endif;  ?>  
                
                <?php if(isset($_GET['sidebarads-type']) && !empty($_GET['sidebarads-type'])  ) : ?> 
                    <input type="hidden" name="sidebarads-type" value="<?= $_GET['sidebarads-type']; ?>">
                <?php endif;  ?>    
                
                
                <?php if(isset($_GET['copypopup-cateory']) && !empty($_GET['copypopup-cateory'])  ) : ?> 
                    <input type="hidden" name="copypopup-cateory" value="<?= $_GET['copypopup-cateory']; ?>">
                <?php endif;  ?> 

                <?php if(isset($_GET['mobile-results-iframe']) && !empty($_GET['mobile-results-iframe'])  ) : ?> 
                    <input type="hidden" name="mobile-results-iframe" value="<?= $_GET['mobile-results-iframe']; ?>">
                <?php endif;  ?> 

                <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                    <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
                <?php endif; ?> 

                <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? trim($_GET['position']) : 'before'; ?>">

                <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && $_GET['one_word'] != 'on') ? 'off' : 'on'; ?>">

                <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && $_GET['two_word'] != 'on') ? trim($_GET['one_word']) : 'on'; ?>">

                <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    $value = $columns_count == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
                <?php endif; ?> 

                <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                    <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
                <?php endif; ?> 

                <?php
                $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                if (is_array($industry) || is_object($industry)):

                foreach ($industry as $value) : ?>
                    <input type="hidden" name="industry[]" value="<?php $v = trim($value);  echo $v; ?>">
                <?php endforeach;
                endif; ?>

                <input type="hidden" name="fltr" value="rsult">
                <?php if( !empty($filter_options->enable_names_filter->enable_character_count) && isset($filter_options->enable_names_filter->enable_character_count->value) && $filter_options->enable_names_filter->enable_character_count->value == 'true' ): ?>
                    <div class="col-xs-12">
                    <div class="slidecontainer">
                        <div class="flex-row">
                        <h3><?php echo __($filter_options->enable_names_filter->enable_character_count->character_count_title, 'business_name_word_manager'); ?></h3>
                        </div>
                        <input class="sliderRange" name="character" type="text" min="2" max="15" value="<?php echo (isset($_GET['character']) && !empty($_GET['character'])) ? trim($_GET['character']) : '15'; ?>" name="points" step="1">
                    </div>
                    </div>
                    <?php endif; ?>
                </div>
            
                <div class="row">
                <div class="col-xs-12 mar_0">
                    <div class="cstm_label">
                    

                    <h3><?php echo __($filter_options->enable_names_filter->enable_words_filter->words_title, 'business_name_word_manager'); ?></h3>
                    <ul class="live-search-list word-type <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : echo 'rhyming-on'; endif; ?>">
                        
                        <?php if( $filter_options->enable_names_filter->enable_words_filter ) :  ?>
                        
                        <li>
                        <input class="one-word" name="one_word" type="hidden" value="<?php echo (isset($_GET['one_word']) && $_GET['one_word'] != 'on') ? trim($_GET['one_word']) : 'on'; ?>">

                        <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->one_word_title, 'business_name_word_manager'); ?> <input data-index="" class="oneword" value="on" <?php if (!isset($_GET['one_word']) || (isset($_GET['one_word']) && $_GET['one_word'] == 'on')) : ?> checked="checked" <?php endif; ?> type="checkbox" <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <li data-search-term="agriculture">

                        <input class="two-word" name="two_word" type="hidden" value="<?php echo (isset($_GET['two_word']) && $_GET['two_word'] != 'on') ? trim($_GET['two_word']) : 'on'; ?>">

                        <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->two_word_title, 'business_name_word_manager'); ?> <input data-index="" class="twoword" <?php if (!isset($_GET['two_word']) || (isset($_GET['two_word']) && $_GET['two_word'] == 'on' || (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on'))) : ?> checked="checked" <?php endif; ?> value="on" type="checkbox" <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>

                        <?php endif; ?>
                    </ul>
                        <?php if( !empty($filter_options->enable_grammar_filters) && isset($filter_options->enable_grammar_filters->value) && $filter_options->enable_grammar_filters->value == 'true' ): ?>
                        <ul class="live-search-list word-type <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : echo 'rhyming-on';
                                            endif; ?>">
                           
                            <?php if( $filter_options->enable_grammar_filters->masculine_singular_title !== '' ) :  ?>  
                            <li>
                            <label><?php echo __($filter_options->enable_grammar_filters->masculine_singular_title, 'business_name_word_manager'); ?> 
                                <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                                {
                                    if (in_array(('masculine'), $_GET['grammar']))
                                    {
                                    $checked = 'checked="checked"';
                                    }
                                }
                                ?>
                                <input name="grammar[]" class="grammar1" type="checkbox" value="masculine" <?= $checked; ?>>
                                <span class="checkmark"></span>
                            </label>
                            </li>
                            <?php endif; ?>
                            
                            <?php if( $filter_options->enable_grammar_filters->feminine_singular_title !== '' ) :  ?>
                            <li>
                            <label><?php echo __($filter_options->enable_grammar_filters->feminine_singular_title, 'business_name_word_manager'); ?> 
                            <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                                {
                                if (in_array(('feminine'), $_GET['grammar']))
                                {
                                    $checked = 'checked="checked"';
                                }
                                }
                                ?>
                                <input name="grammar[]" class="grammar2" type="checkbox" value="feminine" <?= $checked; ?>>
                                <span class="checkmark"></span>
                            </label>
                            </li>
                            <?php endif; ?>

                            <?php if( $filter_options->enable_grammar_filters->masculine_plural_title !== '' ) :  ?>
                            <li>
                            <label><?php echo __($filter_options->enable_grammar_filters->masculine_plural_title, 'business_name_word_manager'); ?>
                                <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                                {
                                if (in_array(('masculine plural'), $_GET['grammar']))
                                {
                                    $checked = 'checked="checked"';
                                }
                                }
                                ?>
                                <input name="grammar[]" class="grammar3" type="checkbox" value="masculine plural" <?= $checked; ?>>
                                <span class="checkmark"></span>
                            </label>
                            </li>
                            <?php endif; ?>

                            <?php if( $filter_options->enable_grammar_filters->feminine_plural_title !== '' ) :  ?>
                            <li>
                            <label><?php echo __($filter_options->enable_grammar_filters->feminine_plural_title, 'business_name_word_manager'); ?>
                                <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                                {
                                if (in_array(('feminine plural'), $_GET['grammar']))
                                {
                                    $checked = 'checked="checked"';
                                }
                                }
                                ?>
                                <input name="grammar[]" class="grammar4" type="checkbox" value="feminine plural" <?= $checked; ?>>
                                <span class="checkmark"></span>
                            </label>
                            </li>
                            <?php endif; ?> 

                            <?php if( $filter_options->enable_grammar_filters->plural_title !== '' ) :  ?>
                            <li>
                            <label><?php echo __($filter_options->enable_grammar_filters->plural_title, 'business_name_word_manager'); ?>
                                <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                                {
                                if (in_array(('plural'), $_GET['grammar']))
                                {
                                    $checked = 'checked="checked"';
                                }
                                }
                                ?>
                                <input name="grammar[]" class="grammar0" type="checkbox" value="plural" <?= $checked; ?>>
                                <span class="checkmark"></span>
                            </label>
                            </li>
                           <?php endif; ?>
                    </ul>
                    <?php endif; ?>
                    </div>
                </div>
                </div>
                
                <div class="row">
                <?php if( !empty($filter_options->enable_names_filter->enable_keyword_filter) && isset($filter_options->enable_names_filter->enable_keyword_filter->value) && $filter_options->enable_names_filter->enable_keyword_filter->value == 'true' ): ?> 
                <div class="col-xs-12 mar_0">
                    <div class="cstm_label">
                    <h3><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->keyword_title_mobile, 'business_name_word_manager'); ?></h3>
                    <ul class="live-search-list word-type <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : echo 'rhyming-on';
                                        endif; ?>">
                        <li>
                        <label><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->before_title, 'business_name_word_manager'); ?> <input name="position" type="radio" <?php echo (isset($_GET['position']) && $_GET['position'] == 'before') ? 'checked' : ''; ?> value="before">
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <li data-search-term="agriculture">

                        <label><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->after_title, 'business_name_word_manager'); ?><input name="position" type="radio" <?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? 'checked' : ''; ?> value="after">
                            <span class="checkmark"></span>
                        </label>
                        </li>
                    </ul>
                    </div>
                </div>
                <?php endif; ?>
                </div>

                <?php if( !empty($filter_options->enable_names_filter->enable_rhyming_filter) && isset($filter_options->enable_names_filter->enable_rhyming_filter->value) && $filter_options->enable_names_filter->enable_rhyming_filter->value == 'true' ): ?>
                <div class="row">
                <div class="col-xs-12">
                    <div class="filter desk clearfix">
                    <div class="switch-toggle rhyming">
                        <div class="inner">
                        <span class="toggle-title"><?php echo __($filter_options->enable_names_filter->enable_rhyming_filter->rhyming_title, 'business_name_word_manager'); ?></span>
                        <label class="switch deskslide">
                            <input class="rhyming-val" name="rhyming" type="hidden" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">
                            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">
                            <input type="checkbox" <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> checked="checked" <?php endif; ?> value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] != 'on') ? trim($_GET['rhyming']) : 'on'; ?>">
                            <span class="slider round"></span>
                        </label>
                        <div class="condition">
                            <p><?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? __('On', 'business_name_word_manager') : __('Off', 'business_name_word_manager'); ?></p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
            <div class="inner">
                <?php $events_cat = is_page_template(['template-nameideas.php','template-bng340.php','template-domains.php','template-bng371.php']) ? 'ResultsPage' : 'username-resultspage';  ?>
                <button type="submit" class="btn btn-primary apply-btn outbound-link" data-category="<?= $events_cat; ?>" data-action="NamesFilters" data-label=""><?php echo __($filter_options->enable_names_filter->names_apply_button, 'business_name_word_manager'); ?></button>
                <button type="button" class="btn btn-default" data-bs-dismiss="modal"><?php echo __($filter_options->enable_names_filter->names_close_button_text, 'business_name_word_manager'); ?></button>
            </div>
            </div>
         </form>
        </div>
    </div><!-- /.modal-content -->
</div>

<!-- Names Filters End -->
<?php endif; ?>
<?php if( !empty($filter_options->enable_extensions_filter) && isset($filter_options->enable_extensions_filter->value) && $filter_options->enable_extensions_filter->value == 'true' ): ?>
<!-- Extensions Modal Start -->

<div class="modal fade domain-filter" id="extension-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL);?>
            <form  action="<?php echo $arrVals[0]; ?>" method="GET">
            <div class="modal-header">
            <div class="industry-text">
                <span><i class="fas fa-globe"></i></span>
                <h4 class="modal-title"><?php echo __($filter_options->enable_extensions_filter->extensions_title, 'business_name_word_manager'); ?>
                </h4>
            </div>
            </div>
            <div class="modal-body extensions">

            <div class="ul-list list-modal">
                
            <div class="search-bar">
                <div class="search_bar">
                    <input type="text" name="" class="tld-search" placeholder="<?php echo __($filter_options->enable_extensions_filter->extensions_title, 'business_name_word_manager'); ?>">
                </div>
            </div>

            <?php if(isset($_GET['home_id']) && !empty($_GET['home_id'])) : ?>
                <input type="hidden" name="home_id" value="<?=  trim($_GET['home_id']);  ?>">
            <?php endif; ?>
                

            <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                <input type="hidden" name="stickyads" value="true">
            <?php endif;  ?>  

            <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
              <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
            <?php endif; ?>
            
            <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
            <?php endif;  ?>  

            <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])  ) : ?> 
                <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
            <?php endif;  ?>   

            <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
            <?php endif; ?> 
            
            <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    $value = $columns_count == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
            <?php endif; ?>
    
            <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">

            <input type="hidden" name="fltr" value="rsult">

            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

            <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && !empty($_GET['one_word']) ) ? trim($_GET['one_word']) : 'on'; ?>">

            <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && !empty($_GET['two_word']) ) ? trim($_GET['two_word']) : 'on'; ?>">

            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

            <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

            <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
                <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
            <?php endif; ?> 

                <?php 
                
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                if( is_array($grammar) || is_object($grammar)):
                foreach ($grammar as $value) : 
                ?>
                <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach; endif; ?>

                <?php

                $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                if (is_array($industry) || is_object($industry)) :
                foreach ($industry as $value) : ?>
                    <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach;
                endif; ?> 

            <?php $tlds = ['com','co','net','org','ai','cc','info','biz','ag','am','in','at','de','info','es','nl','it','sg','fr','eu','ph','mx','be','bz','ca','ch','cn','cz','de','es','fm','gg','gs','in','io','br','la','us','ca','ms','mx','nl','ph','pl','sg','tc','tk','online','tv','tw','us','ws','com.au','com.mx','co.uk','me.uk','org.uk','co.nz','net.nz','org.nz','com.tw','org.tw','idv.tw','pro','eu','com.ag','net.ag','org.ag','com.pl','net.pl','org.pl','biz.pl','info.pl','mobi','com.co','co.uk','com.ph','co.za','co.in','net.co','asia','net.au','org.au','net.in','org.in','firm.in','gen.in','ind.in','me','com.es','com.ve','org.ve','com.bz','net.bz','co.uk','co.id','tel','nom.es','org.es','com.mx','com.ph','nom.co','co.ve','info.ve','net.ve','web.ve','net.ph','org.ph','uno','menu','luxury','build','bike','clothing','guru','holdings','plumbing','singles','ventures','estate','photography','gallery','camera','lighting','equipment','graphics','kitchen','construction','contractors','directory','today','technology','land','tips','diamonds','enterprises','voyage','careers','recipes','photos','shoes','company','cab','domains','limo','management','systems','center','computer','support','academy','solutions','repair','house','builders','camp','education','glass','international','solar','training','email','institute','florist','coffee','club','codes','farm','holiday','marketing','agency','cheap','zone','bargains','boutique','cool','watch','viajes','expert','works','reviews','dance','democrat','futbol','foundation','cruises','rentals','villas','exposed','flights','vacations','social','immobilien','ninja','consulting','rocks','republican','buzz','maison','properties','tienda','condos','dating','events','productions','partners','london','cards','cleaning','catering','community','wiki','parts','supply','industries','supplies','tools','xyz','ink','bid','report','vision','moda','pub','trade','webcam','fish','actor','kaufen','services','rest','bar','engineering','gripe','capital','exchange','lease','pictures','media','associates','reisen','university','toys','town','haus','financial','wtf','fail','limited','care','clinic','surgery','dental','nyc','cash','tax','fund','investments','vegas','tokyo','furniture','discount','fitness','schule','nagoya','world','charity','shopping','college','studio','movie','kiwi','ltda','degree','vote','army','church','law','digital','legal','blue','place','gives','vip','fit','archi','loans','llc','fun','healthcare','vin','bayern','blackfriday','tech','site','style','money','monster','country','guide','page','horse','blog','ist','family','website','apartments','red','cooking','ski','surf','tattoo','work','sarl','green','school','tennis','voto','rich','golf','promo','rehab','lawyer','gratis','vodka','poker','fan','pics','business','sydney','theatre','flowers','vet','energy','attorney','lol','yoga','xn--6frz82g','dev','fans','hockey','space','gold','global','onl','news','wine','fishing','earth','bingo','ceo','miami','store','amsterdam','paris','qpon','beer','inc','app','doctor','engineer','team','theater','film','taxi','hiphop','lgbt','irish','fyi','health','juegos','protection','shiksha','gifts','restaurant','tube','rodeo','cafe','stream','rip','adult','chat','link','garden','istanbul','casino','delivery','guitars','gift','network','group','moe','design','online','navy','salon','click','one','auto','casa','okinawa','city','wedding','hospital','car','cars','software','sale','dog','black','video','ryukyu','bet','memorial','property','jewelry','credit','courses','band','games','airforce','art','yokohama','diet','help','sexy','finance','autos','show','pink','football','direct','mom','coach','jetzt','love','immo','audio','game','boston','cloud','ltd','security','mba','insure','life','market','accountants','tires','live','tours','dentist','bio','reise','deals','auction','pet','christmas','host','fashion','realestate','coupons','homes','baby','creditcard','mortgage','express','shop','kim','forsale','plus','photo','hosting','soccer','study','melbourne','pizza','claims','gmbh','best','rent','press','run'];  ?>
            <ul class="tld-dropdown-list">
                <?php  foreach($tlds as $tld) : ?>
                    <?php  empty($_GET['tlds']) ? $_GET['tlds'] = 'com' : '';   ?>
                    <?php  $checked = !empty($_GET['tlds']) && $_GET['tlds'] ==  $tld ? 'checked="checked"' : '' ;   ?>
                    <li data-search-term="<?= $tld; ?>">
                        <label>
                        .<?= $tld; ?> <input name="tlds" data-index="" type="radio" <?= $checked; ?> value="<?= $tld; ?>" />
                            <span class="checkmark"></span>
                        </label>
                    </li>
                <?php  endforeach; ?>
            </ul>
            </div>  
            </div>
            <div class="modal-footer">
            <div class="inner">
                <button type="submit" class="btn btn-primary apply-btn indus-filter" data-category="ResultsPage" data-action="IndustryFilters" data-label="" ><?php echo __($filter_options->enable_extensions_filter->extension_apply_button, 'business_name_word_manager'); ?></button>
                <button type="button" class="btn btn-default" data-bs-dismiss="modal"><?php echo __($filter_options->enable_extensions_filter->extension_mobile_close_button, 'business_name_word_manager'); ?></button>
            </div>
            </div>
        </form>
    </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<!-- Extensions Modal End -->
<?php endif; ?>

<?php if( !empty($filter_options->enable_extensions_filter) && isset($filter_options->enable_extensions_filter->value) && $filter_options->enable_extensions_filter->value == 'true' ): ?>

<!-- Saved Ideas Modal Starts -->

<div class="modal fade" id="myModalSaved" tabindex="-1" role="dialog">

<div class="modal-dialog" role="document">
  <div class="modal-content">
    <?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL);?>
     <form id="saved_id" method="POST" action="<?php echo $arrVals[0]; ?>">

      <div class="modal-header">
        <span class="icon_star_alt elegant-icon"></span>
        <h2><?php echo __($filter_options->enable_saved_ideas->saved_ideas_modal_mobiletitle, 'business_name_word_manager'); ?></h2>
      </div>
      <div class="modal-body">
            <div class="savedideas-drop-data">
                <ul class="colm1"><?php $savedideas = (isset($_SESSION['invite_modal_value'])) ?
                    $_SESSION['invite_modal_value'] : array();
                  ?>
                  <?php if (is_array($savedideas) || is_object($savedideas)) {
                    foreach ($savedideas as $key => $value) {
                  ?>       
                      <li id='<?php echo $value; ?>' data-kname="<?php echo str_replace(" ", "+", $key); ?>">
                        <div class='list-data'><div class='text'>
                        <input type='checkbox' class='ids' name='list[]' hidden='' value='<?php echo $key; ?>' checked='checked'><span class="sword"><?php echo $key; ?></span><a href='#'>Register  <i class='fas fa-external-link-alt'></i></a></div><div class='close-icon'>
                        <span class='<?php echo $key; ?> savespan elegant-icon icon_close' onClick="removeSavedResult(this)"></span></div></div>
                      </li>
                  <?php
                    }
                  }
                  ?>
                </ul>
          </div>
      </div>
      <div class="modal-footer savedideas-drop-data">
        <div class="industry-sub-right">
            <div class="save-ideas">
                <p><?php echo __($filter_options->enable_saved_ideas->saved_ideas_subtitle, 'business_name_word_manager'); ?></p>
                <div class="responsebar">
                </div>
                <div class="input-main">
                 <input class="email-input" id="email" name="email" type="Email" data-popup-url="" data-active="" placeholder="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_placeholder, 'business_name_word_manager'); ?>">
                 <a href="javascript:void(0)" class="send sharemail"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?></a>
                 <input type="submit" class="send sharemail" value="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?>" style="display: none;">
                </div>
               <div class="form-group">
                 <input class="chekedomainid" name="marketing_email" type="checkbox" id="html1">
                 <label for="html"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_optin_notice, 'business_name_word_manager'); ?><a href="<?php echo $filter_options->enable_saved_ideas->saved_ideas_policy_url; ?>" rel="nofollow" target="_blank"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_policy_text, 'business_name_word_manager'); ?></a></label>
               </div>
            </div>
       
      <button type="button" class="btn btn-default" data-bs-dismiss="modal"><?php echo $filter_options->enable_saved_ideas->saved_ideas_mobile_close_button; ?></button>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div>
<!-- Saved Ideas Modal Ends -->

<?php endif; ?>

<!-- Mobile Modals End -->