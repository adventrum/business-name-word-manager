<?php

function savedIdeasTranslatedContent($country_code, $lang_code)
{
    $response = [];
    if( $lang_code == 'es' && $country_code == 'MX' ){
        $lang_code = 'MX';
    } elseif( $lang_code == 'pt-br' && $country_code == 'BR' ){
        $lang_code = 'BR';
    } elseif( $lang_code == 'pt-pt' && $country_code == 'PT' ){
        $lang_code = 'PT';
    }
    $country_codes = ['FR','DE','IT','ES','MX','BR','PT'];

    if( strtoupper($country_code) == strtoupper($lang_code) ){
       $check_code = $country_code;
    } elseif( strtoupper($lang_code) == 'EN' && in_array($country_code,$country_codes) ){
       $check_code = 'EN';
    }else{
      $check_code = '';
    }

    switch ($check_code) {
        case 'FR':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Obtenez votre domaine GRATUITEMENT avec n\'importe quel forfait Premium chez WIX !</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Découvrez la plateforme qui vous donne la liberté de créer, concevoir, gérer et développer votre présence sur le Web exactement comme vous le souhaitez.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Pas de carte de crédit nécessaire</p>';
            $learn_more = 'En savoir plus';
            $register = 'Inscrivez-vous gratuitement';
            break;

        case 'DE':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Erhalte deine Domain KOSTENLOS mit jedem Premium-Plan bei WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Entdecken Sie die Plattform, die Ihnen die Freiheit gibt, Ihre Webpräsenz ganz nach Ihren Wünschen zu erstellen, zu gestalten, zu verwalten und zu entwickeln.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Keine Kreditkarte benötigt</p>';
            $learn_more = 'Mehr erfahren';
            $register = 'Gratis registrieren';
            break;
        case 'IT':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Ottieni il tuo dominio GRATIS con qualsiasi pacchetto Premium su WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Scopri la piattaforma che ti dà la libertà di creare, progettare, gestire e sviluppare la tua presenza sul web esattamente come desideri.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Non serve la carta di credito</p>';
            $learn_more = 'Per saperne di più';
            $register = 'Registrati gratis';
            break;

        case 'ES':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">¡Obtén tu dominio GRATIS con cualquier Plan Premium en WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Descubra la plataforma que le brinda la libertad de crear, diseñar, administrar y desarrollar su presencia en la web exactamente de la manera que desee.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">No se requiere tarjeta de crédito</p>';
            $learn_more = 'Leer más';
            $register = 'Regístrese gratis';
            break;

        case 'MX':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">¡Obtén tu dominio GRATIS con cualquier Plan Premium en WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Descubra la plataforma que le brinda la libertad de crear, diseñar, administrar y desarrollar su presencia en la web exactamente de la manera que desee.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">No se requiere tarjeta de crédito</p>';
            $learn_more = 'Leer más';
            $register = 'Regístrese gratis';
            break;

        case 'BR':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Obtenha seu domínio GRATUITO com qualquer Plano Premium no WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Descubra a plataforma que lhe dá a liberdade de criar, projetar, gerenciar e desenvolver sua presença na web exatamente da maneira que você deseja.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Não é necessário cartão de crédito</p>';
            $learn_more = 'Saiba mais';
            $register = 'Cadastre-se grátis';
            break;

        case 'PT':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Obtenha seu domínio GRATUITO com qualquer Plano Premium no WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Descubra a plataforma que lhe dá a liberdade de criar, projetar, gerenciar e desenvolver sua presença na web exatamente da maneira que você deseja.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Não é necessário cartão de crédito</p>';
            $learn_more = 'Saiba mais';
            $register = 'Cadastre-se grátis';
            break;
        
        case 'EN':
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Get your domain FREE with any Premium Plan at WIX!</h2>
                        <p style="color:#fff;font-size:16px;font-family: arial;">Discover the platform that gives you the freedom to create, design, manage and develop your web presence exactly the way you want.</p>
                        <p style="color:#fff;font-size:16px;font-family: arial;">No credit card required</p>';
            $learn_more = 'Learn More';
            $register = 'Register Free';
            break;

        default:
            $copy = '<h2 style="color:#fff;font-size:22px;font-family: arial;">Create a website for Free</h2>
              <p style="color:#fff;font-size:16px;font-family: arial;">Get Started with easy affordable website hosting with GoDaddy.</p>
              <ul style="list-style:none;font-size:16px;color:#fff;padding:0;font-family: arial;">
                <li style="color:#fff;">- Free Site Builder</li>
                <li style="color:#fff;">- One-click Wordpress Install</li>
                <li style="color:#fff;">- 24/7 Live Chat &amp; Phone Support</li>
                <li style="color:#fff;">- 30-Day Money-Back Guarantee</li>
             </ul>';
            $learn_more = 'Learn More';
            $register = 'Register Free';
            break;
    }

    $response['copy'] = $copy;
    $response['register'] = $register;
    $response['learn_more'] = $learn_more;

    return $response;
}
