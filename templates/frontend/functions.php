<?php



if (!function_exists('serach_word_senitize')) :
    function serach_word_senitize($query_word)
    {
        $patterns = array(
            '#^https?\:\/\/#',
            '/www./',
            '/^\s\s+/',
            '/\s\s+$/',
            '/\s\s+/u',
            '/.co.uk/',
            '/.net/',
        );

        $replace = array('', '', '', '', ' ', '', '', '', '', '');
        $query_word = preg_replace($patterns, $replace, $query_word);
        if (stripos($query_word, ',')) {
            $query_word = str_replace(',', ' ', $query_word);
            $query_word = str_replace(', ', ' ', $query_word);
        }
        if (stripos($query_word, '.com')) {
            $query_word = str_replace('.com', '', $query_word);
        }
        if (stripos($query_word, '.org')) {
            $query_word = str_replace('.org', '', $query_word);
        }
        if (stripos($query_word, '.co')) {
            $query_word = str_replace('.co', '', $query_word);
        }
        $query_word = preg_replace('/[^a-zA-Z0-9-\s]/', '', $query_word);

        return $query_word;
    }
endif;





/*--------------------Pagination-------------*/
function paginate_function($total_records, $url)
{

    require_once "Mobile_Detect.php";
    $detect = new Mobile_Detect;

    if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en') {
        $limit = 69;
    } else {
        if (!$detect->isMobile() && !$detect->isTablet()) :
            $limit = 96;
        endif;

        if ($detect->isTablet()) :
            $limit = 72;
        endif;

        if ($detect->isMobile() && !$detect->isTablet()) :
            $limit = 30;
        endif;
    }
    $strURL = $_SERVER['REQUEST_URI'];
    //$arrVals = explode("/",$strURL);
    $arrVals = explode("?", $strURL);
    $arrVals2 = explode("/", $arrVals[0]);
    $numkey = str_word_count(stripslashes($_GET['bname']));
    if ($numkey <= '1') {
        $bname = stripslashes($_GET['bname']);
    } else {
        $fword = explode(' ', stripslashes($_GET['bname']));
        $bname = $fword[0] . ' ' . $fword[1];
    }
    if (isset($_GET['related'])) {
        $element = '?bname=' . $bname . "&related=" . $_GET['related'];
    } else {
        $element = '?bname=' . $bname;
    }
    $found = 0;
    /*if(rawurldecode($arrVals[3])==$element)
     {
        $found=$found+1; 
     }   
     else
     {
        $found= rawurldecode($arrVals[3]);
     } */
    if ($arrVals2 == '') {
            $found = $found + 1;
        } else {
            if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en') {
                $found = $arrVals2[3];
            }
            if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE == 'en') {
                $found = $arrVals2[3];
                if( $arrVals2['2'] == 'register' || $arrVals2['2'] == 'nameideas' || $arrVals2['2'] == 'domains' ){
                    $found = $arrVals2[3];
                }
            }
    }
    //$pagination=$strURL;
    $current_page = (!empty($found) && $found != 1) ? $found : 1;
    $total_pages = ceil($total_records / $limit);

    $pagination = '';
    if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number
        $pagination .= '<div class="Pagination_nav">
                        <ul>';

        if ($detect->isMobile() && !$detect->isTablet()) {
            if ($current_page == 1) {
                $right_links = $current_page + 3;
            } elseif ($current_page > 1 && $current_page < 3) {
                $right_links = $current_page + 2;
            } else {
                $right_links = $current_page;
            }
        } else {
            $right_links = $current_page + 3;
        }

        $previous = $current_page - 3; //previous link
        $next = $current_page + 1; //next link
        $first_link = true; //boolean var to decide our first link

        if ($current_page > 1) {
            $previous_link = $current_page - 1;
            $query = !empty($_SERVER['QUERY_STRING']) ? '/?'. $_SERVER['QUERY_STRING'] : ''; 
            //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
            $pagination .= '<li><a href="' .get_permalink().'page/'.$previous_link.$query. '" data-page="' . $previous_link . '" title="Previous"><span class="arrow_carrot-left elegant-icon"></span>' . __('Previous', 'thegem-child') . '</a></li>'; //previous link
            // }else{
            //  $pagination .= '<li><a href="'.$url . $previous_link . '" data-page="' . $previous_link . '" title="Previous">&lt; Back</a></li>'; //previous link
            // }
            for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
                if ($i > 0) {
                    $pagination .= '<li><a href="' . $url  . $i . '" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if ($first_link) { //if current active page is first link
            $pagination .= '<li class="select" ><a class="select" href="' . $url . $current_page . ' "> ' . $current_page . '</a></li>';
        } elseif ($current_page == $total_pages) { //if it's the last active link
            $pagination .= '<li class="select" ><a class="select" href="' . $url  . $current_page . ' "> ' . $current_page . '</a></li>';
        } else { //regular current link
            $pagination .= '<li class="select"><a class="select" href="' . $url  . $current_page . ' "> ' . $current_page . '</a></li>';
        }

        for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links
            if ($i <= $total_pages) {
                $pagination .= '<li><a href="' . $url . $i . '"   title="Page ' . $i . '">' . $i . '</a></li>';
            }
        }
        if ($current_page < $total_pages) {
            $next_link = $current_page + 1;
            $query = !empty($_SERVER['QUERY_STRING']) ? '/?'. $_SERVER['QUERY_STRING'] : ''; 
            //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
            $pagination .= '<li><a href="' .get_permalink().'page/'.$next_link.$query. '" data-page="' . $next_link . '" title="Next">' . __('Next', 'thegem-child') . ' <span class="arrow_carrot-right elegant-icon"></span></a></li>'; //next link
            // }else{
            //  $pagination .= '<li><a href="' .$url . $next_link . '" data-page="' . $next_link . '" title="Next">'.__('Next', 'thegem-child').' &gt;</a></li>'; //next link
            // }
        }

        $pagination .= '</ul>
                    </div>';
    }
    return $pagination;
}



