<?php

//define(ICL_LANGUAGE_CODE,'en');

get_header();


require_once "Mobile_Detect.php";
$detect = new Mobile_Detect;

if( !$detect->isMobile() && !$detect->isTablet() ):
  $limit = 96;
  $_GET['limit'] = 96;
endif;
if( $detect->isTablet() ):
  $limit = 72;
  $_GET['limit'] = 72;
endif;
if( $detect->isMobile() && !$detect->isTablet() ):
  $limit = 30;
  $_GET['limit'] = 30;
endif;

include_once(dirname(__FILE__) . '/inc/Php-functions.php');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$_GET['paged'] = $paged;
$_GET['tag']   = 'b';
$_GET['separator'] = true;

if( isset($_GET['tlds']) ) {
  $tlds = $_GET['tlds'];
}else{
  $tlds = 'com';
}

$_GET['tld']   = $tlds;
$results = bnwm_search_results($_GET);

$mainstring2 = !empty($results['result']['words']) && is_array($results['result']['words']) ? $results['result']['words'] : '';

include_once(dirname(__FILE__) . '/inc/template-js.php');
 
$domains = (!empty($_GET['bname'])) ? getDomainList($_GET['bname'],$paged,8) : '';

$translations = json_decode(get_option('bnwm_string_translations'));
$total = '';

if (isset($_GET['rhyming'])) {
$result_filter['rhyming'] = ($_GET['rhyming'] == 'on') ? 'on' : '';
} else {
$result_filter['rhyming'] = '';
}


if (isset($_GET['position'])) {
$result_filter['position'] = ($_GET['position'] == 'before') ? '' : 'on';
} else {
$result_filter['position'] = '';
}


if (isset($_GET['synonyms'])) {
$result_filter['synonyms'] = ($_GET['synonyms'] == 'on') ? 'on' : '';
} else {
$result_filter['synonyms'] = 'on';
}

if (isset($_GET['one_word'])) {
$result_filter['one_word'] = ($_GET['one_word'] == 'on') ? 'on' : '';
} else {
$result_filter['one_word'] = 'on';
}
if (isset($_GET['two_word'])) {
$result_filter['two_word'] = ($_GET['two_word'] == 'on') ? 'on' : '';
} else {
$result_filter['two_word'] = 'on';
}

$aff_domains = json_decode(get_option('bnwm_affiliate_links_domains'));
$page_slug = CURRENT_MULTISITE_CODE == '' ? explode('/',$_SERVER['REDIRECT_URL'])[1] : explode('/',$_SERVER['REDIRECT_URL'])[2];
?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <div class="main_wraper2 business-eng result-page domain-page">
  
      <!-- Alert Message -->
      <div class="container alert-box">
          <div class="row domain-center">

              <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                  <?php top_sticky_banner_desk(); ?>
              <?php endif; ?>
              <?php showWordManagerFilters($results['category']); ?>
                         <!-- Generator result html starts -->
                         <div class="domain-generator">
                         <div class="col-sm-12 mar_0">
                            
                            <div class="title_info">
                                
                              <div class="filter_block eng">
                                <div class="imgloadingshow" style="display:none;">
                                  <div class="inner">
                                    
                                  </div>
                                </div>
                                
                                <div class="check_avail domain-gen">
                                  <?php  
                                      $style = "style='display: none;'"; 
                                      $totalFormat = '';
                                      if( !getNameideaAlgo() ) : 
                                        $style = '';
                                        $total = $results['total'];
                                        $totalFormat = number_format($total);
                                      endif;
                                   ?> 
                                  <h4 class="check-dom-txt" <?= $style; ?>>
                                    <span><?= $totalFormat; ?></span> <?= $translations->domainstextadvert; ?>
                                  </h4>
                                     
                                </div>
                                    
                                <div class="row above-row domain-gen-list">
                                   <div class="col-md-12 pad_set">
                                     <ul class="list-star">
                                      <?php
                                       
                                       $domain_list = [];
                                       $tool_tip = __('Click to check domain availability', 'thegem-child');

                                       if ( !empty($mainstring2) &&  is_array($mainstring2)   ) {
                                         
                                         $count = 0;
                                         $general_list_index = (!empty($_SESSION['general_list_index'])) ?  $_SESSION['general_list_index'] : 0;
                                         $i = $general_list_index;
                                         
                                         if( is_array($mainstring2) || is_object($mainstring2)):
                                           foreach ($mainstring2 as $index => $mastring) {
                                           $number = $i . '' . $page;
                                        ?>
                                          <?php if ($mastring != '1' && $mastring != '') { 
                                               

                                            
                                                $country_code = getenv( 'HTTP_GEOIP_COUNTRY_CODE' );
                                                $device = !empty($_GET['device']) ? $_GET['device'] : ''; 
                                                $extra_parameter = '-'.$device.'-'.$country_code.getGclidParam(); 
                                               
                                                $main_string = str_replace('<b>', '', strtolower($mastring)); 
                                                $main_string = str_replace('</b>', '', strtolower($main_string));
                                               
                                                $storemain_string = str_replace('</b>', '', strtolower($main_string));
                                                $main_string = preg_replace('/[\x00-\x1F\x7F-\xFF\']/', '', $main_string);       
                                              
                                                array_push($domain_list,$main_string);
                                              
                                                $claas =  strlen($main_string) > 16 ? 'class="big-text"' : '';

                                                $home_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
                                                
                                                $btn_url = 'https://www.kqzyfj.com/click-3797283-15162961?sid=nameidea-register-post-'.$home_id.$extra_parameter.'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'.$storemain_string.'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate';

                                                if( ( !empty($aff_domains->registerlinkdropdown) &&  !empty( $aff_domains->registerlinkdropdown->url ) )  ){
                                          
                                                  $registerlink =  $aff_domains->registerlinkdropdown; 
                                                  
                                                  $end_link = $registerlink->sponsor == 'godaddy' ? '?sid='.$registerlink->sid.'-'.$home_id.$extra_parameter.'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'.$storemain_string.'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate' : '';
                                                  
                                                  $btn_url = $registerlink->url.$end_link; 
                                                
                                                }

                                                if( in_array($current_country, getGeoCountriesList()) ){
      
                                                  if( ( !empty($aff_domains->registerlinkdropdown) &&  !empty( $aff_domains->registerlinkdropdown->geo_url ) ) &&  $page_slug !== 'nameideas-wix'  ){
                                                                                        
                                                    $registerlinkdropdown =  $aff_domains->registerlinkdropdown; 
                                                    $sid =  $registerlinkdropdown->geo_sponsor == 'wix' ? '&s1=&s2=' :  ( $registerlinkdropdown->geo_sponsor == 'godaddy'  ? '?sid=' : '' ); 
                                                   
                                                    $registerlink = $registerlinkdropdown->geo_url.$sid.$registerlinkdropdown->geo_sid;  
                                                   // echo $registerlink; die;
                                                  }
                                              
                                                }
                                                


                                            ?>
                                            <li class="stardomain" data-domain="<?= $main_string; ?>" data-store="<?= $storemain_string; ?>">
                                              <div class="inner">
                                                <div class="click-event-box">
                                                   <a class="tld-links register-time-to-click" href="<?= $btn_url; ?>" data-category="domains-ResultsPage" data-action="DomainAvailabilityCheck" data-label="Register-Results" target="_blank">
                                                       <div class="text"> <p <?= $claas; ?>><?= $mastring; ?></p>
                                                     <div  class="status" target="_blank">
                                                     <span class="loding"><i class="fa fa-circle-o-notch fa-spin"></i></span>
                                                     </div>
                                                  </div>
                                                  </a>
                                                  <div class="star-result">
                                                    <span class="icon_star_alt elegant-icon result_<?php echo $number; ?> click-event"
                                                    onclick="setSelectedTestPlan('<?php echo $main_string; ?>','<?php echo $number; ?>', this);" data-category="domains-ResultsPage" data-action="SavedIdeas" data-label="SaveStar"></span>
                                                  </div>
                                                </div>
                                              </div>
                                            </li>
                                            <?php }
                                            ?>
                                            <?php
                                              //Tablet view results rows
                                              if ($detect->isTablet()) : ?>
                                                <?php if ($count == 17) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 1
                                        if (!empty($domains)) :
                                          if (count($domains) > 3) {
                                            $newlist = array_slice($domains, 0, 4);
                                          } else {
                                            $newlist = $domains;
                                          }
                                    ?>
                                    <div class="row mobile-products">
                                      
                                      <?php before_carousel_ads_domains();   ?>  
                                      
                                      <div class="col-md-12 domainify height">
                                          <!-- Tab Top Domainify Products -->
                                          <?php carousel_ads_new($newlist,'USD','domains-ResultsPage');   ?>
                                     </div>
                                     <div class="mobile-view-more">
                                        <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                     </div>
                                    </div>
                                    <?php endif;
                                       //Destop domains results end Row 1
                                    ?>

                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 35) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                      // For Tablet Top View
                                      banner_ad();
                                    ?>
                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>


                                        <?php if ($count == 53) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 2
                                        if (!empty($domains)) :
                                          if (count($domains) > 3) {
                                            $newlist = array_slice($domains, 4, 4);
                                          } else {
                                            $newlist = $domains;
                                          }
                                          // print_r($newlist);
                                    ?>
                                    <div class="row mobile-products">
                                        
                                       <?php before_carousel_ads_domains();   ?> 
                                      
                                       <div class="col-md-12 domainify height">
                                             <!-- Tab Bottom Domainify Products -->
                                             <?php carousel_ads_new($newlist,'USD','domains-ResultsPage');   ?>
                                        </div>
                                        <div class="mobile-view-more">
                                           <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif;
                                                //Destop domains results end Row 2
                                        ?>

                                      <?php
                                              //Mobile View results
                                              elseif ($detect->isMobile() && !$detect->isTablet()) : ?>
                                        <?php if ($count == 9) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 1
                                        if (!empty($domains)) :
                                          if (count($domains) > 4) {
                                            $newlist = array_slice($domains, 0, 4);
                                          } else {
                                            $newlist = $domains;
                                          }

                                    ?>
                                    <div class="row mobile-products">
                                      
                                       <?php before_carousel_ads_domains();   ?>
                                      
                                       <div class="col-md-12 domainify height">
                                          <!-- Mobile Top Domainify Products -->
                                          <?php  carousel_ads_new($newlist,'USD','domains-ResultsPage');   
                                          ?>
                                          <div class="mobile-view-more">
                                             <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                          </div>
                                      </div>
                                    </div>
                                    <?php endif;
                                       //Destop domains results end Row 1
                                    ?>

                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 19) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                      // For Mobile Top View
                                      banner_ad();
                                   ?>
                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                      <?php else : //Destop View results 
                                      ?>

                                        <?php if ($count == 23) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php if (!empty($domains)) :
                                        //Destop domains results Starts

                                        if (count($domains) > 4) {
                                          $newlist = array_slice($domains, 0, 4);
                                        } else {
                                          $newlist = $domains;
                                        }

                                     ?>
                                    <div class="row mobile-products">
                                      
                                      <?php before_carousel_ads_domains();   ?> 
                                      
                                      <div class="col-md-12 domainify height">
                                          <!-- Desktop Top Domainify Products -->
                                          <?php carousel_ads_new($newlist,'USD','domains-ResultsPage');   ?>
                                      </div>
                                      <div class="mobile-view-more">
                                         <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                      </div>
                                    </div>
                                    <?php endif;
                                       //Destop domains results end 
                                    ?>

                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 47) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                      // For Desktop Top View
                                      banner_ad();
                                    ?>
                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>


                                        <?php if ($count == 71) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 2
                                        if (!empty($domains)) :
                                          if (count($domains) > 4) {
                                            $newlist = array_slice($domains, 4, 4);
                                          } else {
                                            $newlist = $domains;
                                          }

                                    ?>
                                      <div class="row mobile-products">
                                        
                                        <?php before_carousel_ads_domains();   ?>
                                        
                                        <div class="col-md-12 domainify height">
                                            <!-- Desktop Bottom Domainify Products -->
                                            <?php carousel_ads_new($newlist,'USD','domains-ResultsPage');   ?>
                                        </div>
                                        <div class="mobile-view-more">
                                           <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                        </div>
                                      </div>
                                    <?php endif;
                                                  //Destop domains results end row 2
                                    ?>

                                    <div class="row below-row domain-gen-list">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                          <?php endif; ?>

                                           <?php endif; ?>

                                          <?php $count++;
                                                } endif;
                                              } else {
                                                $rlocation = home_url();
                                              }
                                          ?>
                                        </ul>

                                        <?php if (!$detect->isMobile() && !$detect->isTablet()) : ?>
                                          <?php if(!empty($mainstringnew) && ( is_array($mainstringnew) && is_array($mainstring2)) ) : ?>
                                          <?php if (count($mainstringnew) < 50 || count($mainstring2) < 50) : ?>
                                            <?php echo do_shortcode('[insertadvnew id="36848"]'); ?>
                                          <?php endif; ?>
                                          <?php endif; ?>
                                        <?php endif; ?>
                                        
                                        <div class="Pagination_nav">
                                         <?php  if ( !getNameideaAlgo() ) {
                                            echo get_paginationWithoutAJax($total, $limit);
                                          }
                                         ?>
                                        </div>


                                        <?php if (!$detect->isMobile() && !$detect->isTablet()) : ?>
                                          <?php
                                            // For Bottom Desktop Ads 
                                          ?>
                                        <?php endif; ?>

                                       

                                        <?php if ($detect->isMobile() && !$detect->isTablet()) : ?>
                                      </div>
                                    </div>
                                    <?php
                                        // For Bottom Mobile  
                                    ?>
                                  <?php endif; ?>
                                  </div>
                                </div>
                                <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                                    
                                    <?php bottom_sticky_banner_desk(); ?>
                                    <?php bottom_sticky_banner_mob(); ?> 

                                <?php endif; ?> 

                              </div>
                           </div>
                           </div>
                          </div>
                      </div>
                          </div>
                      </div>
                    </div>
                    
               
                    
                    <!-- KeywordMatching Purpose -->
                    <?php click_event_purpose('domains-ResultsPage');   ?> 

                    <script>
                    
                      jQuery(document).ready(function() {
                         
                         if( jQuery(window).innerWidth() <= 1024 ){
                           jQuery('#saved_id .save-ideas label').on('click', function(){
                               jQuery(this).prev().trigger('click');
                           });
                         }
                         
                         if( jQuery(window).innerWidth() <= 1024 ){
                            jQuery(".email-input:eq(0)").remove();
                            jQuery("#saved_id:eq(0)").remove();
                         }

                       // jQuery(window).scroll(filtersFixScroll);
                        
                        <?php
                        $cookie = $_SESSION['invite_modal_value'];
                        $cookie_arr = [];
                        if( is_array($cookie) && !empty($cookie) ){
                          foreach ($cookie as $cookie_name => $cookie_id) {
                           array_push($cookie_arr, $cookie_name); 
                          } 
                        }
                        $json = json_encode($cookie_arr);
                        ?>
                        jQuery.removeCookie('savedideas');
                        var arr = <?php echo $json;  ?>;
                        var json_str = JSON.stringify(arr);
                        jQuery.cookie('savedideas', json_str, {
                          expires: 15
                        });
                        

                        var h = parseInt("1965");
                                  jQuery(window).scroll(function() {
                                    var scroll = jQuery(window).scrollTop()+201;
                                    if(scroll >= h){
                                      jQuery('.left-scroll').css('position','absolute');
                                      jQuery('.right-scroll').css('position','absolute');
                                      jQuery('.results-left-widget .textwidget.custom-html-widget').css('top','1732px');
                                      jQuery('.results-right-widget .textwidget.custom-html-widget').css('top','1732px');
                                    }else{
                                      jQuery('.left-scroll').css('position','fixed');
                                      jQuery('.right-scroll').css('position','fixed');
                                      jQuery('.results-left-widget .textwidget.custom-html-widget').css('top','0px');
                                      jQuery('.results-right-widget .textwidget.custom-html-widget').css('top','0px');
                                    }
                        });
                        jQuery('.cancel_btn').click(function() {
                          var resultarr = <?php echo json_encode($result_filter); ?>;
                          jQuery.each(resultarr, function(key, value) {

                            if (key == 'one_word' && value == 'on') {
                              jQuery('.oneword').prop('checked', true);
                            }
                            if (key == 'one_word' && value == '') {
                              jQuery('.oneword').prop('checked', false);
                            }


                            if (key == 'two_word' && value == 'on') {
                              jQuery('.twoword').prop('checked', true);
                            }
                            if (key == 'two_word' && value == '') {
                              jQuery('.twoword').prop('checked', false);
                            }

                            if (key == 'position' && value == 'on') {
                              jQuery('.can_position').prop('checked', true);
                            }
                            if (key == 'position' && value == '') {
                              jQuery('.can_position').prop('checked', false);
                            }

                            if (key == 'synonyms' && value == 'on') {
                              jQuery('.can_synonyms').prop('checked', true);
                            }
                            if (key == 'synonyms' && value == '') {
                              jQuery('.can_synonyms').prop('checked', false);
                            }

                            if (key == 'synonyms' && value == 'on') {
                              jQuery('.can_synonyms').prop('checked', true);
                            }
                            if (key == 'synonyms' && value == '') {
                              jQuery('.can_synonyms').prop('checked', false);
                            }


                            if (key == 'rhyming' && value == 'on') {
                              jQuery('.can_rhyming').prop('checked', true);
                              jQuery('.word-type').addClass('rhyming-on');
                            }

                            if (key == 'rhyming' && value == '') {
                              jQuery('.can_rhyming').prop('checked', false);
                              jQuery('.word-type').removeClass('rhyming-on');
                            }
                          });
                        });

                        // jQuery('.int_cancel_btn').click(function() {
                        //   var int_filter = echo json_encode($int_filter); ;
                        //   var int_grammer = echo json_encode($int_grammer); ;
                        //   jQuery('.grammar0 , .grammar1 , .grammar2 , .grammar3 , .grammar4').prop('checked', false);
                         
                        //   jQuery.each( int_filter, function( key, value ) {

                        //      if(key == 'int_position' && value == 'both') { jQuery('.can_after,.can_before').prop('checked',true); }

                        //      if(key == 'int_position' && value == 'after'){ 
                        //       jQuery('.can_after').prop('checked',true); 
                        //       jQuery('.can_before').prop('checked',false); 
                        //      }

                        //      if(key == 'int_position' && value == 'before'){ 
                        //       jQuery('.can_before').prop('checked',true); 
                        //       jQuery('.can_after').prop('checked',false); 
                        //      }

                        //   });

                        //   jQuery.each(int_grammer, function(ke, val) {
                        //     jQuery("input[value='" + val + "']").prop('checked', true);
                        //   });
                        // });
                   
                      });


                      jQuery(document).on('click','.social_share_link',function(e) {
                        e.preventDefault();
                        var url = '<?php echo 'https://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI']; ?>';
                        var surl = '';
                        if (jQuery(this).hasClass('social_facebook_link')) {
                          window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank');
                        } else if (jQuery(this).hasClass('social_twitter_link')) {
                          window.open('https://twitter.com/home?status=Hey,%20check%20out%20these%20amazing%20business%20name%20ideas%20' + url, '_blank');
                        } else if (jQuery(this).hasClass('social_linkedin_link')) {
                          window.open('https://www.linkedin.com/shareArticle?mini=true&title=Business%20Name%20Generator&url=' + url, '_blank');
                        } else {

                        }
                      });

                      /**
                       *
                       *Generator V2 code script starts
                       *
                       *
                       */


                      jQuery(window).resize(function() {

                        eleBgClass();

                        new_eng_filters();

                        jQuery(document).on('click','.dropdown-main .cstm-dropdown2',function(){
                          if (jQuery(window).width() > 767) {
                            if (jQuery('.busi-filter .savedideas-drop-data li').length == 0) {
                              jQuery('.empty-idea-msg').remove();
                              jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery('.busi-filter .savedideas-drop-data').toggle();
                            jQuery(this).toggleClass('drop-opened');
                          } else {
                            if (jQuery('#myModalSaved .savedideas-drop-data li').length == 0) {
                              jQuery('.imgloadingshow').show();
                              jQuery('#myModalSaved .empty-idea-msg').remove();
                              jQuery('#myModalSaved .savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                              jQuery('.imgloadingshow').hide();
                            }
                            jQuery("#myModalSaved").modal('show');
                          }
                        });
                      });


                      // function handleOutboundLinkClicks(event, filter) {
                      //   __gaTracker('send', 'event', {
                      //     eventCategory: 'ResultsPage',
                      //     eventAction: 'IndustryFilters',
                      //     eventLabel: filter,
                      //     transport: 'beacon'
                      //   });
                      // }

                      // function handleOutboundAnchorLinkClicks(event, DomainName) {
                      //   __gaTracker('send', 'event', {
                      //     eventCategory: 'ResultsPage',
                      //     eventAction: 'Domainify Ad',
                      //     eventLabel: DomainName,
                      //     transport: 'beacon'
                      //   });
                      // }

                      jQuery(document).ready(function($) {

                        eleBgClass();

                        new_eng_filters();

                        window.addEventListener("orientationchange", function() {
                          if (window.orientation == 0 || window.orientation == 90 || window.orientation == -90) {
                            eleBgClass();
                          }
                        }, false);

                          setTimeout(function() {
                            if( jQuery('.indus-wrap').find('li:not(.all-indus-check) input:checked').length > 0 ){
                                jQuery('.keywordmatching').attr('data-label','KeywordMatched');
                                jQuery('.keywordmatching').click();
                            }
                          }, 2500); 


                          <?php if (isset($_SESSION['keyMatching']) && !$page) : ?>
                            setTimeout(function() {
                                jQuery('.keywordmatching').attr('data-label','NoMatch');
                                jQuery('.keywordmatching').click();
                            }, 2500); 

                          <?php endif; ?>

                        jQuery('.eng-ind-filter').find('.apply-btn').click(function(e) {
                          e.preventDefault();
                          var arr = [];
                          jQuery('.eng-ind-filter').find('.drop-down-data :checkbox').each(function() {
                            if (jQuery(this).is(":checked")) {
                              arr.push(jQuery(this).val());
                            }
                          });
                          var filter = arr.join(',');

                          //handleOutboundLinkClicks(jQuery(this), filter);

                          jQuery(this).parents('form').submit();

                        });

                        jQuery('.domain-buy .inner a').click(function(e) {
                          e.preventDefault();

                         // handleOutboundAnchorLinkClicks(jQuery(this), jQuery(this).data('domainname'));

                          window.open(jQuery(this).attr('href'), '_blank');

                        });

                        if (jQuery(window).width() > 767 && jQuery(window).width() < 992) {
                          jQuery('.fiter-drop.fltr-ipd button').click(function(e) {
                            e.preventDefault();
                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.mobile-filter').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery('.fiter-drop.tablet-new-filter button').addClass('hidden-btn');

                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.mobile-filter').addClass('hide-me');
                            }
                          });
                        }

                        if (jQuery(window).width() > 767 && jQuery(window).width() < 992) {
                          jQuery('.fiter-drop.tablet-new-filter button').click(function(e) {
                            e.preventDefault();

                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.domain-filters').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.mobile-filter').addClass('hide-me');
                              jQuery('.fiter-drop.fltr-ipd button').addClass('hidden-btn');

                              jQuery('.btn-flitr').click(function(e) {
                                e.preventDefault();
                                jQuery(this).parents('form').submit();
                              });
                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                            }
                          });
                        }

                        jQuery('.selected_industry .icon_close').click(function() {
                          var form1 = jQuery(this).parents('form');
                          jQuery(this).parents('.applyed-fl').remove();
                          setTimeout(function() {
                            form1.submit();
                          }, 500);
                        });

                          jQuery(document).on('click','.eng-industry-filter button',function(e){
                           
                            e.preventDefault();
                            
                            // Previous Filter Closed 
                            if( jQuery('.cstm-dropdown2').hasClass('drop-opened') ){
                              jQuery('.cstm-dropdown2').removeClass('drop-opened');
                              jQuery('.cstm-dropdown2').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                              jQuery('.savedideas-drop-data').hide(); 
                            }
                            if( jQuery('.tlds-dropdown').hasClass('opened') ){
                              jQuery('.tlds-dropdown').removeClass('opened').hide();
                              jQuery('.extensions-dropdown').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            }
                          
                          
                            if (jQuery(this).hasClass('hidden-btn')) {
                            
                              jQuery('.eng-ind-filter').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery('.eng-other-filter button').addClass('hidden-btn');
                              jQuery('.eng-other-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down'); 

                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery(this).find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            } else {
                             
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.eng-ind-filter').addClass('hide-me');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up'); 
                             
                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            }
                          });

                          jQuery('.eng-other-filter button').click(function(e) {
                            
                            e.preventDefault();
                            
                            // Previous Filter Closed 
                            if( jQuery('.cstm-dropdown2').hasClass('drop-opened') ){
                              jQuery('.cstm-dropdown2').removeClass('drop-opened');
                              jQuery('.cstm-dropdown2').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                              jQuery('.savedideas-drop-data').hide(); 
                            }
                            if( jQuery('.tlds-dropdown').hasClass('opened') ){
                              jQuery('.tlds-dropdown').removeClass('opened').hide();
                              jQuery('.extensions-dropdown').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            }
                            
                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.domain-filters').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.eng-ind-filter').addClass('hide-me');
                              jQuery('.eng-industry-filter button').addClass('hidden-btn');
                              jQuery('.eng-industry-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down'); 

                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.eng-ind-filter .live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up'); 
                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            }

                          });

                        var grammar = [<?php if (isset($_GET['grammar'])) : foreach ($_GET['grammar'] as $key => $value) {
                                            if (end($_GET['grammar']) != $value) {
                                              echo '"' . trim($value) . '",';
                                            } else {
                                              echo '"' . trim($value) . '"';
                                            }
                                          }
                                        endif; ?>];
                        if (grammar.length > 0) {
                          for (var i = 0; i < grammar.length; i++) {
                            jQuery('.grammar-drop-down-data .live-search-list, #myModalGrammer .live-search-list').find('input').each(function() {
                              if (jQuery(this).val() == grammar[i]) {
                                jQuery(this).attr('checked', 'checked');
                              }

                            });
                          }
                        }


                        jQuery('.domain-filters .ins-keyword input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });

                        jQuery('.busi-filter .mobile-filter input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });
                       
                        setTimeout(function() {

                          if (jQuery.cookie('savedideas')) {
                            jQuery('.savedideas-drop-data ul').html('');
                            var s = JSON.parse(jQuery.cookie('savedideas'));
                            var arr = jQuery.unique(s.sort()).sort();
                            if (arr.length > 0) {
                              jQuery.each(arr, function(key, value) {
                                var dt = value.replace(' ', '+');
                                dt = jQuery.trim(dt);
                                
                                savedideas_list(key,value,dt,'domains-ResultsPage','DomainAvailabilityCheck ','Register-SavedIdeas');
                            
                              });

                              jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');
                              jQuery('.cstm-dropdown2 .idea-count').text(Object.keys(s).length).show().prev('span').removeClass('icon_star_alt').addClass('icon_star');
                              jQuery('.industry-sub-right').addClass('added-saved-ideas');

                              for (var i = 0; i < arr.length; i++) {
                                jQuery('.list-star').find('[data-string="' + arr[i] + '"]').next().addClass('clicked-link');
                                jQuery('.list-star').find('[data-string="' + arr[i] + '"]').next().find('span').removeClass('icon_star_alt').addClass('icon_star').addClass('click');
                              }
                            } else {
                              jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                            }
                          } else {
                            jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                          }
                        
                        }, 3000);
                     
                        jQuery('.sliderRange').asRange({
                          min: 2,
                          max: 15,
                          value: 15,
                          range: false,
                          limit: false,
                          keyboard: true,
                          format: function(value) {
                            return (value == 15) ? value + "+" : value;
                          }
                        });



                        jQuery(document).on('click','.dropdown-main .cstm-dropdown2',function(){

                          
                          // Previous Filter Closed
                          if( jQuery('.tlds-dropdown').hasClass('opened') ){
                            jQuery('.tlds-dropdown').removeClass('opened').hide();
                            jQuery('.extensions-dropdown').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                          }

                          if( !jQuery('.eng-industry-filter button').hasClass('hidden-btn') ){
                            jQuery('.eng-industry-filter button').addClass('hidden-btn'); 
                            jQuery('.eng-ind-filter').addClass('hide-me');
                            jQuery('.eng-industry-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                          }

                          if( !jQuery('.eng-other-filter button').hasClass('hidden-btn') ){
                            jQuery('.eng-other-filter button').addClass('hidden-btn');
                            jQuery('.domain-filters').addClass('hide-me');
                            jQuery('.eng-other-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                          }

                          if (jQuery(window).width() > 1024) {
                            if (jQuery('.busi-filter .savedideas-drop-data li').length == 0) {
                              jQuery('.empty-idea-msg').remove();
                              jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery('.busi-filter .savedideas-drop-data').toggle();
                            
                            if(jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').hasClass('arrow_carrot-down') ){
                              jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down');
                            }else{
                              jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            } 
                            jQuery(this).toggleClass('drop-opened');
                          } else {
                            if (jQuery('#myModalSaved .savedideas-drop-data li').length == 0) {
                              jQuery('#myModalSaved .empty-idea-msg').remove();
                              jQuery('#myModalSaved .savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery("#myModalSaved").modal('show');
                          }
                        });
                        
                        jQuery('.extensions-dropdown').click(function() {
                          

                          // Previous Filter Closed
                          if( !jQuery('.eng-industry-filter button').hasClass('hidden-btn') ){
                            jQuery('.eng-industry-filter button').addClass('hidden-btn'); 
                            jQuery('.eng-ind-filter').addClass('hide-me');
                            jQuery('.eng-industry-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                          }

                          if( !jQuery('.eng-other-filter button').hasClass('hidden-btn') ){
                            jQuery('.eng-other-filter button').addClass('hidden-btn');
                            jQuery('.domain-filters').addClass('hide-me');
                            jQuery('.eng-other-filter').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                          }
                          
                          if( jQuery('.cstm-dropdown2').hasClass('drop-opened') ){
                            jQuery('.cstm-dropdown2').removeClass('drop-opened');
                            jQuery('.cstm-dropdown2').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            jQuery('.savedideas-drop-data').hide(); 
                          } 

                          if( jQuery('.tlds-dropdown').hasClass('opened') ){
                            jQuery('.tlds-dropdown').removeClass('opened')
                            jQuery(this).find('.count-down .arrow_carrot-up').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            jQuery('.tlds-dropdown').hide();
                          }else{
                            jQuery(this).find('.count-down .arrow_carrot-down').removeClass('arrow_carrot-down').addClass('arrow_carrot-up');
                            jQuery('.tlds-dropdown').addClass('opened')
                            jQuery('.tlds-dropdown').show();
                          } 
                          
                        });

                        jQuery('.sliderRange').on('asRange::change', function(e) {
                          jQuery('.character').val();
                        });

                        // jQuery('.busi-filter .sliderRange.deskslide').next().find(".asRange-pointer").on('asRange::moveEnd', function (e) {
                        //   jQuery(this).parents('form').submit();
                        // });

                        jQuery('.send-or-email').click(function() {
                          jQuery('body #myModalShare').find('.sendids').html('');
                          jQuery('.busi-filter .savedideas-drop-data li').each(function() {
                            jQuery('#myModalShare #saved_id .sendids').append(jQuery(this).find('.ids'));
                          });
                        });

                        // jQuery('.cstm-dropdown, .cstm-dropdown2').focusout(function(){
                        //   jQuery('.drop-down-data, .savedideas-drop-data').hide();
                        // });

                        jQuery('.dropdown-main .share-saved').click(function() {
                          var idea = jQuery(this).prev('input');
                          jQuery('.append-share-idea').html('').append(idea);
                        });

                        // jQuery('.live-search-list li').each(function() {
                        //   jQuery(this).attr('data-search-term', jQuery(this).find(':checkbox').val().toLowerCase());
                        // });

                    
                      });

                      jQuery('.drop-down-data .btn.clear').click(function() {
                          jQuery('.drop-down-data :checkbox').removeAttr('checked');
                          jQuery("input[type='hidden'][name='all-indus']").remove();
                      });

                      jQuery('.tlds-cancel').click(function() {
                          jQuery('.tlds-dropdown').removeClass('opened').hide();
                          jQuery('.extensions-dropdown').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                      });

                  

                      jQuery('.mobile-filter .btn.clear').click(function() {
                        jQuery('.mobile-filter .live-search-list :checkbox').removeAttr('checked');
                      });

                      jQuery('#main-filter-modal').on("shown.bs.modal", function() {
                        new_eng_filters();
                      });

                
                        jQuery('.eng-ind-filter .btn.cancel').click(function() {

                          jQuery('.eng-industry-filter button').addClass('hidden-btn');
                          jQuery('.eng-ind-filter').addClass('hide-me');

                          jQuery('.eng-ind-filter').find('.live-search-list li input').removeAttr('checked');

                          jQuery('.applyed-fl').each(function() {
                            var checked = jQuery(this).text();
                            jQuery('.eng-ind-filter').find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                          })

                          jQuery('.eng-ind-filter').find('.search_bar').find('input').val("");
                          jQuery('.eng-ind-filter').find('.live-search-list li').show();
                        });

                        jQuery('.domain-filters .btn.cancel').click(function() {
                          jQuery('.eng-other-filter button').addClass('hidden-btn');
                          jQuery('.domain-filters').addClass('hide-me');

                          jQuery('.applyed-fl').each(function() {
                            var checked = jQuery(this).text();
                            jQuery('.domain-filters').find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                          })

                          jQuery('.domain-filters').find('.search_bar').find('input').val("");
                          jQuery('.domain-filters').find('.live-search-list li').show();
                        });

                        jQuery('.mobile-filter .btn.cancel').click(function() {
                          jQuery('.mobile-filter').toggle();
                        });

                    
                      jQuery('#myModal').on("shown.bs.modal", function() {
                        jQuery('#myModal .clear').click(function() {
                          jQuery('.domains-list :checkbox').removeAttr('checked');
                        });
                      });

                      jQuery('#myModalGrammer').on("shown.bs.modal", function() {
                        jQuery('#myModalGrammer .clear').click(function() {
                          jQuery('#myModalGrammer .live-search-list :checkbox').removeAttr('checked');
                        });
                      });

                      jQuery('#myModalDomains').on("shown.bs.modal", function() {
                        jQuery("body #myModalDomains").find('.imgloadingshow').show();

                        setTimeout(function() {
                          jQuery("#myModalDomains").find('.imgloadingshow').hide();
                        }, 1000);
                      });

                      
                        var url = '<?php echo 'https://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI']; ?>';
                        var surl = encodeURIComponent(url);
                        jQuery(document).on('click','.social_whatsapp_link',function(e) {
                          e.preventDefault();
                          if (window.innerWidth > 767) {
                            jQuery(this).parents('li').find('#whatsapp').attr('href', 'https://web.whatsapp.com/send?text=' + surl).text('https://web.whatsapp.com/send?text=' + surl);
                            jQuery(this).parents('li').find('#whatsapp')[0].click();

                          } else {
                            jQuery(this).parents('li').find('#whatsapp').attr('href', 'https://api.whatsapp.com/send?text=' + surl).text('whatsapp://send?text=' + surl);

                            jQuery(this).parents('li').find('#whatsapp')[0].click();
                          }

                          // setTimeout(function(){
                          //   jQuery('#whatsapp').click();
                          // },800);
                        });

                      

                      jQuery('.list-star li:nth-child(4n)').addClass('openafter');

                    
                      /*
                      On click result check domain 
                      */
                      jQuery.xhrPool = []; // array of uncompleted requests
                      jQuery.xhrPool.abortAll = function() { // our abort function
                        jQuery(this).each(function(idx, jqXHR) {
                          jqXHR.abort();
                        });
                        jQuery.xhrPool.length = 0
                      };
                      jQuery(document).on('click','.ntbs',function(e){

                        
                        jQuery('.list-star #results').remove();
                        var nameidea = jQuery(this).data('string');
                        nameidea = nameidea.replace(/\s/g, '');
                        nameidea = nameidea.toLowerCase();

                        jQuery.xhrPool.abortAll();
                        if (!jQuery(this).hasClass('checking')) {
                          jQuery('.stardomain.checked .ntbs.click-event.checking').trigger('click');
                        
                          jQuery(this).addClass('checking');
                          jQuery('.list-star li').removeClass('checked');
                          jQuery(this).parents('li').addClass('checked');


                          var domainslist = '';
                          var index = jQuery(this).parents('li').index();
                          var el = jQuery(this);
                          var cls = '';
                          switch (index) {
                            case 0:
                              cls = 'domain-click-0';
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;
                            case 1:
                              cls = 'domain-click-1';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;
                            case 2:
                              cls = 'domain-click-2';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;

                            case 3:
                              cls = 'domain-click-3';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              el.parents('.pad_set').addClass(cls);
                              break;
                          }
                          
                             
                          jQuery('.list-star #results .imgloadingshow').show();

 
                          //jQuery(".list-star").find(".innerResult").prepend(' echo $advert; ?>');
                          //}


                          if(!jQuery(this).parents('.inner').find('.clickable-arrow').hasClass('clicked-link')){
                            jQuery(this).parents('.inner').find('.clickable-arrow').trigger('click');
                          }
                        } else {
                          
                          jQuery(this).removeClass('checking');
                          jQuery(this).parents('li').removeClass('checked');

                        }
                      });

                      /*
                       * Generator v2 Save  ideas on star
                       * click
                       * 
                       **/

                      var counter = <?php echo (isset($_SESSION['invite_modal_value'])) ? count($_SESSION['invite_modal_value']) : '0'; ?>;
                      
                      
                      // jQuery(document).on('click','.tracking_event',function(e){
                                             
                      //   var data-label = jQuery(this).attr('data-label');
                      //  __gaTracker('send', 'event', 'ResultsPage', 'DomainAvailabilityCheck', ''+data-label+'');


                      // });


                      function setSelectedTestPlan(name, number, obj) {

                        //__gaTracker('send', 'event', 'ResultsPage', 'SavedIdeas', 'SaveStar');
                        

                        if (counter == 0) {
                          jQuery('.savedideas-drop-data ul').removeClass('ideas-added');
                        }

                        if (jQuery(obj).parents('.stardomain').find("span.elegant-icon").hasClass("click")) {
                           
                          counter = counter - 1;
                          var count = jQuery('.dropdown-main .idea-count').length;
                          (counter == 0) ? jQuery('.dropdown-main .idea-count').hide(): '';
                          var liden = 'li#' + number;
                          var neiurl = '<?php bloginfo('template_url'); ?>';
                          //jQuery("#imgloadingshow").show();
                          jQuery(obj).parents('.stardomain').find("span.elegant-icon").removeClass("icon_star").addClass("icon_star_alt").removeClass('click');

                          jQuery(obj).parents('.stardomain').find("a.result_" + number).removeClass("clicked-link");
                         
                          jQuery.ajax({
                            type: "POST",
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            data: {
                              action: 'ajax_con',
                              id: number,
                              name: name,
                              active: '0'
                            },
                            cache: true,
                            success: function(result) {
                              jQuery(obj).parents('.stardomain').find(".result_" + number).removeAttr("disabled", false);
                              //jQuery("#imgloadingshow").hide();
                              jQuery(obj).parents('.stardomain').find(".result_" + number).removeClass("click");
                              jQuery("#" + number).remove();
                              var arr = [];

                              //jQuery('#savedideas').find(liden).remove();
                              jQuery('.savedideas-drop-data ul').html('');
                              jQuery.each(jQuery.parseJSON(result), function(key, value) {
                             
                                arr.push(key);
                              });

                              if (jQuery.cookie('savedideas')) {
                                var old_json_str = jQuery.cookie('savedideas');
                                var newarr = JSON.parse(old_json_str);

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(newarr.sort()).sort();

                                var filtered = savearr.filter(function(value, index, arr) {

                                  return value !== name;

                                }, name);

                                if (filtered.length > 0) {
                                  jQuery.removeCookie('savedideas');
                                  var json_str = JSON.stringify(filtered);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });
                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  jQuery.each(filtered, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt,'domains-ResultsPage','DomainAvailabilityCheck ','Register-SavedIdeas');
                                  });

                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(filtered.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');
                                } else {
                                  jQuery.removeCookie('savedideas');
                                  jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                                  // jQuery('.cstm-dropdown2 .idea-count').hide();
                                  // jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  // jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?php //echo __('click the <span class="icon_star_alt elegant-icon"></span> to save ideas', 'thegem-child'); ?>');
                                  // jQuery('.cstm-dropdown2 span.eicon_star').removeClass('icon_star').addClass('icon_star_alt');
                                }
                              } else {
                                var json_str = JSON.stringify(arr);
                                jQuery.cookie('savedideas', json_str, {
                                  expires: 15
                                });

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(arr.sort()).sort();

                                jQuery.each(arr, function(key, value) {
                                  var dt = value.replace(' ', '+');
                                  dt = jQuery.trim(dt);

                                   savedideas_list(key,value,dt,'domains-ResultsPage','DomainAvailabilityCheck ','Register-SavedIdeas');
                                });
                                jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(savearr.length);
                              }

                              if (jQuery('.savedideas-drop-data li').length == 0) {
                                jQuery('.cstm-dropdown2 .idea-count').hide();
                                jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');

                                jQuery('.cstm-dropdown2 span.icon_star').removeClass('icon_star').addClass('icon_star_alt');
                              } else {
                                jQuery('.empty-idea-msg').remove();
                                jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                //jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(Object.keys(jQuery.parseJSON(result)).length);
                              }

                              jQuery('.savedideas-drop-data .icon_close').click(function() {
                                var value = jQuery(this).data('remove-item');

                                if (arr.length == 0) {
                                  //jQuery('.savedideas-drop-data').hide();
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                }
                              });
                            }
                          });

                        } else {
                          if (counter < 100) {
                            var neiurl = '<?php bloginfo('template_url'); ?>';
                            jQuery(obj).parents('.stardomain').find(".elegant-icon.result_" + number).attr('disabled', 'disabled').removeClass("icon_star_alt").addClass("icon_star").addClass("click");

                            jQuery(obj).parents('.stardomain').find("a.result_" + number).addClass("clicked-link");
                           
                            //jQuery("#imgloadingshow").show();
                            jQuery.ajax({
                              type: "POST",
                              url: '<?php echo admin_url('admin-ajax.php'); ?>',
                              data: {
                                action: 'ajax_con',
                                id: number,
                                name: name,
                                active: '1'
                              },
                              cache: true,
                              success: function(result) {
                                counter += 1;
                                //jQuery("#imgloadingshow").hide();
                                jQuery('.savedideas-drop-data ul').html('');
                                var arr = [];
                                jQuery.each(jQuery.parseJSON(result), function(key, value) {
                                  arr.push(key);
                                });

                                  var json_str = JSON.stringify(arr);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });

                                  jQuery('.savedideas-drop-data ul').html('');
                                  savearr = jQuery.unique(arr.sort()).sort();

                                  jQuery.each(arr, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt,'domains-ResultsPage','DomainAvailabilityCheck ','Register-SavedIdeas');
                                  });
                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(savearr.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');

                              


                                if (jQuery('.savedideas-drop-data li').length == 0) {
                                  //jQuery('.cstm-dropdown2 .idea-count').hide();
                                  jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                } else {
                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  // jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(Object.keys(jQuery.parseJSON(result)).length);
                                  jQuery('.cstm-dropdown2 span.icon_star_alt').removeClass('icon_star_alt').addClass('icon_star');
                                }

                                jQuery('.savedideas-drop-data .icon_close').click(function() {
                                  var value = jQuery(this).data('remove-item');

                                  if (Object.keys(jQuery.parseJSON(result)).length == 0) {
                                    //jQuery('.savedideas-drop-data').hide();
                                    jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                  }
                                });
                                // jQuery('#savedideas').append("<li id='"+number+"'><span class='"+name+" savespan elegant-icon icon_close' onClick=newremove('"+number+"')></span><input type='checkbox' class='ids' name='list[]' hidden='' value='"+name+"' checked='checked'>"+name+"</li>");

                                // if(window.innerWidth<768){
                                //     if(jQuery('#savedideas li').length==1){
                                //       alert('Find all your saved ideas at the bottom of the page');   
                                //     }
                                // }
                                
                              }
                            });
                          }
                        }
                      }

                      /*
                       * Generator v2 removeSavedResult 
                       * 
                       **/

                     function removeSavedResult(obj)
                      {
                       // jQuery(obj).parents('li').remove();
                        var neiurl = '<?php bloginfo('template_url'); ?>';
                        if (jQuery('.savedideas-drop-data li').length == 0) {
                          jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                          jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                        }

                        var value = jQuery(obj).parents('li').data('kname');
                       

                        if (value) {
                          var newvalue = value.replace('+', ' ');
                        }
                        jQuery.ajax({
                          type: "POST",
                          url: '<?php echo admin_url('admin-ajax.php'); ?>',
                          data: {
                            action: 'ajax_con',
                            id: "",
                            name: newvalue,
                            active: '0'
                          },
                          cache: true,
                          success: function(result) {
                            if (result.length > 0) {
                              jQuery("[data-domain='" + newvalue + "']").find('.click').removeClass("click");
                              jQuery("[data-domain='" + newvalue + "']").find('span.elegant-icon').removeClass('icon_star').addClass('icon_star_alt');
                              
                              jQuery('.savedideas-drop-data ul').html('');




                              if (jQuery.cookie('savedideas')) {
                                var old_json_str = jQuery.cookie('savedideas');
                                var newarr = JSON.parse(old_json_str);

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(newarr.sort()).sort();

                                var filtered = savearr.filter(function(value, index, arr) {
                                  return value !== newvalue;
                                }, newvalue);

                                if (filtered.length > 0) {
                                  jQuery.removeCookie('savedideas');
                                  var json_str = JSON.stringify(filtered);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });

                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  jQuery.each(filtered, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt,'domains-ResultsPage','DomainAvailabilityCheck ','Register-SavedIdeas');
                                  });

                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(filtered.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');

                                } else {

                                  jQuery.removeCookie('savedideas');
                                  jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');

                                  jQuery('.cstm-dropdown2 .idea-count').hide();
                                  jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                  
                                }
                              } else {
                                jQuery('.cstm-dropdown2 .idea-count').hide();
                                jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                              }

                            }
                          }
                        });

                      }

                      jQuery(document).on('submit, click','.sharemail',function(e){
                        e.preventDefault();

                        
                        var email = jQuery(".email-input").val();
                     
                        if (jQuery('#saved_id .savedideas-drop-data ul li').length < 1 ) {
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            "<p><?php echo addslashes($translations->emailerror1 ); ?></p>" +
                            '</div>');
                          return false;
                        
                        } else if (email == '') {
                          
                          <?php $share_id_error = str_replace("\\",'',$translations->emailerror2); ?>
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            '<p><?php echo addslashes($share_id_error); ?></p>' +
                            '</div>');
                          return false;
                        } else if (!isValidEmailAddress(jQuery(".email-input").val())) {
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            "<p><?php echo addslashes($translations->emailerror3); ?></p>" +
                            '</div>');
                          return false;
                        } else {
                          var list = [];
                          jQuery('#saved_id  input[name*="list[]"]').each(function() { 
                            list.push(jQuery(this).val());
                          });
                        
                          
                          jQuery('#idearmailbtn').attr("disabled", true);
                          jQuery("#mailsendgif").show();
                          jQuery.ajax({
                            type: "POST",
                            url : '<?php echo admin_url('admin-ajax.php'); ?>',
                            data: {action: 'send_saved_ideas',
                                   list: list,
                                   email:jQuery('#saved_id .email-input').val(),
                                   },
                            success: function(response) {
                                jQuery('#idearmailbtn1').removeAttr("disabled", false);
                                jQuery("#mailsendgif").hide();
                                
                                if(response == "success")
                                {
                                 
                                 
                                 jQuery(".responsebar").html('').append('<div class="alert alert-success">' +
                                   '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                                   "<p><?php echo isset($translations->sucessmessage) ? $translations->sucessmessage : 'Your ideas have been sent to your inbox...'; ?></p>" +
                                   '</div>');

                                }else{
                                 
                                   jQuery(".responsebar").html(response).show();
                                }
                            }
                          });
                        }
                      });
                      jQuery('#semail').keypress(function(e) {
                        if (e.which == 13) { //Enter key pressed
                          e.preventDefault();
                          e.stopPropagation();
                          jQuery('#idearmailbtn').click(); //Trigger search button click event
                        }
                      });

                      function isValidEmailAddress(emailAddress) {
                        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
                        return pattern.test(emailAddress);
                      }

                      jQuery('.domain-filters .btn-flitr').click(function(e) {

                        if (!jQuery('.oneword, .twoword, .rhyming input[type=checkbox], .synonyms input[type=checkbox]').is(':checked')) {

                          jQuery('.oneword, .twoword').prop("checked", true);
                          jQuery('.one-word, .two-word').val('on');
                        }
                        jQuery('this').parents('form').submit();
                      });

                      function new_eng_filters() {
                        jQuery('.oneword').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.oneword').prop("checked", false);
                            jQuery('.one-word').val('off');
                          } else {
                            jQuery('.one-word').val('on');
                          }
                        });

                        jQuery('.twoword').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.twoword').prop("checked", false);
                            jQuery('.two-word').val('off')
                          } else {
                            jQuery('.two-word').val('on');
                          }

                        });

                        jQuery('#main-filter-modal').find('.ins-keyword input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });

                        jQuery('.ins-keyword input[type=checkbox]').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                            jQuery('.pos').val('before');
                            setTimeout(function() {
                              jQuery('.filter.desk').find('form').submit();
                            }, 500);

                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                            jQuery('.pos').val('after');
                            setTimeout(function() {
                              jQuery('.filter.desk').find('form').submit();
                            }, 500);

                          }
                        });

                        jQuery('.busi-filter .filter.desk .synonyms input[type=checkbox] , #main-filter-modal .synonyms input[type=checkbox]').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Off', 'thegem-child'); ?>');
                            jQuery('.synonyms-val').val('off');

                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('On', 'thegem-child'); ?>');
                            jQuery('.synonyms-val').val('on');
                            jQuery('.rhyming-val').val('off');
                            if (jQuery('.one-word').val() === "on" && jQuery('.two-word').val() === "off") {
                              jQuery('.one-word').val('off');
                              jQuery('.two-word').val('off');
                            }
                          }
                        });

                        jQuery('.rhyming input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Off', 'thegem-child'); ?>');
                            jQuery('.rhyming-val').val('off');
                            jQuery('.oneword, .twoword').removeAttr('disabled');
                            jQuery('.oneword, .twoword').prop("checked", true);
                            jQuery('.one-word, .two-word').val('on');
                            jQuery('.word-type').removeClass('rhyming-on');
                            jQuery('.synonyms-val').val('on');
                            jQuery('.rhyming-val').val('off');
                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('On', 'thegem-child'); ?>');
                            jQuery('.rhyming-val').val('on');
                            jQuery('.oneword').prop("checked", false).attr('disabled', 'disabled');
                            jQuery('.twoword').prop("checked", true).attr('disabled', 'disabled');
                            jQuery('.one-word, .two-word').val('off');
                            jQuery('.synonyms-val').val('off');
                            jQuery('.word-type').addClass('rhyming-on');
                            jQuery('.synonyms-val').val('off');
                            jQuery('.rhyming-val').val('on');
                          }
                        });

                      }

                      function eleBgClass() {
                        jQuery('.list-star').children('li').removeClass('bg');
                        if (window.innerWidth >= 992) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(3)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+9)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+10)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+11)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+12)').addClass('bg');
                        } else if (window.innerWidth < 992 && window.innerWidth >= 767) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(3)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+7)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+8)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+9)').addClass('bg');
                        } else if (window.innerWidth < 768 && window.innerWidth >= 480) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4n+5)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4n+6)').addClass('bg');
                        } else {
                          jQuery('.list-star').children('li:nth-child(2n+1)').addClass('bg');
                        }
                      }


                      jQuery.xhrPool = []; // array of uncompleted requests
                      jQuery.xhrPool.abortAll = function() { // our abort function
                        jQuery(this).each(function(idx, jqXHR) {
                          jqXHR.abort();
                        });
                        jQuery.xhrPool.length = 0
                      };

                     

                      jQuery('#Search').keypress(function(e) {
                        if (e.keyCode === 8) {
                          return true;
                        }
                        var regex = new RegExp("^[\x7f-\xffa-zA-Z0-9\-\.]+$");
                        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                        if (regex.test(str)) {
                          return true;
                        }
                        if (e.which == 13) {
                          jQuery(this).submit();
                          return false;
                        }

                        e.preventDefault();
                        return false;
                      });
                      
                      jQuery(document).ready(function(){

                         <?php 
                               $bname  =  !empty($_GET['bname']) ? $_GET['bname'] : ''; 
                               $home_id = !empty($_GET['home_id']) ? $_GET['home_id'] : '';
                               $btn_url = '/?home_id='.$home_id.'&bname='.$bname; 

                               $country_code = getenv( 'HTTP_GEOIP_COUNTRY_CODE' );
                               $device = !empty($_GET['device']) ? $_GET['device'] : ''; 
                               $extra_parameter = '-'.$device.'-'.$country_code.getGclidParam(); 
                               
                          ?> 

                         jQuery('.result-page-menu').click(function(e){
                           e.preventDefault();
                           var pre = jQuery(this).find('a').attr('title');
                           var btn_link = '/'+pre+'<?= $btn_url; ?>';
                           window.open(btn_link);
                         }); 
                         
                         jQuery(document).on('click','.tld-links',function(){
                            jQuery('.add-event').attr('data-category',jQuery(this).data('category'));
                            jQuery('.add-event').attr('data-action',  jQuery(this).data('action'));
                            jQuery('.add-event').attr('data-label',   jQuery(this).data('label'));
                            jQuery('.add-event').click();
                         });

                         jQuery('.pure-form').on('submit',function(e){
                              
                              jQuery('.add-event').attr('data-category','domains-ResultsPage');
                              jQuery('.add-event').attr('data-action','DomainAvailabilityCheck');
                              jQuery('.add-event').attr('data-label','Register-DomainChecker');
                              jQuery('.add-event').click();

                              
                              var extra_parameter = '<?= $extra_parameter ?>';
                              var home_id = '<?= $home_id ?>';
                              var tld = '<?= !empty($_GET['tlds']) ? $_GET['tlds'] : 'com';   ?>'; 
                              
                              var search = jQuery(this).find('#Search').val();
                              if(search !=  ''){
                              }else{
                                search = '<?= $bname; ?>';
                              }
                              
                              <?php 
                              
                              if( ( !empty($aff_domains->registerlinkdropdown) &&  !empty( $aff_domains->registerlinkdropdown->url ) )  ){
                                $registerlink = $registerlink->url; 
                              }
                              ?>
                              
                              <?php if( !empty($registerlink) ) :  ?>
                                
                                var btn_link = '<?= $registerlink ?>?sid=checker-register-post-'+home_id+extra_parameter+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+search+'.'+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate';

                              <?php else : ?>
                               
                                var btn_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=checker-register-post-'+home_id+extra_parameter+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+search+'.'+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate';
                              
                              <?php endif; ?>
                              
                              
                              
                              e.preventDefault();

                              jQuery('.add-event').after('<a class="open_link" href="'+btn_link+'" target="_blank">Search</a>');
                              jQuery('.open_link')[0].click();
                              jQuery('.open_link').remove();

                              return false; 
                         });
                         
                         jQuery('.open-affiliate-link').click('.open-affiliate-link',function(e){
                        
                           e.preventDefault()
                           var search = '<?= $bname; ?>';
                           open_affiliate(search);
                         });

                         function open_affiliate(search){

                          var extra_parameter = '<?= $extra_parameter ?>';
                          var home_id = '<?= $home_id ?>';
                          var tld = '<?= !empty($_GET['tlds']) ? $_GET['tlds'] : 'com';   ?>'; 
                          

                          <?php 
                              
                              if( ( !empty($aff_domains->registerlinkdropdown) &&  !empty( $aff_domains->registerlinkdropdown->url ) )  ){
                                $registerlink = $registerlink->url; 
                              }
                            ?>
                              
                            <?php if( !empty($registerlink) ) :  ?>
                                
                                var btn_link = '<?= $registerlink ?>?sid=checker-register-post-'+home_id+extra_parameter+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+search+'.'+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate';

                            <?php else : ?>
                               
                              var btn_link = 'https://www.kqzyfj.com/click-3797283-15162961?sid=checker-register-post-'+home_id+extra_parameter+'&url=https%3A%2F%2Fwww.godaddy.com%2Fdomainsearch%2Ffind%3FdomainToCheck%3D'+search+'.'+tld+'%26checkAvail%3D1%26tmskey%3d1dom_03_affiliate';
                              
                            <?php endif; ?>
                          
                          window.open(btn_link);
                          var search = jQuery(this).find('#Search').val('');
                          return false;  

                         }
                      
                          
                         jQuery('#domain-checker-form').addClass("new-result-page");
                         

                         setTimeout(function(){ 

                            var page_id = "<?= !empty($_GET['home_id']) ? $_GET['home_id'] : '' ?>"; 
                           
                            <?php $domain_list = json_encode($domain_list); ?>;

                           
                            var data = {
                              'action': 'dc_display_domains_with_tld',
                              'domains_list': '<?= $domain_list;  ?>',
                              'page_id' : page_id,
                              'device' : Device_Detect(),
                              'lang': '<?php echo ICL_LANGUAGE_CODE; ?>',
                              'page_slug' : "<?php echo $page_slug  ?>",
                              'security': domain_checker_ajax.domain_checker_nonce
                            };

                            jQuery.ajaxSetup({
                              beforeSend: function(jqXHR) { // before jQuery send the request we will push it to our array
                                jQuery.xhrPool.push(jqXHR);
                              },
                              complete: function(jqXHR) { // when some of the requests completed it will splice from the array
                                var index = jQuery.xhrPool.indexOf(jqXHR);
                                if (index > -1) {
                                  jQuery.xhrPool.splice(index, 1);
                                }
                              }
                            });

                            jQuery.ajax({
                              type: 'post',
                              url: domain_checker_ajax.ajaxurl,
                              data: data,
                              success: function(response) {
         
                                response = jQuery.parseJSON(response);
                                var domains = jQuery.parseJSON(response['result']);
                                
                                <?php $registertext = str_replace("\\",'',$translations->domaincheckerregister); ?>                        
                                
                                jQuery.each(domains['domains'], function( index, value ) {
                                    
                                    var domain =  value['domain'];
                                    
                                    if( response['affiliate_link'] !== null  && response['affiliate_link'] !== ''  ){
                                     jQuery('.stardomain[data-domain*="'+domain+'"]').find('.tld-links').attr('href',response['affiliate_link']); 
                                    }
                                    
                                  
                                    
                                    if(!value['available']){
                                      var href = jQuery('.stardomain[data-domain*="'+domain+'"]').find('.tld-links').attr('href');
                                      href = href.replace("nameidea-register", "nameidea-whois");
                                      
                                      if( value['geo_country']  ){
                                        
                                        <?php  if( ( !empty($aff_domains->registerlinkdropdown) && !empty( $aff_domains->registerlinkdropdown->geo_sid ) && !empty( $aff_domains->registerlinkdropdown->geo_un_sid )  )  ) :
                                        ?>
                                           
                                           href = href.replace("<?= $aff_domains->registerlinkdropdown->geo_sid ?>", "<?= $aff_domains->registerlinkdropdown->geo_un_sid ?>");
                                           
                                           href = href.replace("nameidea-register", "nameidea-whois");
                                           
                                        <?php endif; ?> 

                                      }else{
                                           
                                        <?php  if( ( !empty($aff_domains->registerlinkdropdown) && !empty( $aff_domains->registerlinkdropdown->sid ) && !empty( $aff_domains->registerlinkdropdown->un_sid )  )  ) :
                                        ?>
                                          
                                          href = href.replace("<?= $aff_domains->registerlinkdropdown->sid ?>", "<?= $aff_domains->registerlinkdropdown->un_sid ?>");

                                          href = href.replace("nameidea-register", "nameidea-whois");
                                        
                                        <?php endif; ?>  

                                      }
                                      jQuery('.stardomain[data-domain*="'+domain+'"]').find('.tld-links').attr('href',href);
                                      jQuery('.stardomain[data-domain*="'+domain+'"]').addClass('who-is').find('.status').html('Whois<span><i class="fas fa-external-link-alt" aria-hidden="true"></i></span>');     
                                      jQuery('.stardomain[data-domain*="'+domain+'"]').find('.tld-links').attr('data-label','WHOIS-Results');
                                    }else{
                                      jQuery('.stardomain[data-domain*="'+domain+'"]').find('.status').html('<?= addslashes($registertext) ?><span><i class="fas fa-external-link-alt" aria-hidden="true"></i></span>');
                                    }

                                });

                                // if (response) {
                                //   jQuery.each(response, function(key, val) {
                                //     console.log(response[0]);
                                    
                                  

                                //   });
                                // }
                              }
                            });

                         }, 200);


                      });

                      jQuery(document).on('click','.navbar .navbar-toggler', function(){
                          
                        if( jQuery(window).width() < 768 ){  
                          if( jQuery(this).hasClass('opened') ){
                              var ele = jQuery(this);
                              jQuery(this).removeClass('opened').addClass('collapsed');
                                setTimeout(function(){
                                  jQuery(ele).next('div').removeClass('show');
                                  jQuery(ele).parent().parent('.navbar').removeClass('navbar-onscroll'); 
                              }, 400);
                          }else{
                            jQuery(this).addClass('opened').removeClass('collapsed');
                          }
                        }
                      }); 

                      /**
                       *
                       *Generator V2 code script ends
                       *
                       *
                       */
                    </script>
                    <?php get_footer(); 
                    include_once(dirname(__FILE__) . '/footer-modals.php'); 
                    include_once(dirname(__FILE__) . '/inc/Js-functions.php'); 

