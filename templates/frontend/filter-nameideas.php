<?php $translations = json_decode(get_option('bnwm_string_translations'));  ?>
<?php if( !empty($filter_options->enable_saved_ideas) && isset($filter_options->enable_saved_ideas->value) && $filter_options->enable_saved_ideas->value == 'true' ): ?>
<div class="drop-down-area">
    <?php $strURL = $_SERVER['REQUEST_URI']; $arrVals = explode("?", $strURL);?>
    <form id="saved_id" method="POST" action="<?php echo $arrVals[0]; ?>">
        <div class="dropdown-main update">
        <div class="cstm-dropdown2">
            
            <button type="button" class="btn btn-default">
            <div class="text">
                <span class="icon_star_alt elegant-icon"></span> 
                <p><?php echo __($filter_options->enable_saved_ideas->saved_ideas_title, 'business_name_word_manager'); ?></p>
            </div>
            
            <div class="count-down">
                <span class="idea-count" style="display:none"></span>
                <span class="arrow_carrot-down elegant-icon"></span>
            </div>
            </button>
        </div>
        <div class="savedideas-drop-data">
            <div class="row">
                <div class="col-md-12">
                <ul></ul>
                </div>
                <div class="col-md-12">
                <div class="save-ideas">
                        <p><?php echo __($filter_options->enable_saved_ideas->saved_ideas_subtitle, 'business_name_word_manager'); ?></p>
                        
                        <div class="responsebar">
                        </div>
                
                        <div class="input-main">
                        <input class="email-input" id="email" name="email" type="Email" data-popup-url="" data-active="" placeholder="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_placeholder, 'business_name_word_manager'); ?>">
                        <a href="javascript:void(0)" class="send sharemail"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?></a>
                        <input type="submit" class="send sharemail" value="<?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_send_text, 'business_name_word_manager'); ?>" style="display: none;">
                        </div>
                        <div class="form-group">
                        <input class="chekedomainid" name="marketing_email" type="checkbox" id="html">
                        
                        <label for="html"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_email_optin_notice, 'business_name_word_manager'); ?><a href="<?php echo $filter_options->enable_saved_ideas->saved_ideas_policy_url; ?>" rel="nofollow" target="_blank"><?php echo __($filter_options->enable_saved_ideas->saved_ideas_policy_text, 'business_name_word_manager'); ?></a></label>
                        </div>
                </div>
                </div>
            </div>
        </div>
        </div>
    </form>
</div>
<?php endif; ?>
<?php if( !empty($filter_options->enable_industry_filters) && isset($filter_options->enable_industry_filters->value) && $filter_options->enable_industry_filters->value == 'true' ): ?>
    <div class="elm indus-wrap">
        <?php $columns = [];
        if(!empty($selectedIndustries)){
            $columns = $selectedIndustries;
        } else {
            $columns = $_GET['industry'];
        }
        $inscolumns = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : array();
        ?>
        <div class="cstm-dropdown eng-industry-filter">
        
        <button type="button" class="btn btn-default hidden-btn">

            <div class="industry-text">
            
            <span><i class="fas fa-filter"></i></span>
            <p><?php echo __($filter_options->enable_industry_filters->industry_title, 'business_name_word_manager'); ?></p>
                
                <?php if( (is_array($columns) && !empty($columns) ) || (is_array($inscolumns) && !empty($inscolumns))  ) : ?>
                
                <?php $indus_count = !empty($columns) ? $columns : $inscolumns; ?>
                
                <span class="idea-count" style="display: flex;"><?= count($indus_count); ?></span>
            
                <?php endif; ?>
            </div>
            
            <span class="arrow_carrot-down elegant-icon"></span>
        </button>
        
        </div>
        <div class="drop-down-data eng-ind-filter hide-me">
        <?php $strURL = $_SERVER['REQUEST_URI'];
            $arrVals = explode("?", $strURL);
        ?>
        <form action="<?php echo $arrVals[0]; ?>" method="GET">
        
            <?php add_more_filter_inputs();  ?> 
            <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">

            <input type="hidden" name="fltr" value="rsult">

            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

            <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && $_GET['one_word'] != 'on') ? 'off' : 'on'; ?>">

            <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && $_GET['two_word'] != 'on') ? trim($_GET['one_word']) : 'on'; ?>">

            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

            <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

            <?php 
                
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                if( is_array($grammar) || is_object($grammar)):
                foreach ($grammar as $value) : 
            ?>
                <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
            <?php endforeach; endif; ?>

            <div class="row">
                <div class="col-md-12 search_bar">
                <input type="text" name="" class="indus-srch-fltr"  placeholder="<?php echo __($filter_options->enable_industry_filters->industry_search_placeholder, 'business_name_word_manager'); ?>">
                </div>
            </div>

            <div class="row cstm_label" style="overflow-x:hidden;">

                <?php
                
                global $wpdb;
                $columsitem = [];

                $columsitem = $wpdb->get_results("SELECT id, name FROM ".$wpdb->prefix."bnwm_word_categories WHERE status = 'Active' ");
                $inds_count = count($columsitem);

                $rows = ceil(count($columsitem) / 3);
                $lists  = array_chunk($columsitem, $rows);
                if( is_array($lists) || is_object($lists)):
                $index = 0;

                foreach ($lists as $column) :
                    echo '<div class="col-md-12"><ul class="live-search-list" id="live-search-list"><li class="all-indus-check"><label>'. $translations->all.'<input type="checkbox" name="all-indus" value="all"><span class="checkmark"></span></label></li>';
                    foreach ($column as $name) :
                ?>
                    <li data-search-term="<?= $name->name; ?>">
                    <?php 
                    $checked  = '';
                    $disabled = '';
                        
                    $name->name = strtolower($name->name) == 'realestate' ? 'Real Estate' : $name->name;
                     
                    if (is_array($columns) && !empty($columns) && (in_array($name->id, $columns) || in_array($name->name, $columns) ) ) :
                        $checked = 'checked="checked"';
                    endif;

                    if (is_array($inscolumns) && !empty($inscolumns) && in_array($name->id, $inscolumns)) :
                        $checked = 'checked="checked"';
                    endif;
                    ?>

                    <label><?php echo $name->name; ?>
                        <input name="industry[]" data-index="<?php echo $index; ?>" type="checkbox" value="<?php echo $name->name; ?>" <?= $disabled.''.$checked; ?>>
                        <span class="checkmark"></span>
                    </label>
                    </li>
                    <?php endforeach; ?>
                    <?php echo '</ul></div>'; ?>
                <?php endforeach; ?>
                <?php endif;
                ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="industry-sub">
                    <div class="industry-sub-left">
                    <button type="submit" data-category="ResultsPage" data-action="IndustryFilters" data-label="" class="apply-btn btn indus-filter"><?php echo __($filter_options->enable_industry_filters->industry_apply_button_text, 'business_name_word_manager'); ?></button>
                    </div>
                    <div class="industry-sub-right">
                    <button type="button" class="btn clear"><?php echo __($filter_options->enable_industry_filters->industry_clear_button_text, 'business_name_word_manager'); ?></button> 
                    </div>
                </div>
                </div>
            </div>
        </form>
        </div>
    </div>
    <?php endif; ?>
    <?php if( !empty($filter_options->enable_names_filter) && isset($filter_options->enable_names_filter->value) && $filter_options->enable_names_filter->value == 'true' ): ?>
    <div class="visible-md visible-lg">
        <div class="elm">
        <!--<h3>Result Filters</h3> -->
        <div class="cstm-dropdown eng-other-filter">
            <button type="button" class="btn btn-default hidden-btn"> 

            <div class=" select-text">
            <span><i class="fas fa-sliders-h"></i></span>
            <p><?php echo __($filter_options->enable_names_filter->names_title, 'business_name_word_manager'); ?></p>
            </div>
            <span class="arrow_carrot-down elegant-icon"></span>
            </button>
        </div>
        <div class="drop-down-data domain-filters hide-me">
            <?php $strURL = $_SERVER['REQUEST_URI'];
                $arrVals = explode("?", $strURL);

                if (defined('ICL_LANGUAGE_CODE')) {
                    $lang = ICL_LANGUAGE_CODE;
                } 
            ?>
            <form action="<?php echo $arrVals[0]; ?>" method="GET">
            <div class="row">
                <div class="col-md-12">
                <div class="slidecontainer">
                    <?php add_more_filter_inputs();  ?>
                    <input type="hidden" name="bname" value="<?php echo (isset($_GET['bname']) && !empty($_GET['bname'])) ? trim($_GET['bname']) : ''; ?>">
                    <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? trim($_GET['position']) : 'before'; ?>">
                    <?php

                    $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                    if (is_array($industry) || is_object($industry)) :
                    foreach ($industry as $value) : ?>
                        <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
                    <?php endforeach;
                    endif; ?>

                    <!-- <input type="hidden" name="fltr" value="rsult"> -->
                    <?php if( !empty($filter_options->enable_names_filter->enable_character_count) && isset($filter_options->enable_names_filter->enable_character_count->value) && $filter_options->enable_names_filter->enable_character_count->value == 'true' ): ?> 
                    <div class="flex-row">
                        <h3><?php echo __($filter_options->enable_names_filter->enable_character_count->character_count_title, 'business_name_word_manager'); ?></h3>
                    </div>
                    <input class="sliderRange deskslide" name="character" type="text" min="2" max="15" value="<?php echo (isset($_GET['character']) && !empty($_GET['character'])) ? trim($_GET['character']) : '15'; ?>" name="points" step="1">
                    <?php endif; ?> 
                
                </div>
                </div>

            
                <div class="col-md-12 cstm_label">
                <ul class="live-search-list word-type <?php if ( isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : echo 'rhyming-on'; endif; ?>">
                    
                <?php if( !empty($filter_options->enable_names_filter->enable_words_filter) && isset($filter_options->enable_names_filter->enable_words_filter->value) && $filter_options->enable_names_filter->enable_words_filter->value == 'true' ): ?>    
                    
                    <h3><?php echo __($filter_options->enable_names_filter->enable_words_filter->words_title, 'business_name_word_manager'); ?></h3>
                    <li>
                        <input class="one-word" name="one_word" type="hidden" value="<?php echo (isset($_GET['one_word']) && $_GET['one_word'] != 'on') ? trim($_GET['one_word']) : 'on'; ?>">

                        <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->one_word_title, 'business_name_word_manager'); ?> <input data-index="" class="oneword" value="on" <?php if (!isset($_GET['one_word']) || (isset($_GET['one_word']) && $_GET['one_word'] == 'on')) : ?> checked="checked" <?php endif; ?> <?php if ( isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?> type="checkbox">
                        <span class="checkmark"></span>
                        </label>
                    </li>
                    <li data-search-term="<?php echo $_GET['bname']; ?>">

                        <input class="two-word" name="two_word" type="hidden" value="<?php echo (isset($_GET['two_word']) && $_GET['two_word'] != 'on') ? trim($_GET['two_word']) : 'on'; ?>">

                        <label><?php echo __($filter_options->enable_names_filter->enable_words_filter->two_word_title, 'business_name_word_manager'); ?> <input data-index="" class="twoword" <?php if (!isset($_GET['two_word']) || (isset($_GET['two_word']) && $_GET['two_word'] == 'on' || $_GET['rhyming'] == 'on')) : ?> checked="checked" <?php endif; ?> <?php if ( isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> disabled="disabled" <?php endif; ?> value="on" type="checkbox">
                        <span class="checkmark"></span>
                        </label>
                    </li> 
                    <?php endif; ?>
                    <?php if( !empty($filter_options->enable_grammar_filters) && isset($filter_options->enable_grammar_filters->value) && $filter_options->enable_grammar_filters->value == 'true' ):
            
                    $checked = '';
                    
                    ?>
                    <h3><?php echo __($filter_options->enable_names_filter->enable_words_filter->grammar_title, 'business_name_word_manager'); ?></h3>
                        <?php if( $filter_options->enable_grammar_filters->masculine_singular_title !== '' ) :  ?>
                        <li>
                        <label><?php echo __($filter_options->enable_grammar_filters->masculine_singular_title, 'business_name_word_manager'); ?> 
                            <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                            {
                                if (in_array(('masculine'), $_GET['grammar']))
                                {
                                $checked = 'checked="checked"';
                                }
                            }
                            ?>
                            <input name="grammar[]" class="grammar1" type="checkbox" value="masculine" <?= $checked; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <?php endif; ?>

                        <?php if( $filter_options->enable_grammar_filters->feminine_singular_title !== '' ) :  ?>
                        <li>
                        <label><?php echo __($filter_options->enable_grammar_filters->feminine_singular_title, 'business_name_word_manager'); ?>
                        <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                            {
                            if (in_array(('feminine'), $_GET['grammar']))
                            {
                                $checked = 'checked="checked"';
                            }
                            }
                            ?>
                            <input name="grammar[]" class="grammar2" type="checkbox" value="feminine" <?= $checked; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <?php endif; ?>

                        <?php if( $filter_options->enable_grammar_filters->masculine_plural_title !== '' ) :  ?> 
                        <li>
                        <label><?php echo __($filter_options->enable_grammar_filters->masculine_plural_title, 'business_name_word_manager'); ?>
                            <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                            {
                            if (in_array(('masculine plural'), $_GET['grammar']))
                            {
                                $checked = 'checked="checked"';
                            }
                            }
                            ?>
                            <input name="grammar[]" class="grammar3" type="checkbox" value="masculine plural" <?= $checked; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <?php endif; ?>

                        <?php if( $filter_options->enable_grammar_filters->feminine_plural_title !== '' ) :  ?>
                        <li>
                        <label><?php echo __($filter_options->enable_grammar_filters->feminine_plural_title, 'business_name_word_manager'); ?>
                            <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                            {
                            if (in_array(('feminine plural'), $_GET['grammar']))
                            {
                                $checked = 'checked="checked"';
                            }
                            }
                            ?>
                            <input name="grammar[]" class="grammar4" type="checkbox" value="feminine plural" <?= $checked; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <?php endif; ?>

                        <?php if( $filter_options->enable_grammar_filters->plural_title !== '' ) :  ?>
                        <li>
                        <label><?php echo __($filter_options->enable_grammar_filters->plural_title, 'business_name_word_manager'); ?>
                            <?php if( isset($_GET['grammar']) && !empty($_GET['grammar']))
                            {
                            if (in_array(('plural'), $_GET['grammar']))
                            {
                                $checked = 'checked="checked"';
                            }
                            }
                            ?>
                            <input name="grammar[]" class="grammar0" type="checkbox" value="plural" <?= $checked; ?>>
                            <span class="checkmark"></span>
                        </label>
                        </li>
                        <?php endif; ?>

                    <?php endif; ?>

                </ul>
                </div>
                <?php if( !empty($filter_options->enable_names_filter->enable_keyword_filter) && isset($filter_options->enable_names_filter->enable_keyword_filter->value) && $filter_options->enable_names_filter->enable_keyword_filter->value == 'true' ): ?> 
                <div class="col-md-12 cstm_label">
                <ul class="keywords">
                    <h3><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->keyword_title, 'business_name_word_manager'); ?></h3>
                    <li>
                        <input class="radio-btn" type="radio" name="position" id="inlineRadio" value="before" <?php echo (isset($_GET['position']) && $_GET['position'] == 'before') ? 'checked' : ''; ?>>
                        <label class="form-check-label" for="inlineRadio"><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->before_title, 'business_name_word_manager'); ?></label>
                    </li>
                    <li>
                        <input class="radio-btn" type="radio" name="position" id="inlineRadio2" value="after" <?php echo (isset($_GET['position']) && $_GET['position'] == 'after') ? 'checked' : ''; ?>>
                        <label class="form-check-label" for="inlineRadio2"><?php echo __($filter_options->enable_names_filter->enable_keyword_filter->after_title, 'business_name_word_manager'); ?></label>
                    </li>
                </ul>
                </div>
                <?php endif; ?>

                <?php if( !empty($filter_options->enable_names_filter->enable_rhyming_filter) && isset($filter_options->enable_names_filter->enable_rhyming_filter->value) && $filter_options->enable_names_filter->enable_rhyming_filter->value == 'true' ): ?>  
                
                <div class="col-md-12 col-sm-12">
                <div class="filter desk clearfix">
            
                    <div class="switch-toggle rhyming">
                    <div class="inner">
                        <span><?php echo __($filter_options->enable_names_filter->enable_rhyming_filter->rhyming_title, 'business_name_word_manager'); ?></span>
                        <label class="switch deskslide">
                        <input class="rhyming-val" name="rhyming" type="hidden" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">
                            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">
                        <input type="checkbox" class="can_rhyming" <?php if (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') : ?> checked="checked" <?php endif; ?> value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] != 'on') ? trim($_GET['rhyming']) : 'on'; ?>">
                        <span class="slider round"></span>
                        </label>
                        <div class="condition">
                        <p><?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? __('On', 'business_name_word_manager') : __('Off', 'business_name_word_manager'); ?>
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

                <?php endif; ?> 

            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="industry-sub">
                    <div class="industry-sub-left">
                    <?php $string_leng = strlen(__($filter_options->enable_names_filter->names_apply_button, 'business_name_word_manager')); 
                          $style = $string_leng > 14 ? 'style="font-size: 12px"' : '';
                     ?>
                    <button type="submit" <?= $style; ?> data-category="ResultsPage" data-action="NamesFilters" data-label="" class="apply-btn btn btn-flitr click-event"><?php echo __($filter_options->enable_names_filter->names_apply_button, 'business_name_word_manager'); ?></button>
                    </div>
                    <div class="industry-sub-right">
                    <button type="button" class="btn cancel cancel_btn"><?php echo __($filter_options->enable_names_filter->names_cancel_button, 'business_name_word_manager'); ?></button>
                    </div>
                </div>
                </div>
            </div>
            </form>
        </div>
        </div>
        <?php endif; ?>
        <?php if( !empty($filter_options->enable_domains_availability_widget) && isset($filter_options->enable_domains_availability_widget->value) && $filter_options->enable_domains_availability_widget->value == 'true' ): ?>
        <div class="domain-checker">
            <div  class="domains-head">
            <span><i class="fas fa-search"></i></span> 
            <p><?php echo __($filter_options->enable_domains_availability_widget->domains_title, 'business_name_word_manager'); ?></p>
            </div>
            <div class="domain-widget">
            
            <?php
            $button_text = __($filter_options->enable_domains_availability_widget->domains_button_text, 'business_name_word_manager');
            $placeholder = __($filter_options->enable_domains_availability_widget->domains_name_placeholder, 'business_name_word_manager');
            echo do_shortcode('[wpdomainchecker button="Check" placeholdermessage="'.$placeholder.'" button_text="'.$button_text.'"]'); ?>
            </div> 
        </div>
        <?php endif; ?>
        </div>
    </div>