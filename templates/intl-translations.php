<form method="post" id="saveTranslations" enctype="multipart/form-data">
    <fieldset>
        <legend><h2>Nameideas Result Page Translation</h2></legend>
        <table class="form-table">
            <tbody>
                
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Generate</label>
                    </th>
                    <td>
                        <input type="text" name="nameideasgeneratetext" value="<?php echo isset($translations->nameideasgeneratetext) ? $translations->nameideasgeneratetext : 'Generate'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Placeholder text for generate</label>
                    </th>
                    <td>
                        <input type="text" name="nameideasgplaceholdertext" value="<?php echo isset($translations->nameideasgplaceholdertext) ? $translations->nameideasgplaceholdertext : 'Enter words and click generate...'; ?>">
                    </td>
                </tr>


                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Text Advert</label>
                    </th>
                    <td>
                        <input type="text" name="nameideastextadvert" value="<?php echo isset($translations->nameideastextadvert) ? $translations->nameideastextadvert : 'Click a name to check Domain Availability'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Affiliate Advert Text</label>
                    </th>
                    <td>
                        <input type="text" name="nameideasaffiliateadverttext" value="<?php echo isset($translations->nameideasaffiliateadverttext) ? $translations->nameideasaffiliateadverttext : 'Get a FREE Domain with Web Hosting. Only $1.99/mo with GoDaddy.'; ?>">
                    </td>
                </tr>
           
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Filter</label>
                    </th>
                    <td>
                        <input type="text" name="nameideasfilter" value="<?php echo isset($translations->nameideasfilter) ? $translations->nameideasfilter : 'Filter'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Results</label>
                    </th>
                    <td>
                        <input type="text" name="nameideasresults" value="<?php echo isset($translations->nameideasresults) ? $translations->nameideasresults : 'results'; ?>">
                    </td>
                </tr>
                
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Tooltip Text</label>
                    </th>
                    <td>
                        <input type="text" name="nameideastooltip" value="<?php echo isset($translations->nameideastooltip) ? $translations->nameideastooltip : 'Click to check domain availability'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Tooltip Save To Save Tooltio</label>
                    </th>
                    <td>
                        <input type="text" name="nameideastooltipclicktosave" value="<?php echo isset($translations->nameideastooltipclicktosave) ? $translations->nameideastooltipclicktosave : 'Click to Save'; ?>">
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend><h2>Domains Result Page Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Text Advert</label>
                    </th>
                    <td>
                        <input type="text" name="domainstextadvert" value="<?php echo isset($translations->domainstextadvert) ? $translations->domainstextadvert : 'Domain name ideas generated'; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend><h2>Nameideas-Wix Result Page Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Text Advert</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix_textadvert" value="<?php echo isset($translations->nameideas_wix_textadvert) ? $translations->nameideas_wix_textadvert : 'Domain name ideas generated'; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    

    <fieldset>
        <legend><h2>Common Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">All</label>
                    </th>
                    <td>
                        <input type="text" name="all" value="<?php echo isset($translations->all) ? $translations->all : 'All'; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Kickstart your business with a Premium Domain and Branding Package!</label>
                    </th>
                    <td>
                        <input type="text" name="domains_heading" value="<?php echo isset($translations->domains_heading) ? $translations->domains_heading : 'Kickstart your business with a Premium Domain and Branding Package!'; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">View more premium domains</label>
                    </th>
                    <td>
                        <input type="text" name="domains_link_text" value="<?php echo isset($translations->domains_link_text) ? $translations->domains_link_text : 'View more premium domains'; ?>">
                    </td>
                </tr>
             </tbody>
        </table>
    </fieldset>

    <fieldset>
        <legend><h2>Error Messages Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Click the to save ideas text</label>
                    </th>
                    <td>
                        <input type="text" name="saveideaserrortext" value="<?php echo isset($translations->saveideaserrortext) ? $translations->saveideaserrortext : 'click the'; ?>">
                    </td>
                    <td>
                        <input type="text" name="saveideaserrorsubtext" value="<?php echo isset($translations->saveideaserrorsubtext) ? $translations->saveideaserrorsubtext : 'to save ideas'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Add words into the search box.</label>
                    </th>
                    <td>
                        <input type="text" name="generatorerror1" value="<?php echo isset($translations->generatorerror1) ? $translations->generatorerror1 : 'Add words into the search box.'; ?>">
                    </td>
                </tr>
                
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Enter at least three characters.</label>
                    </th>
                    <td>
                        <input type="text" name="generatorerror2" value="<?php echo isset($translations->generatorerror2) ? $translations->generatorerror2 : 'Enter at least three characters.'; ?>">
                    </td>
                </tr>
                
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Please enter a maximum of 6 keywords.</label>
                    </th>
                    <td>
                        <input type="text" name="generatorerror3" value="<?php echo isset($translations->generatorerror3) ? $translations->generatorerror3 : 'Please enter a maximum of 6 keywords.'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Please save a name idea</label>
                    </th>
                    <td>
                        <input type="text" name="emailerror1" value="<?php echo isset($translations->emailerror1) ? str_replace("\\",'',$translations->emailerror1) : 'Please save a name idea..'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Please enter  email</label>
                    </th>
                    <td>
                        <input type="text" name="emailerror2" value="<?php echo isset($translations->emailerror2) ? str_replace("\\",'',$translations->emailerror2) : 'Please enter email'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Please enter valid email</label>
                    </th>
                    <td>
                        <input type="text" name="emailerror3" value="<?php echo isset($translations->emailerror3) ? str_replace("\\",'',$translations->emailerror3) : 'Please enter valid email..'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Please enter your domain.</label>
                    </th>
                    <td>
                        <input type="text" name="emailerror4" value="<?php echo isset($translations->emailerror4) ? str_replace("\\",'',$translations->emailerror4) : 'Please enter your domain.'; ?>">
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>

    
    <fieldset>
        <legend><h2>Domain Checker Widget Translations</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Register button text</label>
                    </th>
                    <td>
                        <input type="text" name="domaincheckerregister" value="<?php echo isset($translations->domaincheckerregister) ? str_replace("\\",'',$translations->domaincheckerregister) : 'Register'; ?>">
                    </td>
                </tr>
                
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Disclosure text</label>
                    </th>
                    <td>
                        <input type="text" name="domaincheckerdisclosuretext" value="<?php echo isset($translations->domaincheckerdisclosuretext) ? str_replace("\\",'',$translations->domaincheckerdisclosuretext) : 'Our name generator is funded in part by affiliate commissions, at no extra cost to users'; ?>">
                    </td>
                </tr>


                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Check for alternative domains button</label>
                    </th>
                    <td>
                        <input type="text" name="domaincheckercheckforalt" value="<?php echo isset($translations->domaincheckercheckforalt) ? $translations->domaincheckercheckforalt : 'Check for alternative domains'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Learn More</label>
                    </th>
                    <td>
                        <input type="text" name="domaincheckerlearnmore" value="<?php echo isset($translations->domaincheckerlearnmore) ? $translations->domaincheckerlearnmore : 'Learn more.'; ?>">
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>
   
    <fieldset>
        <legend><h2>Sucess Message Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Your ideas have been sent to your inbox</label>
                    </th>
                    <td>
                        <input type="text" name="sucessmessage" value="<?php echo isset($translations->sucessmessage) ? $translations->sucessmessage : 'Your ideas have been sent to your inbox'; ?>">
                    </td>
                </tr>
             </tbody>
        </table>
    </fieldset>

   
    <fieldset>
        <legend><h2>Pagination Translation</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Next</label>
                    </th>
                    <td>
                        <input type="text" name="next" value="<?php echo isset($translations->next) ? $translations->next : 'Next'; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Previous</label>
                    </th>
                    <td>
                        <input type="text" name="previous" value="<?php echo isset($translations->previous) ? $translations->previous : 'Previous'; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    

    <?php
    $btnText = (isset($id)) ? "Update Settings" : "Save Settings";
    ?>
    <p class="submit"><input type="submit" name="translationSettings" id="emailSettings-btn" class="button button-primary" value="<?php echo $btnText; ?>"></p>

</form>