<form method="post" id="createrequest" enctype="multipart/form-data">
    <fieldset>
        <legend><h2>Request Detail</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="name">Request Title</label>
                    </th>
                    <td>
                        <input name="name" type="text" id="name" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60" value="<?php echo isset($request) ? $request->name : ''; ?>">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="name">Request Data Language</label>
                    </th>
                    <td>
                    <select name="bnwm_data_language" id="">
                        <?php foreach( $languages as $code => $name ): ?>
                        <option value="<?php echo $code; ?>"><?php echo $name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </td>
                </tr>

                <tr class="form-field">
                    <th scope="row"><label for="request_type">Request Type</label></th>
                    <td>
                        <select name="request_type" id="request_select">
                            <option value="txt">Raw Text</option>
                            <option value="csv">CSV</option>
                        </select>
                    </td>
                </tr>

                <tr class="form-field txt-control ctrl">
                    <th scope="row"><label for="request_data">Requested Data</label></th>
                    <td>
                        <textarea name="request_data" id="request_data" rows="15"><?php echo isset($request) ? $request->request_data : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field init-hide csv-control ctrl">
                    <th scope="row"><label for="csv_file">Upload CSV File</label></th>
                    <td>
                        <input type="file" name="csv_file" accept=".csv,.xls,.xlsx"  />
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>
    

    <?php
    $btnText = (isset($id)) ? "Update Request" : "Add New Request";
    ?>
    <p class="submit"><input type="submit" name="createrequest" id="createrequest-btn" class="button button-primary" value="<?php echo $btnText; ?>"></p>

</form>