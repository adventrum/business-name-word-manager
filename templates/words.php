<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="wrap word-manager-container">
    <h1 class="wp-heading-inline">Word Manager</h1>
    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&add_new_word=true" class="page-title-action">Add New</a>
    <?php if( isset($_GET['search_word']) ): ?>
        <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words" class="page-title-action">Back to Words Dashboard</a>
    <?php endif; ?>
    <h2 class="wp-heading-inline">Edit Words</h2>
    <form class="search-box" action="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words">
        <label class="screen-reader-text" for="post-search-input">Search Words:</label>
        <input type="hidden" name="page" value="bnwm_words">
        <input type="search" id="post-search-input" name="search_word" value="">
        <input type="submit" id="search-submit" class="button" value="Search Words">
    </form>
    <form>
        <div class="tablenav top">
            <div class="bulk-approve"><a href="javascript:void(0);" class="button button-primary">Bulk approve words</a></div>
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                        <?php echo $viewData['pagination']; ?>
            </div>
            <br class="clear">
        </div>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Keyword</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Meanings</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Homophones</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Adjectives</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Nouns</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Associated Topics</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Refresh Data</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Status</span></th>
                </tr>
            </thead>

            <tbody id="word-list">
                <?php foreach( $viewData['data'] as $rq ): ?>
                <tr id="word-<?php echo $rq->id; ?>" class="iedit author-self level-0 word-<?php echo $rq->id; ?> type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong><a class="row-title"><?php echo $rq->name; ?></a></strong>
                        <div class="row-actions"><span class="edit"><a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&edit_word=<?php echo $rq->id; ?>" aria-label="Edit word">Edit</a></span> | <span class="trash"><a data-wordId="<?php echo $rq->id; ?>" href="javascript:void(0)" aria-label="trash word">Delete</a></span></div>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><a href="javascript:void(0);" class="edit-word-col" data-keyword="<?php echo $rq->name; ?>" data-column="meanings" data-id="<?php echo $rq->id; ?>">Edit</a></td>
                    <td><a href="javascript:void(0);" class="edit-word-col" data-keyword="<?php echo $rq->name; ?>" data-column="homophones" data-id="<?php echo $rq->id; ?>">Edit</a></td>
                    <td><a href="javascript:void(0);" class="edit-word-col" data-keyword="<?php echo $rq->name; ?>" data-column="adjectives" data-id="<?php echo $rq->id; ?>">Edit</a></td>
                    <td><a href="javascript:void(0);" class="edit-word-col" data-keyword="<?php echo $rq->name; ?>" data-column="nouns" data-id="<?php echo $rq->id; ?>">Edit</a></td>
                    <td><a href="javascript:void(0);" class="edit-word-col" data-keyword="<?php echo $rq->name; ?>" data-column="associated_topics" data-id="<?php echo $rq->id; ?>">Edit</a></td>
                    <td class="has-loader"><a href="javascript:void(0)" data-id="<?php echo $rq->id; ?>" data-name="<?php echo $rq->name; ?>" class="fetch-word-data button button-primary">Fetch</a><i class="loader fa fa-circle-o-notch fa-spin"></i><i class="loaded fa fa-check"></i></td>
                    <td>
                    <?php if( $rq->status != 'Pending' ):?>
                            <select name="word_status" class="word_status">
                                <option value="Pending Approval"<?php echo $rq->status == 'Pending Approval' ? ' selected': ''; ?>>Pending Approval</option>
                                <option value="Approved"<?php echo $rq->status == 'Approved' ? ' selected':'' ?>>Approved</option>
                            </select>
                        <?php else:
                            echo $rq->status; 
                        endif;
                    ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Keyword</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Meanings</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Homophones</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Adjectives</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Nouns</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Associated Topics</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Refresh Data</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Status</span></th>
                </tr>
            </tfoot>

        </table>
        <div class="tablenav bottom">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                <?php echo $viewData['pagination']; ?>
            </div>
        </div>
    </form>
</div>
<div class="edit-words-list">
    <div class="frame-loader">
        <h2>Saving the Approved words</h2>
        <div class="center">
            <div class="dot-1"></div>
            <div class="dot-2"></div>
            <div class="dot-3"></div>
        </div>
    </div>
    <div class="word-editor">
    <div class="title removed">
        <h3>Removed Words</h3>
        <form class="add-new-word">
            <input type="text" name="add_new_word" placeholder="Add a word" required>
            <input type="submit" class="add-btn" value="Add Word">
        </form>
    </div>
        <div class="word-editor-inner">
            <div class="word-editor-wrap">
                <div class="removed-words">
                </div>
            </div>
        </div>
        <h3 class="title approved">Approved Words</h3>
        <div class="word-editor-inner">
            <div class="word-editor-wrap">
                <div class="all-words">

                </div>
            </div>
        </div>
        <div class="word-filter-btns">
            <a href="javascript:void(0);" class="cancel">Cancel</a>
            <a href="javascript:void(0);" class="apply">Apply</a>
        </div>
    </div>
</div>