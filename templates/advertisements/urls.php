<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<form method="post" id="saveAffiliateLinks" enctype="multipart/form-data">
<h2>Affiliate URLs</h2>
<!-- Tab links -->
<div class="geo-countries-wrap">
    <label for="geo-countries">Select GEO Countries</label>
    <select class="geo-countries" id="geo-countries" name="geo_countries[]" multiple="multiple">
        <option value="AD: ANDORRA">ANDORRA</option>
        <option value="AE: UNITED ARAB EMIRATES">UNITED ARAB EMIRATES</option>
        <option value="AF: AFGHANISTAN">AFGHANISTAN</option>
        <option value="AG: ANTIGUA AND BARBUDA">ANTIGUA AND BARBUDA</option>
        <option value="AI: ANGUILLA">ANGUILLA</option>
        <option value="AL: ALBANIA">ALBANIA</option>
        <option value="AM: ARMENIA">ARMENIA</option>
        <option value="AN: NETHERLANDS ANTILLES">NETHERLANDS ANTILLES</option>
        <option value="AO: ANGOLA">ANGOLA</option>
        <option value="AQ: ANTARCTICA">ANTARCTICA</option>
        <option value="AR: ARGENTINA">ARGENTINA</option>
        <option value="AS: AMERICAN SAMOA">AMERICAN SAMOA</option>
        <option value="AT: AUSTRIA">AUSTRIA</option>
        <option value="AU: AUSTRALIA">AUSTRALIA</option>
        <option value="AW: ARUBA">ARUBA</option>
        <option value="AZ: AZERBAIJAN">AZERBAIJAN</option>
        <option value="BA: BOSNIA AND HERZEGOVINA">BOSNIA AND HERZEGOVINA</option>
        <option value="BB: BARBADOS">BARBADOS</option>
        <option value="BD: BANGLADESH">BANGLADESH</option>
        <option value="BE: BELGIUM">BELGIUM</option>
        <option value="BF: BURKINA FASO">BURKINA FASO</option>
        <option value="BG: BULGARIA">BULGARIA</option>
        <option value="BH: BAHRAIN">BAHRAIN</option>
        <option value="BI: BURUNDI">BURUNDI</option>
        <option value="BJ: BENIN">BENIN</option>
        <option value="BL: SAINT BARTHELEMY">SAINT BARTHELEMY</option>
        <option value="BM: BERMUDA">BERMUDA</option>
        <option value="BN: BRUNEI DARUSSALAM">BRUNEI DARUSSALAM</option>
        <option value="BO: BOLIVIA">BOLIVIA</option>
        <option value="BR: BRAZIL">BRAZIL</option>
        <option value="BS: BAHAMAS">BAHAMAS</option>
        <option value="BT: BHUTAN">BHUTAN</option>
        <option value="BW: BOTSWANA">BOTSWANA</option>
        <option value="BY: BELARUS">BELARUS</option>
        <option value="BZ: BELIZE">BELIZE</option>
        <option value="CA: CANADA">CANADA</option>
        <option value="CC: COCOS (KEELING) ISLANDS">COCOS (KEELING) ISLANDS</option>
        <option value="CD: CONGO, THE DEMOCRATIC REPUBLIC OF THE">CONGO, THE DEMOCRATIC REPUBLIC OF THE</option>
        <option value="CF: CENTRAL AFRICAN REPUBLIC">CENTRAL AFRICAN REPUBLIC</option>
        <option value="CG: CONGO">CONGO</option>
        <option value="CH: SWITZERLAND">SWITZERLAND</option>
        <option value="CI: COTE D IVOIRE">COTE D IVOIRE</option>
        <option value="CK: COOK ISLANDS">COOK ISLANDS</option>
        <option value="CL: CHILE">CHILE</option>
        <option value="CM: CAMEROON">CAMEROON</option>
        <option value="CN: CHINA">CHINA</option>
        <option value="CO: COLOMBIA">COLOMBIA</option>
        <option value="CR: COSTA RICA">COSTA RICA</option>
        <option value="CU: CUBA">CUBA</option>
        <option value="CV: CAPE VERDE">CAPE VERDE</option>
        <option value="CX: CHRISTMAS ISLAND">CHRISTMAS ISLAND</option>
        <option value="CY: CYPRUS">CYPRUS</option>
        <option value="CZ: CZECH REPUBLIC">CZECH REPUBLIC</option>
        <option value="DE: GERMANY">GERMANY</option>
        <option value="DJ: DJIBOUTI">DJIBOUTI</option>
        <option value="DK: DENMARK">DENMARK</option>
        <option value="DM: DOMINICA">DOMINICA</option>
        <option value="DO: DOMINICAN REPUBLIC">DOMINICAN REPUBLIC</option>
        <option value="DZ: ALGERIA">ALGERIA</option>
        <option value="EC: ECUADOR">ECUADOR</option>
        <option value="EE: ESTONIA">ESTONIA</option>
        <option value="EG: EGYPT">EGYPT</option>
        <option value="ER: ERITREA">ERITREA</option>
        <option value="ES: SPAIN">SPAIN</option>
        <option value="ET: ETHIOPIA">ETHIOPIA</option>
        <option value="FI: FINLAND">FINLAND</option>
        <option value="FJ: FIJI">FIJI</option>
        <option value="FK: FALKLAND ISLANDS (MALVINAS)">FALKLAND ISLANDS (MALVINAS)</option>
        <option value="FM: MICRONESIA, FEDERATED STATES OF">MICRONESIA, FEDERATED STATES OF</option>
        <option value="FO: FAROE ISLANDS">FAROE ISLANDS</option>
        <option value="FR: FRANCE">FRANCE</option>
        <option value="GA: GABON">GABON</option>
        <option value="GB: UNITED KINGDOM">UNITED KINGDOM</option>
        <option value="GD: GRENADA">GRENADA</option>
        <option value="GE: GEORGIA">GEORGIA</option>
        <option value="GH: GHANA">GHANA</option>
        <option value="GI: GIBRALTAR">GIBRALTAR</option>
        <option value="GL: GREENLAND">GREENLAND</option>
        <option value="GM: GAMBIA">GAMBIA</option>
        <option value="GN: GUINEA">GUINEA</option>
        <option value="GQ: EQUATORIAL GUINEA">EQUATORIAL GUINEA</option>
        <option value="GR: GREECE">GREECE</option>
        <option value="GT: GUATEMALA">GUATEMALA</option>
        <option value="GU: GUAM">GUAM</option>
        <option value="GW: GUINEA-BISSAU">GUINEA-BISSAU</option>
        <option value="GY: GUYANA">GUYANA</option>
        <option value="HK: HONG KONG">HONG KONG</option>
        <option value="HN: HONDURAS">HONDURAS</option>
        <option value="HR: CROATIA">CROATIA</option>
        <option value="HT: HAITI">HAITI</option>
        <option value="HU: HUNGARY">HUNGARY</option>
        <option value="ID: INDONESIA">INDONESIA</option>
        <option value="IE: IRELAND">IRELAND</option>
        <option value="IL: ISRAEL">ISRAEL</option>
        <option value="IM: ISLE OF MAN">ISLE OF MAN</option>
        <option value="IN: INDIA">INDIA</option>
        <option value="IQ: IRAQ">IRAQ</option>
        <option value="IR: IRAN, ISLAMIC REPUBLIC OF">IRAN, ISLAMIC REPUBLIC OF</option>
        <option value="IS: ICELAND">ICELAND</option>
        <option value="IT: ITALY">ITALY</option>
        <option value="JM: JAMAICA">JAMAICA</option>
        <option value="JO: JORDAN">JORDAN</option>
        <option value="JP: JAPAN">JAPAN</option>
        <option value="KE: KENYA">KENYA</option>
        <option value="KG: KYRGYZSTAN">KYRGYZSTAN</option>
        <option value="KH: CAMBODIA">CAMBODIA</option>
        <option value="KI: KIRIBATI">KIRIBATI</option>
        <option value="KM: COMOROS">COMOROS</option>
        <option value="KN: SAINT KITTS AND NEVIS">SAINT KITTS AND NEVIS</option>
        <option value="KP: KOREA DEMOCRATIC PEOPLES REPUBLIC OF">KOREA DEMOCRATIC PEOPLES REPUBLIC OF</option>
        <option value="KR: KOREA REPUBLIC OF">KOREA REPUBLIC OF</option>
        <option value="KW: KUWAIT">KUWAIT</option>
        <option value="KY: CAYMAN ISLANDS">CAYMAN ISLANDS</option>
        <option value="KZ: KAZAKSTAN">KAZAKSTAN</option>
        <option value="LA: LAO PEOPLES DEMOCRATIC REPUBLIC">LAO PEOPLES DEMOCRATIC REPUBLIC</option>
        <option value="LB: LEBANON">LEBANON</option>
        <option value="LC: SAINT LUCIA">SAINT LUCIA</option>
        <option value="LI: LIECHTENSTEIN">LIECHTENSTEIN</option>
        <option value="LK: SRI LANKA">SRI LANKA</option>
        <option value="LR: LIBERIA">LIBERIA</option>
        <option value="LS: LESOTHO">LESOTHO</option>
        <option value="LT: LITHUANIA">LITHUANIA</option>
        <option value="LU: LUXEMBOURG">LUXEMBOURG</option>
        <option value="LV: LATVIA">LATVIA</option>
        <option value="LY: LIBYAN ARAB JAMAHIRIYA">LIBYAN ARAB JAMAHIRIYA</option>
        <option value="MA: MOROCCO">MOROCCO</option>
        <option value="MC: MONACO">MONACO</option>
        <option value="MD: MOLDOVA, REPUBLIC OF">MOLDOVA, REPUBLIC OF</option>
        <option value="ME: MONTENEGRO">MONTENEGRO</option>
        <option value="MF: SAINT MARTIN">SAINT MARTIN</option>
        <option value="MG: MADAGASCAR">MADAGASCAR</option>
        <option value="MH: MARSHALL ISLANDS">MARSHALL ISLANDS</option>
        <option value="MK: MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF">MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF</option>
        <option value="ML: MALI">MALI</option>
        <option value="MM: MYANMAR">MYANMAR</option>
        <option value="MN: MONGOLIA">MONGOLIA</option>
        <option value="MO: MACAU">MACAU</option>
        <option value="MP: NORTHERN MARIANA ISLANDS">NORTHERN MARIANA ISLANDS</option>
        <option value="MR: MAURITANIA">MAURITANIA</option>
        <option value="MS: MONTSERRAT">MONTSERRAT</option>
        <option value="MT: MALTA">MALTA</option>
        <option value="MU: MAURITIUS">MAURITIUS</option>
        <option value="MV: MALDIVES">MALDIVES</option>
        <option value="MW: MALAWI">MALAWI</option>
        <option value="MX: MEXICO">MEXICO</option>
        <option value="MY: MALAYSIA">MALAYSIA</option>
        <option value="MZ: MOZAMBIQUE">MOZAMBIQUE</option>
        <option value="NA: NAMIBIA">NAMIBIA</option>
        <option value="NC: NEW CALEDONIA">NEW CALEDONIA</option>
        <option value="NE: NIGER">NIGER</option>
        <option value="NG: NIGERIA">NIGERIA</option>
        <option value="NI: NICARAGUA">NICARAGUA</option>
        <option value="NL: NETHERLANDS">NETHERLANDS</option>
        <option value="NO: NORWAY">NORWAY</option>
        <option value="NP: NEPAL">NEPAL</option>
        <option value="NR: NAURU">NAURU</option>
        <option value="NU: NIUE">NIUE</option>
        <option value="NZ: NEW ZEALAND">NEW ZEALAND</option>
        <option value="OM: OMAN">OMAN</option>
        <option value="PA: PANAMA">PANAMA</option>
        <option value="PE: PERU">PERU</option>
        <option value="PF: FRENCH POLYNESIA">FRENCH POLYNESIA</option>
        <option value="PG: PAPUA NEW GUINEA">PAPUA NEW GUINEA</option>
        <option value="PH: PHILIPPINES">PHILIPPINES</option>
        <option value="PK: PAKISTAN">PAKISTAN</option>
        <option value="PL: POLAND">POLAND</option>
        <option value="PM: SAINT PIERRE AND MIQUELON">SAINT PIERRE AND MIQUELON</option>
        <option value="PN: PITCAIRN">PITCAIRN</option>
        <option value="PR: PUERTO RICO">PUERTO RICO</option>
        <option value="PT: PORTUGAL">PORTUGAL</option>
        <option value="PW: PALAU">PALAU</option>
        <option value="PY: PARAGUAY">PARAGUAY</option>
        <option value="QA: QATAR">QATAR</option>
        <option value="RO: ROMANIA">ROMANIA</option>
        <option value="RS: SERBIA">SERBIA</option>
        <option value="RU: RUSSIAN FEDERATION">RUSSIAN FEDERATION</option>
        <option value="RW: RWANDA">RWANDA</option>
        <option value="SA: SAUDI ARABIA">SAUDI ARABIA</option>
        <option value="SB: SOLOMON ISLANDS">SOLOMON ISLANDS</option>
        <option value="SC: SEYCHELLES">SEYCHELLES</option>
        <option value="SD: SUDAN">SUDAN</option>
        <option value="SE: SWEDEN">SWEDEN</option>
        <option value="SG: SINGAPORE">SINGAPORE</option>
        <option value="SH: SAINT HELENA">SAINT HELENA</option>
        <option value="SI: SLOVENIA">SLOVENIA</option>
        <option value="SK: SLOVAKIA">SLOVAKIA</option>
        <option value="SL: SIERRA LEONE">SIERRA LEONE</option>
        <option value="SM: SAN MARINO">SAN MARINO</option>
        <option value="SN: SENEGAL">SENEGAL</option>
        <option value="SO: SOMALIA">SOMALIA</option>
        <option value="SR: SURINAME">SURINAME</option>
        <option value="ST: SAO TOME AND PRINCIPE">SAO TOME AND PRINCIPE</option>
        <option value="SV: EL SALVADOR">EL SALVADOR</option>
        <option value="SY: SYRIAN ARAB REPUBLIC">SYRIAN ARAB REPUBLIC</option>
        <option value="SZ: SWAZILAND">SWAZILAND</option>
        <option value="TC: TURKS AND CAICOS ISLANDS">TURKS AND CAICOS ISLANDS</option>
        <option value="TD: CHAD">CHAD</option>
        <option value="TG: TOGO">TOGO</option>
        <option value="TH: THAILAND">THAILAND</option>
        <option value="TJ: TAJIKISTAN">TAJIKISTAN</option>
        <option value="TK: TOKELAU">TOKELAU</option>
        <option value="TL: TIMOR-LESTE">TIMOR-LESTE</option>
        <option value="TM: TURKMENISTAN">TURKMENISTAN</option>
        <option value="TN: TUNISIA">TUNISIA</option>
        <option value="TO: TONGA">TONGA</option>
        <option value="TR: TURKEY">TURKEY</option>
        <option value="TT: TRINIDAD AND TOBAGO">TRINIDAD AND TOBAGO</option>
        <option value="TV: TUVALU">TUVALU</option>
        <option value="TW: TAIWAN, PROVINCE OF CHINA">TAIWAN, PROVINCE OF CHINA</option>
        <option value="TZ: TANZANIA, UNITED REPUBLIC OF">TANZANIA, UNITED REPUBLIC OF</option>
        <option value="UA: UKRAINE">UKRAINE</option>
        <option value="UG: UGANDA">UGANDA</option>
        <option value="US: UNITED STATES">UNITED STATES</option>
        <option value="UY: URUGUAY">URUGUAY</option>
        <option value="UZ: UZBEKISTAN">UZBEKISTAN</option>
        <option value="VA: HOLY SEE (VATICAN CITY STATE)">HOLY SEE (VATICAN CITY STATE)</option>
        <option value="VC: SAINT VINCENT AND THE GRENADINES">SAINT VINCENT AND THE GRENADINES</option>
        <option value="VE: VENEZUELA">VENEZUELA</option>
        <option value="VG: VIRGIN ISLANDS, BRITISH">VIRGIN ISLANDS, BRITISH</option>
        <option value="VI: VIRGIN ISLANDS, U.S.">VIRGIN ISLANDS, U.S.</option>
        <option value="VN: VIET NAM">VIET NAM</option>
        <option value="VU: VANUATU">VANUATU</option>
        <option value="WF: WALLIS AND FUTUNA">WALLIS AND FUTUNA</option>
        <option value="WS: SAMOA">SAMOA</option>
        <option value="XK: KOSOVO">KOSOVO</option>
        <option value="YE: YEMEN">YEMEN</option>
        <option value="YT: MAYOTTE">MAYOTTE</option>
        <option value="ZA: SOUTH AFRICA">SOUTH AFRICA</option>
        <option value="ZM: ZAMBIA">ZAMBIA</option>
        <option value="ZW: ZIMBABWE">ZIMBABWE</option>
    </select>
</div>
<div class="tab">
  <button class="Nameideas tablinks" onclick="openResultspageTab(event, 'Nameideas')">Nameideas</button>
  <button class="tablinks" onclick="openResultspageTab(event, 'Domains')">Domains</button>
  <button class="tablinks" onclick="openResultspageTab(event, 'Nameideas-wix')">Nameideas Wix</button>
</div>

<!-- Tab content -->
<div id="Nameideas" class="tabcontent">
  <h2>Nameideas</h2>
  <?php $textadvert = isset($nameideas->textadvert) ? $nameideas->textadvert : ''; ?>
  <fieldset>
        <h3>Text Advert</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[textadvert][url]" value="<?php echo ( is_object($textadvert) && isset($textadvert->url) && $textadvert->url != '' ) ? $textadvert->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[textadvert][sid]" value="<?php echo ( is_object($textadvert) && isset($textadvert->sid) && $textadvert->sid != '' ) ? $textadvert->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                        <?php $sponsor = ( is_object($textadvert) && isset($textadvert->sponsor) && $textadvert->sponsor != '' ) ? $textadvert->sponsor : '0'; ?>
                        <select name="nameideas[textadvert][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Geo URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[textadvert][geo_url]" value="<?php echo ( is_object($textadvert) && isset($textadvert->geo_url) && $textadvert->geo_url != '' ) ? $textadvert->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[textadvert][geo_sid]" value="<?php echo ( is_object($textadvert) && isset($textadvert->geo_sid) && $textadvert->geo_sid != '' ) ? $textadvert->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                        <?php $geo_sponsor = ( is_object($textadvert) && isset($textadvert->geo_sponsor) && $textadvert->geo_sponsor != '' ) ? $textadvert->geo_sponsor : '0'; ?>
                        <select name="nameideas[textadvert][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $topbanner = isset($nameideas->topbanner) ? $nameideas->topbanner : ''; ?>
    <fieldset>
        <h3>Top Banner ( shown in place of Text Advert )</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->url) && $topbanner->url != '' ) ? $topbanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->sid) && $topbanner->sid != '' ) ? $topbanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($topbanner) && isset($topbanner->sponsor) && $topbanner->sponsor != '' ) ? $topbanner->sponsor : '0'; ?>
                        <select name="nameideas[topbanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Geo URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][geo_url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->geo_url) && $topbanner->geo_url != '' ) ? $topbanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][geo_sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->geo_sid) && $topbanner->geo_sid != '' ) ? $topbanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($topbanner) && isset($topbanner->geo_sponsor) && $topbanner->geo_sponsor != '' ) ? $topbanner->geo_sponsor : '0'; ?>
                        <select name="nameideas[topbanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India URL( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][ind_url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->ind_url) && $topbanner->ind_url != '' ) ? $topbanner->ind_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India SID( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[topbanner][ind_sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->ind_sid) && $topbanner->ind_sid != '' ) ? $topbanner->ind_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India Sponsor( IN )</label>
                    </th>
                    <td>
                    <?php $ind_sponsor = ( is_object($topbanner) && isset($topbanner->ind_sponsor) && $topbanner->ind_sponsor != '' ) ? $topbanner->ind_sponsor : '0'; ?>
                        <select name="nameideas[topbanner][ind_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $ind_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $ind_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $middlebanner = isset($nameideas->middlebanner) ? $nameideas->middlebanner : ''; ?>
    <fieldset>
        <h3>Middle Banner</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->url) && $middlebanner->url != '' ) ? $middlebanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->sid) && $middlebanner->sid != '' ) ? $middlebanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($middlebanner) && isset($middlebanner->sponsor) && $middlebanner->sponsor != '' ) ? $middlebanner->sponsor : '0'; ?>
                        <select name="nameideas[middlebanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][geo_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_url) && $middlebanner->geo_url != '' ) ? $middlebanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][geo_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_sid) && $middlebanner->geo_sid != '' ) ? $middlebanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($middlebanner) && isset($middlebanner->geo_sponsor) && $middlebanner->geo_sponsor != '' ) ? $middlebanner->geo_sponsor : '0'; ?>
                        <select name="nameideas[middlebanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India URL( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][ind_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_url) && $middlebanner->ind_url != '' ) ? $middlebanner->ind_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India SID( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[middlebanner][ind_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_sid) && $middlebanner->ind_sid != '' ) ? $middlebanner->ind_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India Sponsor( IN )</label>
                    </th>
                    <td>
                    <?php $ind_sponsor = ( is_object($middlebanner) && isset($middlebanner->ind_sponsor) && $middlebanner->ind_sponsor != '' ) ? $middlebanner->ind_sponsor : '0'; ?>
                        <select name="nameideas[middlebanner][ind_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $ind_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $ind_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $leftsidebarbottom = isset($nameideas->leftsidebarbottom) ? $nameideas->leftsidebarbottom : ''; ?>
    <fieldset>
        <h3>Left Sidebar Bottom</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[leftsidebarbottom][url]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->url) && $leftsidebarbottom->url != '' ) ? $leftsidebarbottom->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[leftsidebarbottom][sid]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->sid) && $leftsidebarbottom->sid != '' ) ? $leftsidebarbottom->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->sponsor) && $leftsidebarbottom->sponsor != '' ) ? $leftsidebarbottom->sponsor : '0'; ?>
                        <select name="nameideas[leftsidebarbottom][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[leftsidebarbottom][geo_url]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_url) && $leftsidebarbottom->geo_url != '' ) ? $leftsidebarbottom->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[leftsidebarbottom][geo_sid]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_sid) && $leftsidebarbottom->geo_sid != '' ) ? $leftsidebarbottom->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_sponsor) && $leftsidebarbottom->geo_sponsor != '' ) ? $leftsidebarbottom->geo_sponsor : '0'; ?>
                        <select name="nameideas[leftsidebarbottom][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $dropdownbanner = isset($nameideas->dropdownbanner) ? $nameideas->dropdownbanner : ''; ?>
    <fieldset>
        <h3>Dropdown Banner</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[dropdownbanner][url]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->url) && $dropdownbanner->url != '' ) ? $dropdownbanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[dropdownbanner][sid]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->sid) && $dropdownbanner->sid != '' ) ? $dropdownbanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($dropdownbanner) && isset($dropdownbanner->sponsor) && $dropdownbanner->sponsor != '' ) ? $dropdownbanner->sponsor : '0'; ?>
                        <select name="nameideas[dropdownbanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[dropdownbanner][geo_url]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->geo_url) && $dropdownbanner->geo_url != '' ) ? $dropdownbanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[dropdownbanner][geo_sid]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->geo_sid) && $dropdownbanner->geo_sid != '' ) ? $dropdownbanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($dropdownbanner) && isset($dropdownbanner->geo_sponsor) && $dropdownbanner->geo_sponsor != '' ) ? $dropdownbanner->geo_sponsor : '0'; ?>
                        <select name="nameideas[dropdownbanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinkdropdown = isset($nameideas->registerlinkdropdown) ? $nameideas->registerlinkdropdown : ''; ?>
    <fieldset>
        <h3>Register link dropdown</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->url) && $registerlinkdropdown->url != '' ) ? $registerlinkdropdown->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sid) && $registerlinkdropdown->sid != '' ) ? $registerlinkdropdown->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][un_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->un_sid) && $registerlinkdropdown->un_sid != '' ) ? $registerlinkdropdown->un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sponsor) && $registerlinkdropdown->sponsor != '' ) ? $registerlinkdropdown->sponsor : '0'; ?>
                        <select name="nameideas[registerlinkdropdown][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][geo_url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_url) && $registerlinkdropdown->geo_url != '' ) ? $registerlinkdropdown->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][geo_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sid) && $registerlinkdropdown->geo_sid != '' ) ? $registerlinkdropdown->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Unvailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdropdown][geo_un_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_un_sid) && $registerlinkdropdown->geo_un_sid != '' ) ? $registerlinkdropdown->geo_un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sponsor) && $registerlinkdropdown->geo_sponsor != '' ) ? $registerlinkdropdown->geo_sponsor : '0'; ?>
                        <select name="nameideas[registerlinkdropdown][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinksavedidea = isset($nameideas->registerlinksavedidea) ? $nameideas->registerlinksavedidea : ''; ?>
    <fieldset>
        <h3>Register link savedidea</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinksavedidea][url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->url) && $registerlinksavedidea->url != '' ) ? $registerlinksavedidea->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinksavedidea][sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sid) && $registerlinksavedidea->sid != '' ) ? $registerlinksavedidea->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sponsor) && $registerlinksavedidea->sponsor != '' ) ? $registerlinksavedidea->sponsor : '0'; ?>
                        <select name="nameideas[registerlinksavedidea][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinksavedidea][geo_url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_url) && $registerlinksavedidea->geo_url != '' ) ? $registerlinksavedidea->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinksavedidea][geo_sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sid) && $registerlinksavedidea->geo_sid != '' ) ? $registerlinksavedidea->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sponsor) && $registerlinksavedidea->geo_sponsor != '' ) ? $registerlinksavedidea->geo_sponsor : '0'; ?>
                        <select name="nameideas[registerlinksavedidea][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinkdomainchecker = isset($nameideas->registerlinkdomainchecker) ? $nameideas->registerlinkdomainchecker : ''; ?>
    <fieldset>
        <h3>Register link domain checker</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][url]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->url) && $registerlinkdomainchecker->url != '' ) ? $registerlinkdomainchecker->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->sid) && $registerlinkdomainchecker->sid != '' ) ? $registerlinkdomainchecker->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][un_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->un_sid) && $registerlinkdomainchecker->un_sid != '' ) ? $registerlinkdomainchecker->un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->sponsor) && $registerlinkdomainchecker->sponsor != '' ) ? $registerlinkdomainchecker->sponsor : '0'; ?>
                        <select name="nameideas[registerlinkdomainchecker][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][geo_url]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_url) && $registerlinkdomainchecker->geo_url != '' ) ? $registerlinkdomainchecker->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][geo_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_sid) && $registerlinkdomainchecker->geo_sid != '' ) ? $registerlinkdomainchecker->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[registerlinkdomainchecker][geo_un_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_un_sid) && $registerlinkdomainchecker->geo_un_sid != '' ) ? $registerlinkdomainchecker->geo_un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_sponsor) && $registerlinkdomainchecker->geo_sponsor != '' ) ? $registerlinkdomainchecker->geo_sponsor : '0'; ?>
                        <select name="nameideas[registerlinkdomainchecker][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $checkforaltdomainchecker = isset($nameideas->checkforaltdomainchecker) ? $nameideas->checkforaltdomainchecker : ''; ?>
    <fieldset>
        <h3>Check for alternate Domain Checker</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdomainchecker][url]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->url) && $checkforaltdomainchecker->url != '' ) ? $checkforaltdomainchecker->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdomainchecker][sid]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->sid) && $checkforaltdomainchecker->sid != '' ) ? $checkforaltdomainchecker->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->sponsor) && $checkforaltdomainchecker->sponsor != '' ) ? $checkforaltdomainchecker->sponsor : '0'; ?>
                        <select name="nameideas[checkforaltdomainchecker][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdomainchecker][geo_url]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_url) && $checkforaltdomainchecker->geo_url != '' ) ? $checkforaltdomainchecker->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdomainchecker][geo_sid]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_sid) && $checkforaltdomainchecker->geo_sid != '' ) ? $checkforaltdomainchecker->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_sponsor) && $checkforaltdomainchecker->geo_sponsor != '' ) ? $checkforaltdomainchecker->geo_sponsor : '0'; ?>
                        <select name="nameideas[checkforaltdomainchecker][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $checkforaltdropdown = isset($nameideas->checkforaltdropdown) ? $nameideas->checkforaltdropdown : ''; ?>
    <fieldset>
        <h3>Check for alternate Dropdown</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdropdown][url]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->url) && $checkforaltdropdown->url != '' ) ? $checkforaltdropdown->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdropdown][sid]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->sid) && $checkforaltdropdown->sid != '' ) ? $checkforaltdropdown->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->sponsor) && $checkforaltdropdown->sponsor != '' ) ? $checkforaltdropdown->sponsor : '0'; ?>
                        <select name="nameideas[checkforaltdropdown][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdropdown][geo_url]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_url) && $checkforaltdropdown->geo_url != '' ) ? $checkforaltdropdown->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas[checkforaltdropdown][geo_sid]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_sid) && $checkforaltdropdown->geo_sid != '' ) ? $checkforaltdropdown->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_sponsor) && $checkforaltdropdown->geo_sponsor != '' ) ? $checkforaltdropdown->geo_sponsor : '0'; ?>
                        <select name="nameideas[checkforaltdropdown][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>

<div id="Domains" class="tabcontent">
  <h2>Domains</h2>
    <?php $middlebanner = isset($domains->middlebanner) ? $domains->middlebanner : ''; ?>
    <fieldset>
        <h3>Middle Banner</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->url) && $middlebanner->url != '' ) ? $middlebanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->sid) && $middlebanner->sid != '' ) ? $middlebanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($middlebanner) && isset($middlebanner->sponsor) && $middlebanner->sponsor != '' ) ? $middlebanner->sponsor : '0'; ?>
                        <select name="domains[middlebanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][geo_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_url) && $middlebanner->geo_url != '' ) ? $middlebanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][geo_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_sid) && $middlebanner->geo_sid != '' ) ? $middlebanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($middlebanner) && isset($middlebanner->geo_sponsor) && $middlebanner->geo_sponsor != '' ) ? $middlebanner->geo_sponsor : '0'; ?>
                        <select name="domains[middlebanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India URL( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][ind_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_url) && $middlebanner->ind_url != '' ) ? $middlebanner->ind_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India SID( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="domains[middlebanner][ind_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_sid) && $middlebanner->ind_sid != '' ) ? $middlebanner->ind_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India Sponsor( IN )</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($middlebanner) && isset($middlebanner->ind_sponsor) && $middlebanner->ind_sponsor != '' ) ? $middlebanner->ind_sponsor : '0'; ?>
                        <select name="domains[middlebanner][ind_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $ind_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $ind_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinkdropdown = isset($domains->registerlinkdropdown) ? $domains->registerlinkdropdown : ''; ?>
    <fieldset>
        <h3>Register button for nameideas</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->url) && $registerlinkdropdown->url != '' ) ? $registerlinkdropdown->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sid) && $registerlinkdropdown->sid != '' ) ? $registerlinkdropdown->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][un_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->un_sid) && $registerlinkdropdown->un_sid != '' ) ? $registerlinkdropdown->un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sponsor) && $registerlinkdropdown->sponsor != '' ) ? $registerlinkdropdown->sponsor : '0'; ?>
                        <select name="domains[registerlinkdropdown][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][geo_url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_url) && $registerlinkdropdown->geo_url != '' ) ? $registerlinkdropdown->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][geo_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sid) && $registerlinkdropdown->geo_sid != '' ) ? $registerlinkdropdown->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinkdropdown][geo_un_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_un_sid) && $registerlinkdropdown->geo_un_sid != '' ) ? $registerlinkdropdown->geo_un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sponsor) && $registerlinkdropdown->geo_sponsor != '' ) ? $registerlinkdropdown->geo_sponsor : '0'; ?>
                        <select name="domains[registerlinkdropdown][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinksavedidea = isset($domains->registerlinksavedidea) ? $domains->registerlinksavedidea : ''; ?>
    <fieldset>
        <h3>Register link savedidea</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinksavedidea][url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->url) && $registerlinksavedidea->url != '' ) ? $registerlinksavedidea->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinksavedidea][sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sid) && $registerlinksavedidea->sid != '' ) ? $registerlinksavedidea->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sponsor) && $registerlinksavedidea->sponsor != '' ) ? $registerlinksavedidea->sponsor : '0'; ?>
                        <select name="domains[registerlinksavedidea][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinksavedidea][geo_url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_url) && $registerlinksavedidea->geo_url != '' ) ? $registerlinksavedidea->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="domains[registerlinksavedidea][geo_sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sid) && $registerlinksavedidea->geo_sid != '' ) ? $registerlinksavedidea->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sponsor) && $registerlinksavedidea->geo_sponsor != '' ) ? $registerlinksavedidea->geo_sponsor : '0'; ?>
                        <select name="domains[registerlinksavedidea][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>

<div id="Nameideas-wix" class="tabcontent">
  <h2>Nameideas-wix</h2>
  <?php $textadvert = isset($nameideas_wix->textadvert) ? $nameideas_wix->textadvert : ''; ?>
  <fieldset>
        <h3>Text Advert</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[textadvert][url]" value="<?php echo ( is_object($textadvert) && isset($textadvert->url) && $textadvert->url != '' ) ? $textadvert->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[textadvert][sid]" value="<?php echo ( is_object($textadvert) && isset($textadvert->sid) && $textadvert->sid != '' ) ? $textadvert->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($textadvert) && isset($textadvert->sponsor) && $textadvert->sponsor != '' ) ? $textadvert->sponsor : '0'; ?>
                        <select name="nameideas_wix[textadvert][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[textadvert][geo_url]" value="<?php echo ( is_object($textadvert) && isset($textadvert->geo_url) && $textadvert->geo_url != '' ) ? $textadvert->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[textadvert][geo_sid]" value="<?php echo ( is_object($textadvert) && isset($textadvert->geo_sid) && $textadvert->geo_sid != '' ) ? $textadvert->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($textadvert) && isset($textadvert->geo_sponsor) && $textadvert->geo_sponsor != '' ) ? $textadvert->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[textadvert][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $topbanner = isset($nameideas_wix->topbanner) ? $nameideas_wix->topbanner : ''; ?>
    <fieldset>
        <h3>Top Banner ( shown in place of Text Advert )</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->url) && $topbanner->url != '' ) ? $topbanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->sid) && $topbanner->sid != '' ) ? $topbanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($topbanner) && isset($topbanner->sponsor) && $topbanner->sponsor != '' ) ? $topbanner->sponsor : '0'; ?>
                        <select name="nameideas_wix[topbanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][geo_url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->geo_url) && $topbanner->geo_url != '' ) ? $topbanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][geo_sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->geo_sid) && $topbanner->geo_sid != '' ) ? $topbanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($topbanner) && isset($topbanner->geo_sponsor) && $topbanner->geo_sponsor != '' ) ? $topbanner->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[topbanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India URL( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][ind_url]" value="<?php echo ( is_object($topbanner) && isset($topbanner->ind_url) && $topbanner->ind_url != '' ) ? $topbanner->ind_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India SID( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[topbanner][ind_sid]" value="<?php echo ( is_object($topbanner) && isset($topbanner->ind_sid) && $topbanner->ind_sid != '' ) ? $topbanner->ind_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India Sponsor( IN )</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($topbanner) && isset($topbanner->ind_sponsor) && $topbanner->ind_sponsor != '' ) ? $topbanner->ind_sponsor : '0'; ?>
                        <select name="nameideas_wix[topbanner][ind_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $ind_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $ind_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $middlebanner = isset($nameideas_wix->middlebanner) ? $nameideas_wix->middlebanner : ''; ?>
    <fieldset>
        <h3>Middle Banner</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->url) && $middlebanner->url != '' ) ? $middlebanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->sid) && $middlebanner->sid != '' ) ? $middlebanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($middlebanner) && isset($middlebanner->sponsor) && $middlebanner->sponsor != '' ) ? $middlebanner->sponsor : '0'; ?>
                        <select name="nameideas_wix[middlebanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][geo_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_url) && $middlebanner->geo_url != '' ) ? $middlebanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][geo_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->geo_sid) && $middlebanner->geo_sid != '' ) ? $middlebanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($middlebanner) && isset($middlebanner->geo_sponsor) && $middlebanner->geo_sponsor != '' ) ? $middlebanner->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[middlebanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India URL( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][ind_url]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_url) && $middlebanner->ind_url != '' ) ? $middlebanner->ind_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India SID( IN )</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[middlebanner][ind_sid]" value="<?php echo ( is_object($middlebanner) && isset($middlebanner->ind_sid) && $middlebanner->ind_sid != '' ) ? $middlebanner->ind_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>India Sponsor( IN )</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($middlebanner) && isset($middlebanner->ind_sponsor) && $middlebanner->ind_sponsor != '' ) ? $middlebanner->ind_sponsor : '0'; ?>
                        <select name="nameideas_wix[middlebanner][ind_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $ind_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $ind_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $leftsidebarbottom = isset($nameideas_wix->leftsidebarbottom) ? $nameideas_wix->leftsidebarbottom : ''; ?>
    <fieldset>
        <h3>Left Sidebar Bottom</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[leftsidebarbottom][url]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->url) && $leftsidebarbottom->url != '' ) ? $leftsidebarbottom->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[leftsidebarbottom][sid]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->sid) && $leftsidebarbottom->sid != '' ) ? $leftsidebarbottom->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->sponsor) && $leftsidebarbottom->sponsor != '' ) ? $leftsidebarbottom->sponsor : '0'; ?>
                        <select name="nameideas_wix[leftsidebarbottom][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[leftsidebarbottom][geo_url]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_url) && $leftsidebarbottom->geo_url != '' ) ? $leftsidebarbottom->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[leftsidebarbottom][geo_sid]" value="<?php echo ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_sid) && $leftsidebarbottom->geo_sid != '' ) ? $leftsidebarbottom->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($leftsidebarbottom) && isset($leftsidebarbottom->geo_sponsor) && $leftsidebarbottom->geo_sponsor != '' ) ? $leftsidebarbottom->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[leftsidebarbottom][geo_sponsor]">
                            <option value="0">Select the geo_sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $dropdownbanner = isset($nameideas_wix->dropdownbanner) ? $nameideas_wix->dropdownbanner : ''; ?>
    <fieldset>
        <h3>Dropdown Banner</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[dropdownbanner][url]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->url) && $dropdownbanner->url != '' ) ? $dropdownbanner->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[dropdownbanner][sid]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->sid) && $dropdownbanner->sid != '' ) ? $dropdownbanner->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($dropdownbanner) && isset($dropdownbanner->sponsor) && $dropdownbanner->sponsor != '' ) ? $dropdownbanner->sponsor : '0'; ?>
                        <select name="nameideas_wix[dropdownbanner][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[dropdownbanner][geo_url]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->geo_url) && $dropdownbanner->geo_url != '' ) ? $dropdownbanner->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[dropdownbanner][geo_sid]" value="<?php echo ( is_object($dropdownbanner) && isset($dropdownbanner->geo_sid) && $dropdownbanner->geo_sid != '' ) ? $dropdownbanner->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($dropdownbanner) && isset($dropdownbanner->geo_sponsor) && $dropdownbanner->geo_sponsor != '' ) ? $dropdownbanner->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[dropdownbanner][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinkdropdown = isset($nameideas_wix->registerlinkdropdown) ? $nameideas_wix->registerlinkdropdown : ''; ?>
    <fieldset>
        <h3>Register link dropdown</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdropdown][url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->url) && $registerlinkdropdown->url != '' ) ? $registerlinkdropdown->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdropdown][sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sid) && $registerlinkdropdown->sid != '' ) ? $registerlinkdropdown->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdropdown][un_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->un_sid) && $registerlinkdropdown->un_sid != '' ) ? $registerlinkdropdown->un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->sponsor) && $registerlinkdropdown->sponsor != '' ) ? $registerlinkdropdown->sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinkdropdown][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdropdown][geo_url]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_url) && $registerlinkdropdown->geo_url != '' ) ? $registerlinkdropdown->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdropdown][geo_sid]" value="<?php echo ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sid) && $registerlinkdropdown->geo_sid != '' ) ? $registerlinkdropdown->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinkdropdown) && isset($registerlinkdropdown->geo_sponsor) && $registerlinkdropdown->geo_sponsor != '' ) ? $registerlinkdropdown->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinkdropdown][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinksavedidea = isset($nameideas_wix->registerlinksavedidea) ? $nameideas_wix->registerlinksavedidea : ''; ?>
    <fieldset>
        <h3>Register link savedidea</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinksavedidea][url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->url) && $registerlinksavedidea->url != '' ) ? $registerlinksavedidea->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinksavedidea][sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sid) && $registerlinksavedidea->sid != '' ) ? $registerlinksavedidea->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->sponsor) && $registerlinksavedidea->sponsor != '' ) ? $registerlinksavedidea->sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinksavedidea][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinksavedidea][geo_url]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_url) && $registerlinksavedidea->geo_url != '' ) ? $registerlinksavedidea->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinksavedidea][geo_sid]" value="<?php echo ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sid) && $registerlinksavedidea->geo_sid != '' ) ? $registerlinksavedidea->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinksavedidea) && isset($registerlinksavedidea->geo_sponsor) && $registerlinksavedidea->geo_sponsor != '' ) ? $registerlinksavedidea->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinksavedidea][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $registerlinkdomainchecker = isset($nameideas_wix->registerlinkdomainchecker) ? $nameideas_wix->registerlinkdomainchecker : ''; ?>
    <fieldset>
        <h3>Register link domain checker</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][url]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->url) && $registerlinkdomainchecker->url != '' ) ? $registerlinkdomainchecker->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Available domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->sid) && $registerlinkdomainchecker->sid != '' ) ? $registerlinkdomainchecker->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID (for Unavailable domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][un_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->un_sid) && $registerlinkdomainchecker->un_sid != '' ) ? $registerlinkdomainchecker->un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->sponsor) && $registerlinkdomainchecker->sponsor != '' ) ? $registerlinkdomainchecker->sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinkdomainchecker][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][geo_url]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_url) && $registerlinkdomainchecker->geo_url != '' ) ? $registerlinkdomainchecker->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Available Domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][geo_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_sid) && $registerlinkdomainchecker->geo_sid != '' ) ? $registerlinkdomainchecker->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID (for Unavailable Domains)</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[registerlinkdomainchecker][geo_un_sid]" value="<?php echo ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_un_sid) && $registerlinkdomainchecker->geo_un_sid != '' ) ? $registerlinkdomainchecker->geo_un_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($registerlinkdomainchecker) && isset($registerlinkdomainchecker->geo_sponsor) && $registerlinkdomainchecker->geo_sponsor != '' ) ? $registerlinkdomainchecker->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[registerlinkdomainchecker][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $checkforaltdomainchecker = isset($nameideas_wix->checkforaltdomainchecker) ? $nameideas_wix->checkforaltdomainchecker : ''; ?>
    <fieldset>
        <h3>Check for alternate Domain Checker</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdomainchecker][url]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->url) && $checkforaltdomainchecker->url != '' ) ? $checkforaltdomainchecker->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdomainchecker][sid]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->sid) && $checkforaltdomainchecker->sid != '' ) ? $checkforaltdomainchecker->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->sponsor) && $checkforaltdomainchecker->sponsor != '' ) ? $checkforaltdomainchecker->sponsor : '0'; ?>
                        <select name="nameideas_wix[checkforaltdomainchecker][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdomainchecker][geo_url]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_url) && $checkforaltdomainchecker->geo_url != '' ) ? $checkforaltdomainchecker->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdomainchecker][geo_sid]" value="<?php echo ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_sid) && $checkforaltdomainchecker->geo_sid != '' ) ? $checkforaltdomainchecker->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($checkforaltdomainchecker) && isset($checkforaltdomainchecker->geo_sponsor) && $checkforaltdomainchecker->geo_sponsor != '' ) ? $checkforaltdomainchecker->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[checkforaltdomainchecker][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <?php $checkforaltdropdown = isset($nameideas_wix->checkforaltdropdown) ? $nameideas_wix->checkforaltdropdown : ''; ?>
    <fieldset>
        <h3>Check for alternate Dropdown</h3>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdropdown][url]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->url) && $checkforaltdropdown->url != '' ) ? $checkforaltdropdown->url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdropdown][sid]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->sid) && $checkforaltdropdown->sid != '' ) ? $checkforaltdropdown->sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Sponsor</label>
                    </th>
                    <td>
                    <?php $sponsor = ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->sponsor) && $checkforaltdropdown->sponsor != '' ) ? $checkforaltdropdown->sponsor : '0'; ?>
                        <select name="nameideas_wix[checkforaltdropdown][sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO URL</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdropdown][geo_url]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_url) && $checkforaltdropdown->geo_url != '' ) ? $checkforaltdropdown->geo_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO SID</label>
                    </th>
                    <td>
                        <input type="text" name="nameideas_wix[checkforaltdropdown][geo_sid]" value="<?php echo ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_sid) && $checkforaltdropdown->geo_sid != '' ) ? $checkforaltdropdown->geo_sid : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>GEO Sponsor</label>
                    </th>
                    <td>
                    <?php $geo_sponsor = ( is_object($checkforaltdropdown) && isset($checkforaltdropdown->geo_sponsor) && $checkforaltdropdown->geo_sponsor != '' ) ? $checkforaltdropdown->geo_sponsor : '0'; ?>
                        <select name="nameideas_wix[checkforaltdropdown][geo_sponsor]">
                            <option value="0">Select the sponsor</option>
                            <option value="godaddy"<?php echo $geo_sponsor == 'godaddy' ? ' selected' : ''; ?>>GoDaddy</option>
                            <option value="wix"<?php echo $geo_sponsor == 'wix' ? ' selected' : ''; ?>>Wix</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>
<?php
    $btnText = (isset($id)) ? "Update Settings" : "Save Settings";
    ?>
    <p class="submit"><input type="submit" name="affiliateLinkSettings" id="affiliateLinkSettings-btn" class="button button-primary" value="<?php echo $btnText; ?>"></p>
</form>
<script>
    var geo_countries = <?php echo json_encode($geo_countries); ?>;
    $(document).ready(function() {
        $('.geo-countries').select2();
        $('.geo-countries').val(geo_countries).change();
    });
</script>