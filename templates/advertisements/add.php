<?php
    $media = '/wp-includes/images/media/default.png';
?>
<form method="post" class="add-advertisement">
    <?php if( isset($_GET['edit_advertisement']) ): ?>
        <input type="hidden" name="edit_advertisement" value="<?php echo $_GET['edit_advertisement']; ?>">
    <?php endif; ?>
        <legend>
            <h2>Create a new Advertisement</h2>
        </legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Name</label>
                    </th>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($advertisement->name) ? $advertisement->name : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Title</label>
                    </th>
                    <td>
                        <input type="text" name="title" value="<?php echo isset($advertisement->title) ? $advertisement->title : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Subtitle</label>
                    </th>
                    <td>
                        <input type="text" name="subtitle" value="<?php echo isset($advertisement->subtitle) ? $advertisement->subtitle : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Image</label>
                    </th>
                    <td>

                    <?php 
                        if(isset($advertisement) && !empty($advertisement->img)) {
                            $thumb_media = wp_get_attachment_url($advertisement->img);
                        }
                    ?>
                        <div class='media-preview-wrapper'>
                            <img id='card-thumb-preview' src='<?php echo isset($thumb_media) ? $thumb_media : $media; ?>' width='100' height='100' style='max-height: 100px; width: 100px;'>
                        </div>
                        <input id="card_thumb_button" type="button" class="button upload-media" value="<?php _e( 'Upload Media' ); ?>" data-preview="#card-thumb-preview" data-attachment="#card_thumb_attachment_id" media-type="Image" />
                        <input type='hidden' name='img' id='card_thumb_attachment_id' value="<?php echo isset($advertisement) ? $advertisement->img : ''; ?>">


                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Button Caption</label>
                    </th>
                    <td>
                        <input type="text" name="button_caption" value="<?php echo isset($advertisement->button_caption) ? $advertisement->button_caption : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Button Link</label>
                    </th>
                    <td>
                        <input type="text" name="button_link" value="<?php echo isset($advertisement->button_link) ? $advertisement->button_link : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Content</label>
                    </th>
                    <td>
                    <textarea name="content" id="content" rows="15"><?php echo isset($advertisement->content) ? $advertisement->content : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Advertisement Type</label>
                    </th>
                    <td>
                        <select name="advertisement_type">
                            <option value="normal" <?php if(isset($advertisement->advertisement_type) && $advertisement->advertisement_type == "normal"): ?> selected="selected" <?php endif; ?> >Normal</option>
                            <option value="sticky" <?php if(isset($advertisement->advertisement_type) && $advertisement->advertisement_type == "sticky"): ?> selected="selected" <?php endif; ?> >Sticky</option>
                        </select>
                    </td>
                </tr>

                <?php $class =  isset($advertisement->advertisement_type) && $advertisement->advertisement_type == "normal"  ? 'init-hide' : '';   ?>


                <?php $positions = json_decode($advertisement->positions);  ?>

                <tr class="form-field form-required position-fields <?= $class ?>">
                    <th scope="row">
                        <label for="">Position</label>
                    </th>
                    <td>
                       &nbsp;
                    </td>
                </tr>

                <tr class="form-field form-required position-fields <?= $class ?>">
                    <th scope="row">
                     Top   
                    </th>
                    <td>
                        <input name="top_val" type="text"  placeholder="" autocapitalize="none" maxlength="7" value="<?= isset($positions->top_val) ? $positions->top_val : ''; ?>" style="width: 150px;">
                    </td>
                </tr>

                <tr class="form-field form-required position-fields <?= $class ?>">
                    <th scope="row">
                     Bottom  
                    </th>
                    <td>
                        <input name="bottom_val" type="text"  placeholder="" autocapitalize="none" maxlength="7" value="<?= isset($positions->bottom_val) ? $positions->bottom_val : ''; ?>" style="width: 150px;">
                    </td>
                </tr>

                <tr class="form-field form-required position-fields <?= $class ?>">
                    <th scope="row">
                     Right  
                    </th>
                    <td>
                        <input name="right_val" type="text"  placeholder="" autocapitalize="none" maxlength="7" value="<?= isset($positions->right_val) ? $positions->right_val : ''; ?>" style="width: 150px;">
                    </td>
                </tr>

                <tr class="form-field form-required position-fields <?= $class ?>">
                    <th scope="row">
                     left  
                    </th>
                    <td>
                        <input name="left_val" type="text"  placeholder="" autocapitalize="none" maxlength="7" value="<?= isset($positions->left_val) ? $positions->left_val : ''; ?>" style="width: 150px;">
                    </td>
                </tr>

               
            </tbody>
        </table>
    <?php if (isset($_GET['edit_advertisement'])) : ?>
        <input type="hidden" name="edit_advertisement" value="<?php echo $_GET['edit_advertisement']; ?>">
    <?php endif; ?>
    <p class="submit" style="margin-top: 0;"><input type="submit" name="createrequest" id="createrequest-btn" class="button button-primary" value="<?php echo (isset($_GET['edit_advertisement'])) ? 'Update Advertisement' : 'Add New Advertisement'; ?>"></p>

</form>