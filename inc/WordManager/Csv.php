<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\WordManager;

use Exception;

/**
 * CSV
 */
class Csv
{
    private string $basepath;
    private string $filename;
    private $handle;
    private array $result = [];
    private array $header = [];
    private bool $hasHeader = false;
    private array $specialHeaders = [];
    private bool $isExclude = true;
    private array $fakeHeaders = [];

    public function __construct(string $basepath, string $filename)
    {
        $this->basepath = $basepath;
        $this->filename = $filename;
    }

    public function getBasepath(): string {
        return $this->basepath;
    }

    public function setBasepath(string $basepath): void {
        $this->basepath = $basepath;
    }

    public function getFilename(): string {
        return $this->filename;
    }

    public function setFilename(string $filename): void {
        $this->filename = $filename;
    }

    public function getFullPath() : string {
        return $this->getBasepath().DIRECTORY_SEPARATOR.$this->getFilename();
    }

    public function isFileExists() {
        return file_exists($this->getFullPath());
    }

    public function setHandle(): void {
        try{
            if($this->isFileExists()) {
                $this->handle = fopen($this->getFullPath(), "r");
            }else {
                // throw new Exception('File not found.');
                $this->handle = null;
            }
        }catch ( Exception $e ) {
            echo $e->getMessage();
            exit;
        } 
    }

    public function getHandle() {
        return $this->handle;
    }

    public function getRow() {
        return fgetcsv($this->getHandle());
    }

    public function addResult($row): void {
        $this->result[] = $row;
    }

    public function getResult() {
        return $this->result;
    }

    public function isHeader(): bool {
        return $this->hasHeader;
    }

    public function setHasHeader(bool $status): void {
        $this->hasHeader = $status;
    }

    public function headerStatus(bool $status = false): Csv {
        $this->setHasHeader($status);
        return $this;
    }

    public function getSpecialHeaders(): array {
        return $this->specialHeaders;
    }

    public function setSpecialHeaders(array $specialHeaders): void {
        $this->specialHeaders = $specialHeaders;
    }

    public function specialHeaders(array $specialHeaders): Csv {
        $this->setSpecialHeaders($specialHeaders);
        return $this;
    }
    
    public function getExclude(): bool {
        return $this->isExclude;
    }

    public function setExclude(bool $status): void {
        $this->isExclude = $status;
    }

    public function excludeSpecialHeaderStatus(bool $status): Csv {
        $this->setExclude($status);
        return $this;
    } 

    public function getHeader(): array{
        return $this->header;
    }

    public function setHeader(array $row): void{
        $this->header = $row;
    }

    public function getFakeHeader(): array {
        return $this->fakeHeaders;
    }

    public function setFakeHeader(array $data): void {
        if(!empty($data)) {
            foreach($data as $key => $row) {
                $this->fakeHeaders[$key] = "FAKE-HEAD-".$key;
            }
        }
    }

    public function cleanData(&$data): void {
        if(!empty($this->getSpecialHeaders())) {
            $temporaryIncluded = [];
            foreach($this->getSpecialHeaders() as $h) {
                if($this->getExclude()) {
                    unset($data[$h]);
                }else{
                    $temporaryIncluded[$h] = $data[$h];
                }
            }
            if(!$this->getExclude()) {
                $data = $temporaryIncluded;
            }
        }
        $this->setFakeHeader($data);
    }

    public function readCSV(): array {
        $this->setHandle();
        if($this->getHandle() == null) {
            $this->setHeader([]);
            $this->addResult([]);
            return $this->getResult();
        }
        $first = true;
        while(($data = $this->getRow()) !== FALSE) {

            $this->cleanData($data);
            
            if($first && $this->isHeader()) {
                $this->setHeader($data);
            }else{
                $this->addResult($data);
            }
            
            $first = false;
        }
        return $this->getResult();

    }

}