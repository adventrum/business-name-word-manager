<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\WordManager;

use BNWM\Base\Config;
use BNWM\Base\Pager;
use PDO;

/**
 * DataBaseManager
 */
class DataBaseManager
{
    protected $db; 
    protected $config;
    protected $pdo;
    protected $dbuser;
    protected $dbpassword;
    protected $dbname;
    protected $dbhost;

    public function __construct() 
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->config = Config::instance();
        $this->dbuser = $wpdb->dbuser;
        $this->dbpassword = $wpdb->dbpassword;
        $this->dbname = $wpdb->dbname;
        $this->dbhost = $wpdb->dbhost;
        // $this->setPDO();
    }

    public function __destruct() {
        $this->pdo = null;
    }

    public function setPDO() {
        if(empty($this->pdo)){
            $this->pdo = new PDO("mysql:host=$this->dbhost;dbname=".$this->dbname, $this->dbuser, $this->dbpassword);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }


    public function insertMany(string $tablename, array $columns, array $data) {
        $fields = implode(", ", $columns);
        $sql = " INSERT IGNORE INTO $tablename ($fields) VALUES ";
        $values = [];
		foreach($data as $row) {
            if(is_array($row)) {
                $subVal = [];
                foreach($row as $single) {
                    $subVal[] = '"'.$single.'"';
                }
                $innerD = implode(", ",$subVal);
                $values = ' ('.$innerD.') ';
            }else{
                $values[] = ' ("'.$row.'") ';   
            }
		}

        $finalValues = implode(",", $values);
        $this->db->query($sql. $finalValues );

    }


}