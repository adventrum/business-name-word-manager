<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Pages;

use BNWM\Api\SettingsApi;
use BNWM\Base\BaseController;
use BNWM\Api\Callbacks\AdminCallbacks;
use BNWM\Api\Callbacks\AdvertisementCallbacks;
use BNWM\Api\Callbacks\TemplateCallbacks;
use BNWM\WordManager\WordManager;

class Admin extends BaseController
{
	public $callbacks;
	public $templateCallBacks;
	public $advertisementCallBacks;

    public $settings;

    public $wordManager;

	public $pages = array();

	public $subpages = array();

	public function register() 
	{
		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->templateCallBacks = TemplateCallbacks::instance();

		$this->advertisementCallBacks = AdvertisementCallbacks::instance();

		$this->wordManager = new WordManager();

		$this->setPages();

		$this->setSubpages();

		$this->settings->addPages( $this->pages )->withSubPage( 'Dashboard' )->addSubPages( $this->subpages )->register();
	}

	public function setPages() 
	{
		$this->pages = array(
			array(
				'page_title' => 'Word Manager', 
				'menu_title' => 'Word Manager', 
				'capability' => 'manage_options', 
				'menu_slug' => 'business_name_word_manager', 
				'callback' => array( $this->callbacks, 'adminDashboard' ), 
				'icon_url' => 'dashicons-welcome-learn-more', 
				'position' => 110
			)
		);
	}

	public function setSubpages()
	{
		$this->subpages = array(
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Word Requests', 
				'menu_title' => 'Word Requests', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_word_requests', 
				'callback' => array( $this->callbacks, 'adminAddRequest' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Words', 
				'menu_title' => 'Words', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_words', 
				'callback' => array( $this->callbacks, 'getWordsDashboard' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Word Categories', 
				'menu_title' => 'Word Categories', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_word_categories', 
				'callback' => array( $this->callbacks, 'getWordCategories' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Generator Shortcodes', 
				'menu_title' => 'Generator Shortcodes', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_generator_shortcodes', 
				'callback' => array( $this->callbacks, 'getGeneratorShortcodes' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Filter Options', 
				'menu_title' => 'Filter Options', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_generator_filters', 
				'callback' => array( $this->callbacks, 'getGeneratorFilters' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Templates', 
				'menu_title' => 'Templates', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_generator_templates', 
				'callback' => array( $this->templateCallBacks, 'templatePages' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Advertisements', 
				'menu_title' => 'Advertisements', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_generator_advertisements', 
				'callback' => array( $this->advertisementCallBacks, 'advertisementPages' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Afiiliate Links', 
				'menu_title' => 'Afiiliate Links', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_generator_affiliates', 
				'callback' => array( $this->advertisementCallBacks, 'affiliateUrls' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'API to CSV', 
				'menu_title' => 'API to CSV', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_api_to_csv', 
				'callback' => array( $this->callbacks, 'adminCreateCsv' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Email Template', 
				'menu_title' => 'Email Template', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_email_template', 
				'callback' => array( $this->callbacks, 'emailTemplateSettings' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'String Translations', 
				'menu_title' => 'String Translations', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_string_setting', 
				'callback' => array( $this->callbacks, 'stringTranslations' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Sitemap Settings', 
				'menu_title' => 'Sitemap Settings', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_sitemap_settings', 
				'callback' => array( $this->callbacks, 'sitemapSettings' )
			),
			array(
				'parent_slug' => 'business_name_word_manager', 
				'page_title' => 'Settings', 
				'menu_title' => 'Settings', 
				'capability' => 'manage_options', 
				'menu_slug' => 'bnwm_settings', 
				'callback' => array( $this->callbacks, 'settingsPage' )
			)
		);
	}
}