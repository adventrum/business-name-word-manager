<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\DataMuse;


class DataMuseResponse extends Response {

    private $response;
    private $responseCode;    
    /**
     * __construct
     *
     * @param  mixed $res
     * @return void
     */
    function __construct($res) {
      $this->response = json_decode($res->getResponse(), true);
      $this->responseCode = $res->getResponseCode();
    }
    
    /**
     * getResponse
     *
     * @return array
     */
    public function getResponse(): array {
        return $this->response;
    }

    public function setResponse($resp): void {
        $this->response = $resp;
    }
    
    /**
     * getResult
     *
     * @return array
     */
    public function getResult(): array {
        $result = [
            'data' => $this->getResponse(),
            'responseCode' => $this->responseCode
        ];
        return $result;
    }
    
    /**
     * extract
     *
     * @param  array $fields
     * @return DataMuseResponse
     */
    public function extract($fields, $addOns = [], $excludedWords = []): DataMuseResponse {

        $response = [];
        $arrayResponse = (is_array($this->response)) ? $this->response : json_decode($this->response);
        if(!empty($arrayResponse)){
            $i = 0;
            foreach($arrayResponse as $key => $value) {
                $cKey = $i;
                if(is_array($value)) {
                    if(isset($value["word"]) && !in_array(strtolower(trim($value["word"])), $excludedWords)) {
                        foreach($value as $k => $v) {
                            if(in_array($k, $fields) || in_array("*", $fields) ) {
                                $response[$cKey][$k] = $v;
                            }
                        }
                        $i++;
                    }
                }
                if(!empty($addOns)) {
                    foreach($addOns as $customKey => $val) {
                        if(!isset($response[$cKey][$customKey])) {
                            $response[$cKey][$customKey] = $val;
                        }else {
                            if(is_array($response[$key][$customKey])) {
                                $response[$cKey][$customKey][] = $val;
                            }
                        }
                    }
                }
            }
        }
        $this->setResponse($response);
        return $this;
    }

}