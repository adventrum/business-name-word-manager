<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\DataMuse;

/**
 *
 *
 * @since      1.0.0
 * @package    BusinessNameWordManager
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WpErrorHandler' ) ) :

    final class WpErrorHandler implements ErrorHandlerInterface {

        private array $errDictionary;

        /**
         * @var Instance
         * @since 1.0
         */
        protected static $instance = null;


        public static function instance(): ErrorHandlerInterface {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        
        /**
         * getErr
         *
         * @param  mixed $key
         * @return string
         */
        public function getErr($key): string {
            return $this->errDictionary[$key];
        }        
        /**
         * setErr
         *
         * @param  mixed $key
         * @param  mixed $value
         * @return void
         */
        public function setErr($key, $value): void {
            $this->errDictionary[$key] = $value;
        }        
        /**
         * actionErr
         *
         * @param  mixed $function
         * @param  mixed $message
         * @param  mixed $version
         * @return void
         */
        public function actionErr($function, $message, $version): void {
            _doing_it_wrong($function, __($message), $version);
        }

    }


endif; 