<?php
/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\Utility;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

trait ArrayUtility {

    public function arrayWithDefault($array, $key, $default) {
        if(is_array($array)) {
            return isset($array[$key]) ? $array[$key] : $default;
        }
        return $default;
    }

    public function dd($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        die;
    }

    public function extractArrayOut($array, $preserveKey = false) {
        return iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($array)), $preserveKey);
    }
}

?>