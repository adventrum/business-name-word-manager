<?php

/**
 * @package BusinessNameWordManager
 *
 */

namespace BNWM\Base;

class Pager
{
    private $baseUrl;
    public function findStart($baseUrl, $limit)
    {
        if ((!isset($_GET['nav'])) || ($_GET['nav'] == "1")) {
            $start = 0;
            $_GET['nav'] = 1;
        } else {
            $start = ($_GET['nav'] - 1) * $limit;
        }
        $this->baseUrl = $baseUrl;
        return $start;
    }

    /*
       * int findPages (int count, int limit) 
       * Returns the number of pages needed based on a count and a limit 
       */
    public function findPages($count, $limit)
    {
        $pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;

        return $pages;
    }

    /* 
    * string pageList (int curpage, int pages) 
    * Returns a list of pages in the format of "« < [pages] > »" 
    **/
    public function pageList($curpage, $pages)
    {
        $page_list  = '<span class="pagination-links">';

        /* Print the first and previous page links if necessary */
        if (($curpage != 1) && ($curpage)) {
            $page_list .= "  <a class='first-page button' href=\" " . $this->baseUrl . "&nav=1\" title=\"First Page\">«</a> ";
        }

        if (($curpage - 1) > 0) {
            $page_list .= "<a class='prev-page button' href=\" " . $this->baseUrl . "&nav=" . ($curpage - 1) . "\" title=\"Previous Page\">‹</a> ";
        }

        /* Print the numeric page list; make the current page unlinked and bold */
        $page_list .= '<span id="table-paging" class="paging-input">Page <span class="tablenav-paging-text">';
        $page_list .= $curpage;
        $page_list .= " of $pages</span></span></span>";

        /* Print the Next and Last page links if necessary */
        if (($curpage + 1) <= $pages) {
            $page_list .= "<a class='next-page button' href=\"" . $this->baseUrl . "&nav=" . ($curpage + 1) . "\" title=\"Next Page\">›</a> ";
        }

        if (($curpage != $pages) && ($pages != 0)) {
            $page_list .= "<a class='last-page button' href=\"" . $this->baseUrl . "&nav=" . $pages . "\" title=\"Last Page\">»</a> ";
        }
        $page_list .= "</span>\n";

        return $page_list;
    }

    /*
    * string nextPrev (int curpage, int pages) 
    * Returns "Previous | Next" string for individual pagination (it's a word!) 
    */
    public function nextPrev($curpage, $pages)
    {
        $next_prev  = "";

        $next_prev .= '<span class="tablenav-pages-navspan button" aria-hidden="true">';
        
        if (($curpage - 1) <= 0) {
            $next_prev .= "Previous";
        } else {
            $next_prev .= "<a href=\"" . $this->baseUrl . "&nav=" . ($curpage - 1) . "\">Previous</a>";
        }

        $next_prev .= '</span>';

        $next_prev .= " | ";

        if (($curpage + 1) > $pages) {
            $next_prev .= "Next";
        } else {
            $next_prev .= "<a class='next-page button' href=\"" . $this->baseUrl . "&nav=" . ($curpage + 1) . "\">Next</a>";
        }

        return $next_prev;
    }
}
