<?php

/**
 * @package BusinessNameWordManager
 *
 */

namespace BNWM\Base;

use BNWM\Api\Callbacks\AdminCallbacks;

class Deactivate
{
    public static function deactivate()
    {
        global $wpdb;
        $adminCb = new AdminCallbacks();
        $adminCb->unsetCronJob();

      //  $wpdb->query("DROP table if exists wp_bnwm_requests, wp_bnwm_word_categories, wp_bnwm_word_categories_associations, wp_bnwm_word_dictionary, wp_bnwm_word_types, wp_bnwm_word_types_associations, wp_bnwm_generator_shortcodes, wp_bnwm_generator_filter_options, wp_bnwm_language_categories, wp_bnwm_languages, wp_bnwm_language_words;");
        flush_rewrite_rules();
    }
}
