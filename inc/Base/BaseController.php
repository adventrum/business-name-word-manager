<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Base;

class BaseController
{
	public $plugin_path;

	public $plugin_url;

	public $plugin_name;
	
	/**
	 * db
	 *
	 * @var mixed
	 */
	public $db;

	public $requests_table;

	public $dictionary_table;

	public $cron_hook;

	public $cron_hook_domain;

	public function __construct() {
		global $wpdb;
		$this->plugin_path = WORD_MANAGER_PLUGIN_PATH;
		$this->plugin_url = WORD_MANAGER_PLUGIN_URL;
		$this->plugin_name = WORD_MANAGER_PLUGIN_NAME;
		$this->db = $wpdb;

		$this->requests_table = $this->db->prefix.'bnwm_requests';
		$this->dictionary_table = $this->db->prefix.'bnwm_word_dictionary';

		$this->cron_hook = 'bnwm_requests_processor';
		$this->cron_hook_domain = 'bnwm_domain_fetcher';
	}
}