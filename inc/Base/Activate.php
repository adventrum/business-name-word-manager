<?php

/**
 * @package BusinessNameWordManager
 *
 */

namespace BNWM\Base;

use BNWM\Api\Callbacks\AdminCallbacks;

class Activate
{
    public static function activate()
    {
        global $wpdb;
		$config = Config::instance();

        $charset_collate = $wpdb->get_charset_collate();

		/**
		 * Table: languages
		 */
		$language_table = $config->getLanguageTableName();
		$sql = "CREATE TABLE IF NOT EXISTS $language_table (
				code varchar(20) NOT NULL PRIMARY KEY,
				name varchar(100) NULL
			)  
			$charset_collate;";
		
		$wpdb->query($sql);

		/**
		 * Insert Initial 
		 * 
		 */
		$language_records = [
			["code"=>"en", "name"=>"English"],
			["code"=>"de", "name"=>"German"],
			["code"=>"es", "name"=>"Spanish"],
			["code"=>"fr", "name"=>"French"],
			["code"=>"it", "name"=>"Italy"],
			["code"=>"pt", "name"=>"Portuguese"],
		];
		$sql_insert_languages = "INSERT INTO $language_table (code, name) VALUES ";
		$values = [];
		foreach($language_records as $row) {
			$values[] = ' ("'.$row["code"].'", "'.$row["name"].'") ';
		}
		$wpdb->query($sql_insert_languages. implode(",", $values) );

		
		/**
		 * Table: requests
		 */
        $requests_table = $config->getRequestTableName();
        $sql = "CREATE TABLE IF NOT EXISTS $requests_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(100) NULL, 
			request_type varchar(100) default 'txt',
			request_data longtext NULL,
			status varchar(100) NULL,
			total_items int(11) NULL,
			processed_items int(11) default 0, 
			language_code varchar(20),
			created_at datetime NULL, 
			updated_at datetime NULL
			)  
			$charset_collate;";

        $wpdb->query($sql);

		/**
		 * Table: word_types
		 */
		$word_type_table = $config->getWordTypeTableName();
		$sql_word_type = "CREATE TABLE IF NOT EXISTS $word_type_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(255),
			ordering int(2) default 0,
			loaded_languages varchar(255) null
		)
		$charset_collate;";
		$wpdb->query($sql_word_type);

		/**
		 * Insert Initial 
		 * 
		 */
		$word_type_records = [
			["name"=>"Keywords", "ordering"=>1],
			["name"=>"Category-Based", "ordering"=>2],
			["name"=>"Matching-Based", "ordering"=>3],
			["name"=>"General", "ordering"=>4],
			["name"=>"Word-With-Attributes", "ordering"=>5],
			["name"=>"Category", "ordering"=>6],
		];

		$sql_insert_word_type = "INSERT INTO $word_type_table (name, ordering) VALUES ";
		$values = [];
		foreach($word_type_records as $row) {
			$values[] = ' ("'.$row["name"].'", '.$row["ordering"].') ';
		}
		$wpdb->query($sql_insert_word_type. implode(",", $values) );
		
		/**
		 * Table: word_categories
		 * Schema Definition
		 */
		$category_table = $config->getWordCategoriesTableName();
		$sql_category = "CREATE TABLE IF NOT EXISTS $category_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(255) UNIQUE KEY,
			status enum('Active', 'Inactive') default 'Active'
		)
		$charset_collate;";
		$wpdb->query($sql_category);

		/**
		 * Table: language_categories
		 * Schema Definition
		 */
		$categories_language_words_table = $config->getLanguageCategoryTableName();
		$sql_language_categories = "CREATE TABLE IF NOT EXISTS $categories_language_words_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			language_code varchar(20),
			category_id bigint(20)
		)
		$charset_collate;";
		$wpdb->query($sql_language_categories);

		/**
		 * Table word_dictionary
		 * Schema Definition
		 */
        $dictionary_table = $config->getWordDictionaryTableName();

        $sql_list = "CREATE TABLE IF NOT EXISTS $dictionary_table (
				id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				request_id bigint(20) NULL default 0, 
				name varchar(255) UNIQUE KEY,
				selected_keyword tinyint(1) default 0,
				meanings JSON NULL,
				homophones JSON NULL,
				adjectives JSON NULL,
				nouns JSON NULL,
				associated_topics JSON NULL,
                status varchar(100) NULL
			)
			$charset_collate;";

        $wpdb->query($sql_list);

		/**
		 * Table: language_words
		 * Schema Definition
		 */
		$word_language_words_table = $config->getLanguageWordTableName();
		$sql_language_words = "CREATE TABLE IF NOT EXISTS $word_language_words_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			language_code varchar(20),
			word_id bigint(20),
			use_before varchar(20) NULL,
			use_after varchar(20) NULL,
			before_after varchar(20) NULL,
			use_end_only varchar(20) NULL,
			masculine varchar(255) NULL,
			feminine varchar(255) NULL,
			plural varchar(255) NULL,
			masculine_plural varchar(255) NULL,
			feminine_plural varchar(255) NULL
		)
		$charset_collate;";
		$wpdb->query($sql_language_words);
		

		/**
		 * Table: word_categories_associations
		 * Schema Definition
		 */
		$word_category_association_table = $config->getWordCatAssocTableName();
		$sql_word_category = "CREATE TABLE IF NOT EXISTS $word_category_association_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			word_id bigint(20),
			category_id bigint(20)
		)
		$charset_collate;";
		$wpdb->query($sql_word_category);

		/**
		 * Table: word_types_associations
		 * Schema Definition
		 */
		$word_type_association_table = $config->getWordTypeAssocTableName();
		$sql_word_type_association = "CREATE TABLE IF NOT EXISTS $word_type_association_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			word_id bigint(20),
			word_type_id bigint(20)
		)
		$charset_collate;";
		$wpdb->query($sql_word_type_association);

		/**
		 * Table: generator_shortcodes
		 * Schema Definition
		 */
		$generator_shortcodes_table = $config->getGeneratorShortcodeTableName();
		$sql_generator_shortcodes = "CREATE TABLE IF NOT EXISTS $generator_shortcodes_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			filters_id bigint(20),
			name varchar(256),
			shortcode_options JSON NULL
		)
		$charset_collate;";
		$wpdb->query($sql_generator_shortcodes);

        $adminCb = new AdminCallbacks();
        $adminCb->cronInitialization();

		/**
		 * Table: generator_filters
		 * Schema Definition 
		 */
		$generator_filters_table = $config->getGeneratorFiltersTableName();
		$sql_generator_filters = "CREATE TABLE IF NOT EXISTS $generator_filters_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(256),
			filter_options JSON NULL
		)
		$charset_collate;";
		$wpdb->query($sql_generator_filters);              


		/**
		 * Table: templates
		 * Schema Definition 
		 */
		$template_table = $config->getTemplateTableName();
		$sql_template = "CREATE TABLE IF NOT EXISTS $template_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(255),
			content TEXT NULL
		)
		$charset_collate;";
		$wpdb->query($sql_template);
		
		/**
		 * Table: advertisements
		 * Schema Definition 
		 */
		$advertisement_table = $config->getAdvertisementTableName();
		$sql_advertisement = "CREATE TABLE IF NOT EXISTS $advertisement_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(255),
			title varchar(255) NULL,
			subtitle varchar(255) NULL,
			img varchar(255),
			content TEXT NULL,
			button_caption varchar(255) NULL,
			button_link varchar(255) NULL,
			advertisement_type enum('normal', 'sticky') default 'normal'
		)
		$charset_collate;";
		$wpdb->query($sql_advertisement);


		/**
		 * Table: domains
		 * Schema Definition 
		 */
		$domains_table = $config->getDomainsTableName();
		$sql_domains = "CREATE TABLE IF NOT EXISTS $domains_table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title varchar(255),
			product_id int(10),
			price double(8,2),
			regular_price varchar(255),
			sale_price varchar(255),
			status varchar(255),
			featured_src varchar(255),
			tags varchar(255),
			categories varchar(255),
			permalink varchar(255),
			price_html varchar(255),
			alt varchar(255) NULL
		)
		$charset_collate;";
		$wpdb->query($sql_domains);


		/**
		 * Indexing
		 */
		// Category Name Indexing
		$sql = "CREATE INDEX idx_word_categories_name on ".$config->getWordCategoriesTableName()."(name)";
		$wpdb->query($sql);


		// Word Category Association word_id
		$sql = "CREATE INDEX idx_word_categories_associations_word_id on ".$config->getWordCatAssocTableName()."(word_id)";
		$wpdb->query($sql);

		// Word Category Association category_id
		$sql = "CREATE INDEX idx_word_categories_associations_category_id on ".$config->getWordCatAssocTableName()."(category_id)";
		$wpdb->query($sql);

		// Word Category Association category_id+word_id
		$sql = "CREATE INDEX idx_word_categories_associations_word_id_category_id on ".$config->getWordCatAssocTableName()."(category_id, word_id)";
		$wpdb->query($sql);

		// Word Language Association word_id
		$sql = "CREATE INDEX idx_language_words_word_id on ".$config->getLanguageWordTableName()."(word_id)";
		$wpdb->query($sql);

		// Word Language Association word_id+use_end_only
		$sql = "CREATE INDEX idx_language_words_word_id_use_end_only on ".$config->getLanguageWordTableName()."(word_id, use_end_only)";
		$wpdb->query($sql);

		// Word Dictionary name
		$sql = "CREATE INDEX idx_word_dictionary_name on ".$config->getWordDictionaryTableName()."(name)";
		$wpdb->query($sql);
	

        // $adminCb = new AdminCallbacks();
        // Disable due to load issue
		//$adminCb->cronInitialization();

        flush_rewrite_rules();
    }
}
