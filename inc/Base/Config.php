<?php

/**
 * @package BusinessNameWordManager
 *
 */

namespace BNWM\Base;

class Config
{

    /**
	 * @var Instance
	 * @since 1.0
	 */
	protected static $instance = null;
    
    private $db;

    private $pluginPrefix;

    private string $requestTbl = "requests";
    private string $wordTypesTbl = "word_types";
    private string $wordCategoriesTbl = "word_categories";
    private string $wordDictionaryTbl = "word_dictionary";
    private string $wordCategoryAssociationTbl = "word_categories_associations";
    private string $wordTypesAssociationTbl = "word_types_associations";
    private string $languageTbl = "languages";
    private string $languageWordTbl = "language_words";
    private string $languageCategoryTbl = "language_categories";
    private string $generatorShortcodesTbl = "generator_shortcodes";
    private string $generatorFiltersTbl = "generator_filter_options";
    private string $templateTbl = "templates";
    private string $advertisementTbl = "advertisements";
    private string $domainsTbl = "domains";
    private string $settings = "settings";
    private string $popular_keywords = "popular_keywords";
    

    private string $basePath;
    private string $pluginSlug;

    private string $csvDirectory = "/assets/admin/data/";
    private string $categoriesCsvFileName = "categories.csv";
    private string $keywordCsvFileName = "keywords.csv";
    private string $categoryWordCsvFileName = "category_words.csv";
    private string $matchingWordCsvFileName = "matching_words.csv";
    private string $attributeCsvFileName = "attribute_words.csv";

    private string $language = "en";

    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->setPluginPrefix("bnwm_");
        $this->pluginSlug = "business-name-word-manager";
        $this->basePath = WP_PLUGIN_DIR;
    }

    

	/**
     * instance
     *
     * @return Config
     */
    public static function instance(): Config {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

    public function getLanguageTableName(): string {
        return $this->getTablePrefix().$this->languageTbl;
    }
    
    /**
     * getLanguage
     *
     * @return string
     */
    public function getLanguage(): string {
        return $this->language;
    }
    
    /**
     * setLanguage
     *
     * @param  mixed $lang
     * @return void
     */
    public function setLanguage(string $lang): void {
        $this->language = $lang;
    }
    
    /**
     * getTablePrefix
     *
     * @return string
     */
    public function getTablePrefix(): string {
        return $this->db->prefix.$this->pluginPrefix;
    }
    
    /**
     * getDBPrefix
     *
     * @return string
     */
    public function getDBPrefix(): string {
        return $this->db->prefix;
    }
    
    /**
     * getPluginPrefix
     *
     * @return string
     */
    public function getPluginPrefix(): string {
        return $this->pluginPrefix;
    }
    
    /**
     * setPluginPrefix
     *
     * @param  mixed $prefix
     * @return void
     */
    public function setPluginPrefix(string $prefix = "bnwm_"): void {
        $this->pluginPrefix = $prefix;
    }
    
    /**
     * getRequestTableName
     *
     * @return string
     */
    public function getRequestTableName(): string {
        return $this->getTablePrefix().$this->requestTbl;
    }
    
    /**
     * getWordTypeTableName
     *
     * @return string
     */
    public function getWordTypeTableName(): string {
        return $this->getTablePrefix().$this->wordTypesTbl;
    }
    
    /**
     * getWordCategoriesTableName
     *
     * @return string
     */
    public function getWordCategoriesTableName(): string {
        return $this->getTablePrefix().$this->wordCategoriesTbl;
    }
    
    /**
     * getWordDictionaryTableName
     *
     * @return string
     */
    public function getWordDictionaryTableName(): string {
        return $this->getTablePrefix().$this->wordDictionaryTbl;
    }
    
    /**
     * getWordCatAssocTableName
     *
     * @return string
     */
    public function getWordCatAssocTableName(): string {
        return $this->getTablePrefix().$this->wordCategoryAssociationTbl;
    }

    public function getLanguageWordTableName(): string {
        return $this->getTablePrefix().$this->languageWordTbl;
    }

    public function getLanguageCategoryTableName(): string {
        return $this->getTablePrefix().$this->languageCategoryTbl;
    }
    
    /**
     * getWordTypeAssocTableName
     *
     * @return string
     */
    public function getWordTypeAssocTableName(): string {
        return $this->getTablePrefix().$this->wordTypesAssociationTbl;
    }

    /**
     * getGeneratorShortcodeTableName
     *
     * @return string
     */
    public function getGeneratorShortcodeTableName(): string {
        return $this->getTablePrefix().$this->generatorShortcodesTbl;
    }

    
     /**
     * getPopularkeywordsTableName
     *
     * @return string
     */
    public function getPopularkeywordsTableName(): string {
        return $this->getTablePrefix().$this->popular_keywords;
    }

    
    /**
     * getGeneratorFiltersTableName
     *
     * @return string
     */
    public function getGeneratorFiltersTableName(): string {
        return $this->getTablePrefix().$this->generatorFiltersTbl;
    }

    /**
     * getTemplateTableName
     *
     * @return string
     */
    public function getTemplateTableName(): string {
        return $this->getTablePrefix().$this->templateTbl;
    }

    /**
     * getAdvertisementTableName
     *
     * @return string
     */
    public function getAdvertisementTableName(): string {
        return $this->getTablePrefix().$this->advertisementTbl;
    }

    /**
     * getAdvertisementTableName
     *
     * @return string
     */
    public function getDomainsTableName(): string {
        // With Plugin Prefix
        // return $this->getTablePrefix().$this->domainsTbl;
        // Without Plugin Prefix
        return $this->domainsTbl;
    }


    /**
     * getsettingsTableName
     *
     * @return string
     */
    public function getsettingsTableName(): string {
        return $this->getTablePrefix().$this->settings;
    }
    
    /**
     * getBasePath
     *
     * @return string
     */
    public function getBasePath(): string {
        return $this->basePath;
    }
    
    /**
     * getPluginPath
     *
     * @return string
     */
    public function getPluginPath(): string {
        return $this->getBasePath().DIRECTORY_SEPARATOR.$this->getPluginSlug();
    }
    
    /**
     * getPluginSlug
     *
     * @return string
     */
    public function getPluginSlug(): string {
        return $this->pluginSlug;
    }
    
    /**
     * getCsvDirectory
     *
     * @return string
     */
    public function getCsvDirectory(): string {
        return $this->getPluginPath().$this->csvDirectory.$this->getLanguage().DIRECTORY_SEPARATOR;
    }

    public function getAttributeFilename(): string {
        return $this->attributeCsvFileName;
    }


    
    /**
     * getKeywordCSVFilename
     *
     * @return string
     */
    public function getKeywordCSVFilename(): string {
        return $this->keywordCsvFileName;
    }
    
    /**
     * getCategoryWordCSVFilename
     *
     * @return string
     */
    public function getCategoryWordCSVFilename(): string {
        return $this->categoryWordCsvFileName;
    }
    
    /**
     * getCategoriesCsvFileName
     *
     * @return string
     */
    public function getCategoriesCsvFileName(): string{
        return $this->categoriesCsvFileName;
    }
    
    /**
     * getMatchingWordCSVFilename
     *
     * @return string
     */
    public function getMatchingWordCSVFilename(): string {
        return $this->matchingWordCsvFileName;
    }
    
    /**
     * getKeywordCSVFilePath
     *
     * @return string
     */
    public function getKeywordCSVFilePath(): string {
        return $this->getCsvDirectory().$this->getKeywordCSVFilename();
    }
    
}