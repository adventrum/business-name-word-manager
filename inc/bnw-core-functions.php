<?php

use BNWM\Api\Callbacks\AdminCallbacks;
use BNWM\Api\Callbacks\ShortcodeCallbacks;
use BNWM\Api\Callbacks\TemplateCallbacks;
use BNWM\Base\Config;
$bnwm_config = Config::instance();



if(!function_exists("bnwm_get_search_results")) {

    function bnwm_get_search_results() {

    }
}

if(!function_exists("bnwm_search_results")) {
    
    function bnwm_search_results($params = array()) {
         
        
        if( empty($params["bname"]) )
         return false; 
        
        global $bnwm;

        $wmObj = $bnwm->wordmanager->word($params["bname"]);
        
        isset($params["position"])  ? $wmObj->position($params["position"]) : '';
        ( isset($params["character"]) && !empty($params["character"]) )  ? $wmObj->length((int) $params["character"]) : 0;
        
        $words = [];
        (isset($params["one_word"]) && $params["one_word"] == 'on' ) ? array_push($words,'one_word') : null;
        (isset($params["two_word"]) && $params["two_word"] == 'on' ) ? array_push($words,'two_word') : null;
       
        (isset($words) && !empty($words)) ? $wmObj->wordTypes($words) : '';  
        
        (isset($params["industry"]) && is_array($params["industry"]) && !empty($params["industry"]) ) ? $wmObj->categoryIds($params["industry"]) : '';             
        (isset($params["grammar"]) && is_array($params["grammar"]) && !empty($params["grammar"]) ) ? $wmObj->genderFilter($params["grammar"]) : '';             
        (isset($params["rhyming"]) && ($params["rhyming"] == 'on') ) ? $wmObj->isRhym(true) : $wmObj->isRhym(false);
       
        isset($params["limit"]) ? $wmObj->limit($params['limit'])  : '';
       
        isset($params['paged']) ? $wmObj->page($params['paged'])  : '';
       
        isset($params['separator']) ? $wmObj->separator('')  : $wmObj->separator(' ');
         
        isset($params["tld"]) ? $wmObj->tld($params["tld"])  : '';
       
        isset($params['tag']) ? $wmObj->wrapTag($params['tag'])  : '';
        
        $callbacks = new BNWM\Api\Callbacks\AdminCallbacks();
        $callbacks->getAlgorithm_type() == 'nameidea_algo_2' ?  $wmObj->nameIdeaAlgo2(true) : $wmObj->nameIdeaAlgo2(false);
        
        $languages = ['it','es','de','fr','pt-pt','pt-br'];
        $current_site = getCurrentSite();        
        $lang_code = in_array($current_site, $languages) ? $current_site : 'en';

        $wmObj->oldLang($lang_code);
        

        $response = []; 
        $response['result'] = $wmObj->getSearchResults();
        
        // Track total only for nameIdea_algo_1
        if($callbacks->getAlgorithm_type() != 'nameidea_algo_2') {
          $response['total'] = $wmObj->getTotalCount();  
        }
        
        $response['category'] = $bnwm->wordmanager->getPopulatedCategories(); 
        
        // echo "<pre>";
        // print_r($result);
        // dd($bnwm->wordmanager->getTotalCount());

        // exit;
        return $response;

    }
}

if(!function_exists("dd")) {
    function dd($array) {
        global $bnwm;
        $bnwm->dd($array);
    }
}

if( !function_exists("showWordManagerFilters") ){
    function showWordManagerFilters($selectedIndustries, $showToggles = ''){
        global $wpdb;
        $shortcode_id = ( isset($_GET['shortcode_id']) ) ? $_GET['shortcode_id'] : '1';
        $filter_options = $wpdb->get_col("SELECT filtr.filter_options FROM ".$wpdb->prefix."bnwm_generator_filter_options filtr LEFT JOIN ".$wpdb->prefix."bnwm_generator_shortcodes shortcode ON filtr.id=shortcode.filters_id WHERE shortcode.id=".$shortcode_id)[0];
        $filter_options = json_decode($filter_options);

        $columsname = $wpdb->get_results("SELECT id, name FROM ".$wpdb->prefix."bnwm_word_categories WHERE status = 'Active' ");
        $columns_count = count($columsname);
        if( get_page_template_slug() == 'template-domains.php' ):
            include_once WORD_MANAGER_PLUGIN_PATH.'templates/frontend/filter-domains.php';
        elseif( get_page_template_slug() == 'template-results.php' ):
              include_once WORD_MANAGER_PLUGIN_PATH.'templates/frontend/filter-results.php';
        elseif( get_page_template_slug() == 'template-nameideas.php' ):
            if( $showToggles == 'nameideas-toggles' ):
                include_once WORD_MANAGER_PLUGIN_PATH.'templates/frontend/filter-toggles-nameideas.php';
            else:
                include_once WORD_MANAGER_PLUGIN_PATH.'templates/frontend/filter-nameideas.php';
            endif;
        endif;
    }
}

if(!function_exists("getDomainList")) {
    function getDomainList($search_word, $page = 1, $limit = 4) {
        $aC = AdminCallbacks::instance();
        $results = $aC->getDomains($search_word, $page, $limit);
        return $results; 
    }
}


function get_total_record(){
    

    $data = $_REQUEST;
   
    if( empty($data["bname"]) )
        return false; 
    
    global $bnwm;

    $wmObj = $bnwm->wordmanager->word($data["bname"]);
    
    isset($data["position"])  ? $wmObj->position($data["position"]) : '';
    ( isset($params["character"]) && !empty($params["character"]) )  ? $wmObj->length((int) $params["character"]) : 0;
    
    $words = [];
    (isset($data["one_word"]) && $data["one_word"] == 'on' ) ? array_push($words,'one_word') : null;
    (isset($data["two_word"]) && $data["two_word"] == 'on' ) ? array_push($words,'two_word') : null;
    
    (isset($words) && !empty($words)) ? $wmObj->wordTypes($words) : '';  
    
    (isset($data["industry"]) && is_array($data["industry"]) && !empty($data["industry"]) ) ? $wmObj->categoryIds($data["industry"]) : '';             
    
    (isset($data["grammar"]) && is_array($data["grammar"]) && !empty($data["grammar"]) ) ? $wmObj->genderFilter($data["grammar"]) : '';             

    (isset($data["rhyming"]) && ($data["rhyming"] == 'on') ) ? $wmObj->isRhym(true) : $wmObj->isRhym(false);
    
    isset($data["limit"]) ? $wmObj->limit($data['limit'])  : '';
    
    isset($data['paged']) ? $wmObj->page($data['paged'])  : '';

    isset($data['separator']) ? $wmObj->separator('')  : $wmObj->separator(' ');
     
    isset($data["tld"]) ? $wmObj->tld($data["tld"])  : '';
    
    isset($data['tag']) ? $wmObj->wrapTag($data['tag'])  : '';

    $callbacks = new BNWM\Api\Callbacks\AdminCallbacks();
    $callbacks->getAlgorithm_type() == 'nameidea_algo_2' ?  $wmObj->nameIdeaAlgo2(true) : $wmObj->nameIdeaAlgo2(false);
 
    $languages = ['it','es','de','fr','pt-pt','pt-br'];
    $current_site = getCurrentSite();        
    $lang_code = in_array($current_site, $languages) ? $current_site : 'en';

    $wmObj->oldLang($lang_code);

    $total = $wmObj->getTotalCount();
    //dd($total);
    $totalFormat = number_format($total);
    
    $response = [];
    $response['total_format'] = $totalFormat;
    $response['total'] = $total;
    
    echo json_encode($response);  die;

}

add_action('wp_ajax_get_total_record', 'get_total_record');
add_action('wp_ajax_nopriv_get_total_record', 'get_total_record');


if(!function_exists("renderTemplate")) {
    function renderTemplate($template_id, $data = []) {

        $tC = TemplateCallbacks::instance();        
        $html = $tC->renderTemplate($template_id, $data);
        return $html;

    }
}

if(!function_exists("getAdvertisements")) {

    function getAdvertisements($shortcode_id) {
        
        $sC = ShortcodeCallbacks::instance();
        $advertisements = $sC->getAdvertisements($shortcode_id, 'normal');
        return $advertisements;

    }

}

if(!function_exists("getStickyAdvertisements")) {

    function getStickyAdvertisements($shortcode_id) {
        
        $sC = ShortcodeCallbacks::instance();
        $advertisements = $sC->getStickyAdvertisements($shortcode_id, "sticky");
        return $advertisements;

    }
}


if(!function_exists("getCurrentSite")) {
    function getCurrentSite() {
       
       if( is_multisite() ){
         $languages = ['it','es','de','fr','pt-pt','pt-br','nl','sv'];
         $current_site =  str_replace("/",'',get_blog_details()->path);
         return in_array($current_site, $languages) ? $current_site : 'en';
       }else{
         return 'en';
       }
    }
}

if(!function_exists("getNameideaAlgo")) {
    function getNameideaAlgo() {
       
      $callbacks = new BNWM\Api\Callbacks\AdminCallbacks();
      return $callbacks->getAlgorithm_type() == 'nameidea_algo_2' ?  true : false;
    }
}

if(!function_exists("getGeoCountriesList")) {
    function getGeoCountriesList() {
       
      return $country_codes = ['FR','DE','IT','ES','MX','BR','PT','NL','SE'];
    }
}

function get_paginationWithoutAJax($total_records = '', $limit = 96 )
{
   
   $strURL = esc_url(site_url() . '' . $_SERVER['REQUEST_URI']);
   $query_string = $_SERVER['QUERY_STRING'];  
   $arrVals = explode("?", $strURL);
   $arrVals2 = explode("/", $arrVals[0]);
   $numkey = str_word_count(stripslashes($_GET['bname']));
   $translations = json_decode(get_option('bnwm_string_translations'));
   if ($numkey <= '1') {
     $bname = stripslashes($_GET['bname']);
   } else {
     $fword = explode(' ', stripslashes($_GET['bname']));
     $bname = $fword[0] . ' ' . $fword[1];
   }
   if (isset($_GET['related'])) {
     $element = '?bname=' . $bname . "&related=" . $_GET['related'];
   } else {
     $element = '?bname=' . $bname;
   }
   $found = 0;
   $paged = count(explode('/',$_SERVER['REDIRECT_URL']));
   $paged = isset($paged) && !empty($paged) ? $paged : 5;
   if ($arrVals2 == '' || $paged < 5) {
       $found = $found + 1;
   } else {
       $found = $arrVals2[$paged];
   }
   //$pagination=$strURL;
   $current_page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
   $total_pages = ceil($total_records / $limit);
   
   $pagination = '';
   if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number
       $pagination .= '<ul>';
   
       if (isset($_GET['device']) && $_GET['device'] == 'mobile') {
           if ($current_page == 1) {
               $right_links = $current_page + 3;
           } elseif ($current_page > 1 && $current_page < 3) {
               $right_links = $current_page + 2;
           } else {
               $right_links = $current_page;
           }
       } else {
           $right_links = $current_page + 3;
       }
   
       $previous = $current_page - 3; //previous link
       $next = $current_page + 1; //next link
       $first_link = true; //boolean var to decide our first link
   
       if ($current_page > 1) {
           $previous_link = $current_page - 1;
           $prev_url = get_permalink().'page/'.$previous_link.'/?'. $query_string;
           //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
           $pagination .= '<li><a href="' .$prev_url. '" data-page="' . $previous_link . '" title="Previous"><span class="arrow_carrot-left elegant-icon"></span>' .$translations->previous . '</a></li>'; //previous link
           // }else{
           //  $pagination .= '<li><a href="'.$url . $previous_link . '" data-page="' . $previous_link . '" title="Previous">&lt; Back</a></li>'; //previous link
           // }
           for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
               $url = get_permalink().'page/'.$i.'/?'. $query_string;
               if ($i > 0) {
                   $pagination .= '<li><a href="' .$url. '" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
               }
           }
           $first_link = false; //set first link to false
       }
       $url = get_permalink().'page/'.$current_page.'/?'. $query_string;
       if ($first_link) { //if current active page is first link
           $pagination .= '<li class="select" ><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
       } elseif ($current_page == $total_pages) { //if it's the last active link
           $pagination .= '<li class="select" ><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
       } else { //regular current link
           $pagination .= '<li class="select"><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
       }
   
       for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links
           $url = get_permalink().'page/'.$i.'/?'. $query_string;
           if ($i <= $total_pages) {
               $pagination .= '<li><a href="' .$url. '"   title="Page ' . $i . '">' . $i . '</a></li>';
           }
       }
       if ($current_page < $total_pages) {
           $next_link = $current_page + 1;
           $next_url = get_permalink().'page/'.$next_link.'/?'. $query_string;
           //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
           $pagination .= '<li><a href="' .$next_url. '" data-page="' . $next_link . '" title="Next">' . $translations->next . ' <span class="arrow_carrot-right elegant-icon"></span></a></li>'; //next link
           // }else{
           //  $pagination .= '<li><a href="' .$url . $next_link . '" data-page="' . $next_link . '" title="Next">'.__('Next', 'thegem-child').' &gt;</a></li>'; //next link
           // }
       }
   
       $pagination .= '</ul>';
   }
   return $pagination; die;

}

function getAdditionalParameter(){
  
  $country_code = getenv('HTTP_GEOIP_COUNTRY_CODE');
  $home_id =  !empty($_GET['home_id']) ? $_GET['home_id'] : '';
  $device  =  !empty($_GET['device'])  ? $_GET['device']  : '';
 
  return '-'. $home_id . '-' . $device . '-' . $country_code . '';
   
}



function WixAffiliateLinks()
{

  $response = [];
  
  $lang_code = getCurrentSite();
  $country_code = getenv('HTTP_GEOIP_COUNTRY_CODE');
  

  $home_id =  !empty($_GET['home_id']) ? $_GET['home_id'] : '';
  $device  =  !empty($_GET['device'])  ? $_GET['device']  : '';
  $parameters  = $home_id . '-' . $device . '-' . $country_code .getGclidParam();

  if( $lang_code == 'es' && $country_code == 'MX' ){
      $lang_code = 'MX';
  } elseif( $lang_code == 'pt-br' && $country_code == 'BR' ){
      $lang_code = 'BR';
  } elseif( $lang_code == 'pt-pt' && $country_code == 'PT' ){
      $lang_code = 'PT';
  } elseif( $lang_code == 'sv' ){
    $lang_code = 'SE'; $country_code = 'SE';
  } elseif( $lang_code == 'nl'  ){
    $lang_code = 'NL'; $country_code = 'NL';  
  }

  if( strtoupper($country_code) == strtoupper($lang_code) ){
     $check_code = $country_code;
  } elseif( strtoupper($lang_code) == 'EN' && in_array($country_code,getGeoCountriesList()) ){
     $check_code = 'EN';
  }else{
    $check_code = '';
  }
   
  switch ($check_code) {
    case 'FR':
      $url            = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=';
      $textadvert     = 'Créez votre site avec Wix et obtenez un nom de domaine gratuit lorsque vous souscrivez à un forfait Premium';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=793&c=2913';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'DE':
      $url            = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=';
      $textadvert     = 'Erstelle deine Website mit Wix mit einem Premiumpaket und erhalte deine Domain kostenlos';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=772&c=2894';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'IT':
      $url            = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=';
      $textadvert     = 'Crea il tuo sito su Wix con un pacchetto Premium e ottieni un dominio gratuito';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=791&c=2911';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'ES':
      $url            = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=';
      $textadvert     = 'Crea tu sitio en Wix con un plan Premium y obtén tu dominio gratis';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=792&c=2912';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'MX':
      $url            = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=';
      $textadvert     = 'Crea tu sitio en Wix con un plan Premium y obtén tu dominio gratis';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=792&c=2912';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'BR':
      $url            = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=';
      $textadvert     = 'Crie seu site no Wix e obtenha um domínio gratuito ao adquirir um plano Premium';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=794&c=2914';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'PT':
      $url            = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=';
      $textadvert     = 'Crie seu site no Wix e obtenha um domínio gratuito ao adquirir um plano Premium';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=794&c=2914';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;
    
    case 'EN':
      $url            = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=';
      $textadvert     = 'Create your site on Wix with a Premium plan and get your domain for free';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=790&c=2910';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;

    case 'NL':
      $url            = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=';
      $textadvert     = 'Create your site on Wix with a Premium plan and get your domain for free';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=857&c=2977';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=857&c=2977&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;  

    case 'SE':
      $url            = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=';
      $textadvert     = 'Create your site on Wix with a Premium plan and get your domain for free';
      $textadvertlink = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=&s2=results-textad-'.$parameters;
      $popunder       = 'https://wixstats.com/?a=54991&oc=831&c=2951';
      $dropdownlink   = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=&s2=bng';
      $toggleLabel    = 'WIX';
      $sid_parameter  = '&s1=&s2=post';
      break;    

    default:
      $url            = 'https://www.tkqlhce.com/click-3797283-10730504';
      $textadvert     = 'Get a FREE Domain with Web Hosting. Only $1.99/mo with GoDaddy.';
      $textadvertlink = 'https://www.kqzyfj.com/1b110js0ys-FJNPNIOJFHLHJHIPH?sid=results-textad-'.$parameters;
      $popunder       = '';
      $dropdownlink   = 'https://www.tkqlhce.com/click-3797283-15162423';
      $sid_parameter  = '?SID=popunder-post';
      $toggleLabel    = '';
      break;
  }

  $response['url']             = $url;
  $response['country_code']    = $country_code;
  $response['textadvert']      = $textadvert;
  $response['textadvertlink']  = $textadvertlink;
  $response['toggleLabel']     = $toggleLabel;
  $response['popunder']        = $popunder;
  $response['dropdownlink']    = $dropdownlink;
  $response['sid_parameter']   = $sid_parameter;

  return $response;
}


function cronsetup() {

    if(getCurrentSite() == "en" ) {
        $adminCallback = new AdminCallbacks();

        $cron_hook_name = "bnwm_domain_fetcher_new";
        
        $timestamp = wp_next_scheduled( $cron_hook_name );

        //If $timestamp === false schedule daily backups since it hasn't been done previously
        if( $timestamp === false ){
            //Schedule the event for right now, then to repeat daily using the hook 
            wp_schedule_event( time(), 'every_day', $cron_hook_name );
        }
        add_action( $cron_hook_name, array($adminCallback, 'cronDomainFetch'));
    }
}


function getGclidParam(){
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $queries = explode('&', explode('?', $actual_link)[1]);
  $sid = '';
  foreach( $queries as $qs ){
      $query = explode('=', $qs);
      if( substr($qs,0,5) == 'gclid' ){
          $sid .= '-gclid-'.urldecode( $query[1] );
      }
  }
  return $sid;
}

function getUtmInputs(){
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $queries = explode('&', explode('?', $actual_link)[1]);
  $utms = '';
  foreach( $queries as $qs ){
      $query = explode('=', $qs);
      if( strtolower( substr($qs,0,3) ) == 'utm' ){
          $utms .= '<input type="hidden" name="'.$query[0].'" value="'.urldecode( $query[1] ).'">';
      } elseif( substr($qs,0,5) == 'gclid' ){
          $utms .= '<input type="hidden" name="gclid" value="'.urldecode( $query[1] ).'">';
      }
  }
  return $utms;
}


function getCurrentSiteName(){

    $site_url =  $_SERVER['HTTP_HOST'];
    
    if( $site_url == 'bng1106.wpengine.com' || $site_url == 'businessnamegenerator.com' ){
      return 'BNG';
    }
    else if( $site_url == 'bnwizstaging.wpengine.com' || $site_url == 'biznamewiz.com' ){
      return 'BNW';
    }
    else if( $site_url == 'bnamemaker.wpengine.com' || $site_url == 'businessnamemaker.com' || $site_url == 'bnwnewtheme.wpengine.com'  ){
      return 'BNM';
    }else{
      return 'BNG';
    }

}