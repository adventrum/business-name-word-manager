<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;

use BNWM\Base\Config;
use stdClass;

class ShortcodeCallbacks
{
	protected static $instance = null;
    private $db;
    private $textDomain;
    public $config;
	private $advertisementCallback;
    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->createShortcode();
        $this->config = Config::instance();
        $this->textDomain = 'business_name_word_manager';
		$this->advertisementCallback = AdvertisementCallbacks::instance();
	}
    
    /**
     * instance
     *
     * @return ShortcodeCallbacks
     */
    public static function instance(): ShortcodeCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}
    
    /**
     * createShortcode
     *
     * @return ShortcodeCallbacks
     */
    public function createShortcode(): void {
        add_shortcode( 'insertgenerator', [ $this, 'getShortcodeHtml' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'addShortcodeScripts' ] );
    }

    /**
     * getShortcodeHtml
     *
     * @return ShortcodeCallbacks
     */
    public function getShortcodeHtml( $atts ): string {
        $atts = shortcode_atts( array(
            'id' => '1',
			'placeholdertext' => '',
			'generator_placeholder_mob' => '',
			'btntxt' => '',
			'checkboxlabel' => '',
			'industry' => '',
			'generator-css-mode' => '',
			'toggle-btn-css-mode' => '',
			'active' => '',
			'mobile-results-iframe' => '',
			'popup-url' => '',
			'disablead' => '',
			'stickyads' => '',
			'stickyads-category'   => '',
			'toggle-visibility' => 'true'
        ), $atts, 'insertgenerator' );
        wp_enqueue_style( 'bnwm-generator-style' );
        wp_enqueue_script( 'bnwm-generator-script' );
        
        $data = $this->db->get_row("SELECT * FROM ".$this->config->getGeneratorShortcodeTableName()." WHERE id=".$atts['id']);
        if( $data == NULL ){
            return "";
        }
        $shortcode_options = json_decode($data->shortcode_options);

		$industry_list = '';
		$keyword = '';
		$string = '';
		$target = '';
		$disablead = "false";

		// Disable Advertisement
		if (isset($shortcode_options->hide_popunder_advert) && $shortcode_options->hide_popunder_advert != '') {
			$disablead = $shortcode_options->hide_popunder_advert;
		}
		if (isset($atts['disablead']) && $atts['disablead'] != '') {
			$disablead = esc_attr(trim($atts['disablead']));
		} 

		// Word Categories/Industries to apply by default
		if (!empty($shortcode_options->word_industries)) {
			$industry_list = $shortcode_options->word_industries;
		}
		if ($atts['industry']) {
			$data_industry = esc_attr(trim($atts['industry']));
			$industry_list = explode(',', $data_industry);
		}

		$industry = "";
		$refined_industries = [];
		$indus_ids = [];
        $WixAffiliateLinks = WixAffiliateLinks();

		if( !empty( $industry_list ) ):
			foreach( $industry_list as $il ){
				if( is_numeric($il) ){
					$indus_ids[] = $il;
				} else {
					$refined_industries[] = $il;
				}
			}
			// $indus_ids = implode('", "',$indus_ids);
			$indus_ids = implode(',',$indus_ids);
			// $new_indus_ids = $this->db->get_col('SELECT name FROM '.$this->config->getWordCategoriesTableName().' WHERE name IN ("'.$indus_ids.'")');
			$new_indus_ids = $this->db->get_col('SELECT name FROM '.$this->config->getWordCategoriesTableName().' WHERE id IN ('.$indus_ids.')');
			
			
			foreach( $new_indus_ids as $in ){
				$refined_industries[] = $in;
			}
			$refined_industries = array_unique($refined_industries);
		endif;

		if (is_array($refined_industries) && count($refined_industries) > 0) {
			for ($i = 0; $i < count($refined_industries); $i++) {
				$industry .= '<input name="industry[]" type="hidden" value="' . trim($refined_industries[$i]) . '">';
			}
		} else {
			$industry = "";
		}

		// Results page redirect slug
		$loca = "/$shortcode_options->results_page_slug/";
		
		$current_country  =  getenv( 'HTTP_GEOIP_COUNTRY_CODE' );
		
		$checked = "checked='checked'";
		$inupts = '';
		$character = '';
		$alert = '';
		$pagetype = '';
		$grammar = '';
		$grammarinupts = '';
		$hide_advert = '';
	
		// Affiliate URL
		$current_affiliate = ( $shortcode_options->default_generator_affiliate_url != '' ? $shortcode_options->default_generator_affiliate_url : '' );
		if( !empty($shortcode_options->generator_affiliate_urls) ){
			foreach( $shortcode_options->generator_affiliate_urls as $gau ){
				if( $current_country == $gau->country_code ){
					$current_affiliate = $gau->affiliate_url;
				}
			}
		}

		$popupurlwix = $WixAffiliateLinks['popunder'];
		$popupurl = ( $atts['popup-url'] != '' ) ? $atts['popup-url'] : $current_affiliate;
		$popupurl = $popupurlwix !== '' ? $popupurlwix : $popupurl;


		// Current Affiliate Object
		$current_affiliate_obj = null;
		$lblSponsor = null;
		$sidParameter = null;
		  
		// Update Affiliate Object By Country Record
		if(isset($shortcode_options->generator_affiliate_urls) &&
			is_array($shortcode_options->generator_affiliate_urls) &&
			!empty($shortcode_options->generator_affiliate_urls)
		) {
			foreach($shortcode_options->generator_affiliate_urls as $affiliate_obj) {
				if($affiliate_obj->country_code == $current_country) {
					$current_affiliate_obj = $affiliate_obj;
					break;
				}
			}
		}

		if(is_null($current_affiliate_obj) || empty($current_affiliate_obj)) {
			$current_affiliate_obj = new stdClass;

			$current_affiliate_obj->generator_affiliate_desk_urls = $shortcode_options->default_generator_affiliate_desk_urls;
			
			$current_affiliate_obj->generator_affiliate_desk_sponsor = $shortcode_options->default_generator_affiliate_desk_sponsor;
			
			$current_affiliate_obj->generator_affiliate_tab_mob_urls = $shortcode_options->default_generator_affiliate_tab_mob_urls;
			
			$current_affiliate_obj->generator_affiliate_tab_mob_sponsor = $shortcode_options->default_generator_affiliate_tab_mob_sponsor;

		}


		$detect = new \Detection\MobileDetect();
		if(!empty($current_affiliate_obj) && !is_null($current_affiliate_obj)) {
			if($detect->isMobile() || $detect->isTablet()) {
				$popupurl = $current_affiliate_obj->generator_affiliate_tab_mob_urls;
				$lblSponsor = $current_affiliate_obj->generator_affiliate_tab_mob_sponsor;
			}else {
				$popupurl = $current_affiliate_obj->generator_affiliate_desk_urls;
				$lblSponsor = $current_affiliate_obj->generator_affiliate_desk_sponsor;
			}

			$sidParameter = ($lblSponsor == "wix") ?  "&s1=&s2=post" : "?SID=popunder-post";

		}


		if (!is_page_template('businessname.php') && !is_page_template('template-nameideas.php') && !is_page_template('template-bng340.php') && $disablead == "false") {	

			// Toggle Text
			$checkboxlabel = ( $atts['checkboxlabel'] != '' ) ? $atts['checkboxlabel'] : ( $shortcode_options->generator_toggle_text != '' ? $shortcode_options->generator_toggle_text : 'Check domain availability' );

			$checkboxlabelwix =  "";//$lblSponsor;
		    // $checkboxlabel    =  __( $checkboxlabel, $this->textDomain );   
		    // $checkboxlabel    =  $checkboxlabelwix !== '' ? str_replace("GoDaddy",$checkboxlabelwix,$checkboxlabel) : $checkboxlabel;      

			$target = 'target="_blank"';
             
			$popular_search = '';	
			
			$popular_toggle_colors = !empty($shortcode_options->popular_toggle_txt_color) ?  'style="color:'.$shortcode_options->popular_toggle_txt_color.' !important"' : '';  
             
			$lists = $this->db->get_results('SELECT `keyword` FROM '.$this->config->getPopularkeywordsTableName().' WHERE shortcode_id = '.$atts['id'].' ORDER BY RAND() LIMIT 3','ARRAY_A');
        
			if( !empty($lists) ){
				$popular_search_extra_class = !empty($shortcode_options->popular_toggle_txt_color) ?  'white-bg' : '';  	
				$popular_title  = !empty($shortcode_options->popular_title) ? $shortcode_options->popular_title : 'Popular Search' ;
				$popular_search = "<div class='search-text'><p class='".$popular_search_extra_class."' ".$popular_toggle_colors.">".$popular_title.":</p><ul class='keyword_lists'>";
				foreach( $lists as $lkey => $list ){
					if( trim($list) !== '' ) { 
						$popular_search .= "<li class='popular_keyword ".$popular_search_extra_class."' ".$popular_toggle_colors.">".$list['keyword']."</li>";	
					}	
				}
				$popular_search .= "</ul></div>";	
			} 
			
				
			
              	
			

			// Show Advert Toggle
			$show_advert = ($atts['toggle-visibility'] == 'true' ) ? true : ( $shortcode_options->show_advert_toggle == 'true' ? true : false );
			if( $show_advert == true  ) {
             
				$rand = rand(1,1000);
                $popular_toggle_colors = !empty($shortcode_options->popular_toggle_txt_color) ?  'color:'.$shortcode_options->popular_toggle_txt_color.' !important"' : '';   
                $checkbox = '<div class="row mb-5"><div class="col-md-6">'.$popular_search.'</div><div class="col-md-6"><div class="form-check form-switch text-end">
                              <input class="form-check-input chekedomainid float-end mt-0" type="checkbox" id="flexSwitchCheckChecked-'.$rand.'" checked>
                              <label class="form-check-label float-end me-5" style="font-size: .8rem; '.$popular_toggle_colors.'"  for="flexSwitchCheckChecked-'.$rand.'">' . $checkboxlabel . '</label>
                            </div></div></div>';            
            
            }

			$alert = '<div class="alert alert-danger fade in"><span>Please Enter a Word then click Generate.</span><a href="#" class="close" data-dismiss="alert" aria-label="close">Ã—</a></div>';
		} else {

			$checkbox = '';

			if (isset($_SESSION['filterd_columns'])) {
				$indust = $_SESSION['filterd_columns'];
			} else {
				$indust = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
			}

			if (!empty($indust)) {
				foreach ($indust as $value) :
					$inupts .= '<input type="hidden" name="industry[]" value="' . $value . '">';
				endforeach;
			}

			if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en') {
				$grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';


				if (!empty($grammar)) {
					foreach ($grammar as $value) :
						$grammarinupts .= '<input type="hidden" name="grammar[]" value="' . $value . '">';
					endforeach;
				}
			}

			$character = (isset($_GET['character'])) ? trim($_GET['character']) : '';
			// if(isset($_GET['character'])){
			$inupts .= '<input type="hidden" class="character" name="character" value="' . $character . '">';
			//}
			$pos = (isset($_GET['position']) && $_GET['position'] != 'after') ? trim($_GET['position']) : 'after';
			//if(isset($_GET['character'])){
			$inupts .= '<input type="hidden" class="pos" name="position" value="' . $pos . '">';
			//}
			$synonyms = (isset($_GET['synonyms']) && $_GET['synonyms'] == 'on') ? 'on' : 'off';
			$rhyming = (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off';

			if ($rhyming == 'on') {
				$one_word = 'off';
				$two_word = 'off';
			} else {
				$one_word = (isset($_GET['one_word']) && $_GET['one_word'] == 'on') ? 'on' : 'on';
				$two_word = (isset($_GET['two_word']) && $_GET['two_word'] == 'on') ? 'on' : 'on';
			}

			$inupts .= '<input type="hidden" class="one-word" name="one_word" value="' . $one_word . '">';
			$inupts .= '<input type="hidden" class="two-word" name="two_word" value="' . $two_word . '">';
			$inupts .= '<input type="hidden" class="synonyms-val" name="synonyms" value="' . $synonyms . '">';
			$inupts .= '<input type="hidden" class="rhyming-val" name="rhyming" value="' . $rhyming . '">';
		}

		
		
		// Generator Input Placeholder Desktop
		$placeholdertext = ( $atts['placeholdertext'] != '' ) ? $atts['placeholdertext'] : ( $shortcode_options->generator_placeholder != '' ? $shortcode_options->generator_placeholder : '' );
		
		if($detect->isMobile() || $detect->isTablet()) {
		
			// Generator Input Placeholder Tablet/Mobile
			$placeholdertext_mob = ( $atts['generator_placeholder_mob'] != '' ) ? $atts['generator_placeholder_mob'] : ( $shortcode_options->generator_placeholder_mob != '' ? $shortcode_options->generator_placeholder_mob : '' );
           
			$placeholdertext =  !empty($placeholdertext_mob) ? $placeholdertext_mob : $placeholdertext;

		}

		// Generator button text
		$buttontext = ( $atts['btntxt'] != '' ) ? $atts['btntxt'] : ( $shortcode_options->generator_button_text != '' ? $shortcode_options->generator_button_text : '' );
		
		// Autofocus on Generator input after page load
		$focus_attr = ( $atts['active'] != '' ) ? $atts['active'] : ( $shortcode_options->autofocus_on_load == 'true' ? 'autofocus="autofocus"' : '' );
		
		$page_id = (!empty($_GET['home_id']) && isset($_GET['home_id']) ) ? $_GET['home_id'] : get_the_ID();

		$page_id_in = '<input type="hidden" value="'.$page_id.'" name="home_id">';

		// Enable Sticky Ads
		$stickyads = ( $atts['stickyads'] != '' ) ? $atts['stickyads'] : ( $shortcode_options->enable_sticky_ads != '' ? $shortcode_options->enable_sticky_ads : '' );

		if($stickyads == 'true' || $_GET['stickyads'] != '' ){
			$page_id_in .= '<input type="hidden" value="true" name="stickyads">';
		}

		if(!empty($_GET['device']) ){
			$page_id_in .= '<input type="hidden" value="'.$_GET['device'].'" name="device">';
		}
        
        $page_id_in .= '<input type="hidden" class="country-code" value="'.$current_country.'">';
        $page_id_in .= '<input type="hidden" class="sid-parameter" value="'.$sidParameter.'">';

		$aff_nameideas = json_decode(get_option('bnwm_affiliate_links_nameideas'));
		$popunder_link = !empty( $aff_nameideas->registerlinkdropdown->url ) ? $aff_nameideas->registerlinkdropdown->url : 'https://www.dpbolvw.net/click-100386512-11774111';  
        
        $translations = json_decode(get_option('bnwm_string_translations'));
        $page_id_in .= '<input type="hidden" class="generatorerror1" value="'.$translations->generatorerror1.'">';
        $page_id_in .= '<input type="hidden" class="generatorerror2" value="'.$translations->generatorerror2.'">';
        $page_id_in .= '<input type="hidden" class="generatorerror3" value="'.$translations->generatorerror3.'">';
		$page_id_in .= '<input type="hidden" class="popunder_link"  value="'.$popunder_link.'">';
        $page_id_in .= '<input type="hidden" class="gclid_val" value="'.getGclidParam().'">';
        $page_id_in .= getUtmInputs();
        
        

		$shortcode_id_input = '<input type="hidden" name="shortcode_id" value="'.$atts['id'].'">';

		$container_class = ( $atts['generator-css-mode'] != '' ) ? $atts['generator-css-mode'] : ( $shortcode_options->generator_container_class != '' ? $shortcode_options->generator_container_class : '' );

		//   $stickyads_category = !empty($_GET['stickyads-category']) ? $_GET['stickyads-category'] : $a['stickyads-category'];
		//   $page_id_in .= '<input type="hidden" value="'.$stickyads_category.'" name="stickyads-category">';
	
		
        $cta_colors = 'style="background-color:'.$shortcode_options->generator_button_color.' !important; color:'.$shortcode_options->generator_button_text_color.'"';
	    
		$cta_colors = !empty($shortcode_options->generator_button_color) || !empty($shortcode_options->generator_button_text_color)  ? $cta_colors : '';
	
		$style = '<style>.name-search.word-generator form:before {background: url('.WORD_MANAGER_PLUGIN_URL.'assets/images/search-icon.svg) no-repeat;}</style>';

		$isTooltip = (isset($shortcode_options->tooltip_status) && $shortcode_options->tooltip_status == "active");

		$tooltipTitle = (isset($shortcode_options->tooltip_title)) ? $shortcode_options->tooltip_title : "";
		$tooltipSubTitle = (isset($shortcode_options->tooltip_sub_title)) ? $shortcode_options->tooltip_sub_title : "";

		$toolTipHtml = '';

		if($isTooltip && !empty($tooltipTitle)) {
			$toolTipHtml = '<div class="tooltip-box"> <p class="keywords">'.$tooltipTitle.'</p> <p>'.$tooltipSubTitle.'</p> <a class="close-box" href="#">×</a> </div>';
		}
        
		$class_site_based = getCurrentSiteName() == 'BNG' ? 'bng-generator' : 'bnw-generator';
		
		$string .= $style.'<div class="'.$class_site_based.' name-search word-generator ' . $container_class . '">
		<form method="GET"  class="generator-form" action="'.home_url() . $loca . '" ' . $target . '>'.$page_id_in.'' . $pagetype . '' . $inupts . $shortcode_id_input . '' . $industry . ' ' . $grammarinupts . ' ' .'<div class="input-group mt-5 mb-3">
		   <input class="form-control p-4 inputMain" name="bname" type="text" default="" value="' . $keyword . '" '.$focus_attr.' data-popup-url="' . esc_attr($popupurl) . '" data-active="' . esc_attr($shortcode_options->autofocus_on_load) . '" placeholder="' . $placeholdertext . '"> '.$toolTipHtml.' <button class="btn m-0 px-5 generatorbtn" type="submit" '.$cta_colors.'>' . $buttontext . '</button></div>'. $checkbox .'</form></div>';

		return $string;
	}
	
    public function addShortcodeScripts()
    {
        wp_register_style( 'bnwm-generator-style', WORD_MANAGER_PLUGIN_URL.'assets/frontend/generator.css' );
        wp_register_script( 'bnwm-generator-script', WORD_MANAGER_PLUGIN_URL.'assets/frontend/generator.js' );
    }

	public function getShortcodePage()
	{
		if(isset($_GET['add_generator_shortcode']) && $_GET['add_generator_shortcode'] == 'true' ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addGeneratorShortcode($_POST);
			}
			$cats = $this->fetchWordCategories();
			$filters = $this->db->get_results("SELECT id, name FROM ".$this->config->getGeneratorFiltersTableName());
			$advertisementList = $this->db->get_results("SELECT id, name FROM ".$this->config->getAdvertisementTableName()." WHERE advertisement_type = 'normal' ");
			$stickyList = $this->db->get_results("SELECT id, name FROM ".$this->config->getAdvertisementTableName()." WHERE advertisement_type = 'sticky' ");
			$templateList = $this->db->get_results("SELECT id, name FROM ".$this->config->getTemplateTableName());


			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-shortcode.php" );
		} elseif( isset($_GET['edit_generator_shortcode']) ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addGeneratorShortcode($_POST);
			}
			$data = $this->fetchGeneratorShortcodes('*', 'WHERE id = '.$_GET['edit_generator_shortcode']);
			$shortcode_options = json_decode($data[0]->shortcode_options);
			$shortcode_id = $data[0]->id;
			$shortcode_name = $data[0]->name;
			$saved_filters = $data[0]->filters_id;
			$cats = $this->fetchWordCategories();
			$filters = $this->db->get_results("SELECT id, name FROM ".$this->config->getGeneratorFiltersTableName());
			$filters = $this->db->get_results("SELECT id, name FROM ".$this->config->getGeneratorFiltersTableName());
			$advertisementList = $this->db->get_results("SELECT id, name FROM ".$this->config->getAdvertisementTableName()." WHERE advertisement_type = 'normal' ");
			$stickyList = $this->db->get_results("SELECT id, name FROM ".$this->config->getAdvertisementTableName()." WHERE advertisement_type = 'sticky' ");
			

			$templateList = $this->db->get_results("SELECT id, name FROM ".$this->config->getTemplateTableName());
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-shortcode.php" );
		} else {
			$viewData = $this->fetchGeneratorShortcodes('id, name');
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/shortcodes.php" );
		}
	}

	public function addGeneratorShortcode( $post_data )
	{
		$shortcode_options = [];
		$shortcode_options['generator_placeholder'] = $post_data['generator_placeholder'];
		$shortcode_options['generator_placeholder_mob'] = $post_data['generator_placeholder_mob'];
		$shortcode_options['hide_popunder_advert'] = $post_data['hide_popunder_advert'];
		$shortcode_options['show_advert_toggle'] = $post_data['show_advert_toggle'];
		$shortcode_options['enable_sticky_ads'] = $post_data['enable_sticky_ads'];
		$shortcode_options['autofocus_on_load'] = $post_data['autofocus_on_load'];
		$shortcode_options['generator_container_class'] = $post_data['generator_container_class'];
		$shortcode_options['generator_button_text'] = $post_data['generator_button_text'];
		$shortcode_options['generator_button_color'] = $post_data['generator_button_color'];
		$shortcode_options['generator_button_text_color'] = $post_data['generator_button_text_color'];
		$shortcode_options['popular_toggle_txt_color'] = $post_data['popular_toggle_txt_color'];
		$shortcode_options['popular_title'] = $post_data['popular_title'];
		$shortcode_options['word_industries'] = $post_data['word_industries'];
		$shortcode_options['results_page_slug'] = $post_data['results_page_slug'];
		$shortcode_options['generator_toggle_text'] = $post_data['generator_toggle_text'];
		$shortcode_options['default_generator_affiliate_desk_urls'] = $post_data['default_generator_affiliate_desk_urls'];
		$shortcode_options['default_generator_affiliate_desk_sponsor'] = $post_data['default_generator_affiliate_desk_sponsor'];
		$shortcode_options['default_generator_affiliate_tab_mob_urls'] = $post_data['default_generator_affiliate_tab_mob_urls'];
		$shortcode_options['default_generator_affiliate_tab_mob_sponsor'] = $post_data['default_generator_affiliate_tab_mob_sponsor'];
		$shortcode_options['tooltip_status'] = $post_data['tooltip_status'];
		$shortcode_options['tooltip_title'] = $post_data['tooltip_title'];
		$shortcode_options['tooltip_sub_title'] = $post_data['tooltip_sub_title'];
		
	    
        for( $i=0 ; $i < count($post_data['generator_affiliate_countries']); $i++ ){
			 
			if( !empty($post_data['generator_affiliate_countries'][$i]) && !empty($post_data['generator_affiliate_desk_urls'][$i]) && !empty($post_data['generator_affiliate_desk_sponsor'][$i])  && !empty($post_data['generator_affiliate_tab_mob_urls'][$i]) && !empty($post_data['generator_affiliate_tab_mob_sponsor'][$i])  ){
			   
			   $country = explode(': ', $post_data['generator_affiliate_countries'][$i]);
			   $shortcode_options['generator_affiliate_urls'][$i]['country_code'] = $country[0];
			   $shortcode_options['generator_affiliate_urls'][$i]['country_name'] = $country[1];
			   $shortcode_options['generator_affiliate_urls'][$i]['generator_affiliate_desk_urls'] = $post_data['generator_affiliate_desk_urls'][$i];
			   $shortcode_options['generator_affiliate_urls'][$i]['generator_affiliate_desk_sponsor'] = $post_data['generator_affiliate_desk_sponsor'][$i];
			   $shortcode_options['generator_affiliate_urls'][$i]['generator_affiliate_tab_mob_urls'] = $post_data['generator_affiliate_tab_mob_urls'][$i];
			   $shortcode_options['generator_affiliate_urls'][$i]['generator_affiliate_tab_mob_sponsor'] = $post_data['generator_affiliate_tab_mob_sponsor'][$i];
			
			}
		} 

		if(isset($post_data["advtpl"]) && !empty($post_data["advtpl"])) {
			$shortcode_options["advertisement_templates"] = array_values($post_data["advtpl"]);
		}

		if(isset($post_data["stickytpl"]) && !empty($post_data["stickytpl"])) {
			$shortcode_options["sticky_templates"] = array_values($post_data["stickytpl"]);
		}
		 

		if( $post_data['edit_generator_shortcode'] != '' ){
			$id = $this->db->update( $this->config->getGeneratorShortcodeTableName(), [
				'name' => $post_data['shortcode_name'],
				'shortcode_options' => json_encode($shortcode_options),
				'filters_id' => $post_data['word_filters']
			],[
				'id' => $post_data['edit_generator_shortcode']
			] );
            
			$this->db->delete( $this->config->getPopularkeywordsTableName(), [
				'shortcode_id' => $post_data['edit_generator_shortcode'] 
			] );

			$wordArray = explode(",", $post_data['popular_search']);
            
			$shortcode_id = $post_data['edit_generator_shortcode'];
            $stmt = $this->db->query("INSERT INTO ".$this->config->getPopularkeywordsTableName()." (keyword,shortcode_id) VALUES ('" . implode("',$shortcode_id), ('", $wordArray) . "', $shortcode_id)");
		
			// $stmt = $this->db->insert( $this->config->getPopularkeywordsTableName(), [
			// 	'keyword' => implode("'), ('", $wordArray),
			// 	'shortcode_id' => $post_data['edit_generator_shortcode'],
			// ] );
	
		} else {
			$id = $this->db->insert( $this->config->getGeneratorShortcodeTableName(), [
				'name' => $post_data['shortcode_name'],
				'shortcode_options' => json_encode($shortcode_options),
				'filters_id' => $post_data['word_filters']
			] );
            $lastid = $this->db->insert_id;
			$wordArray = explode(",", $post_data['popular_search']);
            
		    $stmt = $this->db->query("INSERT INTO ".$this->config->getPopularkeywordsTableName()." (keyword,shortcode_id) VALUES ('" . implode("',$lastid), ('", $wordArray) . "', $lastid)");
		}

		$viewData = '';

		if( $id ){
			return $id;
		}

		return false;
	}

	public function fetchWordCategories(){
		$results = $this->db->get_results("SELECT id, name FROM ".$this->config->getWordCategoriesTableName());
		return $results;
	}

	public function fetchGeneratorShortcodes($fieldName = 'shortcode_options', $where = '')
	{
		$results = $this->db->get_results("SELECT $fieldName from ".$this->config->getGeneratorShortcodeTableName()." $where" );
		return $results;
	}

	public function find($shortCodeId) {
		$shortcodeSQL = " SELECT * FROM ".$this->config->getGeneratorShortcodeTableName()." SC WHERE SC.id=".$shortCodeId." LIMIT 1";

		$matched = $this->db->get_results($shortcodeSQL, ARRAY_A);
		if(!empty($matched)) {
			return $matched[0];
		}

		return [];
	}

	public function getAdvertisements($shortcode_id, $type = "normal") {
		$shortcode = $this->find($shortcode_id);
		$advertisements = [];
		if(empty($shortcode))  {
			return $advertisements;
		}

		$data = json_decode($shortcode['shortcode_options']);
		if(isset($data->advertisement_templates) & !empty($data->advertisement_templates)) {
			foreach($data->advertisement_templates as $single) {
				$adv_id = $single->advertisement;
				$template_id = $single->template;

				$advertisement = $this->advertisementCallback->find($adv_id);
				if(!empty($advertisement) && $advertisement->advertisement_type == $type) {
					array_push($advertisements, ["advertisement"=>$advertisement, "template_id"=> $template_id]);
				}

			}
		}
		return $advertisements;
	}

	public function getStickyAdvertisements($shortcode_id, $type = "sticky") {
		$shortcode = $this->find($shortcode_id);
		$advertisements = [];
		if(empty($shortcode))  {
			return $advertisements;
		}

		$data = json_decode($shortcode['shortcode_options']);
		if(isset($data->sticky_templates) & !empty($data->sticky_templates)) {
			foreach($data->sticky_templates as $single) {
				$adv_id = $single->advertisement;
				$template_id = $single->template;

				$advertisement = $this->advertisementCallback->find($adv_id);
				if(!empty($advertisement) && $advertisement->advertisement_type == $type) {
					array_push($advertisements, ["advertisement"=>$advertisement, "template_id"=> $template_id]);
				}

			}
		}
		return $advertisements;
	}


}