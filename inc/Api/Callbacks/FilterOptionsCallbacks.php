<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;

use BNWM\Base\Config;

class FilterOptionsCallbacks
{
	protected static $instance = null;
    private $db;
    private $textDomain;
    public $config;

    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->config = Config::instance();
        $this->textDomain = 'business_name_word_manager';
        $this->runFilterActions();
    }
    
    /**
     * instance
     *
     * @return FilterOptionsCallbacks
     */
    public static function instance(): FilterOptionsCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}

    public function runFilterActions()
    {
        add_action( 'wp_enqueue_scripts', [ $this, 'addFilterScripts' ] );
    }

    public function addFilterScripts()
    {
        // wp_register_style( 'bnwm-generator-style', WORD_MANAGER_PLUGIN_URL.'assets/frontend/generator.css' );
        // wp_register_script( 'bnwm-generator-script', WORD_MANAGER_PLUGIN_URL.'assets/frontend/generator.js' );
    }

	public function getFiltersPage()
	{
		if(isset($_GET['add_generator_filter']) && $_GET['add_generator_filter'] == 'true' ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addGeneratorFilter($_POST);
			}
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-filterOptions.php" );
		} elseif( isset($_GET['edit_generator_filter']) ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addGeneratorFilter($_POST);
			}
			$data = $this->fetchGeneratorFilters('*', 'WHERE id = '.$_GET['edit_generator_filter']);
			$filter_options = json_decode($data[0]->filter_options);
			$filter_id = $data[0]->id;
			$filter_name = $data[0]->name;
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-filterOptions.php" );
		} else {
			$viewData = $this->fetchGeneratorFilters('id, name');
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/filters.php" );
		}
	}

	public function addGeneratorFilter( $post_data )
	{
		$filter_options = [];
		$filter_options['filter_name'] = $post_data['filter_name'];
		$filter_options['filter_title'] = $post_data['filter_title'];
		$filter_options['results_count_text'] = $post_data['results_count_text'];

		if( isset($post_data['enable_saved_ideas']) && $post_data['enable_saved_ideas'] == 'true' ){
			$filter_options['enable_saved_ideas']['value'] = $this->filterTheText($post_data['enable_saved_ideas']);
			$filter_options['enable_saved_ideas']['saved_ideas_title'] = $this->filterTheText($post_data['saved_ideas_title']);
			$filter_options['enable_saved_ideas']['saved_ideas_title_mobile'] = $this->filterTheText($post_data['saved_ideas_title_mobile']);
			$filter_options['enable_saved_ideas']['saved_ideas_modal_mobiletitle'] = $this->filterTheText($post_data['saved_ideas_modal_mobiletitle']);
			$filter_options['enable_saved_ideas']['saved_ideas_subtitle'] = $this->filterTheText($post_data['saved_ideas_subtitle']);
			$filter_options['enable_saved_ideas']['saved_ideas_email_placeholder'] = $this->filterTheText($post_data['saved_ideas_email_placeholder']);
			$filter_options['enable_saved_ideas']['saved_ideas_email_send_text'] = $this->filterTheText($post_data['saved_ideas_email_send_text']);
			$filter_options['enable_saved_ideas']['saved_ideas_email_optin_notice'] = $this->filterTheText($post_data['saved_ideas_email_optin_notice']);
			$filter_options['enable_saved_ideas']['saved_ideas_share_email_text'] = $this->filterTheText($post_data['saved_ideas_share_email_text']);
			$filter_options['enable_saved_ideas']['saved_ideas_policy_url'] = $post_data['saved_ideas_policy_url'];
			$filter_options['enable_saved_ideas']['saved_ideas_mobile_close_button'] = $this->filterTheText($post_data['saved_ideas_mobile_close_button']);
			$filter_options['enable_saved_ideas']['saved_ideas_policy_text'] = $this->filterTheText($post_data['saved_ideas_policy_text']);
		}

		if( isset($post_data['enable_industry_filters']) && $post_data['enable_industry_filters'] == 'true' ){
			$filter_options['enable_industry_filters']['value'] = $this->filterTheText($post_data['enable_industry_filters']);
			$filter_options['enable_industry_filters']['industry_title'] = $this->filterTheText($post_data['industry_title']);
			$filter_options['enable_industry_filters']['industry_modal_title'] = $this->filterTheText($post_data['industry_modal_title']);
			$filter_options['enable_industry_filters']['industry_search_placeholder'] = $this->filterTheText($post_data['industry_search_placeholder']);
			$filter_options['enable_industry_filters']['industry_clear_button_text'] = $this->filterTheText($post_data['industry_clear_button_text']);
			$filter_options['enable_industry_filters']['industry_apply_button_text'] = $this->filterTheText($post_data['industry_apply_button_text']);
			$filter_options['enable_industry_filters']['industry_close_button_text'] = $this->filterTheText($post_data['industry_close_button_text']);
			
		}


		if( isset($post_data['enable_names_filter']) && $post_data['enable_names_filter'] == 'true' ){
			$filter_options['enable_names_filter']['value'] = $this->filterTheText($post_data['enable_names_filter']);
			$filter_options['enable_names_filter']['names_title'] = $this->filterTheText($post_data['names_title']);
			$filter_options['enable_names_filter']['names_mobile_modal_title'] = $this->filterTheText($post_data['names_mobile_modal_title']);
			$filter_options['enable_names_filter']['names_apply_button'] = $this->filterTheText($post_data['names_apply_button']);
			$filter_options['enable_names_filter']['names_cancel_button'] = $this->filterTheText($post_data['names_cancel_button']);
			$filter_options['enable_names_filter']['names_close_button_text'] = $this->filterTheText($post_data['names_close_button_text']);

			if( isset($post_data['enable_character_count']) && $post_data['enable_character_count'] == 'true' ){
				$filter_options['enable_names_filter']['enable_character_count']['value'] = $post_data['enable_character_count'];
				$filter_options['enable_names_filter']['enable_character_count']['character_count_title'] = $post_data['character_count_title'];
			}
			
			if( isset($post_data['enable_words_filter']) && $post_data['enable_words_filter'] == 'true' ){
				$filter_options['enable_names_filter']['enable_words_filter']['value'] = $post_data['enable_words_filter'];
				$filter_options['enable_names_filter']['enable_words_filter']['words_title'] = $post_data['words_title'];
				$filter_options['enable_names_filter']['enable_words_filter']['one_word_title'] = $post_data['one_word_title'];
				$filter_options['enable_names_filter']['enable_words_filter']['two_word_title'] = $post_data['two_word_title'];
			}

			if( isset($post_data['enable_keyword_filter']) && $post_data['enable_keyword_filter'] == 'true' ){
				$filter_options['enable_names_filter']['enable_keyword_filter']['value'] = $post_data['enable_keyword_filter'];
				$filter_options['enable_names_filter']['enable_keyword_filter']['keyword_title'] = $post_data['keyword_title'];
				$filter_options['enable_names_filter']['enable_keyword_filter']['keyword_title_mobile'] = $post_data['keyword_title_mobile'];
				$filter_options['enable_names_filter']['enable_keyword_filter']['before_title'] = $post_data['before_title'];
				$filter_options['enable_names_filter']['enable_keyword_filter']['after_title'] = $post_data['after_title'];
			}

			if( isset($post_data['enable_rhyming_filter']) && $post_data['enable_rhyming_filter'] == 'true' ){
				$filter_options['enable_names_filter']['enable_rhyming_filter']['value'] = $post_data['enable_rhyming_filter'];
				$filter_options['enable_names_filter']['enable_rhyming_filter']['rhyming_title'] = $post_data['rhyming_title'];
			}
		}

		if( isset($post_data['enable_grammar_filters']) && $post_data['enable_grammar_filters'] == 'true' ){
			$filter_options['enable_grammar_filters']['value'] = $post_data['enable_grammar_filters'];
			$filter_options['enable_grammar_filters']['plural_title'] = $post_data['plural_title'];
			$filter_options['enable_grammar_filters']['grammar_title'] = $post_data['grammar_title'];
			$filter_options['enable_grammar_filters']['masculine_singular_title'] = $post_data['masculine_singular_title'];
			$filter_options['enable_grammar_filters']['feminine_singular_title'] = $post_data['feminine_singular_title'];
			$filter_options['enable_grammar_filters']['masculine_plural_title'] = $post_data['masculine_plural_title'];
			$filter_options['enable_grammar_filters']['feminine_plural_title'] = $post_data['feminine_plural_title'];
		}

		if( isset($post_data['enable_extensions_filter']) && $post_data['enable_extensions_filter'] == 'true' ){
			$filter_options['enable_extensions_filter']['value'] = $post_data['enable_extensions_filter'];
			$filter_options['enable_extensions_filter']['extensions_title'] = $post_data['extensions_title'];
			$filter_options['enable_extensions_filter']['extension_apply_button'] = $post_data['extension_apply_button'];
			$filter_options['enable_extensions_filter']['extension_search_placeholder'] = $post_data['extension_search_placeholder'];
			$filter_options['enable_extensions_filter']['extension_cancel_button'] = $post_data['extension_cancel_button'];
			$filter_options['enable_extensions_filter']['extension_mobile_close_button'] = $post_data['extension_mobile_close_button'];

		}

		if( isset($post_data['enable_domains_availability_widget']) && $post_data['enable_domains_availability_widget'] == 'true' ){
			$filter_options['enable_domains_availability_widget']['value'] = $post_data['enable_domains_availability_widget'];
			$filter_options['enable_domains_availability_widget']['domains_title'] = $post_data['domains_title'];
			$filter_options['enable_domains_availability_widget']['domains_name_placeholder'] = $post_data['domains_name_placeholder'];
			$filter_options['enable_domains_availability_widget']['domains_button_text'] = $post_data['domains_button_text'];
			
		}

		if( isset($post_data['edit_generator_filter']) ){
			$id = $this->db->update( $this->config->getGeneratorFiltersTableName(), [
				'name' => $post_data['filter_name'],
				'filter_options' => json_encode($filter_options),
			],[
				'id' => $post_data['edit_generator_filter']
			] );
		} else {
			$id = $this->db->insert( $this->config->getGeneratorFiltersTableName(), [
				'name' => $post_data['filter_name'],
				'filter_options' => json_encode($filter_options)
			] );
		}

		$viewData = '';

		if( $id ){
			return $id;
		}

		return false;
	}

    public function fetchGeneratorFilters($fieldName = 'filter_options', $where = '')
	{
		$results = $this->db->get_results("SELECT $fieldName from ".$this->config->getGeneratorFiltersTableName()." $where" );
		return $results;
	}

	public function filterTheText( $text="" ){
       
       return str_replace("\\",'',$text);
	}
}