<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;
// TODO: Create Separate Callbacks for each admin module
use BNWM\Base\BaseController;
use BNWM\Base\Config;
use BNWM\DataMuse\DataMuse;
use BNWM\WordManager\WordManager;

class AdminCallbacks extends BaseController
{
	public $wordManager, $config, $shortcodeCallbacks, $wordsDashboard;
	public static $instance;

	public function __construct()
	{
		$this->wordManager = new WordManager;
		parent::__construct();
        $this->config = Config::instance();
        $this->shortcode = ShortcodeCallbacks::instance();
        $this->wordsDashboard = WordsDashboardCallbacks::instance();
        $this->filterOptions = FilterOptionsCallbacks::instance();
	}

	/**
     * instance
     *
     * @return AdminCallbacks
     */
    public static function instance(): AdminCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}

	public function register()
	{
        add_action( $this->cron_hook, array($this, 'cronExecution'));
        add_action( $this->cron_hook_domain, array($this, 'cronDomainFetch'));
		add_filter( 'cron_schedules', [ $this, 'cronScheduleSetup'] );
		add_shortcode('mlsg_sitemap', [$this, 'sitemapShortcode']);

		// AJAX Requests
		add_action("wp_ajax_getWordColumnData", [$this, "getWordColumnData"]);

		add_action("wp_ajax_saveApprovedWords", [$this, "saveApprovedWords"]);

		add_action("wp_ajax_ajaxDownloadCsv", [$this, "ajaxDownloadCsv"]);

		add_action("wp_ajax_ajaxUpdateCategoryName", [$this, "ajaxUpdateCategoryName"]);

		add_action("wp_ajax_loadCategories", [$this, "loadCategories"]);
		
		add_action("wp_ajax_loadCategoryWords", [$this, "loadCategoryWords"]);

		add_action("wp_ajax_loadKeywords", [$this, "loadKeywords"]);

		add_action("wp_ajax_loadWordAttributes", [$this, "loadWordAttributes"]);

		add_action("wp_ajax_ajaxUpdateWordData", [$this, "ajaxUpdateWordData"]);

		add_action("wp_ajax_ajaxUpdateWordStatus", [$this, "ajaxUpdateWordStatus"]);

		add_action("wp_ajax_checkIfWordExists", [$this, "checkIfWordExists"]);

		add_action("wp_ajax_UpdateAlgorithm", [$this, "UpdateAlgorithm"]);

		add_action("wp_ajax_createPopularKeywordsTable", [$this, "createPopularKeywordsTable"]);
	}
	
	public function adminDashboard()
	{
		$viewData = $this->adminDashboardView();
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/dashboard.php" );
	}

	public function adminDashboardView()
	{
		$response = [];
		$response['requests'] = $this->db->get_results('SELECT name, total_items FROM '.$this->config->getRequestTableName().' WHERE created_at BETWEEN NOW() - INTERVAL 30 DAY AND NOW()');

		return $response;
	}

	public function requestsView()
	{
		$viewData = $this->wordManager->getRequestsData();
		$languages = $this->wordManager->getLanguages();
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/requests.php" );
	}

	public function adminAddRequest() {
		if( isset($_GET['add_new_request']) && $_GET['add_new_request'] == 'true' ){

			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$filename = "";

				if(isset($_POST["request_type"]) && !empty($_POST["request_type"])) {

					switch($_POST["request_type"]) {
						case "csv";
							if(!empty($_FILES['csv_file'])) {
								$filename = uniqid().".csv";
								$filepath = WORD_MANAGER_PLUGIN_PATH.'uploads/'.$filename;
								move_uploaded_file($_FILES['csv_file']['tmp_name'], $filepath);
							}
						break;
					}

				}
				$this->db->insert($this->config->getRequestTableName(), [
					'name'=> !empty($_POST['name']) ? $_POST['name'] : 'Auto-Generated',
					'request_type' => $_POST["request_type"],
					'request_data' => ($_POST["request_type"] == "csv") ? $filename : $_POST['request_data'],
					'status'	=> "Pending",
					'language_code'	=> $_POST['bnwm_data_language'],
					'created_at'	=> date("Y-m-d h:i:s"),
					'updated_at'	=> date("Y-m-d h:i:s"),
				]);
				$id = $this->db->insert_id;

				wp_redirect('<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_word_requests');
			}
			$languages = $this->wordManager->getLanguages();
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-request.php" );
		} else {
			$this->requestsView();
		}
	}

	public function adminCreateCsv()
	{
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/createCsv.php" );
	} 

	public function ajaxDownloadCsv()
	{
		$dm = DataMuse::instance();
		$res = $this->flattenArray($dm->query($_REQUEST['apiUrl']));

		$response = $this->array_to_csv_download($res,"data_export_" . date("Y-m-d") . ".csv");
		echo json_encode($response);

		die;
	}

	public function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
		$uploadDir = WORD_MANAGER_PLUGIN_PATH . "uploads/";
        $uploadUrl = WORD_MANAGER_PLUGIN_URL . "uploads/";

        $f = fopen($uploadDir . $filename, 'w');
		fputcsv($f, ['Words'], $delimiter);
		foreach( $array as $res ){
			fputcsv($f, $res, $delimiter);
		}
        fclose($f);

        $response['url'] = $uploadUrl.$filename;
        $response['type'] = 'success';

		return $response;
	} 

	public function flattenArray($arr)
	{
		$flattened = [];
		$it = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arr));
		foreach($it as $v) {
			$flattened[]['word'] = $v;
		}
		return $flattened;
	}

	public function cronInitialization()
	{
		$timestamp = wp_next_scheduled( $this->cron_hook );

        //If $timestamp === false schedule daily backups since it hasn't been done previously
        if( $timestamp === false ){
            //Schedule the event for right now, then to repeat daily using the hook 
            wp_schedule_event( time(), 'three_minutes', $this->cron_hook );
        }

		$timestamp = wp_next_scheduled( $this->cron_hook_domain );

        //If $timestamp === false schedule daily backups since it hasn't been done previously
        if( $timestamp === false ){
            //Schedule the event for right now, then to repeat daily using the hook 
            wp_schedule_event( time(), 'every_day', $this->cron_hook_domain );
        }

	}

	public function cronScheduleSetup( $schedules )
	{
		$schedules['three_minutes'] = array(
			'interval' => 180,
			'display'  => esc_html__( 'Every 3 minutes' ), );

		$schedules['every_day'] = array(
			'interval' => 86400,
			'display'  => esc_html__( 'Every Day' ), );
		return $schedules;
	}

	public function cronExecution()
	{
		// $this->makeActiveRequest();
		// $this->fetchPendingWords();
		// $this->updateRequestStatus();
	}

	public function getDomains($search_word, $page = 1, $limit = 4) {

		$search_word = strtolower(trim($search_word));
        $wordList = explode(" ", $search_word);

        $offset = ($limit * ($page-1));

        $sql = " SELECT DISTINCT `id`,`title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM ".$this->config->getDomainsTableName()." WHERE status = '1' AND price < 4000 ";

        $orderVars = [
            "title",
            "tags",
            "categories"
        ];

        $sql .= " ORDER BY ";
        $orderByClause = [];
        if(!empty($wordList)) {
            foreach($orderVars as $vars) {
                foreach($wordList as $word) {
                    $orderByClause[] = " title LIKE '".$word."%' DESC ";
                }
            }
        }
        $orderByClause[] = " price DESC ";
        $sql .= implode(", ", $orderByClause);
        $sql .=  " limit ".$offset.", ".$limit;
        $results = $this->db->get_results($sql, ARRAY_A);
		return $results;

	}

	public function getDomainifyData() {
		$handle = curl_init();
		$url = 'https://domainify.com/wc-api/v3/products?filter[limit]=650&consumer_key=ck_9bd4fc6fa4e2cfe56359ae9bfb943c782846a627&consumer_secret=cs_31927dcd544e3552e65cb9f007ac9970dda46876';

		// Set the url
		curl_setopt($handle, CURLOPT_URL, $url);
		// Set the result output to be a string.
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec($handle);

		curl_close($handle);
		$data = json_decode($output, true);

		return $data;
	}

	public function cronDomainFetch()
	{
		global $wpdb;
		$domains = $this->getDomainifyData();

		$domain_list = array();

		$all_res = $wpdb->get_results("SELECT `product_id` from ".$this->config->getDomainsTableName(), ARRAY_N);
		$all_name = $wpdb->get_results("SELECT `title` from ".$this->config->getDomainsTableName(), ARRAY_N);

		$previous_data = array();
		$previous_name_data = array();
		$new_data = array();

		foreach ($all_res as $key => $value) {
			foreach ($value as $id) {
				array_push($previous_data, $id);
			}
		}

		foreach ($all_name as $key => $value) {
			foreach ($value as $title) {
				array_push($previous_name_data, $title);
			}
		}

		foreach ($domains as $key => $domain) {

			foreach ($domain as $data) {
				array_push($new_data, $data['id']);
			}
		}

		foreach ($previous_data as $key => $data_id) {
			if (!in_array($data_id, $new_data) && !empty($previous_data)) {
				$wpdb->query($wpdb->prepare(
					"DELETE FROM `".$this->config->getDomainsTableName()."`
						WHERE product_id = %d
						",
					$data_id
				));
			}
		}


		$result = '';
		$image = '';
		$ctg = '';
		$tags = '';
		foreach ($domains as $key => $value) {

			foreach ($value as $ts) {
				$concat_title = strtolower(str_replace(".","",$ts['title']));
				if (in_array($ts['id'], $previous_data) && in_array($ts['title'], $previous_name_data) && !empty($previous_data)) {

					if (stripos($ts['featured_src'], '.jpg')) {
						$image = str_replace('.jpg', '-100x100.jpg', $ts['featured_src']);
					}

					if (stripos($ts['featured_src'], '.png')) {
						$image = str_replace('.png', '-100x100.png', $ts['featured_src']);
					}

					if (count($ts['categories']) > 0 && is_array($ts['categories'])) {
						$ctg = implode(',', $ts['categories']);
					}

					if (count($ts['tags']) > 0 && is_array($ts['tags'])) {
						$tags = implode(',', $ts['tags']);
					}
					$uploaddir = wp_get_upload_dir();
					$filename = basename($image);
					copy($image, $uploaddir['basedir'].'/domains/'.$concat_title.$filename);
					$image = $uploaddir['baseurl'].'/domains/'.$concat_title.$filename;

					if( isset($ts['images'][0]['alt']) && !empty($ts['images'][0]['alt']) ){
						$alt_tag = $ts['images'][0]['alt'];
					} else {
						$alt_tag = str_replace('.', '-', $ts['title']);
					}

					$result = $wpdb->query($wpdb->prepare(
						"UPDATE `".$this->config->getDomainsTableName()."` SET `title` = '%s', `product_id` = '%d', `price`= '%s', `regular_price`= '%s', `sale_price`= '%s', `status`= '%s', `featured_src`= '%s', `tags`= '%s', `categories`= '%s', `permalink`= '%s', `price_html`= '%s', `alt` = '%s' WHERE `product_id` = '%d'",

						$ts['title'],
						$ts['id'],
						$ts['price'],
						$ts['regular_price'],
						$ts['sale_price'],
						$ts['in_stock'],
						$image,
						$tags,
						$ctg,
						$ts['permalink'],
						$ts['price_html'],
						$alt_tag,
						$ts['id']
						
					));
				} else {

					if (stripos($ts['featured_src'], '.jpg')) {
						$image = str_replace('.jpg', '-100x100.jpg', $ts['featured_src']);
					}

					if (stripos($ts['featured_src'], '.png')) {
						$image = str_replace('.png', '-100x100.png', $ts['featured_src']);
					}

					if (count($ts['categories']) > 0 && is_array($ts['categories'])) {
						$ctg = implode(',', $ts['categories']);
					}

					if (count($ts['tags']) > 0 && is_array($ts['tags'])) {
						$tags = implode(',', $ts['tags']);
					}

					$uploaddir = wp_get_upload_dir();
					$filename = basename($image);
					copy($image, $uploaddir['basedir'].'/domains/'.$concat_title.$filename);
					$image = $uploaddir['baseurl'].'/domains/'.$concat_title.$filename;

					if( isset($ts['images'][0]['alt']) && !empty($ts['images'][0]['alt']) ){
						$alt_tag = $ts['images'][0]['alt'];
					} else {
						$alt_tag = str_replace('.', '-', $ts['title']);
					}

					$wpdb->query($wpdb->prepare(
						"INSERT INTO ".$this->config->getDomainsTableName()." (`title`, `product_id`,`price`, `regular_price`, `sale_price`, `status`, `featured_src`, `tags`, `categories`, `permalink`, `price_html`, `alt`)
						VALUES (%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",

						$ts['title'],
						$ts['id'],
						$ts['price'],
						$ts['regular_price'],
						$ts['sale_price'],
						$ts['in_stock'],
						$image,
						$tags,
						$ctg,
						$ts['permalink'],
						$ts['price_html'],
						$alt_tag
					));
				}
			}
		}
	}

	public function fetchPendingWords()
	{
		// Get 200 Pending Words
		$pendingWords = $this->db->get_results("SELECT word.id, word.name FROM ".$this->config->getWordDictionaryTableName()." word INNER JOIN ".$this->config->getLanguageWordTableName()." lang ON word.id=lang.word_id WHERE lang.language_code='en' AND word.status='Pending' LIMIT 200");
		foreach( $pendingWords as $pw ){
			$this->fetchWordsData($pw->id, $pw->name);
		}
	}

	public function makeActiveRequest() {
		// Get all Pending Requests
		$pendingRequests = $this->db->get_results('SELECT * FROM '.$this->config->getRequestTableName()." WHERE status='Pending'");

		if(!empty($pendingRequests)) {
			foreach($pendingRequests as $request) {
				// read Data
				if(!empty($request->request_data)) {
					switch($request->request_type) {
						case 'txt':
							$this->processTextData($request);
							break;
						case 'csv':
							$this->processCsvData($request);
							break;
					}
				}
			}
		}
	}

	public function processTextData($request) {
		$data = $request->request_data;
		$explodedData = explode("\r\n", $data);
		$this->processData($explodedData, $request);
	}

	private function processData($explodedData, $request) {

		if(!empty($explodedData)) {
			$totalData = count($explodedData);
			// Update Status to Progress
			$this->db->update($this->config->getRequestTableName(),[
				"status" => "Processing",
				"total_items" => $totalData
			],  array("id"=> $request->id));

			// Insert Entries

			$iteration = 0;
			$columns = [
				"name" => null,
				"selected_keyword" => 1,
				"status" => "Pending",
			];
			while($totalData > 0) {
				$chunkSize = ($totalData > 100) ? 100 : $totalData;
				$toInsertData = [];
				$list = ["language_code" => "en", "word_type_id" => WordManager::WORD_TYPES[WordManager::WT_KEYWORDS], "words"=> []];
				
				for($i=0; $i < $chunkSize; $i++) {
					$generatedIdx = (100 * $iteration) + $i;
					if(!empty($explodedData[$generatedIdx])){
						$selectedData = $explodedData[$generatedIdx];
						$list["words"][] = $selectedData;
						$toInsertData[$i] = $this->db->prepare( "(%s, %d, %d, %s)", $selectedData, $request->id, 1, 'Pending');
					}
				}
				$query = "INSERT IGNORE INTO ".$this->config->getWordDictionaryTableName()." (name, request_id, selected_keyword, status) VALUES ";
				$query .= implode( ",\n", $toInsertData );
				$this->db->query($query);
				$this->wordManager->insertSingleWordBatch($list, $columns);
				$totalData = $totalData - $chunkSize;
				$iteration++;
			}
			// Status change to processing	
			$sql = "SELECT id, name from ".$this->config->getWordDictionaryTableName()." WHERE status='Pending'";
			$pending_words = $this->db->get_results($sql);
			foreach( $pending_words as $pw ){
				$this->fetchWordsData($pw->id, $pw->name);
			}
		}
	}

	public function processCsvData($request) {

		$filename = $request->request_data;
		$filepath = WORD_MANAGER_PLUGIN_PATH.'uploads/'.$filename;
		if(!file_exists($filepath)) {
			$this->db->update($this->config->getRequestTableName(),[
				"status" => "Error"
			],  array("id"=> $request->id));
			exit;
		} 

		$file = fopen($filepath, 'r');
		$results = [];
		while (($line = fgetcsv($file)) !== FALSE) {
			$results[] = isset($line[0]) ? $line[0] : "";
		}
		fclose($file);
		$this->processData($results, $request);

	}

	public function fetchWordsData($id, $word)
	{
		/* 
		* TODO: Reduce number of API calls when checking for empty column data in the Word Dictionary 
		*/

		$this->db->update( $this->config->getWordDictionaryTableName(), [ 'status' => 'Processing' ], [ 'id' => $id ] );	
					
		$dm = DataMuse::instance();	
		$responseCode = 200;

		// INFO:  Mean Like
			$meanings = $dm->fields(["word"])
				->limit(200)
				->word($word)
				->meanings()
				->execute();
		$responseCode = $meanings['responseCode'];

		// INFO:  Sound Like
		$homophones = $dm->fields(["word"])
				->limit(200)
				->word($word)
				->homophones()
				->execute();
		$responseCode = $homophones['responseCode'];

		// INFO:  Parts of Speech
		// $parts_of_speech = $dm->fields(["word"])
		// 		->limit(200)
		// 		->word($word)
		// 		->getPartsOfSpeech()
		// 		->execute();
		// $responseCode = $parts_of_speech['responseCode'];

		// INFO:  Nouns
		$nouns = $dm->fields(["word"])
				->limit(200)
				->word($word)
				->relatedWords(["noun"])
				->execute();
		$responseCode = $nouns['responseCode'];

		// INFO:  Adjectives
		$adjectives = $dm->fields(["word"])
				->limit(200)
				->word($word)
				->relatedWords(["adjective"])
				->execute();
		$responseCode = $adjectives['responseCode'];

		// INFO:  Associated Topic based words
		$associated_topics = $dm->fields(["word"])
				->limit(200)
				->word($word)
				->associatedTopics()
				->execute();
		$responseCode = $associated_topics['responseCode'];
		
		
				
		$this->db->update( $this->config->getWordDictionaryTableName(), [	
			'homophones' => json_encode($homophones['data']),	
			'meanings' => json_encode($meanings['data']),	
			'nouns' => json_encode($nouns['data']),	
			'adjectives' => json_encode($adjectives['data']),	
			'associated_topics' => json_encode($associated_topics['data'])
		], [ 'id' => $id ] );	
		
		// NOTE: If data recieved, status change to Pending Approval	
		// NOTE: If one or more field's data is not received, status change to pending	

		if( $responseCode == 200 ){
			$this->db->update( $this->config->getWordDictionaryTableName(), [ 'status' => 'Pending Approval' ], [ 'id' => $id ] );
		} else {
			$this->db->update( $this->config->getWordDictionaryTableName(), [ 'status' => 'Pending' ], [ 'id' => $id ] );
		}
	}

	public function updateRequestStatus()
	{
		// NOTE: Update the status of requests to Done

		$processed_words = $this->db->get_results("SELECT count(res.id) as processed, req.id FROM ".$this->config->getRequestTableName()." req LEFT JOIN ".$this->config->getWordDictionaryTableName()." res ON req.id=res.request_id WHERE res.status='Pending Approval' AND req.status='Processing' GROUP BY req.id");

		$total_words = $this->db->get_results("SELECT count(res.id) as total, req.id FROM ".$this->config->getRequestTableName()." req LEFT JOIN ".$this->config->getWordDictionaryTableName()." res ON req.id=res.request_id WHERE req.status='Processing' GROUP BY req.id");

		$response = [];
		foreach( $processed_words as $pw ){
			$response[$pw->id]['processed'] = $pw->processed;
		}
		foreach( $total_words as $tw ){
			$response[$tw->id]['total'] = $tw->total;
		}

		// Update status of Requests
		if( !empty($response) ){
			foreach( $response as $key => $val ){
				if( $val['processed'] == $val['total'] ){
					$this->db->update( $this->config->getRequestTableName(), [ 'status' => 'Done' ], [ 'id' => $key ] );
				}
			}
		}
	}

	public function unsetCronJob()
    {
        $timestamp = wp_next_scheduled( $this->cron_hook );
        wp_unschedule_event( $timestamp, $this->cron_hook );

		$timestamp = wp_next_scheduled( $this->cron_hook_domain );
        wp_unschedule_event( $timestamp, $this->cron_hook_domain );
    }

	public function getWordColumnData() { 
		$wordId = $_REQUEST['wordId'];
		$column = $_REQUEST['column'];
		$get_words = $this->db->get_col("SELECT $column FROM ".$this->config->getWordDictionaryTableName()." WHERE id=$wordId");
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($get_words);
		die();

	}

	public function saveApprovedWords()
	{
		$approvedWords = $_REQUEST['approvedWords'];
		$wordId = $_REQUEST['wordId'];
		$column = $_REQUEST['column'];
		$update_id = $this->db->update( $this->config->getWordDictionaryTableName(), [ $column => json_encode($approvedWords['approvedWords']) ], [ 'id' => $wordId ] );

		if( $update_id ){
			echo json_encode(["msg" => "success"]);
		} else {
			echo json_encode(["msg" => "failure"]);
		}
		die();
	}

	public function getWordCategories()
	{
		if( (isset($_GET['add_word_category']) && $_GET['add_word_category'] == "true") || isset($_GET['edit_cat_id']) ){
			if( $_GET['edit_cat_id'] != '' ){
				$name = $this->db->get_col("SELECT name from ".$this->config->getWordCategoriesTableName()." WHERE id=".$_GET['edit_cat_id']);
				$cat_name = $name[0];
			}
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-category.php" );
		} else {
			$viewData = $this->wordManager->getAllWordCategories();
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/categories.php" );
		}
	}

	public function getGeneratorShortcodes()
	{
		$this->shortcode->getShortcodePage();	
	}

	public function ajaxUpdateCategoryName()
	{
		if( isset($_POST['categoryId']) ){
			$this->db->update( $this->config->getWordCategoriesTableName(), [ 'name' => $_POST['categoryName'], 'status' => $_POST['status'] ], [ 'id' => $_POST['categoryId'] ] );
		} else {
			$this->db->insert($this->config->getWordCategoriesTableName(), [ 'name' => $_POST['categoryName'], 'status' => $_POST['status'] ]);
		}
		echo json_encode(["msg" => "success", "home_url" => home_url()]);
		die;
	}

	public function settingsPage()
	{
		$languages = $this->wordManager->getLanguages();

		$categories_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([6]));
		$category_words_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([2,3]));
		$keywords_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([1,4]));
		$word_attributes_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([5]));
		$algorithm_type = $this->getAlgorithm_type();
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/settings.php" );
	}
    

    public function getAlgorithm_type()
    {
    	global $wpdb;
    	$res = $wpdb->get_row("SELECT algorithm From ".$this->config->getsettingsTableName()." WHERE id = 1 ");
        if( !empty($res) && !empty($res->algorithm) ){
             return $res->algorithm;
       	}
        return '';
    }

	public function getSettingsLanguageString(array $lang)
	{
		$i = 0;
		$response = '';
		foreach($lang as $cl){
			$response .= ( $i != 0 ) ? ', '.$cl['name'] : $cl['name'];
			$i++;
		}
		return $response;
	}

	public function loadCategories()
	{
		$this->wordManager->loadCategories($_POST['dataLanguage']);
		$data_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([6]));
		echo json_encode(["msg" => "success", "data_language" => $data_language]);
		die;
	}
	public function loadCategoryWords()
	{
		$this->wordManager->loadCategoryAndMatchingWords($_POST['dataLanguage']);
		$data_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([2,3]));
		echo json_encode(["msg" => "success", "data_language" => $data_language]);
		die;
	}

	public function loadKeywords()
	{
		$this->wordManager->loadGeneralAndKeywords($_POST['dataLanguage']);
		$data_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([1,4]));
		echo json_encode(["msg" => "success", "data_language" => $data_language]);
		die;
	}

	public function loadWordAttributes()
	{
		$this->wordManager->loadAttributes($_POST['dataLanguage']);
		$data_language = $this->getSettingsLanguageString($this->wordManager->getLoadedLanguages([5]));
		echo json_encode(["msg" => "success", "data_language" => $data_language]);
		die;
	}

	public function getWordsDashboard()
	{
		$this->wordsDashboard->dashboardCallback();
	}

	public function ajaxUpdateWordData()
	{
		$this->fetchWordsData($_POST['word_id'], $_POST['word']);
		echo json_encode(['msg' => 'success']);
		die;
	}

	public function ajaxUpdateWordStatus()
	{
		$this->db->update( $this->config->getWordDictionaryTableName(), [ 'status' => $_POST['status'] ], [ 'id' => $_POST['wordId'] ] );
		echo json_encode(['msg' => 'success']);
		die;
	}

	public function checkIfWordExists()
	{
		$word = $this->db->get_col("SELECT id FROM ".$this->config->getWordDictionaryTableName()." WHERE name='".$_POST['word']."'");
		if( empty($word) ){
			echo json_encode(['msg' => 'success']);
		} else {
			echo json_encode(['msg' => 'failure']);
		}
		die;
	}

	public function getGeneratorFilters()
	{
		$this->filterOptions->getFiltersPage();
	}

	public function emailTemplateSettings()
	{
		if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
			update_option('bnwm_email_settings', json_encode($_POST));
		}
		$emailSettings = json_decode(get_option('bnwm_email_settings'));
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/email.php" );
	}

	public function stringTranslations()
	{   
	    if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
	    	update_option('bnwm_string_translations', json_encode($_POST));
	    }
	    $translations = json_decode(get_option('bnwm_string_translations')); 
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/intl-translations.php" );
	}

    public function UpdateAlgorithm()
    {   
    	global $wpdb;
        $result = $wpdb->query($wpdb->prepare("UPDATE `".$this->config->getsettingsTableName()."` SET `algorithm` = '".$_POST['algorithmtype']."' WHERE `id` = 1"));
       	if( $result ){
      		echo "success";
      	}else{
      		echo "error";
      	}
        die;
    }  

	public function createPopularKeywordsTable()
	{
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$keywords_table = $this->config->getPopularKeywordsTableName();
        $sql = "CREATE TABLE $keywords_table ( `id` INT(20) NOT NULL AUTO_INCREMENT , `keyword` VARCHAR(256) NOT NULL , `shortcode_id` INT(20) NOT NULL , PRIMARY KEY (`id`)) $charset_collate;";

        $wpdb->query($sql);
		$wpdb->query("ALTER TABLE `$keywords_table` ADD INDEX(`id`);");
		$wpdb->query("ALTER TABLE `$keywords_table` ADD INDEX(`shortcode_id`);");

		echo "success";
		die();
	}

	public function sitemapShortcode($atts)
    {
        $a = shortcode_atts(array(
            'post-category' => '',
            'show-pages' => 'true'
        ), $atts);

        $post_types = get_option('mlsg_include_post_types') ? explode(',', get_option('mlsg_include_post_types')) : ['page', 'post'];
        $exclude_posts = explode(',', get_option('mlsg_exclude_posts'));
        $output = '<div class="mlsg-sitemap">';
        foreach ($post_types as $pt) {
            if (post_type_exists($pt)) {

                if ($pt == 'page') {
                    if ($a['show-pages'] == 'true') {
                        $posts = new \WP_Query(array(
                            'post_type' => trim($pt),
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'suppress_filters' => false,
                            'post__not_in' => $exclude_posts,
                            'meta_query'        => [
                                [
                                    'key'        => '_yoast_wpseo_meta-robots-noindex',
                                    'value'        => 1,
                                    'compare' => 'NOT EXISTS'
                                ]
                            ]
                        ));
                        $post_data = $posts->posts;
                        $child_pages = [];
                        $i = 0;
                        foreach ($post_data as $data) {
                            if ($data->post_parent > 0) {
                                $child_pages[$data->post_parent][$i]['id'] = $data->ID;
                                $child_pages[$data->post_parent][$i]['title'] = $data->post_title;
                                $child_pages[$data->post_parent][$i]['permalink'] = get_permalink($data->ID);
                                $i++;
                            }
                        }

                        ob_start();
					?>
                        <h2>Pages</h2>
                        <ul style="list-style-type:none;">
                            <?php foreach ($post_data as $data) : ?>
                                <?php if ($data->post_parent == 0) : ?>
                                    <li><a href="<?php the_permalink($data->ID); ?>"><?php echo $data->post_title; ?></a></li>
                                    <?php if (array_key_exists($data->ID, $child_pages)) : ?>
                                        <ul style="list-style-type:none;">
                                            <?php foreach ($child_pages[$data->ID] as $cp) : ?>
                                                <li><a href="<?php echo $cp['permalink']; ?>"><?php echo $cp['title']; ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php
                        $output .= ob_get_clean();
                        $posts->wp_reset_postdata();
                    } else {
                        $output .= '';
                    }
                } elseif ($pt == 'post') {
                    // $categories = get_categories();
                    $categories = ($a['post-category'] != '') ? explode(',', $a['post-category']) : get_categories();
                    ob_start();
                    echo '<h2>Posts by category</h2>';
                    echo '<ul style="list-style-type:none;">';
                    foreach ($categories as $category) {
                        if ($a['post-category'] != '') {
                            $category = get_category_by_slug($category);
                        }
                        echo '<li>' . $category->name . '</li>';
                        echo '<ul style="list-style-type:none;">';
                        $posts = new \WP_Query(array(
                            'post_type' => trim($pt),
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'suppress_filters' => false,
                            'post__not_in' => $exclude_posts,
                            'cat' => $category->term_id,
                            'meta_query'        => [
                                [
                                    'key'        => '_yoast_wpseo_meta-robots-noindex',
                                    'value'        => 1,
                                    'compare' => 'NOT EXISTS'
                                ]
                            ]
                        ));
                        $post_data = $posts->posts;
                    ?>
                        <?php foreach ($post_data as $data) : ?>
                            <li><a href="<?php the_permalink($data->ID); ?>"><?php echo $data->post_title; ?></a></li>
                        <?php endforeach; ?>
                    <?php
                        echo '</ul>';
                    }
                    echo '</ul>';
                    $output .= ob_get_clean();
                    $posts->wp_reset_postdata();
                } else {
                    $posts = new \WP_Query(array(
                        'post_type' => trim($pt),
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                        'suppress_filters' => false,
                        'post__not_in' => $exclude_posts,
                        'meta_query'        => [
                            [
                                'key'        => '_yoast_wpseo_meta-robots-noindex',
                                'value'        => 1,
                                'compare' => 'NOT EXISTS'
                            ]
                        ]
                    ));
                    $post_data = $posts->posts;

                    ob_start();
                    ?>
                    <h2><?php echo ucfirst($pt); ?>s</h2>
                    <ul style="list-style-type:none;">
                        <?php foreach ($post_data as $data) : ?>
                            <li><a href="<?php the_permalink($data->ID); ?>"><?php echo $data->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
			<?php
                    $output .= ob_get_clean();
                    $posts->wp_reset_postdata();
                }
            }
        }
        $output .= '</div>';
        return $output;
    }
	public function sitemapSettings()
	{
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' && wp_verify_nonce($_POST['mlsg_nonce'], 'mlsg_sitemap')) {
            update_option('mlsg_exclude_posts', $_POST['exclude_posts']);
            update_option('mlsg_include_post_types', $_POST['include_post_types']);
            update_option('mlsg_blog_page_id', $_POST['blog_page_id']);
            wp_redirect('/wp-admin/admin.php?page=multilingual_sitemap_generator');
        }
		return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/sitemap-settings.php" );
	}
}