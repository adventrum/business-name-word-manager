<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;

use BNWM\Base\Config;

class TemplateCallbacks
{
	protected static $instance = null;
    private $db;
    public $config;

    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->config = Config::instance();
    }

    /**
     * instance
     *
     * @return TemplateCallbacks
     */
    public static function instance(): TemplateCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}

    public function getTemplates($fields, $where = ""): array {
        $fieldStr = (is_array($fields) && !empty($fields)) ? implode(", ", $fields) : " * ";
        
        $results = $this->db->get_results("SELECT $fieldStr from ".$this->config->getTemplateTableName()." $where" );

		return $results;
    }

    public function addTemplate($data) {
        $id = $this->db->insert( $this->config->getTemplateTableName(), [
            'name' => $data['name'],
            'content' => htmlentities($data['content'], ENT_QUOTES)
        ] );

        return $id;
    }

    public function updateTemplate($data, $id) {
        $id = $this->db->update( $this->config->getTemplateTableName(), [
            'name' => $data['name'],
            'content' => htmlentities(stripslashes($data['content']), ENT_QUOTES)
        ],[
            'id' => $id
        ] );
    }

    public function templatePages() {

        if(isset($_GET['add_template']) && $_GET['add_template'] == 'true' ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addTemplate($_POST);
			}
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/templates/add.php" );
		}elseif( isset($_GET['edit_template']) ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->updateTemplate($_POST, $_GET['edit_template']);
			}
			$templates = $this->getTemplates('*', 'WHERE id = '.$_GET['edit_template']);
			$template = !empty($templates) ? $templates[0] : [];
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/templates/add.php" );
		}else {
            $fields = ['id', 'name', 'content'];
            $templates = $this->getTemplates($fields);
            return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/templates/index.php" );
        }

    }

    public function renderTemplate($template_id, $data = []) {

        $templates = $this->getTemplates('*', 'WHERE id = '.$template_id);

        $template = (!empty($templates)) ? $templates[0] : null;

        if(!$template) {
            return "";
        }

        $content = $template->content;

        if (preg_match_all("/{{(.*?)}}/", $content, $m)) {
            foreach ($m[1] as $i => $varname) {
                if(strpos($varname, "img:") !== false){
                    
                    $imageElements = explode(":", $varname);
                    $imageKey = end($imageElements);
                    if(isset($data[$imageKey]) && !empty($data[$imageKey])) {
                        $replacement = wp_get_attachment_url($data[$imageKey]);
                    }else {
                        $replacement = "";
                    }
                }else {
                    $replacement = isset($data[$varname]) ? $data[$varname] : "";
                }

                $content = str_replace($m[0][$i], sprintf('%s', $replacement), $content);
            }
        }

        return html_entity_decode($content);

    }

}

?>