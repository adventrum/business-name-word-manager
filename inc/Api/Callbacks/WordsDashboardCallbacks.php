<?php 
// TODO: Word Manager paused
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;

use BNWM\Base\Config;
use BNWM\Base\Pager;
use BNWM\WordManager\WordManager;

class WordsDashboardCallbacks
{
    protected static $instance = null;
    private $db;
    private $textDomain;
    public $config;

    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->config = Config::instance();
        $this->textDomain = 'business_name_word_manager';
        $this->wordManager = new WordManager;
    }
      
    /**
     * instance
     *
     * @return WordsDashboardCallbacks
     */
    public static function instance(): WordsDashboardCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}
    
    /**
     * dashboardCallback
     *
     * @return string
     */
    public function dashboardCallback(): string {
        if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset( $_POST['word_name'] ) ){
            $this->newWordHandler($_POST);
        }

        if( (isset($_GET['add_new_word']) && $_GET['add_new_word'] == "true") || isset($_GET['edit_word']) ){
			if( isset($_GET['edit_word']) ){
				$name = $this->db->get_col("SELECT name from ".$this->config->getWordDictionaryTableName()." WHERE id=".$_GET['edit_word']);
				$word_name = $name[0];
			}
		    $languages = $this->wordManager->getLanguages();
            $categories = $this->fetchAllCategories();
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/add-word.php" );
		} else {
			$viewData = $this->getAllWords();
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/words.php" );
		}
    }
    
    public function getAllWords()
    {
        // TODO: FIX Join Query in search and Category
        $p = new Pager; 
        
        if( isset($_GET['word_request_id']) ){
            $total_results = $this->db->get_col("SELECT count(id) FROM ".$this->config->getWordDictionaryTableName()." WHERE request_id='".$_GET['word_request_id']."'");

            $baseUrl = '<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&word_request_id='.$_GET['word_request_id'];
        } elseif( isset($_GET['word_category_id']) ){
            $total_results = $this->db->get_col("SELECT count(word.id) FROM ".$this->config->getWordDictionaryTableName()." word LEFT JOIN ".$this->config->getWordCatAssocTableName()." word_cat ON word.id=word_cat.word_id LEFT JOIN ".$this->config->getWordCategoriesTableName()." cat ON word_cat.category_id=cat.id WHERE cat.id='".$_GET['word_category_id']."'");
            
            $baseUrl = '<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&word_category_id='.$_GET['word_category_id'];
        } elseif( isset($_GET['search_word']) ){
            $total_results = $this->db->get_col("SELECT count(word.id) FROM ".$this->config->getWordDictionaryTableName()." word LEFT JOIN ".$this->config->getWordCatAssocTableName()." word_cat ON word.id=word_cat.word_id LEFT JOIN ".$this->config->getWordCategoriesTableName()." cat ON word_cat.category_id=cat.id WHERE word.name LIKE'%".$_GET['search_word']."%' GROUP BY word.id");
        } else {
            $total_results = $this->db->get_col("SELECT count(id) FROM ".$this->config->getWordDictionaryTableName());
            $baseUrl = '<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words';
        }
        
        $limit = 20;
        $start = $p->findStart($baseUrl, $limit); 
		
		$count = $total_results[0]; 
        if( isset($_GET['search_word']) ){
            $count = count($total_results);
        }
		
		$pages = $p->findPages($count, $limit); 

        if( isset($_GET['word_request_id']) ){
            $res = $this->db->get_results("SELECT id, name, status FROM ".$this->config->getWordDictionaryTableName()." WHERE request_id='".$_GET['word_request_id']."' LIMIT $start, $limit");
        } elseif( isset($_GET['word_category_id']) ){
            $res = $this->db->get_results("SELECT word.id, word.name, word.status FROM ".$this->config->getWordDictionaryTableName()." word LEFT JOIN ".$this->config->getWordCatAssocTableName()." word_cat ON word.id=word_cat.word_id LEFT JOIN ".$this->config->getWordCategoriesTableName()." cat ON word_cat.category_id=cat.id WHERE cat.id='".$_GET['word_category_id']."' GROUP BY word.id LIMIT $start, $limit");
        } elseif( isset($_GET['search_word']) ){
            $res = $this->db->get_results("SELECT word.id, word.name, word.status FROM ".$this->config->getWordDictionaryTableName()." word LEFT JOIN ".$this->config->getWordCatAssocTableName()." word_cat ON word.id=word_cat.word_id LEFT JOIN ".$this->config->getWordCategoriesTableName()." cat ON word_cat.category_id=cat.id WHERE word.name LIKE'%".$_GET['search_word']."%' GROUP BY word.id LIMIT $start, $limit");
        } else {
            $res = $this->db->get_results("SELECT id, name, status FROM ".$this->config->getWordDictionaryTableName()." LIMIT $start, $limit");
        }
		
		$pagelist = $p->pageList($_GET['nav'], $pages); 
        
        $response['data'] = $res;
        $response['pagination'] = $pagelist;
        $response['count'] = $count;

        return $response;
    }

    public function fetchAllCategories()
    {
        $categories = $this->db->get_results("SELECT * FROM ".$this->config->getWordCategoriesTableName());
        return $categories;
    }

    public function newWordHandler($post_data)
    {
        $this->db->insert( $this->config->getWordDictionaryTableName(), [ 'name' => $post_data['word_name'] ] );
        $insert_id = $lastid = $this->db->insert_id;

        if( !empty( $post_data['word_language'] ) ){
            foreach ($post_data['word_language'] as $wl){
                $this->db->insert( $this->config->getLanguageWordTableName(), [ 'language_code' => $wl, 'word_id' => $insert_id ] );
            }
        }

        if( !empty( $post_data['word_category'] ) ){
            foreach ($post_data['word_category'] as $wc){
                $this->db->insert( $this->config->getWordCatAssocTableName(), [ 'category_id' => $wc, 'word_id' => $insert_id ] );
            }
        }
		$this->db->insert( $this->config->getWordTypeAssocTableName(), [ 'word_id' => $insert_id, 'word_type_id' => 1 ] );
    }
}